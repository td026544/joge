// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Detail _$DetailFromJson(Map<String, dynamic> json) => Detail(
      json['address'] as String? ?? '',
      json['city'] as String? ?? '',
      const TimestampConverter().fromJson(json['createTime'] as Timestamp),
      json['gameType'] as String,
      json['info'] as String,
      json['isAudit'] as bool,
      json['isOnline'] as bool,
      json['isPublic'] as bool,
      json['needNumbers'] as int,
      json['placeName'] as String,
      json['position'] == null
          ? null
          : Position.fromJson(json['position'] as Map<String, dynamic>),
      const TimestampConverter().fromJson(json['startTime'] as Timestamp),
      json['title'] as String? ?? '',
    );

Map<String, dynamic> _$DetailToJson(Detail instance) => <String, dynamic>{
      'address': instance.address,
      'city': instance.city,
      'createTime': const TimestampConverter().toJson(instance.createTime),
      'gameType': instance.gameType,
      'info': instance.info,
      'isAudit': instance.isAudit,
      'isOnline': instance.isOnline,
      'isPublic': instance.isPublic,
      'needNumbers': instance.needNumbers,
      'placeName': instance.placeName,
      'position': instance.position,
      'startTime': const TimestampConverter().toJson(instance.startTime),
      'title': instance.title,
    };
