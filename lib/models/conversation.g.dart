// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'conversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Conversation _$$_ConversationFromJson(Map<String, dynamic> json) =>
    _$_Conversation(
      json['toId'] as String,
      json['msg'] as String?,
      json['msgType'] as String,
      json['time'] as int,
      json['chatId'] as String,
      json['name'] as String,
      json['sender'] as String,
      json['chatType'] as String,
    );

Map<String, dynamic> _$$_ConversationToJson(_$_Conversation instance) =>
    <String, dynamic>{
      'toId': instance.toId,
      'msg': instance.msg,
      'msgType': instance.msgType,
      'time': instance.time,
      'chatId': instance.chatId,
      'name': instance.name,
      'sender': instance.sender,
      'chatType': instance.chatType,
    };
