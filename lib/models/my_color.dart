import 'package:flutter/material.dart';

const primaryColor = Colors.amber;
const primaryDark = Color(0xffc79100);
const primaryLight = Color(0xfffff350);
