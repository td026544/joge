import 'package:boardgame/models/enums.dart';

class CateGoryItem {
  final String name;
  final String imagePath;
  final CategoryType categoryType;
  final String intro;
  CateGoryItem(this.name, this.imagePath, this.categoryType, this.intro);
}
