// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attendees_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttendeesInfo _$AttendeesInfoFromJson(Map<String, dynamic> json) =>
    AttendeesInfo(
      json['nickname'] as String,
      json['pushToken'] as String,
      json['uid'] as String,
    );

Map<String, dynamic> _$AttendeesInfoToJson(AttendeesInfo instance) =>
    <String, dynamic>{
      'nickname': instance.nickname,
      'pushToken': instance.pushToken,
      'uid': instance.uid,
    };
