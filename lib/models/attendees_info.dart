import 'package:json_annotation/json_annotation.dart';
part 'attendees_info.g.dart';

@JsonSerializable()
class AttendeesInfo {
  final String nickname;
  final String pushToken;
  final String uid;

  AttendeesInfo(
    this.nickname,
    this.pushToken,
    this.uid,
  );

  factory AttendeesInfo.fromJson(Map<String, dynamic> json) =>
      _$AttendeesInfoFromJson(json);

  Map<String, dynamic> toJson() => _$AttendeesInfoToJson(this);
}
