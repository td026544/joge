class AttendUserData {
  final String uid;
  final String nickname;

  AttendUserData({required this.uid, required this.nickname});
}
