import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

/// rooms : [""]
/// id:""
/// phoneNumber : ""
/// gender : ""
/// createTime : 1618724446365
/// nickname : "徐浩程"
/// avatar : {"backGroundColor":"#ffcdd2","accessoriesIndex":2,"faceIndex":2,"facialHairIndex":4,"bodyIndex":3,"skinColorCode":"#EDB98A","hairColorCode":"#000000","clothColorCode2":"#ff9800","clothColorCode1":"#66bb6a","headIndex":26}
/// title : "www"
/// pushToken : ""
/// email : "td026544@gmail.com"
/// age : ""
/// username : "徐浩程"
part 'my_user.g.dart';

@JsonSerializable()
class MyUser {
  late final String id;
  late final String? phoneNumber;
  late final bool gender;
  @TimestampConverter()
  late final Timestamp createTime;
  late final String nickname;
  late final String? pushToken;
  late final String? email;
  @TimestampConverter()
  late final Timestamp age;
  late final String bio;
  late final List<String> hobbies;

  MyUser(this.id, this.phoneNumber, this.gender, this.createTime, this.nickname,
      this.pushToken, this.email, this.age, this.bio, this.hobbies);
  factory MyUser.fromJson(Map<String, dynamic> json) => _$MyUserFromJson(json);

  static Timestamp _fromJson(Timestamp timestamp) => timestamp;
}

class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
  const TimestampConverter();

  @override
  Timestamp fromJson(Timestamp timestamp) {
    return timestamp;
  }

  @override
  Timestamp toJson(Timestamp timestamp) => timestamp;
}
