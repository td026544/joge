// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'doc_status.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$DocStatusTearOff {
  const _$DocStatusTearOff();

  DocStatusNotExist<T> notExist<T>() {
    return DocStatusNotExist<T>();
  }

  DocStatusData<T> data<T>(T value) {
    return DocStatusData<T>(
      value,
    );
  }

  DocStatusLoading<T> loading<T>() {
    return DocStatusLoading<T>();
  }

  DocStatusError<T> error<T>(Object error, [StackTrace? stackTrace]) {
    return DocStatusError<T>(
      error,
      stackTrace,
    );
  }
}

/// @nodoc
const $DocStatus = _$DocStatusTearOff();

/// @nodoc
mixin _$DocStatus<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notExist,
    required TResult Function(T value) data,
    required TResult Function() loading,
    required TResult Function(Object error, StackTrace? stackTrace) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DocStatusNotExist<T> value) notExist,
    required TResult Function(DocStatusData<T> value) data,
    required TResult Function(DocStatusLoading<T> value) loading,
    required TResult Function(DocStatusError<T> value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocStatusCopyWith<T, $Res> {
  factory $DocStatusCopyWith(
          DocStatus<T> value, $Res Function(DocStatus<T>) then) =
      _$DocStatusCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$DocStatusCopyWithImpl<T, $Res> implements $DocStatusCopyWith<T, $Res> {
  _$DocStatusCopyWithImpl(this._value, this._then);

  final DocStatus<T> _value;
  // ignore: unused_field
  final $Res Function(DocStatus<T>) _then;
}

/// @nodoc
abstract class $DocStatusNotExistCopyWith<T, $Res> {
  factory $DocStatusNotExistCopyWith(DocStatusNotExist<T> value,
          $Res Function(DocStatusNotExist<T>) then) =
      _$DocStatusNotExistCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$DocStatusNotExistCopyWithImpl<T, $Res>
    extends _$DocStatusCopyWithImpl<T, $Res>
    implements $DocStatusNotExistCopyWith<T, $Res> {
  _$DocStatusNotExistCopyWithImpl(
      DocStatusNotExist<T> _value, $Res Function(DocStatusNotExist<T>) _then)
      : super(_value, (v) => _then(v as DocStatusNotExist<T>));

  @override
  DocStatusNotExist<T> get _value => super._value as DocStatusNotExist<T>;
}

/// @nodoc

class _$DocStatusNotExist<T> extends DocStatusNotExist<T> {
  const _$DocStatusNotExist() : super._();

  @override
  String toString() {
    return 'DocStatus<$T>.notExist()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is DocStatusNotExist<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notExist,
    required TResult Function(T value) data,
    required TResult Function() loading,
    required TResult Function(Object error, StackTrace? stackTrace) error,
  }) {
    return notExist();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
  }) {
    return notExist?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
    required TResult orElse(),
  }) {
    if (notExist != null) {
      return notExist();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DocStatusNotExist<T> value) notExist,
    required TResult Function(DocStatusData<T> value) data,
    required TResult Function(DocStatusLoading<T> value) loading,
    required TResult Function(DocStatusError<T> value) error,
  }) {
    return notExist(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
  }) {
    return notExist?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
    required TResult orElse(),
  }) {
    if (notExist != null) {
      return notExist(this);
    }
    return orElse();
  }
}

abstract class DocStatusNotExist<T> extends DocStatus<T> {
  const factory DocStatusNotExist() = _$DocStatusNotExist<T>;
  const DocStatusNotExist._() : super._();
}

/// @nodoc
abstract class $DocStatusDataCopyWith<T, $Res> {
  factory $DocStatusDataCopyWith(
          DocStatusData<T> value, $Res Function(DocStatusData<T>) then) =
      _$DocStatusDataCopyWithImpl<T, $Res>;
  $Res call({T value});
}

/// @nodoc
class _$DocStatusDataCopyWithImpl<T, $Res>
    extends _$DocStatusCopyWithImpl<T, $Res>
    implements $DocStatusDataCopyWith<T, $Res> {
  _$DocStatusDataCopyWithImpl(
      DocStatusData<T> _value, $Res Function(DocStatusData<T>) _then)
      : super(_value, (v) => _then(v as DocStatusData<T>));

  @override
  DocStatusData<T> get _value => super._value as DocStatusData<T>;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(DocStatusData<T>(
      value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$DocStatusData<T> extends DocStatusData<T> {
  const _$DocStatusData(this.value) : super._();

  @override
  final T value;

  @override
  String toString() {
    return 'DocStatus<$T>.data(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DocStatusData<T> &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(value);

  @JsonKey(ignore: true)
  @override
  $DocStatusDataCopyWith<T, DocStatusData<T>> get copyWith =>
      _$DocStatusDataCopyWithImpl<T, DocStatusData<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notExist,
    required TResult Function(T value) data,
    required TResult Function() loading,
    required TResult Function(Object error, StackTrace? stackTrace) error,
  }) {
    return data(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
  }) {
    return data?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DocStatusNotExist<T> value) notExist,
    required TResult Function(DocStatusData<T> value) data,
    required TResult Function(DocStatusLoading<T> value) loading,
    required TResult Function(DocStatusError<T> value) error,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class DocStatusData<T> extends DocStatus<T> {
  const factory DocStatusData(T value) = _$DocStatusData<T>;
  const DocStatusData._() : super._();

  T get value => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DocStatusDataCopyWith<T, DocStatusData<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocStatusLoadingCopyWith<T, $Res> {
  factory $DocStatusLoadingCopyWith(
          DocStatusLoading<T> value, $Res Function(DocStatusLoading<T>) then) =
      _$DocStatusLoadingCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$DocStatusLoadingCopyWithImpl<T, $Res>
    extends _$DocStatusCopyWithImpl<T, $Res>
    implements $DocStatusLoadingCopyWith<T, $Res> {
  _$DocStatusLoadingCopyWithImpl(
      DocStatusLoading<T> _value, $Res Function(DocStatusLoading<T>) _then)
      : super(_value, (v) => _then(v as DocStatusLoading<T>));

  @override
  DocStatusLoading<T> get _value => super._value as DocStatusLoading<T>;
}

/// @nodoc

class _$DocStatusLoading<T> extends DocStatusLoading<T> {
  const _$DocStatusLoading() : super._();

  @override
  String toString() {
    return 'DocStatus<$T>.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is DocStatusLoading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notExist,
    required TResult Function(T value) data,
    required TResult Function() loading,
    required TResult Function(Object error, StackTrace? stackTrace) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DocStatusNotExist<T> value) notExist,
    required TResult Function(DocStatusData<T> value) data,
    required TResult Function(DocStatusLoading<T> value) loading,
    required TResult Function(DocStatusError<T> value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class DocStatusLoading<T> extends DocStatus<T> {
  const factory DocStatusLoading() = _$DocStatusLoading<T>;
  const DocStatusLoading._() : super._();
}

/// @nodoc
abstract class $DocStatusErrorCopyWith<T, $Res> {
  factory $DocStatusErrorCopyWith(
          DocStatusError<T> value, $Res Function(DocStatusError<T>) then) =
      _$DocStatusErrorCopyWithImpl<T, $Res>;
  $Res call({Object error, StackTrace? stackTrace});
}

/// @nodoc
class _$DocStatusErrorCopyWithImpl<T, $Res>
    extends _$DocStatusCopyWithImpl<T, $Res>
    implements $DocStatusErrorCopyWith<T, $Res> {
  _$DocStatusErrorCopyWithImpl(
      DocStatusError<T> _value, $Res Function(DocStatusError<T>) _then)
      : super(_value, (v) => _then(v as DocStatusError<T>));

  @override
  DocStatusError<T> get _value => super._value as DocStatusError<T>;

  @override
  $Res call({
    Object? error = freezed,
    Object? stackTrace = freezed,
  }) {
    return _then(DocStatusError<T>(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Object,
      stackTrace == freezed
          ? _value.stackTrace
          : stackTrace // ignore: cast_nullable_to_non_nullable
              as StackTrace?,
    ));
  }
}

/// @nodoc

class _$DocStatusError<T> extends DocStatusError<T> {
  _$DocStatusError(this.error, [this.stackTrace]) : super._();

  @override
  final Object error;
  @override
  final StackTrace? stackTrace;

  @override
  String toString() {
    return 'DocStatus<$T>.error(error: $error, stackTrace: $stackTrace)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DocStatusError<T> &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)) &&
            (identical(other.stackTrace, stackTrace) ||
                const DeepCollectionEquality()
                    .equals(other.stackTrace, stackTrace)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(error) ^
      const DeepCollectionEquality().hash(stackTrace);

  @JsonKey(ignore: true)
  @override
  $DocStatusErrorCopyWith<T, DocStatusError<T>> get copyWith =>
      _$DocStatusErrorCopyWithImpl<T, DocStatusError<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notExist,
    required TResult Function(T value) data,
    required TResult Function() loading,
    required TResult Function(Object error, StackTrace? stackTrace) error,
  }) {
    return error(this.error, stackTrace);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
  }) {
    return error?.call(this.error, stackTrace);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notExist,
    TResult Function(T value)? data,
    TResult Function()? loading,
    TResult Function(Object error, StackTrace? stackTrace)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error, stackTrace);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(DocStatusNotExist<T> value) notExist,
    required TResult Function(DocStatusData<T> value) data,
    required TResult Function(DocStatusLoading<T> value) loading,
    required TResult Function(DocStatusError<T> value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(DocStatusNotExist<T> value)? notExist,
    TResult Function(DocStatusData<T> value)? data,
    TResult Function(DocStatusLoading<T> value)? loading,
    TResult Function(DocStatusError<T> value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class DocStatusError<T> extends DocStatus<T> {
  factory DocStatusError(Object error, [StackTrace? stackTrace]) =
      _$DocStatusError<T>;
  DocStatusError._() : super._();

  Object get error => throw _privateConstructorUsedError;
  StackTrace? get stackTrace => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DocStatusErrorCopyWith<T, DocStatusError<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
