// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyUser _$MyUserFromJson(Map<String, dynamic> json) => MyUser(
      json['id'] as String,
      json['phoneNumber'] as String?,
      json['gender'] as bool,
      const TimestampConverter().fromJson(json['createTime'] as Timestamp),
      json['nickname'] as String,
      json['pushToken'] as String?,
      json['email'] as String?,
      const TimestampConverter().fromJson(json['age'] as Timestamp),
      json['bio'] as String,
      (json['hobbies'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$MyUserToJson(MyUser instance) => <String, dynamic>{
      'id': instance.id,
      'phoneNumber': instance.phoneNumber,
      'gender': instance.gender,
      'createTime': const TimestampConverter().toJson(instance.createTime),
      'nickname': instance.nickname,
      'pushToken': instance.pushToken,
      'email': instance.email,
      'age': const TimestampConverter().toJson(instance.age),
      'bio': instance.bio,
      'hobbies': instance.hobbies,
    };
