// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomDetail _$RoomDetailFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    requiredKeys: const ['id'],
  );
  return RoomDetail(
    json['attendees'] as List<dynamic>,
    json['attendeesData'] as Map<String, dynamic>? ?? {},
    json['commentsCount'] as int,
    Creator.fromJson(json['creator'] as Map<String, dynamic>),
    Detail.fromJson(json['detail'] as Map<String, dynamic>),
    json['id'] as String,
    json['isOutdated'] as bool?,
    json['likes'] as Map<String, dynamic>? ?? {},
    json['reviews'] as List<dynamic>? ?? [],
    json['status'] as String,
    json['reviewsData'] as Map<String, dynamic>? ?? {},
  );
}

Map<String, dynamic> _$RoomDetailToJson(RoomDetail instance) =>
    <String, dynamic>{
      'attendees': instance.attendees,
      'attendeesData': instance.attendeesData,
      'commentsCount': instance.commentsCount,
      'creator': instance.creator.toJson(),
      'detail': instance.detail.toJson(),
      'id': instance.id,
      'isOutdated': instance.isOutdated,
      'likes': instance.likes,
      'reviews': instance.reviews,
      'reviewsData': instance.reviewsData,
      'status': instance.status,
    };
