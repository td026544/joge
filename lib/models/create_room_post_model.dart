import 'package:cloud_firestore/cloud_firestore.dart';

class CreateRoomPostModel {
  late final String address;
  late final Timestamp startTime;
  late final int needNumbers;
  late final String type;
  late final String title;
  late final String info;
  late final String city;
  late final String roomId;
  late final GeoPoint geoPoint;

  late final String placeName;
  late final bool isOnline;
  late final bool isAudit;

  CreateRoomPostModel(
      {required this.address,
      required this.startTime,
      required this.needNumbers,
      required this.type,
      required this.title,
      required this.city,
      required this.geoPoint,
      required this.placeName,
      required this.roomId,
      required this.isOnline,
      required this.isAudit,
      required this.info});
}
