import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'creator.dart';
import 'detail.dart';

part 'room_detail.g.dart';

@JsonSerializable(explicitToJson: true)
class RoomDetail{

  RoomDetail(this.attendees, this.attendeesData,
      this.commentsCount, this.creator, this.detail, this.id, this.isOutdated,
      this.likes, this.reviews, this.status,this.reviewsData);

  List<dynamic> attendees;
  @JsonKey(defaultValue: {})
  Map<String,dynamic> attendeesData;
  int commentsCount;
  Creator creator;
  Detail detail;
  @JsonKey(required: true)
  String id;
  bool? isOutdated;
  @JsonKey(defaultValue: {})
  Map<String,dynamic> likes;
  @JsonKey(defaultValue: [])
  List<dynamic> reviews;
  @JsonKey(defaultValue: {})
  Map<String,dynamic> reviewsData;

  String status;



  factory RoomDetail.fromJson(Map<String, dynamic> json) =>
      _$RoomDetailFromJson(json);

  Map<String, dynamic> toJson() => _$RoomDetailToJson(this);

}


