// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rate_reply.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RateReply _$RateReplyFromJson(Map<String, dynamic> json) => RateReply(
      json['comment'] as String,
      const TimestampConverter().fromJson(json['time'] as Timestamp),
    );

Map<String, dynamic> _$RateReplyToJson(RateReply instance) => <String, dynamic>{
      'comment': instance.comment,
      'time': const TimestampConverter().toJson(instance.time),
    };
