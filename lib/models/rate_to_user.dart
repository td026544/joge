import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'rate_to_user.g.dart';

@JsonSerializable(explicitToJson: true)
class RateToUser {
  final String comment;
  final String nickname;
  final String rateType;
  final String uid;



  factory RateToUser.fromJson(Map<String, dynamic> json) =>
      _$RateToUserFromJson(json);
  Map<String, dynamic> toJson() => _$RateToUserToJson(this);

  RateToUser(this.comment, this.nickname, this.rateType, this.uid);
}