import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'creator.g.dart';

@JsonSerializable()
class Creator  {

  @JsonKey(required: true)
  final String id;
  final String nickname;
  @JsonKey(defaultValue:"")
  final String type;

  Creator(this.id, this.nickname, this.type);

  factory Creator.fromJson(Map<String, dynamic> json) =>
      _$CreatorFromJson(json);


  Map<String, dynamic> toJson() => _$CreatorToJson(this);

}