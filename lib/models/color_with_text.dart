import 'package:boardgame/utils/asset_util.dart';

class ColorWithText {
  final String name;
  final String code;

  ColorWithText(this.name, this.code);

  get color {
    return AssetUtil.hexToColor(code);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ColorWithText &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          code == other.code;

  @override
  int get hashCode => name.hashCode ^ code.hashCode;
}
