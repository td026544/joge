// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Feed _$FeedFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    requiredKeys: const ['uid'],
  );
  return Feed(
    json['nickname'] as String? ?? '',
    const TimestampConverter().fromJson(json['time'] as Timestamp),
    json['uid'] as String,
    json['imgUrl'] as String? ?? '',
    json['msg'] as String? ?? '',
    _$enumDecode(_$FeedTypeEnumMap, json['type']),
    json['navId'] as String,
  );
}

Map<String, dynamic> _$FeedToJson(Feed instance) => <String, dynamic>{
      'nickname': instance.nickname,
      'time': const TimestampConverter().toJson(instance.time),
      'uid': instance.uid,
      'imgUrl': instance.imgUrl,
      'msg': instance.msg,
      'type': _$FeedTypeEnumMap[instance.type],
      'navId': instance.navId,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$FeedTypeEnumMap = {
  FeedType.COMMENT_POST: 'COMMENT_POST',
  FeedType.LIKE_POST: 'LIKE_POST',
};
