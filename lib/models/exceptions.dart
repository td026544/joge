class NoDataException implements Exception {
  String cause;
  NoDataException(this.cause);
}

class NoUserException implements Exception {
  String cause = 'no user';
  NoUserException(this.cause);
}
