// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rate_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RateData _$RateDataFromJson(Map<String, dynamic> json) => RateData(
      const TimestampConverter().fromJson(json['time'] as Timestamp),
      json['roomName'] as String? ?? '',
      Creator.fromJson(json['creator'] as Map<String, dynamic>),
      json['reply'] == null
          ? null
          : RateReply.fromJson(json['reply'] as Map<String, dynamic>),
      RateToUser.fromJson(json['toUser'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RateDataToJson(RateData instance) => <String, dynamic>{
      'time': const TimestampConverter().toJson(instance.time),
      'roomName': instance.roomName,
      'creator': instance.creator.toJson(),
      'reply': instance.reply?.toJson(),
      'toUser': instance.toUser.toJson(),
    };
