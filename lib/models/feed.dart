import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'enums.dart';

part 'feed.g.dart';

@JsonSerializable(explicitToJson: true)
class Feed  {


  @JsonKey(defaultValue: "")
  final String nickname;
  @TimestampConverter()
  final Timestamp time;
  @JsonKey(required: true)
  final String uid;
  @JsonKey(defaultValue: "")
  final String imgUrl;
  @JsonKey(defaultValue: "")
  final String msg;
  final FeedType type;
  final String navId;





  factory Feed.fromJson(Map<String, dynamic> json) =>
      _$FeedFromJson(json);

  Feed(this.nickname, this.time, this.uid, this.imgUrl, this.msg, this.type, this.navId);

  Map<String, dynamic> toJson() => _$FeedToJson(this);

}

class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
  const TimestampConverter();

  @override
  Timestamp fromJson(Timestamp timestamp) {
    return timestamp;
  }

  @override
  Timestamp toJson(Timestamp timestamp) => timestamp;
}
