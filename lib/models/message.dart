import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
  late String id;
  late String sender;
  late String text;
  late int time;
  late String roomId;

  Message({
    required this.id,
    required this.sender,
    required this.text,
    required this.time,
  });

  Message.fromJson(dynamic json) {
    id = json["id"];
    sender = json["sender"];
    text = json["text"];
    time = (json["time"] as Timestamp).millisecondsSinceEpoch;
  }
  Message.fromDB(dynamic json) {
    id = json["id"];
    sender = json["sender"];
    text = json["text"];
    roomId = json["roomId"];

    time = json["time"];
  }
  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["sender"] = sender;
    map["text"] = text;
    map["time"] = time;
    return map;
  }

  @override
  String toString() {
    return 'Message{sender: $sender, text: $text, time: $time}';
  }
}
