import 'package:boardgame/models/position.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'detail.g.dart';

@JsonSerializable()
class Detail  {
   Detail(
    this.address,
    this.city,
    this.createTime,
   this.gameType,
   this.info,
    this.isAudit,
   this.isOnline,
   this.isPublic,
   this.needNumbers,
    this.placeName,
    this.position,
    this.startTime,
    this.title);
   @JsonKey(defaultValue: "")
  final String address;
   @JsonKey(defaultValue: "")
   final String city;
   @TimestampConverter()
   final Timestamp createTime;
   final String gameType;
   final String info;
   final bool isAudit;
   final bool isOnline;
   final bool isPublic;
   final int needNumbers;
   final String placeName;
   final Position? position;

   @TimestampConverter()
   final Timestamp startTime;
   @JsonKey(defaultValue: "")
   final String title;



  factory Detail.fromJson(Map<String, dynamic> json) =>
      _$DetailFromJson(json);


   Map<String, dynamic> toJson() => _$DetailToJson(this);

}

class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
   const TimestampConverter();

   @override
   Timestamp fromJson(Timestamp timestamp) {
      return timestamp;
   }

   @override
   Timestamp toJson(Timestamp timestamp) => timestamp;
}
