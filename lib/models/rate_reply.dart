import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'rate_reply.g.dart';

@JsonSerializable(explicitToJson: true)
class RateReply {

  final String comment;
  @TimestampConverter()
  final Timestamp time;

  factory RateReply.fromJson(Map<String, dynamic> json) =>
      _$RateReplyFromJson(json);
  Map<String, dynamic> toJson() => _$RateReplyToJson(this);

  RateReply(this.comment, this.time);
}
class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
  const TimestampConverter();

  @override
  Timestamp fromJson(Timestamp timestamp) {
    return timestamp;
  }

  @override
  Timestamp toJson(Timestamp timestamp) => timestamp;
}
