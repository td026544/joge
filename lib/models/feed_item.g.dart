// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedItem _$FeedItemFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    requiredKeys: const ['uid'],
  );
  return FeedItem(
    id: json['id'] as String,
    nickname: json['nickname'] as String? ?? '',
    time: const TimestampConverter().fromJson(json['time'] as Timestamp),
    uid: json['uid'] as String,
    msg: json['msg'] as String? ?? '',
    imgUrl: json['imgUrl'] as String? ?? '',
    type: json['type'] as String,
    myId: json['myId'] as String,
    navId: json['navId'] as String,
  );
}

Map<String, dynamic> _$FeedItemToJson(FeedItem instance) => <String, dynamic>{
      'nickname': instance.nickname,
      'time': const TimestampConverter().toJson(instance.time),
      'uid': instance.uid,
      'imgUrl': instance.imgUrl,
      'msg': instance.msg,
      'type': instance.type,
      'navId': instance.navId,
      'id': instance.id,
      'myId': instance.myId,
    };
