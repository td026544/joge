import '../main.dart';

const String FIREBASE_URL = 'https://firebasestorage.googleapis.com/v0/b/';
const String EMULATOR_DOMAIN = 'http://10.0.2.2:9199/v0/b/';
const String BUCKET_NAME = 'joge-dev.appspot.com';
const String EMULATOR_BUCKET_NAME = 'default-bucket';

const String BUCKET_TAIL = '/o';
const String IMAGE_PATH = '/images';
const String USERS_PATH = '/users';
const String ROOMS_PATH = '/rooms';

const String AVATAR_NAME = '_avatar.jpg';
const String COVER_NAME = '_cover.jpg';

const String TRAIL = '?alt=media';
const String SLASH = "%2F";

class URLGetter {
  static String getAvatarUrl(String uid) {
    return domain +
        BUCKET_NAME +
        BUCKET_TAIL +
        USERS_PATH +
        SLASH +
        uid +
        SLASH +
        AVATAR_NAME +
        TRAIL;
  }

  static String getGameRoomCoverUrl(String roomId) {
    return domain +
        BUCKET_NAME +
        BUCKET_TAIL +
        ROOMS_PATH +
        SLASH +
        roomId +
        COVER_NAME +
        TRAIL;
  }

  static String get domain => USE_EMULATOR ? EMULATOR_DOMAIN : FIREBASE_URL;
}
// https://firebasestorage.googleapis.com/v0/b/joge-99fb1.appspot.com/o/images%2Fwu1rmfPNUxchQBblYMJD9yz2cnA2%2Favatar.jpg?alt=media&token=9c32c9a8-1246-4b84-9b37-82931c180761
