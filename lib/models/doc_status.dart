import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'doc_status.freezed.dart';

@freezed
class DocStatus<T> with _$DocStatus<T> {
  const DocStatus._();

  const factory DocStatus.notExist() = DocStatusNotExist<T>;

  const factory DocStatus.data(T value) = DocStatusData<T>;
  const factory DocStatus.loading() = DocStatusLoading<T>;
  factory DocStatus.error(Object error, [StackTrace? stackTrace]) =
      DocStatusError<T>;

  static Future<DocStatus<T>> guard<T>(Future<T> Function() future) async {
    try {
      return DocStatus.data(await future());
    } catch (err, stack) {
      return DocStatus.error(err, stack);
    }
  }

  DocStatusData<T>? get data {
    return map(
      data: (data) => data,
      notExist: (_) => null,
      loading: (_) => null,
      error: (_) => null,
    );
  }

  DocStatus<R> whenData<R>(R Function(T value) cb) {
    return when(
      notExist: () => const DocStatus.notExist(),
      data: (value) {
        try {
          return DocStatus.data(cb(value));
        } catch (err, stack) {
          return DocStatus.error(err, stack);
        }
      },
      loading: () => const DocStatus.loading(),
      error: (err, stack) => DocStatus.error(err, stack),
    );
  }
}
