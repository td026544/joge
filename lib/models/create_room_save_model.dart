class CreateRoomSaveModel {
  final int? startTime;
  final String? address;
  final int? needNumbers;
  final String? type;
  final String? title;
  final String? info;
  final String? city;
  final String? roomId;
  final String? imageByte;
  final double? latitude;
  final double? longitude;
  final String? placeName;
  final bool? isOnline;
  final bool? isAudit;
  final List<int> stepStates;

  CreateRoomSaveModel(
      {required this.address,
      required this.startTime,
      required this.needNumbers,
      required this.type,
      required this.title,
      required this.city,
      required this.latitude,
      required this.longitude,
      required this.placeName,
      required this.roomId,
      required this.isOnline,
      required this.isAudit,
      required this.imageByte,
      required this.info,
      required this.stepStates});

  factory CreateRoomSaveModel.empty() {
    return CreateRoomSaveModel(
        needNumbers: null,
        startTime: null,
        type: null,
        title: null,
        city: null,
        latitude: null,
        longitude: null,
        placeName: null,
        roomId: null,
        isOnline: null,
        isAudit: null,
        imageByte: null,
        info: null,
        address: null,
        stepStates: []);
  }
  bool isEmpty() {
    return [
      needNumbers,
      startTime,
      type,
      title,
      city,
      latitude,
      longitude,
      placeName,
      roomId,
      isOnline,
      isAudit,
      imageByte,
      info,
      address
    ].every((element) => element == null);
  }
}
