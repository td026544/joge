import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'position.g.dart';

@JsonSerializable(explicitToJson: true)
class Position {
  Position(
      this.geohash,this.geopoint,
      );
  // @JsonKey(fromJson:_geoPointFromJson)
  String? geohash;
  @GeoPointConverter()
  GeoPoint? geopoint;
  // static GeoPoint? _geoPointFromJson(GeoPoint? geoPoint) {
  //   return geoPoint;
  // }



  factory Position.fromJson(Map<String, dynamic> json) =>
      _$PositionFromJson(json);
  Map<String, dynamic> toJson() => _$PositionToJson(this);

}

class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
  const TimestampConverter();

  @override
  Timestamp fromJson(Timestamp timestamp) {
    return timestamp;
  }

  @override
  Timestamp toJson(Timestamp timestamp) => timestamp;
}
class GeoPointConverter
    implements JsonConverter<GeoPoint?, GeoPoint?> {
  const GeoPointConverter();

  @override
  GeoPoint? fromJson(GeoPoint? geoPoint) {
    return geoPoint;
  }

  @override
  GeoPoint? toJson(GeoPoint? geoPoint) =>
      geoPoint;
}