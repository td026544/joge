// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rate_to_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RateToUser _$RateToUserFromJson(Map<String, dynamic> json) => RateToUser(
      json['comment'] as String,
      json['nickname'] as String,
      json['rateType'] as String,
      json['uid'] as String,
    );

Map<String, dynamic> _$RateToUserToJson(RateToUser instance) =>
    <String, dynamic>{
      'comment': instance.comment,
      'nickname': instance.nickname,
      'rateType': instance.rateType,
      'uid': instance.uid,
    };
