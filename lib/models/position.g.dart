// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'position.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Position _$PositionFromJson(Map<String, dynamic> json) => Position(
      json['geohash'] as String?,
      const GeoPointConverter().fromJson(json['geopoint'] as GeoPoint?),
    );

Map<String, dynamic> _$PositionToJson(Position instance) => <String, dynamic>{
      'geohash': instance.geohash,
      'geopoint': const GeoPointConverter().toJson(instance.geopoint),
    };
