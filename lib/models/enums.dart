enum MsgType { text, image, service, typing, none }
enum GameType {
  BOARD_GAME,
  ROOM_ESCAPE,
  WEREWOLVES,
  OTHER,
}
enum PaymentType {
  FREE,
  // MY_TREAT,
  PAY_SELF,
  GO_DUTCH,
}
enum EditType {
  NOT_EDIT,
  EDITED,
  EDITING,
}
enum ChatType {
  room,
  user,
}
enum CategoryType {
  board_game,
  sport,
  music,
  hiking,
  travel,
  eat,
  art,
  dance,
  game,
  learning,
  pet,
  business,
  book,
  relax,
  micro_social,
  other,
}
enum FeedType {
  COMMENT_POST,
  LIKE_POST,
}
enum FriendsStatus {
  beInvited,
  invite,
  match,
  ban,
  none,
}
