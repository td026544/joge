import 'package:boardgame/models/detail.dart';
import 'package:boardgame/models/rate_reply.dart';
import 'package:boardgame/models/rate_to_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'creator.dart';

part 'rate_data.g.dart';

@JsonSerializable(explicitToJson: true)
class RateData{
  RateData(this.time, this.roomName, this.creator, this.reply, this.toUser);

  @TimestampConverter()
  final Timestamp time;
  @JsonKey(defaultValue: "")
  final String roomName;
  final Creator creator;

  final RateReply? reply;
  final RateToUser toUser;



  factory RateData.fromJson(Map<String, dynamic> json) =>
      _$RateDataFromJson(json);

  Map<String, dynamic> toJson() => _$RateDataToJson(this);


}
class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
  const TimestampConverter();

  @override
  Timestamp fromJson(Timestamp timestamp) {
    return timestamp;
  }

  @override
  Timestamp toJson(Timestamp timestamp) => timestamp;
}


