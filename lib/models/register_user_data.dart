import 'dart:core';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RegisterUserData {
  late String nickName;
  late Timestamp birthday;
  late String bio;
  late bool isMale;
  late List<String> hobbies;
  late Uint8List? imageByte;
  late String avatarUrl;

  RegisterUserData() {
    nickName = "";
    bio = "";
    isMale = true;
    birthday = Timestamp.fromDate(DateTime.utc(1990, 1, 1));
    hobbies = [];
    imageByte = null;
  }

  @override
  String toString() {
    return 'RegisterUserData{nickName: $nickName, birthday: $birthday, bio: $bio, isMale: $isMale, hobbies: $hobbies}';
  }
}

final registerUserDataProvider =
    StateProvider<RegisterUserData>((ref) => RegisterUserData());
