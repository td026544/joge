class ChatMessage {
  final String msg;
  final int time;
  final String msgType;
  // final int msgStatus;
  final String chatId;
  final String senderId;

  ChatMessage(
      {required this.msg,
      required this.time,
      required this.msgType,
      required this.chatId,
      required this.senderId});

  factory ChatMessage.fromMap(Map map) {
    return ChatMessage(
      msg: map["msg"],
      time: map["time"],
      msgType: map["msgType"],
      chatId: map["chatId"],
      senderId: map["senderId"],
    );
  }

  @override
  String toString() => "msg : $msg, time : $time, "
      "msgType : $msgType, chatId : $chatId";
}
