import 'package:boardgame/models/feed.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import 'enums.dart';

/// nickname : "努努我"
/// time : 12345677
/// id : "xVvrZFvlygNWUYsEZM3dILcdete2"
/// type : "comment_post"
///
part 'feed_item.g.dart';
@JsonSerializable(explicitToJson: true)
class FeedItem {

  @JsonKey(defaultValue: "")
  late final String nickname;
  @TimestampConverter()
  late final Timestamp time;
  @JsonKey(required: true)
  late final String uid;
  @JsonKey(defaultValue: "")
  late final String imgUrl;
  @JsonKey(defaultValue: "")
  late final String msg;
  late final String type;
  late final String navId;
  late final String id;
  late final String myId;

  FeedItem(
      {required this.id,
      required this.nickname,
      required this.time,
      required this.uid,
      required this.msg,
      required this.imgUrl,
      required this.type,
      required this.myId,
      required this.navId});

    FeedItem.fromFeed(Feed feed,String docId) {
    nickname = feed.nickname;
    msg =  feed.msg;
    time = feed.time;
    imgUrl = feed.imgUrl;
    uid = feed.uid;
    type = describeEnum(feed.type);
    myId = "";
    id="";
    navId =feed.navId;
  }

  factory FeedItem.fromJson(Map<String, dynamic> json) =>
      _$FeedItemFromJson(json);

  FeedItem.fromDB(Map<String, dynamic> json)
      : id = json['id'],
        nickname = json['nickname'],
        msg = json["msg"],
        time = Timestamp.fromMillisecondsSinceEpoch(json["time"]),
  imgUrl = json['imgUrl'],
        uid = json["uid"],
        type = json["type"],
  myId = json["myId"],
        navId = json["navId"];





  Map<String, dynamic> toJson() => _$FeedItemToJson(this);
  //  factory FeedItem.fromDB(dynamic json) {
  //   id = json["id"];
  //   nickname = json["nickname"];
  //   msg = json["msg"];
  //   time = json["time"];
  //   imgUrl = json['imgUrl'] ?? "";
  //   uid = json["uid"];
  //   type = json["type"];
  //   myId = json["myId"];
  //   navId = json["navId"];
  // }
  // FeedItem.fromJson(dynamic json) {
  //   id = json["id"];
  //   nickname = json["nickname"];
  //   msg = json["msg"];
  //   time = (json["time"] as Timestamp).millisecondsSinceEpoch;
  //   imgUrl = json['imgUrl'] ?? "";
  //   uid = json["uid"];
  //   type = json["type"];
  //   myId = json["myId"];
  //   navId = json["navId"];
  // }
  //
  // Map<String, dynamic> toJson() {
  //   var map = <String, dynamic>{};
  //   map["nickname"] = nickname;
  //   map["msg"] = msg;
  //   map["time"] = time;
  //   map["imgUrl"] = imgUrl;
  //   map["uid"] = uid;
  //   map["type"] = type;
  //   map["navId"] = navId;
  //   map["myId"] = myId;
  //   return map;
  // }
}
class TimestampConverter implements JsonConverter<Timestamp, Timestamp> {
  const TimestampConverter();

  @override
  Timestamp fromJson(Timestamp timestamp) {
    return timestamp;
  }

  @override
  Timestamp toJson(Timestamp timestamp) => timestamp;
}
