import 'package:boardgame/arguments/game_room_detail_arguments.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/bookmarks_provider.dart';
import 'package:boardgame/providers/geo_provider.dart';
import 'package:boardgame/screen/game_room_detail_page.dart';
import 'package:boardgame/utils/distance_utils.dart';
import 'package:boardgame/utils/text_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NewRoomCard extends ConsumerStatefulWidget {
  const NewRoomCard({required this.data, Key? key}) : super(key: key);
  final RoomDetail data;

  @override
  _NewRoomCardState createState() => _NewRoomCardState();
}

class _NewRoomCardState extends ConsumerState<NewRoomCard> {
  List<dynamic> attendees = [];
  late Map<String, dynamic> likes;
  late int commentsCount;
  late int age;
  String color = "";
  late String avatarUrl;
  late final  RoomDetail data;

  @override
  void initState() {
    super.initState();
    data = widget.data;

    // final Map<String, dynamic> doc = widget.snapshot.data();

    // likes = data.likes;
    commentsCount = data.commentsCount;
    // attendees = data.attendees;
    // age = (DateTime.now().year - ageData.toDate().year);
    avatarUrl = URLGetter.getAvatarUrl(data.creator.id);
  }

  getAddress(String? placeName, String? address) {
    if (placeName != null) return placeName;
    if (address == null) return "";
    if (address.startsWith('台灣')) {
      address = address.substring(2, address.length);
    }
    return address;
  }

  Timestamp getCreateTimeStamp() {
    Timestamp? timestamp = data.detail.createTime;
    if (timestamp == null) return Timestamp.now();
    return timestamp;
  }

  int getAttendeesDataLength() {
      return 2;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(
              context,
              GameRoomDetailScreen.id,
              arguments: GameRoomDetailArguments(roomId: data.id),
            );
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CachedNetworkImage(
                          imageUrl: avatarUrl,
                          fit: BoxFit.fill,
                          height: 36,
                          width: 36,
                          imageBuilder: (context, imageProvider) =>
                              CircleAvatar(
                            radius: 36,
                            backgroundImage: imageProvider,
                          ),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          data.creator.nickname,
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                    Consumer(
                      builder: (context, ref, child) {
                        final bookmarks =
                            ref.watch(bookmarkStateNotifierProvider).value;
                        bool isBookMarked = bookmarks.contains(data.id);
                        return IconButton(
                          color: isBookMarked ? Colors.amber : Colors.grey,
                          icon: isBookMarked
                              ? Icon(Icons.bookmark)
                              : Icon(Icons.bookmark_border),
                          onPressed: () {
                            isBookMarked
                                ? ref
                                    .read(
                                        bookmarkStateNotifierProvider.notifier)
                                    .removeBookMark(data.id)
                                : ref
                                    .watch(
                                        bookmarkStateNotifierProvider.notifier)
                                    .addBookMark(data.id);
                          },
                        );
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            TextUtil.getStartDateText(
                                data.detail.startTime ),
                            style: TextStyle(
                              color: Color(0xffc66900),
                            ),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          data.detail.isOnline
                              ? Row(
                                  children: [
                                    Icon(
                                      Icons.live_tv,
                                      color: Color(0xffc66900),
                                      size: 16,
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      '線上局',
                                      style: TextStyle(
                                          color: Color(0xffc66900),
                                          fontSize: 13),
                                    ),
                                  ],
                                )
                              : Text(
                                  TextUtil.getPlaceName(
                                      data.detail.placeName,
                                      data.detail.address),
                                  // overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Color(0xffc66900), fontSize: 13),
                                ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Expanded(
                      flex: 1,
                      child: CachedNetworkImage(
                        imageUrl: URLGetter.getGameRoomCoverUrl(data.id),
                        fit: BoxFit.fitWidth,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ],
                ),

                // ListTile(
                //   leading:
                //   title: Row(
                //     children: [
                //       Text(
                //         data['creator']['nickname'],
                //         style: Theme.of(context).textTheme.bodyText2,
                //       ),
                //       SizedBox(
                //         width: 8,
                //       ),
                //       Text(
                //         '${age.toString()}y',
                //         style: Theme.of(context).textTheme.caption,
                //       ),
                //     ],
                //   ),
                //   subtitle: Text(
                //     timeago.format(getCreateTimeStamp().toDate(), locale: 'zh'),
                //     style: Theme.of(context).textTheme.caption,
                //   ),

                // ),

                SizedBox(
                  height: 12,
                ),
                Text(
                  data.detail.title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  maxLines: 2,
                  // overflow: TextOverflow.ellipsis,
                ),
                // Text(
                //   data['detail']['title'],
                //   style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                //   // overflow: TextOverflow.ellipsis,
                // ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.group,
                          color: Colors.grey,
                        ),
                        Text(
                          ' ${data.attendees.length}/${data.detail.needNumbers + 1}',
                          style: Theme.of(context).textTheme.caption,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                      ],
                    ),
                    Consumer(
                      builder: (context, ref, child) {
                        AsyncValue<double?> distanceAsyncValue = ref.watch(
                            distanceProvider(
                                data.detail.position?.geopoint));
                        return distanceAsyncValue.maybeWhen(
                          data: (data) {
                            if (data == null) return Text('--m');
                            return Text(
                              '距離' + DistanceUtils.mileToText(data.round()),
                              style: Theme.of(context).textTheme.caption,
                            );
                          },
                          orElse: () {
                            return Text('距離--m',
                                style: Theme.of(context).textTheme.caption);
                          },
                        );
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
