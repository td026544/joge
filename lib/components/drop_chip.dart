import 'package:flutter/material.dart';

class DropChip extends StatelessWidget {
  final bool isSelected;
  final String text;
  final ValueChanged<bool>? onSelected;

  const DropChip(
      {Key? key, required this.isSelected, required this.text, this.onSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isSelected
        ? ChoiceChip(
            padding: EdgeInsets.only(right: 0),
            selected: isSelected,
            label: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: 4,
                ),
                Text(text),
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.arrow_drop_down,
                  size: 18,
                  color: Theme.of(context).primaryColorDark,
                )
              ],
            ),
            onSelected: onSelected,
          )
        : ChoiceChip(
            shape: StadiumBorder(
                side: BorderSide(color: Colors.black12, width: 1.0)),
            padding: EdgeInsets.only(right: 0),
            backgroundColor: Colors.transparent,
            selected: isSelected,
            label: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  text,
                  style: TextStyle(color: Colors.black54),
                ),
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.arrow_drop_down,
                  size: 18,
                  color: Colors.black54,
                )
              ],
            ),
            onSelected: onSelected,
          );
  }
}
