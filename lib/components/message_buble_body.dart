import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/mixins/message_bubble_body.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:boardgame/utils/socail_media_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'bubble_background.dart';
import 'image_full_screen_widget.dart';
import 'message_bubble.dart';

class MyMessageBody extends ConsumerWidget with MessageBody {
  final String time;
  final BorderRadius borderRadius;
  final List<InlineSpan>? textSpans;

  MyMessageBody(
      {Key? key,
      required this.time,
      required this.borderRadius,
      this.textSpans})
      : super(key: key) {
    initAllText(textSpans);
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
      child: Flex(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 4),
            child: Text(
              time,
              style: TextStyle(fontSize: 10, color: Colors.grey),
            ),
          ),
          SizedBox(
            width: 4,
          ),
          Flexible(
            child: InkWell(
              onLongPress: () {
                longPressMenu(context, ref);
              },
              child: ClipRRect(
                borderRadius: borderRadius,
                child: BubbleBackground(
                  colors: [const Color(0xFFFFAB40), const Color(0xFFEF5350)],
                  child: DefaultTextStyle.merge(
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: RichText(
                        text: TextSpan(children: textSpans),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class OtherMessageBody extends ConsumerWidget with MessageBody {
  final String time;
  final BorderRadius borderRadius;
  final List<InlineSpan>? textSpans;
  final BubbleShape bubbleShape;
  final String nickname;
  final String senderId;

  OtherMessageBody({
    Key? key,
    required this.time,
    required this.borderRadius,
    this.textSpans,
    required this.bubbleShape,
    required this.nickname,
    required this.senderId,
  }) : super(key: key) {
    initAllText(textSpans);
  }

  bool isShowAvatar() {
    if (bubbleShape == BubbleShape.head || bubbleShape == BubbleShape.body) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
      child: Flex(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        direction: Axis.horizontal,
        children: [
          isShowAvatar()
              ? GestureDetector(
                  child: ClipOval(
                    child: CachedNetworkImage(
                      imageUrl: URLGetter.getAvatarUrl(senderId),
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      width: 40.0,
                      height: 40.0,
                    ),
                  ),
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      PersonalPageById.id,
                      arguments: PersonalByIdArguments(
                        uid: senderId,
                      ),
                    );
                  },
                )
              : SizedBox(
                  width: 36,
                ),
          SizedBox(
            width: 8,
          ),
          Flexible(
            child: ClipRRect(
              borderRadius: borderRadius,
              child: Material(
                color: Color(0xFFEEEEEE),
                child: InkWell(
                  onLongPress: () {
                    longPressMenu(context, ref);
                  },
                  child: DefaultTextStyle.merge(
                    style: const TextStyle(
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: RichText(
                        text: TextSpan(children: textSpans),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 4,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 4),
            child: Text(
              time,
              style: TextStyle(fontSize: 10, color: Colors.grey),
            ),
          ),
        ],
      ),
    );
  }
}

class ImageMessageBubbleBody extends ConsumerWidget {
  const ImageMessageBubbleBody({
    Key? key,
    required this.isMe,
    required this.time,
    required this.nickname,
    required this.isShowAvatar,
    required this.imageUrl,
    required this.senderId,
  }) : super(key: key);
  final bool isMe;
  final String time;
  final String nickname;
  final bool isShowAvatar;
  final String imageUrl;
  final String senderId;
  void showLongPressMenu(BuildContext context, WidgetRef ref) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        // Using Wrap makes the bottom sheet height the height of the content.
        // Otherwise, the height will be half the height of the screen.
        return Wrap(
          children: [
            ListTile(
              leading: Icon(Icons.share),
              title: Text('分享'),
              onTap: () async {
                await share(context, ref, imageUrl);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  share(BuildContext context, WidgetRef ref, String url) async {
    try {
      await ref.read(socialMediaProvider).urlFileShare(url);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('無法分享，圖片連結可能已失效'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return isMe
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: Flex(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              direction: Axis.horizontal,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 4),
                  child: Text(
                    time,
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  ),
                ),
                SizedBox(
                  width: 36,
                ),
                SizedBox(
                  width: 8,
                ),
                Flexible(
                  child: GestureDetector(
                    onLongPress: () => showLongPressMenu(context, ref),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ImageFullScreen(
                            url: imageUrl,
                          ),
                        ),
                      );
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(4.0),
                      ),
                      child: ImageThumbnail(
                        imageUrl: imageUrl,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(8.0),
            child: Flex(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              direction: Axis.horizontal,
              children: [
                isShowAvatar
                    ? ClipOval(
                        child: CachedNetworkImage(
                          imageUrl: URLGetter.getAvatarUrl(senderId),
                          fit: BoxFit.cover,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          width: 40.0,
                          height: 40.0,
                        ),
                      )
                    : SizedBox(
                        width: 36,
                      ),
                SizedBox(
                  width: 8,
                ),
                ImageThumbnail(
                  imageUrl: imageUrl,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 4),
                  child: Text(
                    time,
                    style: TextStyle(fontSize: 12, color: Colors.grey),
                  ),
                ),
              ],
            ),
          );
  }
}
