import 'package:flutter/material.dart';

enum Section { HEAD, BODY, FOOT }

class SectionTileListView extends StatefulWidget {
  final List<String> texts;
  final ValueChanged<int> onSelectedIndexChanged;
  final int initSelectedIndex;

  const SectionTileListView(
      {Key? key,
      required this.texts,
      required this.onSelectedIndexChanged,
      this.initSelectedIndex = 0})
      : super(key: key);

  @override
  _SectionTileListViewState createState() => _SectionTileListViewState();
}

class _SectionTileListViewState extends State<SectionTileListView> {
  late List<SectionTile> widgets;
  int selectedIndex = 1;
  @override
  void initState() {
    selectedIndex = widget.initSelectedIndex;
    widgets = List.generate(widget.texts.length, (index) {
      if (index == 0) {
        return SectionTile(
          isSelected: selectedIndex == index,
          text: widget.texts[index],
          section: Section.HEAD,
          onTap: () {
            setState(() {
              print(selectedIndex);

              selectedIndex = index;
            });
          },
        );
      } else if (index == widget.texts.length - 1) {
        return SectionTile(
          isSelected: selectedIndex == index,
          text: widget.texts[index],
          section: Section.FOOT,
          onTap: () {
            setState(() {
              print(selectedIndex);
              selectedIndex = index;
            });
          },
        );
      } else {
        return SectionTile(
          isSelected: selectedIndex == index,
          text: widget.texts[index],
          section: Section.BODY,
          onTap: () {
            setState(() {
              print(selectedIndex);

              selectedIndex = index;
            });
          },
        );
      }
    });
    super.initState();
  }

  List<Widget> getWidgets() {
    return widgets = List.generate(widget.texts.length, (index) {
      if (index == 0) {
        return SectionTile(
          isSelected: selectedIndex == index,
          text: widget.texts[index],
          section: Section.HEAD,
          onTap: () {
            setState(() {
              widget.onSelectedIndexChanged(index);
              selectedIndex = index;
            });
          },
        );
      } else if (index == widget.texts.length - 1) {
        return SectionTile(
          isSelected: selectedIndex == index,
          text: widget.texts[index],
          section: Section.FOOT,
          onTap: () {
            setState(() {
              widget.onSelectedIndexChanged(index);
              selectedIndex = index;
            });
          },
        );
      } else {
        return SectionTile(
          isSelected: selectedIndex == index,
          text: widget.texts[index],
          section: Section.BODY,
          onTap: () {
            setState(() {
              widget.onSelectedIndexChanged(index);
              selectedIndex = index;
            });
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black12,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        children: ListTile.divideTiles(
                //          <-- ListTile.divideTiles
                context: context,
                tiles: getWidgets())
            .toList(),
      ),
    );
  }
}

class SectionTile extends StatelessWidget {
  final Section section;
  final bool isSelected;
  final String text;
  final GestureTapCallback? onTap;

  const SectionTile(
      {Key? key,
      this.section = Section.BODY,
      this.isSelected = false,
      required this.text,
      this.onTap})
      : super(key: key);

  BorderRadius? getBorder(Section section) {
    switch (section) {
      case Section.HEAD:
        return BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        );
      case Section.BODY:
        return BorderRadius.all(Radius.circular(0.0));
      case Section.FOOT:
        return BorderRadius.only(
          bottomLeft: Radius.circular(16),
          bottomRight: Radius.circular(16),
        );
    }
  }

  BoxDecoration getBoxDecoration(Section section, BuildContext context) {
    switch (section) {
      case Section.HEAD:
        return BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(15.9),
              topLeft: Radius.circular(15.9),
            ),
            color: isSelected
                ? Theme.of(context).primaryColorLight
                : Colors.transparent);
      case Section.BODY:
        return BoxDecoration(
            color: isSelected
                ? Theme.of(context).primaryColorLight
                : Colors.transparent);
      case Section.FOOT:
        return BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(15.9),
              bottomLeft: Radius.circular(15.9),
            ),
            color: isSelected
                ? Theme.of(context).primaryColorLight
                : Colors.transparent);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: getBorder(section),
      child: ListTile(
        dense: true,
        title: Text(
          text,
          style: isSelected ? null : TextStyle(color: Colors.black45),
        ),
        selected: isSelected,
        selectedTileColor: Theme.of(context).primaryColorLight,
        onTap: onTap,
        shape: section == Section.HEAD
            ? RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16)),
              )
            : null,
      ),
    );
  }
}
