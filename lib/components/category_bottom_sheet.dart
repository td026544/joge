import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/lists.dart';
import 'package:flutter/material.dart';

class CategoryBottomSheet extends StatefulWidget {
  const CategoryBottomSheet({Key? key, required this.onSelected})
      : super(key: key);
  final Function(CategoryType) onSelected;
  @override
  _CategoryBottomSheetState createState() => _CategoryBottomSheetState();
}

class _CategoryBottomSheetState extends State<CategoryBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Flex(
        direction: Axis.vertical,
        children: [
          SizedBox(
            height: 16,
          ),
          Row(
            children: [
              IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.black54,
                ),
                onPressed: () => Navigator.pop(context),
              ),
              SizedBox(
                width: 16,
              ),
              Text(
                '分類',
                style: Theme.of(context).textTheme.subtitle1,
              )
            ],
          ),
          Expanded(
            child: Scrollbar(
              isAlwaysShown: true,
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return Card(
                    child: InkWell(
                      onTap: () {
                        widget.onSelected(categoryItems[index].categoryType);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: 8,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                categoryItems[index].name,
                                style: TextStyle(fontSize: 18),
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              SizedBox(
                                child: Text(
                                  categoryItems[index].intro,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                width: 220,
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: ShaderMask(
                              shaderCallback: (rect) {
                                return LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.transparent, Colors.white],
                                ).createShader(Rect.fromLTRB(
                                    0, 0, rect.width - 40, rect.height));
                              },
                              blendMode: BlendMode.dstIn,
                              child: Image.asset(
                                categoryItems[index].imagePath,
                                height: 80,
                                width: 120,
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: categoryItems.length,
              ),
            ),
          )
        ],
      ),
    );
  }
}
