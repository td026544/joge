import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/rate_data_state_notifier.dart';
import 'package:boardgame/repository/ratings_repository.dart';
import 'package:boardgame/screen/rating_page.dart';
import 'package:boardgame/utils/text_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

enum Rate {
  HUMOR,
  SINCERE,
  TALENT,
}

class RatingCard extends ConsumerStatefulWidget {
  final RateUserData rateData;
  final String docId;
  final String roomName;

  RatingCard(this.rateData, this.docId, this.roomName);

  @override
  _RatingCardState createState() => _RatingCardState();
}

class _RatingCardState extends ConsumerState<RatingCard> {
  late RateUserData rateData;

  @override
  void initState() {
    this.rateData = widget.rateData;
    super.initState();
  }

  Future<RateUserData?> _showSheet(RateUserData oldRateData) async {
    String newComment = "";
    FocusNode commentFocusNode = FocusNode();
    TextEditingController editingController =
        TextEditingController(text: oldRateData.comment);
    int? newSelectIndex;
    if (oldRateData.rateType.isNotEmpty) {
      Rate? rate = EnumToString.fromString(Rate.values, oldRateData.rateType);
      newSelectIndex = rate != null ? Rate.values.indexOf(rate) : null;
    }

    return showModalBottomSheet(
      context: context,
      isScrollControlled: true, // set this to true
      builder: (_) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: StatefulBuilder(
            builder: (context, setState) {
              return Container(
                height: 400,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Flex(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  direction: Axis.vertical,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                          child: Text('取消'),
                          onPressed: () {
                            setState(() {
                              Navigator.pop(context);
                            });
                          },
                        ),
                        TextButton(
                          child: Text('確認'),
                          onPressed: () {
                            setState(() {
                              if (newSelectIndex == null &&
                                  newComment.isEmpty) {
                                Navigator.pop(context);
                              } else {
                                if (newSelectIndex != null) {
                                  oldRateData.rateType =
                                      EnumToString.convertToString(
                                    Rate.values[newSelectIndex!],
                                  );
                                }
                                if (newComment.isNotEmpty) {
                                  oldRateData.comment = newComment;
                                }
                                Navigator.pop(context, oldRateData);
                              }
                            });
                          },
                        ),
                      ],
                    ),
                    ListTile(
                      leading: ClipOval(
                        child: CachedNetworkImage(
                          imageUrl: URLGetter.getAvatarUrl(oldRateData.uid),
                          fit: BoxFit.cover,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          width: 48.0,
                          height: 48.0,
                        ),
                      ),
                      title: Text(oldRateData.nickname),
                      // trailing: Text(snapshot['rateType']),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: List.generate(Rate.values.length, (index) {
                        String rateText =
                            TextUtil.getRateText(Rate.values[index]);
                        return InkWell(
                          onTap: () {
                            setState(() {
                              newSelectIndex = index;
                            });
                          },
                          child: Column(
                            children: [
                              AnimatedContainer(
                                duration: Duration(milliseconds: 300),
                                width: index == newSelectIndex ? 64 : 56,
                                height: index == newSelectIndex ? 64 : 56,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: index == newSelectIndex
                                        ? Colors.red
                                        : Colors.grey,
                                    shape: BoxShape.circle),
                              ),
                              Text(
                                rateText,
                                style: Theme.of(context).textTheme.caption,
                              )
                            ],
                          ),
                        );
                      }),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            focusNode: commentFocusNode,
                            textInputAction: TextInputAction.newline,
                            keyboardType: TextInputType.multiline,
                            maxLines: 6,
                            minLines: 6,
                            maxLength: 240,
                            controller: editingController,
                            onChanged: (value) {
                              if (editingController.text.isNotEmpty) {
                                newComment = editingController.text;
                              } else {
                                newComment = editingController.text;
                              }
                            },
                            decoration: InputDecoration(
                                labelText: '留言',
                                border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(32.0),
                                  ),
                                  borderSide: BorderSide(
                                      width: 0.5, color: Colors.grey),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(32.0),
                                  ),
                                  borderSide: BorderSide(
                                      width: 0.5, color: Colors.black38),
                                ),
                                hintText: ' 寫下留言............',
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                filled: false),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              );
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            leading: ClipOval(
              child: CachedNetworkImage(
                imageUrl: URLGetter.getAvatarUrl(rateData.uid),
                fit: BoxFit.cover,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
                width: 48.0,
                height: 48.0,
              ),
            ),
            title: Text(rateData.nickname),
            // trailing: Text(snapshot['rateType']),
          ),
          Consumer(
            builder: (context, ref, child) {
              final asyncData = ref.watch(
                rateDataStateNotifierProvider(widget.docId),
              );
              return asyncData.when(
                  data: (data) {
                    String uid = rateData.uid;
                    String nickname = rateData.nickname;

                    String comment =  data.comment;
                    String rateType = data.rateType;

                    return Column(
                      children: [
                        Text(rateType),
                        Text(comment),
                        ElevatedButton(
                            onPressed: () async {
                              RateUserData? pendToUpdateData = await _showSheet(
                                RateUserData(
                                    uid: uid,
                                    nickname: nickname,
                                    rateType: rateType,
                                    comment: comment),
                              );
                              if (pendToUpdateData != null) {
                                ref.read(ratingsRepository).addRating1(
                                    pendToUpdateData,
                                    widget.docId,
                                    widget.roomName);
                                ref
                                    .read(rateDataStateNotifierProvider(
                                            widget.docId)
                                        .notifier)
                                    .updateRate(pendToUpdateData.comment,
                                        pendToUpdateData.rateType);
                              }
                            },
                            child: Text('評價')),
                      ],
                    );
                  },
                  error: (e, s, d) => Text(e.toString()),
                  loading: (d) => CircularProgressIndicator());
            },
          )
        ],
      ),
    );
  }
}
