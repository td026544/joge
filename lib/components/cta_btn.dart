import 'package:flutter/material.dart';

class CTAButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String text;
  const CTAButton({Key? key, @required this.onPressed, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text,
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
      style: ElevatedButton.styleFrom(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
        minimumSize: Size(160, 48),
      ),
    );
  }
}
