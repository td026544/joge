import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/utils/text_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class RatingDisplayCard extends StatelessWidget {
  const RatingDisplayCard({Key? key, required this.data, required this.toId})
      : super(key: key);
  final Map<String, dynamic> data;
  final String toId;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {},
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  leading: ClipOval(
                    child: CachedNetworkImage(
                      imageUrl:
                          URLGetter.getAvatarUrl(data['creator']['id'] ?? ""),
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      width: 48.0,
                      height: 48.0,
                    ),
                  ),
                  title: Text(data['creator']['nickname']),
                  subtitle: Text(
                    TextUtil.getTimeAgoFromTimeStamp(
                        data['time'] ?? Timestamp.now()),
                    style: Theme.of(context).textTheme.caption,
                  ),
                  trailing: Chip(
                    padding: EdgeInsets.zero,
                    avatar: CircleAvatar(
                      backgroundColor: Colors.red,
                      radius: 40,
                    ),
                    backgroundColor: Colors.transparent,
                    shape: StadiumBorder(
                      side: BorderSide(color: Colors.grey[400]!),
                    ),
                    label: Text(TextUtil.getRateTextFromTypeText(
                        data['toUser']['rateType'])),
                  ),
                ),
                Text(data['toUser']['comment']),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Colors.grey,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          data['roomName'],
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      child: Text(
                        '回覆',
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        minimumSize: Size(108, 40),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
