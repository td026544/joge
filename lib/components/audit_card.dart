import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/models/attend_user_data.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:boardgame/usecase/room_usecase.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AuditCard extends ConsumerStatefulWidget {
  final Map<String, dynamic> userData;
  final String id;
  final String roomId;
  AuditCard(this.userData, this.id, this.roomId);

  @override
  _AuditCardState createState() => _AuditCardState();
}

class _AuditCardState extends ConsumerState<AuditCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: ListTile(
        leading: GestureDetector(
          child: ClipOval(
            child: CachedNetworkImage(
              imageUrl: URLGetter.getAvatarUrl(widget.id),
              fit: BoxFit.cover,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
              width: 48.0,
              height: 48.0,
            ),
          ),
          onTap: () {
            Navigator.pushNamed(
              context,
              PersonalPageById.id,
              arguments: PersonalByIdArguments(
                uid: widget.id,
              ),
            );
          },
        ),
        title: Text(widget.userData['nickname']),
        trailing: ElevatedButton.icon(
          onPressed: () {
            ref.read(roomUseCaseProvider).confirmAudit(
                widget.roomId,
                AttendUserData(
                    uid: widget.id, nickname: widget.userData['nickname']));
          },
          icon: Icon(
            Icons.check,
            color: Colors.white,
          ),
          label: Text(
            '確認',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
