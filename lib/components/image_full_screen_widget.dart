import 'package:boardgame/utils/socail_media_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ImageFullScreen extends ConsumerStatefulWidget {
  const ImageFullScreen({required this.url, this.tag, Key? key})
      : super(key: key);
  final String url;
  final String? tag;

  @override
  _ImageFullScreenState createState() => _ImageFullScreenState();
}

class _ImageFullScreenState extends ConsumerState<ImageFullScreen> {
  save(String url) async {
    try {
      await ref.read(socialMediaProvider).saveImageFromUrl(url);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('存檔成功')));
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('無法下載，圖片連結可能已失效'),
        ),
      );
    }
  }

  share(String url) async {
    try {
      await ref.read(socialMediaProvider).urlFileShare(url);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('無法分享，圖片連結可能已失效'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            Expanded(
              child: Center(
                child: Hero(
                  tag: widget.tag!,
                  child: CachedNetworkImage(imageUrl: widget.url),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.share),
                  color: Colors.white,
                ),
                IconButton(
                  onPressed: () {
                    save(widget.url);
                  },
                  icon: Icon(Icons.download),
                  color: Colors.white,
                ),
              ],
            ),
            SizedBox(
              height: 16,
            )
          ],
        ),
      ),
    );
  }
}
