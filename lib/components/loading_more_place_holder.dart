import 'package:flutter/material.dart';

class LoadingMorePlaceHolder extends StatelessWidget {
  final bool isLoading;

  const LoadingMorePlaceHolder({Key? key, required this.isLoading})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '載入中',
                style: Theme.of(context).textTheme.caption,
              ),
              CircularProgressIndicator()
            ],
          )
        : Container();
  }
}
