import 'package:boardgame/arguments/chat_screen_arguments.dart';
import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/models/conversation.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/conversations_cards_provider.dart';
import 'package:boardgame/screen/chat_personal_page.dart';
import 'package:boardgame/screen/message_center_page.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:boardgame/usecase/friend_usecase.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FriendCard extends ConsumerWidget {
  FriendCard({this.ableToNavChat = false, required this.doc});
  final FriendDataDoc doc;
  final bool ableToNavChat;
  Widget getTrailing(BuildContext context, WidgetRef ref, FriendsStatus value) {
    switch (value) {
      case FriendsStatus.beInvited:
        return ElevatedButton(
          onPressed: () {
            ref.read(friendUseCaseProvider).acceptFriend(doc.uid);
          },
          child: Text(
            '接受邀請',
            style: TextStyle(color: Colors.white),
          ),
        );
      case FriendsStatus.invite:
        return OutlinedButton(
          onPressed: () {},
          child: Text(
            '邀請中',
          ),
        );
      case FriendsStatus.match:
        return Text('');
      case FriendsStatus.ban:
        return Text('已封鎖');
      default:
        return Text('');
    }
  }

  navToUserChatRoom(BuildContext context, WidgetRef ref) async {
    Conversation? conversation = ref
        .read(conversationsStateNotifierProvider.notifier)
        .getConversationById(doc.uid);
    if (conversation != null) {
      await Navigator.pushNamed(
        context,
        ChatPersonalScreen.id,
        arguments: ChatScreenArguments(conversation: conversation),
      );
    } else {
      await ref.read(chatUseCaseProvider).reloadChats();
      Conversation? conversation = ref
          .read(conversationsStateNotifierProvider.notifier)
          .getConversationById(doc.uid);
      if (conversation != null) {
        await Navigator.pushNamed(
          context,
          ChatPersonalScreen.id,
          arguments: ChatScreenArguments(conversation: conversation),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context, ref) {
    final userDataAsyncValue = ref.watch(getUserDataFutureProvider(doc.uid));
    Conversation? conversation = ref
        .watch(conversationsStateNotifierProvider.notifier)
        .getConversationById(doc.uid);

    return userDataAsyncValue.when(
        data: (userData) => ListTile(
              minVerticalPadding: 20,
              leading: GestureDetector(
                child: ClipOval(
                  child: CachedNetworkImage(
                    imageUrl: URLGetter.getAvatarUrl(userData.id),
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    width: 48.0,
                    height: 48.0,
                  ),
                ),
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    PersonalPageById.id,
                    arguments: PersonalByIdArguments(
                      uid: doc.uid,
                    ),
                  );
                },
              ),
              // subtitle: getSubtitle(doc.status),
              title: Text(
                userData.nickname,
                style: TextStyle(fontSize: 20),
              ),
              trailing: getTrailing(
                context,
                ref,
                EnumToString.fromString(FriendsStatus.values, doc.status) ??
                    FriendsStatus.none,
              ),

              onTap: () async {
                if (ableToNavChat) {
                  navToUserChatRoom(context, ref);
                }
              },
            ),
        loading: (data) => CircularProgressIndicator(),
        error: (data, e, s) => Text(e.toString()));
  }
}
