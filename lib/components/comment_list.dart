import 'package:boardgame/models/url_getter.dart';
import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:timeago/timeago.dart' as timeago;

final commentsStreamProvider = StreamProvider.family
    .autoDispose<QuerySnapshot, String>((ref, roomId) => FirebaseFirestore
        .instance
        .collection('comments')
        .doc(roomId)
        .collection('comments')
        .orderBy('time')
        .snapshots());

class CommentList extends ConsumerWidget {
  final String roomId;

  CommentList(this.roomId);
  @override
  Widget build(BuildContext context, ref) {
    AsyncValue<QuerySnapshot> commentsAsyncValue =
        ref.watch(commentsStreamProvider(roomId));
    return commentsAsyncValue.when(
      loading: (data) => const CircularProgressIndicator(),
      error: (data, err, stack) => Text('Error: $err'),
      data: (snapshot) {
        return ListView.separated(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: snapshot.size,
          itemBuilder: (context, index) {
            Map<String, dynamic> data =
                snapshot.docs[index].data() as Map<String, dynamic>;
            // Avatar avatar = Avatar.fromJson(data['avatar']);
            Timestamp time = data['time'];
            String avatarUrl = URLGetter.getAvatarUrl(data['id']);
            return Padding(
                padding: const EdgeInsets.only(
                  left: 8,
                  right: 8,
                  top: 8,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CachedNetworkImage(
                      imageUrl: avatarUrl,
                      fit: BoxFit.fill,
                      imageBuilder: (context, imageProvider) => CircleAvatar(
                        radius: 20,
                        backgroundImage: imageProvider,
                      ),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Bubble(
                          color: Colors.grey[200],
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data['name'] ?? "用戶",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                data['comment'],
                                style: TextStyle(fontSize: 13),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          timeago.format(time.toDate(), locale: 'zh'),
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                  ],
                ));
          },
          separatorBuilder: (BuildContext context, int index) {
            return Divider();
          },
        );
      },
    );
  }
}
