import 'dart:ui';

import 'package:boardgame/models/url_getter.dart';
import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

class CommentListTile extends StatelessWidget {
  final Map<String, dynamic> data;
  late final Timestamp? time;
  late final String timeText;
  late final String avatarUrl;
  late final String id;
  late final String name;

  final Function(ReplyToUser) onReply;

  CommentListTile({Key? key, required this.data, required this.onReply})
      : super(key: key) {
    time = data['time'];
    id = data['id'];
    name = data['name'];

    DateTime dateTime = time != null ? time!.toDate() : DateTime.now();
    timeText = timeago.format(dateTime, locale: 'zh');
    avatarUrl = URLGetter.getAvatarUrl(data['id']);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedNetworkImage(
            imageUrl: avatarUrl,
            fit: BoxFit.fill,
            imageBuilder: (context, imageProvider) => CircleAvatar(
              radius: 20,
              backgroundImage: imageProvider,
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Bubble(
                margin: BubbleEdges.only(top: 10),
                stick: true,
                nip: BubbleNip.leftTop,
                color: Colors.grey[200],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data['name'] ?? "用戶",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    LimitedBox(
                      maxWidth: 260,
                      maxHeight: 480,
                      child: Text(
                        data['comment'],
                        maxLines: 8,
                        style: TextStyle(
                            fontSize: 14, overflow: TextOverflow.ellipsis),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: [
                  Text(
                    timeText,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  TextButton(
                    onPressed: () => onReply(ReplyToUser(name, id)),
                    child: Text(
                      '回覆',
                      style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 13),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}

class ReplyToUser {
  final String nickName;
  final String uid;

  ReplyToUser(this.nickName, this.uid);
}
