import 'package:boardgame/arguments/chat_room_arguments.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/unread_message_state_notifiyer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:timeago/timeago.dart' as timeago;

class NewChatRoomCard extends StatelessWidget {
  NewChatRoomCard({required this.chatRoomDoc}) {
    data = chatRoomDoc.data() as Map<String, dynamic>;
  }
  late final Map<String, dynamic> data;
  final DocumentSnapshot chatRoomDoc;

  String getTitle() {
    if (chatRoomDoc['type'] == 'CHAT') {
      String toUserId = chatRoomDoc['toUserId'];
      return chatRoomDoc['attendeesData.$toUserId.nickname'];
    } else {
      String title =
          '${chatRoomDoc['title']} (${chatRoomDoc['attendees'].length.toString()})';
      return title;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ListTile(
        leading: ClipOval(
          child: CachedNetworkImage(
            imageUrl: chatRoomDoc['type'] == 'CHAT'
                ? URLGetter.getAvatarUrl(chatRoomDoc['toUserId'])
                : URLGetter.getGameRoomCoverUrl(chatRoomDoc.id),
            fit: BoxFit.cover,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
            width: 48.0,
            height: 48.0,
          ),
        ),
        title: Text(getTitle()),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(data.containsKey('msg') ? data['msg']['last'] : ''),
          ],
        ),
        trailing: Column(
          children: [
            Consumer(builder: (context, watch, child) {
              // final streamMessagesProvider =
              //     watch(remoteMessageFutureProvider(chatRoomDoc.id));

              if (data.containsKey('msg')) {
                Timestamp time = data['msg']['time'];
                int timeInt = time.millisecondsSinceEpoch;
                // context
                //     .read(remoteLastTimeStateProvider(chatRoomDoc.id))
                //     .state = timeInt;
                return Text(
                  timeago.format(DateTime.fromMillisecondsSinceEpoch(timeInt),
                      locale: 'zh'),
                  style: Theme.of(context).textTheme.caption,
                );
              } else {
                return Text('');
              }
            }),
            Consumer(
              builder: (context, ref, child) {
                bool isHaveUnReadMessage = ref.watch(
                    isHaveNewMessageStateNotifierProvider(chatRoomDoc.id));

                return isHaveUnReadMessage
                    ? Container(
                        margin: EdgeInsets.only(top: 12),
                        child: SizedBox(
                          width: 16,
                          height: 16,
                          child: Material(
                            type: MaterialType.circle,
                            color: Colors.redAccent,
                            elevation: 2.0,
                          ),
                        ),
                      )
                    : SizedBox(
                        height: 18,
                      );
              },
            ),
          ],
        ),
        onTap: () async {
          await Navigator.pushNamed(
            context,
            'chatRoomPage',
            arguments: ChatRoomArguments(
              chatRoomDoc: chatRoomDoc,
            ),
          );
        },
      ),
    );
  }
}
