import 'package:flutter/material.dart';

class MyChip extends StatelessWidget {
  final iconData;
  final text;
  const MyChip({Key? key, this.iconData, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Chip(
      avatar: Icon(
        iconData,
        color: Colors.black54,
        size: 20,
      ),
      backgroundColor: Colors.white,
      side: BorderSide(width: 1, color: Colors.grey),
      label: Text(
        text,
        style: TextStyle(color: Colors.black54),
      ),
    );
  }
}