import 'dart:convert';

import 'package:boardgame/models/chat_message.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/providers/users_providers.dart';
import 'package:boardgame/utils/date_utils.dart';
import 'package:boardgame/utils/text_util.dart';
import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

import 'image_full_screen_widget.dart';
import 'message_buble_body.dart';

enum BubbleShape { head, body, single, foot }

@immutable
class MessageBubble extends ConsumerWidget {
  MessageBubble(
      {Key? key,
      required this.message,
      required this.isLastMessageSameDay,
      required this.isMe,
      required this.nickname,
      required this.senderId,
      required this.bubbleShape})
      : super(key: key) {
    if (message.msgType == describeEnum(MsgType.text)) {
      String decodeText = TextUtil.jsonUf8ToString(message.msg);
      generateTextSpan(decodeText);
    }
  }

  // MessageBubble(
  //     {required this.isLastMessageSameDay,
  //     required this.message,
  //     required this.isMe,
  //     required this.nickname,
  //     required this.bubbleShape});

  final ChatMessage message;
  final bool isMe;
  final String nickname;
  final String senderId;
  final BubbleShape bubbleShape;
  final bool isLastMessageSameDay;
  final List<TextSpan> textSpans = [];

  Widget serviceMessageWidget() {
    return Center(
      child: Bubble(
        color: Colors.grey[200],
        margin: BubbleEdges.only(top: 10),
        radius: Radius.circular(20.0),
        child: Text(getText()),
      ),
    );
  }

  BorderRadius getBorder() {
    double smallRadius = 8.0;
    switch (bubbleShape) {
      case BubbleShape.head:
        return isMe
            ? BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
                bottomRight: Radius.circular(smallRadius),
                bottomLeft: Radius.circular(20.0),
              )
            : BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0),
                bottomLeft: Radius.circular(smallRadius),
              );
      case BubbleShape.single:
        return isMe
            ? BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(smallRadius),
                bottomRight: Radius.circular(smallRadius),
                bottomLeft: Radius.circular(30.0),
              )
            : BorderRadius.only(
                topLeft: Radius.circular(smallRadius),
                topRight: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0),
                bottomLeft: Radius.circular(smallRadius),
              );
      case BubbleShape.body:
        return isMe
            ? BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(smallRadius),
                bottomRight: Radius.circular(smallRadius),
                bottomLeft: Radius.circular(30.0),
              )
            : BorderRadius.only(
                topLeft: Radius.circular(smallRadius),
                topRight: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0),
                bottomLeft: Radius.circular(smallRadius),
              );
      case BubbleShape.foot:
        return isMe
            ? BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(smallRadius),
                bottomRight: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
              )
            : BorderRadius.only(
                topLeft: Radius.circular(smallRadius),
                topRight: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
              );
    }
  }

  String handleMatch(Match match) {
    String string = match.group(0).toString();
    if (isMe) {
      textSpans.add(
        TextSpan(
          text: string,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      );
    } else {
      textSpans.add(
        TextSpan(
          text: string,
          style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
        ),
      );
    }
    return string;
  }

  String handleNotMatch(String string) {
    if (isMe) {
      textSpans
          .add(TextSpan(text: string, style: TextStyle(color: Colors.white)));
    } else {
      textSpans
          .add(TextSpan(text: string, style: TextStyle(color: Colors.black)));
    }
    return string;
  }

  void generateTextSpan(String string) {
    RegExp hasTagRegExp = RegExp(
        r'[@]{1}[\u4E00-\u9FA5A-Za-z0-9+]+[\s]'); //brackets are significant
    // var matches = regExp.allMatches(s);
    textSpans.clear();
    var s = string.splitMapJoin((hasTagRegExp),
        onMatch: (m) => handleMatch(m), onNonMatch: (n) => handleNotMatch(n));
  }

  String getText() {
    try {
      var uft8Text = json.decode(message.msg).cast<int>();
      var text = utf8.decode(uft8Text);
      return text;
    } catch (e) {
      return "ERROR:";
    }
  }

  String getTime() {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(message.time);
    return DateFormat.jm('zh').format(dateTime);
  }

  bool isShowAvatar() {
    if (bubbleShape == BubbleShape.head || bubbleShape == BubbleShape.body) {
      return false;
    } else {
      return true;
    }
  }

  Widget getMessageBubble(BuildContext context) {
    if (message.msgType == describeEnum(MsgType.service))
      return serviceMessageWidget();
    else if (message.msgType == describeEnum(MsgType.text)) {
      if (isMe) {
        return isLastMessageSameDay
            ? MyMessageBody(
                time: getTime(),
                borderRadius: getBorder(),
                textSpans: textSpans,
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Align(
                    child: Text(
                      MyTimeUtils.getDateTimeAboveMessageBubble(message.time),
                      style: Theme.of(context).textTheme.caption,
                    ),
                    alignment: Alignment.center,
                  ),
                  MyMessageBody(
                    time: getTime(),
                    borderRadius: getBorder(),
                    textSpans: textSpans,
                  ),
                ],
              );
      } else {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Offstage(
              offstage: isLastMessageSameDay,
              child: Align(
                child: Text(
                  MyTimeUtils.getDateTimeAboveMessageBubble(message.time),
                  style: Theme.of(context).textTheme.caption,
                ),
                alignment: Alignment.center,
              ),
            ),
            Offstage(
              offstage: bubbleShape != BubbleShape.head,
              child: Padding(
                padding: EdgeInsets.only(left: 52),
                child: Consumer(
                  builder: (context, ref, child) {
                    final asyncUser = ref.watch(toUserFutureProvider(senderId));
                    return asyncUser.maybeWhen(
                      data: (user) => user != null
                          ? Text(
                              user.nickname,
                              style: TextStyle(color: Colors.grey),
                            )
                          : Text('用戶', style: TextStyle(color: Colors.grey)),
                      orElse: () => Text(''),
                    );
                  },
                ),
              ),
            ),
            OtherMessageBody(
              time: getTime(),
              nickname: nickname,
              borderRadius: getBorder(),
              bubbleShape: bubbleShape,
              textSpans: textSpans,
              senderId: senderId,
            ),
          ],
        );
      }
    } else if (message.msgType == describeEnum(MsgType.image))
      try {
        return buildImageWidget(context);
      } catch (e) {
        return Container();
      }

    return Container();
  }

  Widget buildImageWidget(BuildContext context) {
    return ImageMessageBubbleBody(
      isShowAvatar: isShowAvatar(),
      isMe: isMe,
      time: getTime(),
      nickname: nickname,
      imageUrl: getText(),
      senderId: senderId,
    );
  }

  @override
  Widget build(BuildContext context, watch) {
    return getMessageBubble(context);
    return Padding(
        padding: EdgeInsets.all(10.0), child: getMessageBubble(context));
  }
}

class ImageThumbnail extends StatelessWidget {
  final String imageUrl;

  const ImageThumbnail({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ImageFullScreen(
              tag: imageUrl,
              url: imageUrl,
            ),
          ),
        );
      },
      child: Hero(
        tag: imageUrl,
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          imageBuilder: (context, imageProvider) => ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: 180.0, //宽度尽可能大
                minHeight: 180.0,
                maxWidth: 240,
                maxHeight: 240, //最小高度为50像素
              ),
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.contain,
                  ),
                ),
              )),
          placeholder: (context, url) => Center(
            child: SizedBox(
              child: CircularProgressIndicator(),
              width: 48,
              height: 48,
            ),
          ),
          errorWidget: (context, url, error) => Container(
            height: 96,
            width: 96,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.photo,
                  size: 48,
                  color: Colors.white,
                ),
                Text(
                  '圖片已過期或失效',
                  style: TextStyle(color: Colors.white, fontSize: 9),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
