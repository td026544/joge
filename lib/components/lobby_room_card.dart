import 'package:boardgame/arguments/game_room_detail_arguments.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/bookmarks_provider.dart';
import 'package:boardgame/providers/geo_provider.dart';
import 'package:boardgame/screen/game_room_detail_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class LobbyRoomCard extends ConsumerStatefulWidget {
  const LobbyRoomCard({required this.data, Key? key}) : super(key: key);
  final Map<String, dynamic> data;

  @override
  _LobbyRoomCardState createState() => _LobbyRoomCardState();
}

class _LobbyRoomCardState extends ConsumerState<LobbyRoomCard> {
  List<dynamic> attendees = [];
  late Map<String, dynamic> likes;
  late int commentsCount;
  late int age;
  String color = "";
  late String avatarUrl;
  late Map<String, dynamic> data;

  @override
  void initState() {
    super.initState();
    data = widget.data;

    // final Map<String, dynamic> doc = widget.snapshot.data();

    likes = data['likes'];
    commentsCount = data['commentsCount'];
    attendees = data['attendees'] ?? [];
    Timestamp ageData = data['creator']['age'];
    // age = (DateTime.now().year - ageData.toDate().year);
    avatarUrl = URLGetter.getAvatarUrl(data['creator']['id']);
  }

  getAddress() {
    String? address = data['detail']['address'];
    if (address == null) return "";
    if (address.startsWith('台灣')) {
      address = address.substring(2, address.length);
    }
    return address;
  }

  Timestamp getCreateTimeStamp() {
    Timestamp? timestamp = data['detail']['createTime'];
    if (timestamp == null) return Timestamp.now();
    return timestamp;
  }

  String getStartDateText() {
    Timestamp timestamp = data['detail']['startTime'] ?? Timestamp.now();
    var dateTime = DateFormat('EEEE M月d日 – kk:mm', 'zh_TW').format(
      timestamp.toDate(),
    );
    return dateTime;
  }

  int getAttendeesDataLength() {
    Map<String, dynamic> attendeesData = data['attendeesData'];

    return attendeesData.length;
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      child: SizedBox(
        width: 344,
        child: Card(
          clipBehavior: Clip.antiAlias,
          child: InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                GameRoomDetailScreen.id,
                arguments: GameRoomDetailArguments(roomId: data['id']),
              );
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  leading: CachedNetworkImage(
                    imageUrl: avatarUrl,
                    fit: BoxFit.fill,
                    height: 40,
                    width: 40,
                    imageBuilder: (context, imageProvider) => CircleAvatar(
                      radius: 20,
                      backgroundImage: imageProvider,
                    ),
                  ),
                  title: Row(
                    children: [
                      Text(
                        data['creator']['nickname'],
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                    ],
                  ),
                  subtitle: Text(
                    timeago.format(getCreateTimeStamp().toDate(), locale: 'zh'),
                    style: Theme.of(context).textTheme.caption,
                  ),
                  trailing: Consumer(
                    builder: (context, ref, child) {
                      final bookmarks =
                          ref.watch(bookmarkStateNotifierProvider).value;
                      bool isBookMarked = bookmarks.contains(data['id']);
                      return IconButton(
                        color: isBookMarked ? Colors.amber : Colors.grey,
                        icon: isBookMarked
                            ? Icon(Icons.bookmark)
                            : Icon(Icons.bookmark_border),
                        onPressed: () {
                          isBookMarked
                              ? ref
                                  .read(bookmarkStateNotifierProvider.notifier)
                                  .removeBookMark(data['id'])
                              : ref
                                  .watch(bookmarkStateNotifierProvider.notifier)
                                  .addBookMark(data['id']);
                        },
                      );
                    },
                  ),
                ),
                LimitedBox(
                  maxWidth: 344,
                  maxHeight: 194,
                  child: CachedNetworkImage(
                    imageUrl: URLGetter.getGameRoomCoverUrl(data['id']),
                    fit: BoxFit.fitWidth,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    data['detail']['title'],
                    // overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    getAddress(),
                    // overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    getStartDateText(),
                    // overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonBar(
                    alignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.favorite,
                            color: Colors.grey[400],
                            size: 20,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            likes.length.toString(),
                            style: Theme.of(context).textTheme.caption,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Icon(
                            Icons.comment,
                            color: Colors.grey[400],
                            size: 20,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            commentsCount.toString(),
                            style: Theme.of(context).textTheme.caption,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            ' 參加人數:${getAttendeesDataLength()}/${data['detail']['needNumbers']}',
                            style: Theme.of(context).textTheme.caption,
                          ),
                          SizedBox(
                            height: 8,
                          ),
                        ],
                      ),
                      Consumer(
                        builder: (context, ref, child) {
                          AsyncValue<double?> distanceAsyncValue = ref.watch(
                              distanceProvider(
                                  data['detail']['position']['geopoint']));
                          return distanceAsyncValue.maybeWhen(
                            data: (data) {
                              if (data == null) return Text('--m');
                              return Text(
                                data.round().toString() + "m",
                                style: Theme.of(context).textTheme.caption,
                              );
                            },
                            orElse: () {
                              return Text('--m',
                                  style: Theme.of(context).textTheme.caption);
                            },
                          );
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
