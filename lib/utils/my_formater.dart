import 'package:intl/intl.dart';

class MyFormater {
  static String getTime(int timeStamp) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timeStamp);

    return DateFormat('yyyy-MM-dd – kk:mm').format(dateTime);
  }
}
