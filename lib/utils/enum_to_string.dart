import 'package:boardgame/models/enums.dart';

class GetNameUtil {
  static String getGameTypeName(GameType gameType) {
    switch (gameType) {
      case GameType.BOARD_GAME:
        return "桌游";
      case GameType.ROOM_ESCAPE:
        return "密室逃脫";
      case GameType.WEREWOLVES:
        return "狼人殺";
      case GameType.OTHER:
        return "其它";
    }
  }

  static String getPaymentTypeName(PaymentType paymentType) {
    switch (paymentType) {
      case PaymentType.FREE:
        return "免費";

      case PaymentType.PAY_SELF:
        return "出自己的";
      case PaymentType.GO_DUTCH:
        return "現場均分";
    }
  }

  static String getCategoryName(CategoryType categoryType) {
    switch (categoryType) {
      case CategoryType.board_game:
        return '桌遊';
      case CategoryType.sport:
        return '運動';
      case CategoryType.music:
        return '音樂&唱歌';
      case CategoryType.hiking:
        return '爬山';
      case CategoryType.travel:
        return '旅遊';
      case CategoryType.eat:
        return '美食';
      case CategoryType.art:
        return '文化&藝術';
      case CategoryType.dance:
        return '跳舞';
      case CategoryType.game:
        return '遊戲';
      case CategoryType.learning:
        return '教育&學習';
      case CategoryType.pet:
        return '寵物';
      case CategoryType.business:
        return '商業';
      case CategoryType.book:
        return '讀書會';
      case CategoryType.micro_social:
        return '微社交';
      case CategoryType.other:
        return '其他';
      case CategoryType.relax:
        return '休閒';
        break;
    }
  }
}
