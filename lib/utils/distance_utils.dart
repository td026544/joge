class DistanceUtils {
  static String mileToText(int miles) {
    if (miles < 1000) {
      return '${miles.toString()}公尺';
    } else {
      return '${miles ~/ 1000}公里';
    }
  }
}
