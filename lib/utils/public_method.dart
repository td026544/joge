import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:flutter/material.dart';

class PublicMethod {
  static void onUserAvatarClick(BuildContext context, creatorId) {
    Navigator.pushNamed(
      context,
      PersonalPageById.id,
      arguments: PersonalByIdArguments(
        uid: creatorId,
      ),
    );
  }
}
