import 'package:intl/intl.dart';

class MyTimeUtils {
  static int calculateDifference(DateTime lastDate, DateTime date) {
    return DateTime(lastDate.year, lastDate.month, lastDate.day)
        .difference(DateTime(date.year, date.month, date.day))
        .inDays;
    // Yesterday : calculateDifference(date) == -1.
    // Today : calculateDifference(date) == 0.
    // Tomorrow : calculateDifference(date) == 1.
  }

  static bool isLastTimeSameDay(int lastTime, int time) {
    DateTime lastDateTime = DateTime.fromMillisecondsSinceEpoch(lastTime);
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(time);
    int diffDays = calculateDifference(dateTime, lastDateTime);
    if (diffDays > 0) {
      return false;
    } else {
      return true;
    }
  }

  static String getDateTimeAboveMessageBubble(int time) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(time);
    String timeString = DateFormat.MMMMd('zh').format(dateTime);
    return timeString;
  }
}
