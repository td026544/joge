import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:extended_image/extended_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:share_plus/share_plus.dart';

final socialMediaProvider = Provider((ref) => SocialMediaUtil());

class SocialMediaUtil {
  Future<String> uploadImageByte(File file) async {
    try {
      String postName = DateTime.now().millisecondsSinceEpoch.toString();
      if (FirebaseAuth.instance.currentUser == null) throw Exception('noUser');
      SettableMetadata metadata = SettableMetadata(
        cacheControl: 'max-age=60',
        customMetadata: <String, String>{
          'userId': FirebaseAuth.instance.currentUser!.uid,
        },
      );
      Reference ref = FirebaseStorage.instance.ref('chatRooms/$postName.jpg');
      await ref.putFile(file);

      String url = await ref.getDownloadURL();
      return url;
    } on FirebaseException catch (e) {
      throw Exception('downloadFail');
      // e.g, e.code == 'canceled'
    }
  }

  Future<Null> urlFileShare(String url) async {
    String path = await saveImageFromUrl(url);

    Share.shareFiles([path.replaceAll('file:', "")], text: 'Great picture');

    // if (Platform.isAndroid) {
    //
    //   Share.shareFile(File('$documentDirectory/flutter.jpg'),
    //       subject: 'URL File Share',
    //       text: 'Hello, check your share files!',
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // } else {
    //   Share.share('Hello, check your share files!',
    //       subject: 'URL File Share',
    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    // }
  }

  Future<String> saveImageFromUrl(String url) async {
    try {
      var response = await Dio()
          .get(url, options: Options(responseType: ResponseType.bytes));
      String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      Map result = await ImageGallerySaver.saveImage(
          Uint8List.fromList(response.data),
          name: fileName);
      String path = result['filePath'];
      return path;
    } catch (e) {
      throw Exception('download fail');
    }
  }

  copyText(BuildContext context, String text) {
    try {
      Clipboard.setData(
        ClipboardData(text: text),
      );
    } catch (e) {}
  }

  shareText(String text) {
    Share.share(text);
  }
}
