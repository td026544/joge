import 'package:boardgame/models/chat_message.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/feed_item.dart';
import 'package:boardgame/models/message.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
// final String contactsTable = "Contacts";

const String messagesTable = "Messages";
const String feedsTable = "Feeds";
const String msgColumn = "msg";
const String uidColumn = "uid";

const String chatsCountTable = "ChatsCount";

const String chatsTable = "Chats";
const String nicknameColumn = "nickname";
const String phoneNumberColumn = "phoneNumber";
const String idColumn = "id";
const String chatRoomIdColumn = "roomId";
const String nameColumn = 'name';
const String usernameColumn = 'sender';
const String textColumn = "text";
const String timeColumn = "time";
const String photoUrlColumn = "photoUrl";
const String msgTypeColumn = 'msgType';
const String isContactColumn = 'isContact';
const String blockStatusColumn = 'blockStat'; // 0 => Blocked, 1 => Unblocked
// const String msgStatusColumn = "msgStatus"; // 0=> Received, 1=> Sent
const String senderIdColumn = "senderId"; // 0=> Received, 1=> Sent

const String imgUrlColumn = "imgUrl";
const String typeColumn = "type";
const String navIdColumn = "navId";
const String myIdColumn = "myId";
final String chatIdColumn = "chatId";
final String toIdColumn = "toId"; //phoneIdColumn
// final String userIdColumn = "userId"; //phoneIdColumn

final String unReadCountColumn = "unReadCount";
final String chatTypeColumn = "chatType"; // 0=> Received, 1=> Sent

class DBManager {
  DBManager._();

  static final DBManager db = DBManager._();

  Database? _database;

  // final String contactsTable = "Contacts";

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    }
    _database = await initDB();
    return _database!;
  }

  initDB() async {
    String dbPath = await getDatabasesPath();
    return await openDatabase(
      join(dbPath, "joge_database.db"),
      version: 3,
      // onCreate is called only if there was no prior database in the specified path
      onCreate: (Database db, int version) async {
        // we need 3 tables
        // 1. for storing users data
        // await db.execute(
        //     "CREATE TABLE $contactsTable ($phoneNumberColumn TEXT PRIMARY KEY, $chatIdColumn TEXT, "
        //     "$nameColumn TEXT, $usernameColumn TEXT,"
        //     "$photoUrlColumn TEXT, $isContactColumn INTEGER, $blockStatusColumn INTEGER)");

        // 2. for storing the messages for the chat screen
        // await db.execute("CREATE TABLE $messagesTable ("
        //     "$chatRoomIdColumn TEXT, $msgColumn TEXT, $msgTypeColumn TEXT, "
        //     "$timeColumn INTEGER, $msgStatusColumn INTEGER)");

        // await db.execute(
        //     "CREATE TABLE $messagesTable ($idColumn TEXT PRIMARY KEY,"
        //     "$chatRoomIdColumn TEXT, $textColumn TEXT, $usernameColumn TEXT, "
        //     "$timeColumn INTEGER)");
        await db.execute("CREATE TABLE $messagesTable ("
            "$chatIdColumn TEXT, $msgColumn TEXT, $msgTypeColumn TEXT, "
            "$timeColumn INTEGER, $senderIdColumn TEXT)");

        await db.execute(
            "CREATE TABLE $chatsTable ($toIdColumn TEXT, $nameColumn TEXT,"
            "$chatIdColumn TEXT, $usernameColumn TEXT, $photoUrlColumn TEXT, $isContactColumn INTEGER, $blockStatusColumn INTEGER,"
            "$msgColumn TEXT, $msgTypeColumn TEXT, "
            "$timeColumn INTEGER, $chatTypeColumn TEXT, "
            "$myIdColumn TEXT, PRIMARY KEY ($chatIdColumn, $myIdColumn))");

        await db.execute(
            "CREATE TABLE $chatsCountTable ($chatIdColumn TEXT PRIMARY KEY,$unReadCountColumn INTEGER,"
            "$myIdColumn TEXT)");

        // -----------------------------
        await db.execute("CREATE TABLE $feedsTable ($idColumn TEXT PRIMARY KEY,"
            "$nicknameColumn TEXT, $msgColumn TEXT, $uidColumn TEXT, "
            "$imgUrlColumn TEXT, $typeColumn TEXT, $navIdColumn TEXT, "
            "$myIdColumn TEXT, $timeColumn INTEGER)");

        // 3. for storing the last message to show in the home screen in the chat cards
        // await db.execute(
        //     "CREATE TABLE $chatsTable ($phoneNumberColumn TEXT PRIMARY KEY, $nameColumn TEXT,"
        //     "$chatRoomIdColumn TEXT, $usernameColumn TEXT, $photoUrlColumn TEXT, $isContactColumn INTEGER, $blockStatusColumn INTEGER,"
        //     " $msgColumn TEXT, $msgTypeColumn TEXT, "
        //     "$timeColumn INTEGER)");
      },
    );
  }

  Future<void> updateMessageToChatTable(String chatId, String newMsg,
      String msgType, int currTime, String myUid) async {
    final Database db = await database;

    int numOfUpdates = await db.rawUpdate(
        "UPDATE $chatsTable SET $msgColumn = ?, $timeColumn = ?, $msgTypeColumn = ? WHERE $chatIdColumn = ?",
        [newMsg, currTime, msgType, chatId]);

    print("$numOfUpdates rows were changed in Chats Table");
  }

  Future<void> updateChatsUnReadTable(
    String chatId,
    int unReadCount,
  ) async {
    final Database db = await database;

    await db.insert(
      chatsCountTable,
      {
        chatIdColumn: chatId,
        unReadCountColumn: unReadCount,
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    // int numOfUpdates = await db.rawUpdate(
    //     "UPDATE $chatsCountTable  SET $chatIdColumn = ?, $unReadCountColumn = ?",
    //     [chatId, unReadCount]);
  }

  Future<Map<String, int>> getChatsUnRead(String myId) async {
    final Database db = await database;
    final Map<String, int> map = {};
    var res = await db.query(
      chatsCountTable,
      columns: [
        chatIdColumn,
        unReadCountColumn,
        myIdColumn,
      ],
      where: "$unReadCountColumn > ? AND $myIdColumn = ?",
      whereArgs: [0, myId],
    );
    res.forEach((element) {
      String chatId = element['chatId'] as String;
      int count = element['unReadCount'] as int;
      map[chatId] = count;
    });
    return map;

    // List<Map> output = List<Map>.from(res);
  }

  Future<void> addNewMessageToMessagesTable(String chatId, String newMsg,
      String msgType, int currTime, String senderId) async {
    final Database db = await database;

    await db.insert(
      messagesTable,
      {
        chatIdColumn: chatId,
        msgColumn: newMsg,
        msgTypeColumn: msgType,
        timeColumn: currTime,
        senderIdColumn: senderId,
      },
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<void> addNewFeedsToMessagesTable(
      List<FeedItem> feedItems, String roomId) async {
    final Database db = await database;
    Batch batch = db.batch();

    feedItems.forEach((feedItem) {
      //assuming you have 'Cities' class defined
      batch.insert(
        feedsTable,
        {
          idColumn: feedItem.id,
          nicknameColumn: feedItem.nickname,
          msgColumn: feedItem.msg,
          uidColumn: feedItem.uid,
          imgUrlColumn: feedItem.imgUrl,
          typeColumn:feedItem.type,
          navIdColumn: feedItem.navId,
          myIdColumn: feedItem.myId,
          timeColumn: feedItem.time.millisecondsSinceEpoch,
        },
        conflictAlgorithm: ConflictAlgorithm.ignore,
      );
    });
    batch.commit();
  }

  Future<ChatMessage> readMessageFromChatTable(String chatId) async {
    final Database db = await database;

    List<Map<String, dynamic>> res = await db.query(chatsTable,
        columns: [
          msgColumn,
          msgTypeColumn,
          timeColumn,
          chatIdColumn,
          senderIdColumn
        ],
        where: "$chatIdColumn = ?",
        whereArgs: [chatId]);

    ChatMessage chatMessage = ChatMessage.fromMap(res[0]);

    print("Most recent message read From Db :::::::: $chatMessage");
    return chatMessage;
  }

  Future<List<ChatMessage>> readAllMessagesfromMessagesTable(
      String chatId) async {
    final Database db = await database;

    var res = await db
        .query(messagesTable, where: "$chatIdColumn = ?", whereArgs: [chatId]);

    return res.map((e) => ChatMessage.fromMap(e)).toList();
  }

  Future<List<ChatMessage>> readMoreLimitMessagesFromMessagesTable(
      String roomId, int limit, int lastTime) async {
    final Database db = await DBManager.db.database;
    if (lastTime == 0) {
      var res = await db.query(messagesTable,
          where: "$chatIdColumn = ?",
          orderBy: "$timeColumn DESC",
          limit: limit,
          whereArgs: [roomId]);
      return res.map((e) => ChatMessage.fromMap(e)).toList();
    } else {
      var res = await db.query(messagesTable,
          where: "$chatIdColumn = ? AND time < ?",
          orderBy: "time DESC",
          limit: limit,
          whereArgs: [roomId, lastTime]);
      return res.map((e) => ChatMessage.fromMap(e)).toList();
    }
  }

  Future<List<Message>?> readAllMessagesFromMessagesTable(String chatId) async {
    final Database db = await database;

    var res = await db.query(messagesTable, where: "$chatIdColumn = ? ",
        // orderBy: "$timeColumn DESC",
        // limit: 10,
        whereArgs: [chatId]);
    print(res);

    return res.map((e) => Message.fromDB(e)).toList();
  }

  Future<ChatMessage> readLastMessagesfromMessagesTable(String chatId) async {
    final Database db = await database;

    var res = await db.query(messagesTable,
        where: "$chatIdColumn = ?",
        limit: 1,
        orderBy: "$timeColumn DESC",
        whereArgs: [chatId]);
    if (res.isNotEmpty) {
      return ChatMessage.fromMap(res.first);
    } else {
      throw Exception('not found');
    }
    // return res.map((e) => ChatMessage.fromMap(e));
  }

  Future<List<FeedItem>?> readLimitFeedsFromMessagesTable(
      String userId, int limit) async {
    final Database db = await database;

    var res = await db.query(feedsTable,
        where: "$myIdColumn = ?",
        orderBy: "$timeColumn DESC",
        limit: limit,
        whereArgs: [userId]);
    print(res);

    return res.map((e) => FeedItem.fromJson(e)).toList();
  }

// get all conversations from chat table to show in the home page
  Future<List<Map<dynamic, dynamic>>> getAlConversationsFromChatTable(
      String myUid) async {
    final Database db = await database;

    // var res = await db.query(chatsTable, where: "$textColumn IS NOT NULL");
    var res = await db
        .query(chatsTable, where: "$myIdColumn = ?", whereArgs: [myUid]);

    List<Map> output = List<Map>.from(res);

    return output;
  }

  Future<void> updateProfilePicInChatsTable(
      String photoUrl, String chatId) async {
    final Database db = await database;

    await db.rawUpdate(
      "UPDATE $chatsTable SET $photoUrlColumn = ? WHERE $chatIdColumn = ?",
      [photoUrl, chatId],
    );
  }

  Future<List<Map<dynamic, dynamic>>> getAllContacts() async {
    final Database db = await database;

    var res = await db.query(
      chatsTable,
      columns: [
        phoneNumberColumn,
        nameColumn,
        usernameColumn,
        photoUrlColumn,
        chatIdColumn,
        myIdColumn,
      ],
      where: "$isContactColumn = ?",
      whereArgs: [1],
    );
    List<Map> output = List<Map>.from(res);

    return output;
  }

  Future<void> createRow(
      {required String toId,
      required String chatId,
      required String contactName,
      required String username,
      required String photoUrl,
      required int isContact,
      required String chatType,
      required String myId}) async {
    final Database db = await database;

    await db.insert(
      chatsTable,
      {
        toIdColumn: toId,
        chatIdColumn: chatId,
        nameColumn: contactName,
        usernameColumn: username,
        photoUrlColumn: photoUrl,
        isContactColumn: isContact,
        blockStatusColumn: 1, // At first it will be unblocked ofcourse
        chatTypeColumn: chatType,
        msgTypeColumn: EnumToString.convertToString(MsgType.none),
        timeColumn: Timestamp.now().millisecondsSinceEpoch,
        myIdColumn: myId,
      },
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<void> deleteContact(String phoneNum) async {
    final Database db = await database;

    await db.delete(chatsTable,
        where: "$phoneNumberColumn = ?", whereArgs: [phoneNum]);
  }

  Future<bool> checkIfContactExistsInDb(String toId) async {
    final Database db = await database;
    var result = await db.rawQuery(
      "SELECT COUNT(1) FROM $chatsTable WHERE $toIdColumn = ? LIMIT 1",
      [toId],
    );
    return result[0]["COUNT(1)"] == 1 ? true : false;
  }

  Future<bool> checkIfUsernameExistsInDb(String username) async {
    final Database db = await database;
    var result = await db.rawQuery(
      "SELECT COUNT(1) FROM $chatsTable WHERE $usernameColumn = ? LIMIT 1",
      [username],
    );
    return result[0]["COUNT(1)"] == 1 ? true : false;
  }

  Future<bool> checkIfChatExistsInDb(String chatId, String myUid) async {
    final Database db = await database;

    var result = await db.rawQuery(
      "SELECT COUNT(1) FROM $chatsTable WHERE $chatIdColumn = ? AND $myIdColumn = ? LIMIT 1",
      [chatId, myUid],
    );
    return result[0]["COUNT(1)"] == 1 ? true : false;
  }

  Future<void> deleteTable() async {
    final Database db = await database;

    await db.delete(chatsTable, where: '1');
  }

  Future<void> deleteConversation(String chatId) async {
    final Database db = await database;

    await db
        .delete(chatsTable, where: "$chatIdColumn = ?", whereArgs: [chatId]);
  }

// check if contact is blocked or unblocked
  Future<bool> isBlocked(String chatId) async {
    final Database db = await database;

    var res = await db.query(chatsTable,
        columns: [blockStatusColumn],
        where: '$chatIdColumn = ?',
        whereArgs: [chatId]);

    return (res[0]["blockStat"] == 1 ? false : true);
  }

  Future<void> updateBlockStatus(int newStatus, String chatId) async {
    final Database db = await database;

    await db.rawUpdate(
      "UPDATE $chatsTable SET $blockStatusColumn = ? WHERE $chatIdColumn = ?",
      [newStatus, chatId],
    );
  }

  Future<void> updateName(String newName, String chatId) async {
    final Database db = await database;

    await db.rawUpdate(
      "UPDATE $chatsTable SET $nameColumn = ? WHERE $chatIdColumn = ?",
      [newName, chatId],
    );
  }
}
