import 'dart:convert';

import 'package:boardgame/components/rating_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class TextUtil {
  static String jsonUf8ToString(String jsonString) {
    try {
      var uft8Text = json.decode(jsonString).cast<int>();
      var text = utf8.decode(uft8Text);
      return text;
    } catch (e) {
      return 'ERROR';
    }
  }

  static String getPlaceName(String? placeName, String? address) {
    if (placeName != null) return placeName;
    if (address == null) return "";
    if (address.startsWith('台灣')) {
      address = address.substring(2, address.length);
    }
    return address;
  }

  static String getStartDateText(Timestamp timestamp) {
    var dateTime = DateFormat('EEEE M月d日 – kk:mm', 'zh_TW').format(
      timestamp.toDate(),
    );
    return dateTime;
  }

  static String getTimeAgoFromData(DateTime dateTime) {
    String time = timeago.format(dateTime);
    return time;
  }

  static String getTimeAgoFromTimeStamp(Timestamp timestamp) {
    String time = timeago.format(timestamp.toDate(), locale: 'zh');
    return time;
  }

  static String getRateText(Rate? rate) {
    switch (rate) {
      case Rate.HUMOR:
        return "幽默風趣";
      case Rate.SINCERE:
        return "待人真誠";
      case Rate.TALENT:
        return "才華洋溢";
      default:
        return "";
    }
  }

  static String getRateTextFromTypeText(String typeText) {
    Rate? rate = EnumToString.fromString(Rate.values, typeText);
    return getRateText(rate);
  }
}
