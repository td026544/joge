import 'package:boardgame/models/enums.dart';
import 'package:boardgame/screen/message_center_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'friends_provider.dart';

final addFriendUnReadCountProvider =
    FutureProvider.autoDispose<int>((ref) async {
  List<FriendDataDoc> friendDataDocs =
      await ref.watch(inviteFriendsFutureProvider.future);
  int count = 0;
  for (FriendDataDoc doc in friendDataDocs) {
    if (doc.status == describeEnum(FriendsStatus.beInvited)) {
      count++;
    }
  }

  return count;
});
