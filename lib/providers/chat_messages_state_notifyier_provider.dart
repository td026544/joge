// import 'package:boardgame/models/feed_item.dart';
// import 'package:boardgame/models/my_user.dart';
// import 'package:boardgame/repository/feeds_repository.dart';
import 'package:boardgame/models/chat_message.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth_providers.dart';

final chatMessagesStateNotifierProvider = StateNotifierProvider.family
    .autoDispose<ChatMessagesStateNotifier, AsyncValue<List<ChatMessage>>,
        String>((ref, chatId) => ChatMessagesStateNotifier(ref.read, chatId));

class ChatMessagesStateNotifier
    extends StateNotifier<AsyncValue<List<ChatMessage>>> {
  Reader read;

  bool hasMore = true; // flag for more products available or not

  int docLimit = 40; // documents to be fetched per request
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  int messageOldestTime = 0;
  List<ChatMessage> oldItems = [];

  final chatId;
  ChatMessagesStateNotifier(
    this.read,
    this.chatId,
  ) : super(AsyncData(<ChatMessage>[])) {
    _init();
  }
  _init() async {
    getMore();
  }

  addMessage() async {
    if (!mounted) return;

    ChatMessage lastChatMessages =
        await DBManager.db.readLastMessagesfromMessagesTable(chatId);
    state = AsyncData([lastChatMessages, ...state.data!.value]);
  }

  Future<void> getMore() async {
    if (!mounted) return;

    if (!hasMore) return;
    if (state == AsyncValue.loading()) return;
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('No user');
    if (!hasMore) return;
    if (state == AsyncValue.loading()) return;
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final feedsData = await DBManager.db
          .readMoreLimitMessagesFromMessagesTable(
              chatId, docLimit, messageOldestTime);
      List<ChatMessage> newItems = feedsData;
      if (newItems.length < docLimit) hasMore = false;
      return [...oldItems, ...newItems];
    });
    if (state.data?.value == null || state.data!.value.isEmpty) return;
    oldItems = [...state.data!.value];
    messageOldestTime = state.data!.value.first.time;
  }
}

final typingStateStateNotifierProvider =
    StateProvider.family<bool, String>((ref, roomId) => false);
