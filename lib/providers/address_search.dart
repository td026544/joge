import 'package:boardgame/providers/place_service_v1.dart';
import 'package:flutter/material.dart';

class AddressSearch extends SearchDelegate<Suggestion?> {
  AddressSearch(this.sessionToken) {
    apiClient = PlaceApiProvider(sessionToken);
  }

  final sessionToken;
  late PlaceApiProvider apiClient;
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        tooltip: '清除',
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: '返回',
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  // @override
  // Widget buildResults(BuildContext context) {
  //   return null;
  // }

  @override
  Widget buildSuggestions(BuildContext context) {
    var a = Localizations.localeOf(context).languageCode;
    return FutureBuilder(
      future: query == "" ? null : apiClient.fetchSuggestions(query, 'zh-TW'),
      builder: (context, snapshot) {
        if (query == "") {
          return Container(
            padding: EdgeInsets.all(16.0),
            child: Text('輸入地點'),
          );
        } else if (snapshot.hasError) {
          return Text('發生一些錯誤');
        } else if (snapshot.hasData) {
          List<Suggestion> suggestions = snapshot.data as List<Suggestion>;
          return ListView.builder(
            itemBuilder: (context, index) => ListTile(
              title: Text((suggestions[index]).description),
              onTap: () {
                close(context, suggestions[index]);
              },
            ),
            itemCount: suggestions.length,
          );
        } else {
          return Container(child: Text('載入中...'));
        }
      },
      // builder: (context, snapshot) => query == ''
      //     ? Container(
      //         padding: EdgeInsets.all(16.0),
      //         child: Text('Enter your address'),
      //       )
      //     : snapshot.hasData
      //         ? ListView.builder(
      //             itemBuilder: (context, index) => ListTile(
      //               title:
      //                   Text((snapshot.data[index] as Suggestion).description),
      //               onTap: () {
      //                 close(context, snapshot.data[index] as Suggestion);
      //               },
      //             ),
      //             itemCount: snapshot.requireData!.length,
      //           )
      //         : Container(child: Text('Loading...')),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  // @override
  // Widget buildResults(BuildContext context) {
  //   throw UnimplementedError();
  // }
}
//     : snapshot.hasData
// ? ListView.builder(
// itemBuilder: (context, index) => ListTile(
// title:
// Text((snapshot.data![index] as Suggestion).description),
// onTap: () {
// close(context, snapshot.data[index] as Suggestion);
// },
// ),
// itemCount: snapshot.requireData!.length,
// )
// : Container(child: Text('Loading...')),
