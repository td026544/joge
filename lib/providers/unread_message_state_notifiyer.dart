import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'my_chat_room_change_notifyer.dart';

final isHaveNewMessageStateNotifierProvider =
    StateNotifierProvider.family<IsHaveNewMessageStateNotifier, bool, String>(
        (ref, roomId) => IsHaveNewMessageStateNotifier(ref.read, roomId));

class IsHaveNewMessageStateNotifier extends StateNotifier<bool> {
  final Reader read;

  int remoteTime = 0;
  int localTime = 0;
  final String roomId;

  IsHaveNewMessageStateNotifier(this.read, this.roomId) : super(false);

  updateRemoteTime(int time) {
    remoteTime = time;
    if (remoteTime > localTime) {
      state = true;
      read(isHaveNewMessageChangeNotifier.notifier)
          .updateHaveUnread(roomId, true);
    } else {
      state = false;
      read(isHaveNewMessageChangeNotifier.notifier)
          .updateHaveUnread(roomId, false);
    }
  }

  updateLocalTime(int time) {
    localTime = time;
    if (remoteTime > localTime) {
      state = true;
      read(isHaveNewMessageChangeNotifier.notifier)
          .updateHaveUnread(roomId, true);
    } else {
      state = false;
      read(isHaveNewMessageChangeNotifier.notifier)
          .updateHaveUnread(roomId, false);
    }
    // localTime = time;
    // if (remoteTime == null) {
    //   state = false;
    //   return;
    // }
    // if (localTime == null) {
    //   state = false;
    //   return;
    // }
    // if (remoteTime! > localTime!) {
    //   state = true;
    // } else {
    //   state = false;
    // }
  }
}
