import 'package:boardgame/models/conversation.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth_providers.dart';

final conversationsStateNotifierProvider = StateNotifierProvider.autoDispose<
        ConversationsStateNotifier, AsyncValue<List<Conversation>>>(
    (ref) => ConversationsStateNotifier(ref.read));

class ConversationsStateNotifier
    extends StateNotifier<AsyncValue<List<Conversation>>> {
  Reader read;

  ConversationsStateNotifier(this.read) : super(AsyncData([])) {
    getData();
  }
  getData() async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');

    List<Conversation> conversations = [];

    List<Map<dynamic, dynamic>> dbData =
        await DBManager.db.getAlConversationsFromChatTable(myUser.id);

    dbData.forEach((dbMap) {
      dbMap as Map<String,dynamic>;
      conversations.add(Conversation.fromJson(dbMap));
    });

    // conversations.sort((b, a) => a.time.compareTo(b.time));
    state = AsyncData(conversations);
  }

  Conversation? getConversationById(String toId) {
    return state.value.firstWhereOrNull((element) => element.toId == toId);
  }

  deleteItem(int index) {
    List<Conversation> newItem = state.value;
    newItem.removeAt(index);
    state = AsyncData(newItem);
  }
}
