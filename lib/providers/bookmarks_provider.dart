import 'package:boardgame/repository/bookmarks_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final bookmarkStateNotifierProvider =
    StateNotifierProvider<BookMarksStateNotifier, AsyncData<List<String>>>(
        (ref) => BookMarksStateNotifier(ref.read));

class BookMarksStateNotifier extends StateNotifier<AsyncData<List<String>>> {
  Reader read;

  BookMarksStateNotifier(this.read) : super(AsyncData([])) {
    init();
  }
  init() async {
    Map<String, dynamic> data =
        await read(bookMarkRepositoryProvider).getBookMarks();
    List<dynamic> list = data['bookmarks'];
    if (list.isNotEmpty) {
      List<String> bookmarks = list.cast<String>();
      state = AsyncData(bookmarks);
    }
  }

  void addBookMark(String roomId) async {
    state = AsyncData([...state.value, roomId]);

    read(bookMarkRepositoryProvider).addToBookMark(roomId).then((value) {});
  }

  void removeBookMark(String roomId) {
    if (state.value.contains(roomId)) {
      List newList = state.value.where((id) => id != roomId).toList();
      state = AsyncData([...newList]);
    }
    read(bookMarkRepositoryProvider).removeBookMark(roomId).then((value) {});
  }
}
