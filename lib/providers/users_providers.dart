import 'package:boardgame/models/my_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final toUserFutureProvider =
    FutureProvider.family<MyUser?, String>((ref, uid) async {
  MyUser? user;
  DocumentSnapshot documentSnapshot =
      await FirebaseFirestore.instance.collection('users').doc(uid).get();
  var data = documentSnapshot.data() as Map<String, dynamic>;
  user = MyUser.fromJson(data);
  return user;
});
