import 'package:boardgame/models/create_room_save_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final createRoomStateProvider =
    StateProvider<CreateRoomSaveModel?>((ref) => null);
