import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geolocator/geolocator.dart';

final isShowGeoProvider = StateProvider<bool>((ref) => false);

final geoProvider = FutureProvider<GeoPoint?>((ref) async {
  late bool serviceEnabled;
  late LocationPermission permission;
  bool isShow = ref.watch(isShowGeoProvider).state;
  if (!isShow) {
    return null;
  }
  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();

  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }
  if (isShow) {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.low);
    GeoPoint geoPoint = GeoPoint(position.latitude, position.longitude);
    return geoPoint;
  } else {
    return null;
  }
});
final distanceProvider =
    FutureProvider.family<double?, GeoPoint?>((ref, geoPoint) async {
  if (geoPoint == null) return null;
  GeoPoint? myGeoPoint = await ref.watch(geoProvider.future);
  if (myGeoPoint == null) return null;
  double distance = Geolocator.distanceBetween(myGeoPoint.latitude,
      myGeoPoint.longitude, geoPoint.latitude, geoPoint.longitude);
  return distance;
});
