import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/game_room_change_notifiyer_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final historyRoomStateNotifierProvider = StateNotifierProvider.autoDispose<
    HistoryRoomListStateNotifier,
    AsyncValue<List<RoomDetail>>>((ref) {
  return HistoryRoomListStateNotifier(ref.read);
});

class HistoryRoomListStateNotifier
    extends StateNotifier<AsyncValue<List<RoomDetail>>>
    with HomeTabList {
  Reader read;

  HistoryRoomListStateNotifier(this.read)
      : super(AsyncData(<RoomDetail>[])) {
    fetch();
  }
  List<RoomDetail> oldItemss=[];
  fetch() async {
    MyUser? user = read(userStateProvider).state;
    if (user == null) return;
    if (!hasMore) return;
    if (state == AsyncValue.loading()) return;
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      QuerySnapshot querySnapshot;
      if (lastDocument == null) {
        querySnapshot = await roomRef
            .limit(documentLimit)
            .orderBy("detail.startTime", descending: true)
            .where("attendees", arrayContains: user.id)
            .where("detail.startTime", isLessThan: Timestamp.now())
            .get();
      } else {
        querySnapshot = await roomRef
            .limit(documentLimit)
            .orderBy("startTime", descending: true)
            .where("attendees", arrayContains: user.id)
            .where("detail.startTime", isLessThan: Timestamp.now())
            .startAfterDocument(lastDocument!)
            .get();
      }
      if (querySnapshot.docs.isNotEmpty) lastDocument = querySnapshot.docs.last;

      List<RoomDetail> newItems = querySnapshot.docs
          .map((e) => RoomDetail.fromJson(e.data() as Map<String, dynamic>))
          .toList();
      if (newItems.length < documentLimit) hasMore = false;
      return [...oldItemss, ...newItems];
    });
    oldItemss = [...state.value];
  }

  reset() {
    lastDocument = null;
    hasMore = true;
  }
}
