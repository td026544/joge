import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';



class HomeTabList {
  bool hasMore = true; // flag for more products available or not
  List<RoomDetail> oldItems = [];
  int documentLimit = 4; // documents to be fetched per
  DocumentSnapshot? lastDocument; // fl// request
  CollectionReference roomRef = FirebaseFirestore.instance.collection('rooms');
}

final allRoomChangeNotifierProvider = StateNotifierProvider<
        AllRoomChangeNotifierProvider, List<RoomDetail>>(
    (ref) => AllRoomChangeNotifierProvider(ref.read));

class AllRoomChangeNotifierProvider
    extends StateNotifier<List<RoomDetail>> {
  Reader read;
  static const _pageSize = 5;
  DocumentSnapshot? lastDocument; // fl// request
  CollectionReference roomRef = FirebaseFirestore.instance.collection('rooms');

  final PagingController<int, RoomDetail> pagingController =
      PagingController(firstPageKey: 0);
  AllRoomChangeNotifierProvider(this.read) : super(<RoomDetail>[]) {
    pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
  }
  Future<void> fetchPage(int pageKey) async {
    try {
      QuerySnapshot querySnapshot;
      if (lastDocument == null) {
        querySnapshot = await roomRef.limit(_pageSize).get();
      } else {
        querySnapshot = await roomRef
            .startAfterDocument(lastDocument!)
            .limit(_pageSize)
            .get();
      }
      if (querySnapshot.docs.isNotEmpty) lastDocument = querySnapshot.docs.last;

      List<RoomDetail> newItems = querySnapshot.docs
          .map((e) => RoomDetail.fromJson(e.data() as Map<String, dynamic>))
          .toList();
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
      state = [...state, ...newItems];
    } catch (error) {
      pagingController.error = error;
    }
  }

  void refresh() {
    state = [];
    lastDocument = null;
    pagingController.refresh();
  }
}
