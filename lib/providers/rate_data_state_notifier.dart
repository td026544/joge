import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/rate_data.dart';
import 'package:boardgame/models/rate_to_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final rateDataStateNotifierProvider = StateNotifierProvider.autoDispose
    .family<RateDataStateNotifier, AsyncValue<RateToUser>, String>(
        (ref, docId) {
  return RateDataStateNotifier(ref.read, docId);
});

class RateDataStateNotifier
    extends StateNotifier<AsyncValue<RateToUser>> {
  Reader read;
  String docId;

  RateDataStateNotifier(this.read, this.docId)
      : super(AsyncValue.loading()) {
    fetch();
  }
  fetch() async {
    MyUser? user = read(userStateProvider).state;
    if (user == null) return;
    if (state == AsyncValue.loading()) return;
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      DocumentReference docRef = FirebaseFirestore.instance
          .collection('ratings')
          .doc(user.id)
          .collection('rates')
          .doc(docId);
      DocumentSnapshot documentSnapshot = await docRef.get();
      if (documentSnapshot.exists) {
        Map<String, dynamic> docData =
            documentSnapshot.data() as Map<String, dynamic>;
        RateData rateData=RateData.fromJson(docData);

        // Map<String, dynamic> data = {
        //   'comment': docData['toUser']['comment'] ?? "",
        //   'rateType': docData['toUser']['rateType'] ?? "",
        // };
        return rateData.toUser;
      } else {
        // Map<String, dynamic> data = {
        //   'comment': '',
        //   'rateType': '',
        // };
        return RateToUser("", "", "", "");
      }
    });
  }

  updateRate(String comment, String rateType) {
    state = AsyncData( RateToUser(comment,state.value.nickname,rateType,state.value.uid));
  }
}
