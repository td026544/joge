import 'package:boardgame/models/my_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'auth_providers.dart';

final commentChangeNotifierProvider = StateNotifierProvider.family<
    CommentChangeNotifierProvider,
    List<Map<String, dynamic>>,
    String>((ref, roomId) => CommentChangeNotifierProvider(ref.read, roomId));

class CommentChangeNotifierProvider
    extends StateNotifier<List<Map<String, dynamic>>> {
  Reader read;
  String roomId;
  static const _pageSize = 5;
  DocumentSnapshot? lastDocument;
  late CollectionReference commentsRef;

  final PagingController<int, Map<String, dynamic>> pagingController =
      PagingController(firstPageKey: 0);
  CommentChangeNotifierProvider(this.read, this.roomId)
      : super(<Map<String, dynamic>>[]) {
    commentsRef = FirebaseFirestore.instance
        .collection('comments')
        .doc(roomId)
        .collection('comments');
    pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
  }
  Future<void> fetchPage(int pageKey) async {
    try {
      QuerySnapshot querySnapshot;
      if (lastDocument == null) {
        querySnapshot = await commentsRef
            .limit(_pageSize)
            .orderBy('time', descending: true)
            .get();
      } else {
        querySnapshot = await commentsRef
            .orderBy('time', descending: true)
            .limit(_pageSize)
            .startAfterDocument(lastDocument!)
            .get();
      }
      if (querySnapshot.docs.isNotEmpty) lastDocument = querySnapshot.docs.last;

      List<Map<String, dynamic>> newItems = querySnapshot.docs
          .map((e) => e.data() as Map<String, dynamic>)
          .toList();
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
      state = [...state, ...newItems];
    } catch (error) {
      pagingController.error = error;
    }
  }

  void refresh() {
    state = [];
    lastDocument = null;
    pagingController.refresh();
  }
}

final justPostCommentChangeNotifierProvider = StateNotifierProvider.family<
        JustPostCommentCommentChangeNotifierProvider,
        List<Map<String, dynamic>>,
        String>(
    (ref, roomId) => JustPostCommentCommentChangeNotifierProvider(ref.read));

class JustPostCommentCommentChangeNotifierProvider
    extends StateNotifier<List<Map<String, dynamic>>> {
  Reader read;

  JustPostCommentCommentChangeNotifierProvider(this.read)
      : super(<Map<String, dynamic>>[]);
  addComment(String text) {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    Map<String, dynamic> data = {
      'comment': text,
      'id': myUser.id,
      'name': myUser.nickname,
      'time': Timestamp.now()
    };
    state = [...state, data].reversed.toList();
  }
}
