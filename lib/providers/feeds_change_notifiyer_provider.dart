import 'package:boardgame/models/feed_item.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/repository/feeds_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'auth_providers.dart';

final localFeedsStateNotifierProvider =
    StateNotifierProvider<LocalFeedsStateNotifier, List<FeedItem>>(
        (ref) => LocalFeedsStateNotifier(ref.read));

class LocalFeedsStateNotifier extends StateNotifier<List<FeedItem>> {
  Reader read;
  List<FeedItem> oldItems = [];
  static const _pageSize = 15;

  bool isLoading = false; // track if products fetching
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  int messageOldestTime = 0;
  final PagingController<int, FeedItem> pagingController =
      PagingController(firstPageKey: 0);
  LocalFeedsStateNotifier(this.read) : super(<FeedItem>[]) {
    pagingController.addPageRequestListener((pageKey) {
      fetch(pageKey);
    });
  }

  fetch(int pageKey) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('No user');
    try {
      final feedsData = await read(feedsRepository)
          .getMoreLimitFeedsFromFeedsTable(
              myUser.id, _pageSize, messageOldestTime);
      List<FeedItem> newItems = feedsData.toList();
      if (newItems.isNotEmpty) {
        messageOldestTime = newItems.last.time.millisecondsSinceEpoch;
      }
      final isLastPage = newItems.length < _pageSize;

      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
      state = [...state, ...newItems];
    } catch (error) {
      pagingController.error = error;
    }
  }
}
