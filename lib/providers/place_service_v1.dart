import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart';

// // For storing our result
class Suggestion {
  final String placeId;
  final String description;
  final String city;

  Suggestion(this.placeId, this.description, this.city);

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $placeId,city:$city)';
  }
}

class PlaceApiProvider {
  final client = Client();

  PlaceApiProvider(this.sessionToken);

  final sessionToken;

  static final String androidKey = 'AIzaSyATclcZfdnKITiloR3I6utz1MgsWdQiFUU';
  static final String iosKey = 'AIzaSyAhHpTcBe-vX_QdO5kCN9Vd5yMxjdR2r5g';
  final apiKey = Platform.isAndroid ? androidKey : iosKey;

  Future<List<Suggestion>> fetchSuggestions(
    String input,
    String lang,
  ) async {
    // print('position.latitude${position.latitude}');
    var request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&language=$lang&key=$apiKey&sessiontoken=$sessionToken&components=country:tw&location=25.0283092,121.5636229';
    var url = Uri.parse(request);

    // if (position.latitude != null && position.longitude != null) {
    //   request =
    //       'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=$lang&components=country:tw&location${position.latitude},${position.longitude}&key=$apiKey&sessiontoken=$sessionToken';
    // }

    final response = await client.get(url);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        print(result['predictions']);
        String city = "";
        for (var element in result['predictions']) {
          for (var offset in element['terms']) {
            if (offset['offset'] == 2) {
              city = offset['value'];
            }
          }
        }
        // compose suggestions in a list
        return result['predictions']
            .map<Suggestion>(
                (p) => Suggestion(p['place_id'], p['description'], city))
            .toList();
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Future<Place> getPlaceDetailFromId(String placeId, String city) async {
    String lang = 'zh-TW';
    String field1 = 'icon';
    String field2 = 'geometry';
    String field3 = 'photo';
    String field4 = 'url';

    String field5 = 'name';
    String field6 = 'vicinity';
    String field7 = 'formatted_address';
    String field8 = 'business_status';
    String field9 = 'address_component';

    final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&language=$lang&fields=$field1,$field2,$field3,$field4,$field5,$field6,$field7,$field8&key=$apiKey&sessiontoken=$sessionToken';
    var url = Uri.parse(request);

    final response = await client.get(url);
    // 地址： 110台北市信義區松仁路253巷1弄2號
    // 營業時間：
    // 已打烊 ⋅ 開始營業時間：10:30
    // 健康與安全: 員工會接受體溫測量 · 更多詳細資料
    // 電話： 02 2720 7471
    // 選單: facebook.com
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        String formattedAddress =
            result['result']['formatted_address'] as String;
        String name = result['result']['name'] as String;
        String icon = result['result']['icon'] as String;

        final geometry =
            result['result']['geometry'] as LinkedHashMap<String, dynamic>;
        double? lat = geometry['location']['lat'];
        double? lng = geometry['location']['lng'];
        // final components =
        //     result['result']['address_components'] as List<dynamic>;
        // build result
        final place = Place();
        place.address = formattedAddress;
        place.name = name;
        if (lat != null && lng != null) {
          place.geoPoint = GeoPoint(lat, lng);
        }
        place.iconUrl = icon;
        place.city = city;
        print(result.toString());

        // components.forEach((c) {
        //   final List type = c['types'];
        //   if (type.contains('locality')) {
        //     place.city = c['short_name'];
        //   }
        //   if (type.contains('administrative_area_level_1')) {
        //     place.city = c['short_name'];
        //   }
        //   if (type.contains('political')) {
        //     place.city = c['short_name'];
        //   }
        // });
        return place;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }
}

// Future<Place> getPlaceDetailFromId(String placeId) async {
//   // if you want to get the details of the selected place by place_id
// }

class Place {
  String? name;
  String? address;
  String? city;
  String? iconUrl;
  GeoPoint? geoPoint;

  Place({
    this.name,
    this.city,
    this.address,
    this.geoPoint,
    this.iconUrl,
  });

  bool validate() {
    return name != null &&
        address != null &&
        iconUrl != null &&
        geoPoint != null &&
        city != null;
  }
}
