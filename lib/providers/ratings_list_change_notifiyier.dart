import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

final ratingsStateNotifierProvider = StateNotifierProvider.autoDispose
    .family<RatingsListStateNotifier, List<Map<String, dynamic>>, String>(
        (ref, userId) {
  return RatingsListStateNotifier(ref.read, userId);
});

class RatingsListStateNotifier
    extends StateNotifier<List<Map<String, dynamic>>> {
  Reader read;
  static const _pageSize = 5;
  DocumentSnapshot? lastDocument; // fl// request
  List<Map<String, dynamic>> oldItems = [];

  final PagingController<int, Map<String, dynamic>> pagingController =
      PagingController(firstPageKey: 0);
  late String userId;
  RatingsListStateNotifier(this.read, String userId)
      : super(<Map<String, dynamic>>[]) {
    this.userId = userId;
    pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
  }

  Future<void> fetchPage(int pageKey) async {
    try {
      QuerySnapshot querySnapshot;
      if (lastDocument == null) {
        querySnapshot = await FirebaseFirestore.instance
            .collectionGroup('rates')
            .where("toUser.uid", isEqualTo: userId)
            .orderBy("time", descending: true)
            .limit(_pageSize)
            .get()
            .then((value) {
          return value;
        }).catchError((e) {
          print(e);
        });
        print('11');
      } else {
        querySnapshot = await FirebaseFirestore.instance
            .collectionGroup('rates')
            .where("toUser.uid", isEqualTo: userId)
            .orderBy("time", descending: true)
            .startAfterDocument(lastDocument!)
            .limit(_pageSize)
            .get();
      }
      if (querySnapshot.docs.isNotEmpty) lastDocument = querySnapshot.docs.last;

      List<Map<String, dynamic>> newItems = querySnapshot.docs.map((e) {
        final data = e.data() as Map<String, dynamic>;
        data.putIfAbsent('roomId', () => e.id);
        return data;
      }).toList();
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
      state = [...state, ...newItems];
    } catch (error) {
      pagingController.error = error;
    }
  }

  void refresh() {
    state = [];
    lastDocument = null;
    pagingController.refresh();
  }
}

class HomeTabList {
  List<DocumentSnapshot> gameRooms = []; // stores fetched products

  bool isLoading = false; // track if products fetching

  bool hasMore = true; // flag for more products available or not

  int documentLimit = 10; // documents to be fetched per
  DocumentSnapshot? lastDocument; // fl// request

}
