import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth_providers.dart';

final chatUnReadCountStateNotifierProvider =
    StateNotifierProvider<ChatsUnReadCountStateNotifier, AsyncValue<int>>(
        (ref) => ChatsUnReadCountStateNotifier(ref.read));

class ChatsUnReadCountStateNotifier extends StateNotifier<AsyncValue<int>> {
  Reader read;
  final Map<String, int> unreadCountMap = {};

  ChatsUnReadCountStateNotifier(
    this.read,
  ) : super(AsyncData(0)) {
    _init();
  }

  updateNewMessage1(String chatId) {
    if (unreadCountMap.containsKey(chatId)) {
      int count = unreadCountMap[chatId] ?? 0;
      unreadCountMap[chatId] = count + 1;
      DBManager.db.updateChatsUnReadTable(chatId, count);
    } else {
      unreadCountMap[chatId] = 1;
      DBManager.db.updateChatsUnReadTable(chatId, 1);
    }
    int unReadCount = 0;
    for (var value in unreadCountMap.values) {
      unReadCount += value;
      print(value);
    }
    state = AsyncData(unReadCount);
  }

  readItMessage(String chatId) {
    unreadCountMap[chatId] = 0;
    DBManager.db.updateChatsUnReadTable(chatId, 0);

    int unReadCount = 0;
    for (var value in unreadCountMap.values) {
      unReadCount += value;
    }
    state = AsyncData(unReadCount);
  }

  _init() async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw ('no user');
    Map<String, int> map = await DBManager.db.getChatsUnRead(myUser.id);
    unreadCountMap.addAll(map);

    int count = 0;
    for (var entry in unreadCountMap.entries) {
      count += entry.value;
    }
    state = AsyncData(count);
    // List<ChatMessage> chatMessages =
    //     await DBManager.db.readAllMessagesfromMessagesTable(chatId);
    // state = AsyncData([...state.data!.value, ...chatMessages]);
  }
}
