import 'package:boardgame/authentication_service.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/repository/user_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final firebaseAuthProvider = Provider<FirebaseAuth>((ref) {
  return FirebaseAuth.instance;
});

final authServiceProvider = Provider<AuthenticationService>((ref) {
  return AuthenticationService(ref.read(firebaseAuthProvider), ref.read);
});

// ignore: top_level_function_literal_block
final authStateProvider = StreamProvider((ref) {
  return ref.watch(authServiceProvider).authStateChanges;
});

// final userProvider=FutureProvider<MyUser>((ref)=> ref.read(userRepository).getUser(FirebaseAuth.instance.currentUser.uid));
// final userProvider = Provider((ref) =>
//     ref.read(userRepository).getUser(FirebaseAuth.instance.currentUser.uid));

final userStateProvider = StateProvider<MyUser?>((ref) => null);

final firebaseUserFutureProvider = FutureProvider<User?>((ref) async {
  final firebaseUser = await ref.watch(authStateProvider.last);
  if (firebaseUser != null) {
    MyUser? myUser = await ref
        .read(userRepository)
        .getUser(FirebaseAuth.instance.currentUser!.uid);
    ref.read(userStateProvider).state = myUser;
    return firebaseUser;
  } else {
    ref.read(userStateProvider).state = null;

    return null;
  }
});
