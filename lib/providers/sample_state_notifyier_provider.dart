import 'package:boardgame/models/feed_item.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/repository/feeds_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth_providers.dart';

final localFeedsStateNotifierProvider =
    StateNotifierProvider<LocalFeedsStateNotifier, AsyncValue<List<FeedItem>>>(
        (ref) => LocalFeedsStateNotifier(ref.read));

class LocalFeedsStateNotifier
    extends StateNotifier<AsyncValue<List<FeedItem>>> {
  Reader read;

  bool isLoading = false; // track if products fetching

  bool hasMore = true; // flag for more products available or not

  int docLimit = 11; // documents to be fetched per request
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  int messageOldestTime = 0;
  LocalFeedsStateNotifier(this.read) : super(AsyncData(<FeedItem>[])) {
    _init();
  }
  _init() {
    getMore();
  }

  getMore() async {
    List<FeedItem> tempFeedItems;
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('No user');

    if (!hasMore) {
      print('No More Products');
      return;
    }
    if (isLoading) {
      return;
    }
    isLoading = true;
    if (state.data!.value.isEmpty) {
      tempFeedItems = await read(feedsRepository)
          .getMoreLimitFeedsFromFeedsTable(myUser.id, docLimit, 0);
    } else {
      tempFeedItems = await read(feedsRepository)
          .getMoreLimitFeedsFromFeedsTable(
              myUser.id, docLimit, state.data!.value.last.time.millisecondsSinceEpoch);
    }
    if (tempFeedItems.length < docLimit) {
      hasMore = false;
    }
    if (tempFeedItems.isNotEmpty) {
      messageOldestTime = tempFeedItems.last.time.millisecondsSinceEpoch;
    }
    state = AsyncData([...state.data!.value, ...tempFeedItems]);
    // localMessages.addAll(tempFeedItems);
    isLoading = false;
  }
}
