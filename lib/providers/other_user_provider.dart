import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/repository/user_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final otherUsersStateProvider = StateProvider<Map<String, MyUser>>((ref) => {});
final otherUsersFutureProvider =
    FutureProvider.family<MyUser?, String>((ref, userId) async {
  if (ref.read(otherUsersStateProvider).state.containsKey(userId)) {
    return ref.read(otherUsersStateProvider).state[userId];
  }

  // ref.watch(myMessagesProvider());
  MyUser? myUser = await ref.read(userRepository).getUser(userId);
  if (myUser != null) {
    ref.read(otherUsersStateProvider).state[myUser.id] = myUser;
  }

  return myUser;
});
