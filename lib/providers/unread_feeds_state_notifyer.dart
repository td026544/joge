import 'package:flutter_riverpod/flutter_riverpod.dart';

final haveNewFeedsStateNotifierProvider =
    StateNotifierProvider<UnreadFeedsStateNotifier, bool>(
        (ref) => UnreadFeedsStateNotifier(ref.read));

class UnreadFeedsStateNotifier extends StateNotifier<bool> {
  final Reader read;

  int remoteMessageCount = 0;
  int readCount = 0;

  UnreadFeedsStateNotifier(this.read) : super(true);
  updateHaveUnread() {
    state = true;
  }

  updateLeftOnRead() {
    state = false;
  }
  // updateRemoteFeedCount(int count) {
  //   remoteMessageCount = count;
  //   state = remoteMessageCount - readCount<0;
  // }
  //
  // updateFeedReadCount(int count) {
  //   readCount = count;
  //   state = remoteMessageCount - readCount;
  // }
}
