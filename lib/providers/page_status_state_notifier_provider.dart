import 'package:boardgame/managers/mqtt_manager.dart';
import 'package:boardgame/models/chat_message.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/screen/game_room_detail_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth_providers.dart';

final roomDocStateNotifierProvider = StateNotifierProvider.family.autoDispose<
    RoomDocStateNotifier,
    AsyncValue<RoomDetail>,
    String>((ref, chatId) => RoomDocStateNotifier(ref, chatId));

class RoomDocStateNotifier
    extends StateNotifier<AsyncValue<RoomDetail>> {
  AutoDisposeProviderRefBase ref;

  bool hasMore = true; // flag for more products available or not

  int docLimit = 40; // documents to be fetched per request
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  int messageOldestTime = 0;
  List<ChatMessage> oldItems = [];

  final toId;
  RoomDocStateNotifier(
    this.ref,
    this.toId,
  ) : super(AsyncValue.loading()) {
    fetch();
  }
  fetch() async {
    MyUser? user = ref.read(userStateProvider).state;
    if (user == null) throw Exception('noUser');
    state = AsyncValue.loading();
    if (state == AsyncValue.loading()) return;
    RoomDetail roomDetail =
        await ref.watch(roomDetailStreamProvider(toId).last);
    try{
      state = AsyncData(roomDetail);
      Map<String, dynamic> attendeesData =
      state.value.attendeesData;
      if (!attendeesData.containsKey(user.id)) {
        ref.read(mqttProvider).unSubscribeTopic(toId);
      }
    }catch(e){
      state = AsyncError(e.toString());

    }

  }

  String getUserNickName(String uid) {
    if (state.value.attendeesData.containsKey(uid)) {
      String nickname = state.value.attendeesData[uid]['nickname'];
      return nickname;
    } else {
      return '';
    }
  }
}
