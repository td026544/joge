import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final isHaveNewMessageChangeNotifier =
    ChangeNotifierProvider<MessageCountChangeNotifier>((ref) {
  return MessageCountChangeNotifier(ref.read);
});

class MessageCountChangeNotifier extends ChangeNotifier {
  final Reader read;

  late Map<String, bool> haveNewMessageMap = Map();
  late Map<String, int> readMessageCountMap = Map();
  int remoteMessageCount = 0;
  int readCount = 0;
  int unReadCount = 0;
  bool isHaveNewMessage = false;

  MessageCountChangeNotifier(this.read);

  void updateHaveUnread(String roomId, bool haveNewMessage) {
    if (haveNewMessageMap.containsKey(roomId)) {
      haveNewMessageMap[roomId] = haveNewMessage;
    } else {
      haveNewMessageMap = {...haveNewMessageMap, roomId: haveNewMessage};
    }

    haveNewMessageMap.forEach((key, value) {
      if (value == true) {
        isHaveNewMessage = value;
        notifyListeners();
        return;
      }
    });
    notifyListeners();
  }

  updateLeftOnRead() {
    isHaveNewMessage = false;
  }
}
