import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/screen/message_center_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final rawFriendsStreamProvider =
    StreamProvider.autoDispose<Map<String, dynamic>>((ref) {
  MyUser? user = ref.read(userStateProvider).state;
  if (user == null) throw Exception('no User');

  return FirebaseFirestore.instance
      .collection('friends')
      .doc(user.id)
      .snapshots()
      .map((friends) {
    if (friends.exists) {
      Map<String, dynamic> users = friends.data()!['users'];
      return users;
    } else {
      return <String, dynamic>{};
    }
  });
});

final inviteFriendsFutureProvider =
    FutureProvider.autoDispose<List<FriendDataDoc>>((ref) async {
  final users = await ref.watch(rawFriendsStreamProvider.last);
  List<FriendDataDoc> friendDataDocs = users.entries
      .map((entry) => FriendDataDoc(entry.key, entry.value))
      .toList();
  List<FriendDataDoc> invitedFriends = friendDataDocs
      .where((element) =>
          EnumToString.fromString(FriendsStatus.values, element.status) ==
          FriendsStatus.beInvited)
      .toList();
  return invitedFriends;
});
final myFriendsFutureProvider =
    FutureProvider.autoDispose<List<FriendDataDoc>>((ref) async {
  final users = await ref.watch(rawFriendsStreamProvider.last);
  List<FriendDataDoc> friendDataDocs = users.entries
      .map((entry) => FriendDataDoc(entry.key, entry.value))
      .toList();
  List<FriendDataDoc> invitedFriends = friendDataDocs
      .where((element) =>
          EnumToString.fromString(FriendsStatus.values, element.status) ==
          FriendsStatus.match)
      .toList();
  return invitedFriends;
});
