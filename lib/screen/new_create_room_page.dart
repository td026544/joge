import 'dart:typed_data';

import 'package:boardgame/components/category_bottom_sheet.dart';
import 'package:boardgame/models/create_room_save_model.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/address_search.dart';
import 'package:boardgame/providers/place_service_v1.dart';
import 'package:boardgame/usecase/room_usecase.dart';
import 'package:boardgame/utils/enum_to_string.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../controller/create_room_page_view_controller.dart';
import 'home_page.dart';
import 'image_editor_page.dart';

class NewCreateRoomPage extends ConsumerStatefulWidget {
  static const String id = '/new_create_room_page';
  final CreateRoomSaveModel createRoomSaveModel;
  final bool isEditMode;
  NewCreateRoomPage(
      {required this.createRoomSaveModel, this.isEditMode = false});

  @override
  _NewCreateRoomPageState createState() => _NewCreateRoomPageState();
}

class _NewCreateRoomPageState extends ConsumerState<NewCreateRoomPage> {
  StepperType stepperType = StepperType.vertical;
  final infoKey = GlobalKey();
  final titleKey = GlobalKey();
  final step1FormKey = GlobalKey<FormState>();
  final titleFocusNode = FocusNode();
  bool isLoaded = false;
  late final CreateRoomSaveModel createRoomSaveModel;

  @override
  void initState() {
    this.createRoomSaveModel = widget.createRoomSaveModel;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!isLoaded) {
      if (!createRoomSaveModel.isEmpty()) {
        ref
            .read(createRoomPageViewController)
            .loadModel(createRoomSaveModel, widget.isEditMode);

        isLoaded = true;
      }
    }
  }

  validateForm(dynamic value) {
    if (value is int) {
      return null;
    }
    if (value == null || value.isEmpty) {
      return '*必填';
    } else {
      return null;
    }
  }

  void dismissRoom(String roomId) async {
    try {
      List<String> userList =
          await ref.read(roomUseCaseProvider).dismissRoom(roomId);
      // SnackBa
      Navigator.popAndPushNamed(context, HomeScreen.id);

      Navigator.popUntil(context, ModalRoute.withName(HomeScreen.id));
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(userList[0]),
        ),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('發生問題，請稍後在試'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final controller = ref.watch(createRoomPageViewController);

    return AbsorbPointer(
      absorbing: controller.isLoading,
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          // appBar: AppBar(
          //   title: Text('開局'),
          //   centerTitle: true,
          // ),
          body: Flex(
            direction: Axis.vertical,
            children: [
              Expanded(
                child: CustomScrollView(
                  slivers: [
                    SliverAppBar(
                      title: Text(
                        '開局',
                        style: TextStyle(color: Colors.white),
                      ),
                      actions: [
                        Offstage(
                          offstage: widget.isEditMode,
                          child: IconButton(
                            onPressed: () {
                              controller.reset();
                            },
                            icon: Icon(Icons.delete),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            controller.save(createRoomSaveModel.roomId);
                          },
                          icon: Icon(Icons.save),
                        )
                      ],
                    ),
                    SliverToBoxAdapter(
                      child: Stepper(
                        physics: ScrollPhysics(),
                        currentStep: controller.currentStep,
                        onStepTapped: (step) => controller.tapped(step),
                        onStepContinue: () async {
                          controller.continued(context, controller.currentStep);
                        },
                        onStepCancel: controller.cancel,
                        controlsBuilder: (BuildContext context,
                            {VoidCallback? onStepContinue,
                            VoidCallback? onStepCancel}) {
                          if (controller.currentStep == 3)
                            return SizedBox(
                              height: 4,
                            );
                          return Row(
                            children: <Widget>[
                              controller.getStepState(controller.currentStep) ==
                                      StepState.complete
                                  ? ElevatedButton(
                                      onPressed: () {
                                        if (controller.currentStep == 2) {
                                          titleFocusNode.requestFocus();
                                        }
                                        onStepContinue!();
                                      },
                                      child: Text(
                                        '完成',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    )
                                  : OutlinedButton(
                                      onPressed: () {
                                        if (controller.currentStep == 2) {
                                          titleFocusNode.requestFocus();
                                        }
                                        onStepContinue!();
                                      },
                                      child: Text(
                                        '下一個',
                                      ),
                                    ),
                              SizedBox(
                                width: 16,
                              ),
                              TextButton(
                                onPressed: onStepCancel,
                                child: Text(
                                  '取消',
                                ),
                              ),
                            ],
                          );
                        },
                        steps: <Step>[
                          Step(
                              title: new Text('時間地點'),
                              content: Form(
                                key: controller.formKeys[0],
                                onChanged: () {
                                  controller.stepFormFieldOnChange(
                                      0, controller.checkStep0);
                                },
                                child: Column(
                                  children: <Widget>[
                                    Offstage(
                                      offstage: widget.isEditMode,
                                      child: Column(
                                        children: [
                                          RadioListTile<bool>(
                                            title: const Text('實體活動'),
                                            value: false,
                                            groupValue: controller.isOnline,
                                            onChanged: (bool? value) {
                                              setState(() {
                                                controller.isOnline =
                                                    value ?? true;
                                              });
                                            },
                                          ),
                                          RadioListTile<bool>(
                                            title: const Text('線上活動'),
                                            value: true,
                                            groupValue: controller.isOnline,
                                            onChanged: (bool? value) {
                                              setState(() {
                                                controller.isOnline =
                                                    value ?? false;
                                              });
                                            },
                                          ),
                                          SizedBox(
                                            height: 16,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Visibility(
                                        child: TextFormField(
                                          controller:
                                              controller.placeEditingController,
                                          validator: (value) =>
                                              controller.validationTime(value),
                                          readOnly: true,
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.room,
                                            ),
                                            filled: true,
                                            labelText: '地點',
                                            // errorStyle: TextStyle(color: Colors.white),
                                          ),
                                          onTap: () async {
                                            // const kGoogleApiKey =
                                            //     "AIzaSyC5EU1AUOuJDzOpS2y8KGzN2hHjSVGFugM";
                                            final sessionToken = Uuid().v4();

                                            final result = await showSearch(
                                              context: context,
                                              delegate:
                                                  AddressSearch(sessionToken),
                                            );
                                            // This will change the text displayed in the TextField
                                            if (result != null) {
                                              Suggestion suggestion = result;
                                              final placeDetails =
                                                  await PlaceApiProvider(
                                                          sessionToken)
                                                      .getPlaceDetailFromId(
                                                          result.placeId,
                                                          result.city);
                                              print(result.placeId);
                                              setState(() {
                                                // controller.placeEditingController.text =
                                                //     result.description;
                                                controller
                                                        .placeEditingController
                                                        .text =
                                                    placeDetails.address ??
                                                        result.description;
                                                controller.place = placeDetails;
                                                print(result.toString());
                                              });
                                            }
                                          },
                                        ),
                                        visible: !controller.isOnline),
                                    SizedBox(
                                      height: 16,
                                    ),
                                    TextFormField(
                                      controller:
                                          controller.timeEditingController,
                                      readOnly: true,
                                      validator: (value) => validateForm(value),
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(
                                          Icons.today,
                                        ),
                                        filled: true,
                                        labelText: '日期',
                                      ),
                                      onTap: () {
                                        var dateNow = DateTime.now();

                                        showModalBottomSheet(
                                          context: context,
                                          builder: (context) {
                                            return Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Text(
                                                        '取消',
                                                      ),
                                                    ),
                                                    TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                        controller
                                                            .confirmSelectedTime();
                                                      },
                                                      child: Text(
                                                        '確認',
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  height: MediaQuery.of(context)
                                                          .copyWith()
                                                          .size
                                                          .height /
                                                      3,
                                                  child: CupertinoDatePicker(
                                                      maximumDate: dateNow.add(
                                                          const Duration(
                                                              days: 30)),
                                                      minimumDate: dateNow,
                                                      minuteInterval: 10,
                                                      initialDateTime: controller
                                                                  .startTime !=
                                                              null
                                                          ? controller.startTime
                                                          : dateNow.add(
                                                              Duration(
                                                                  minutes: 10 -
                                                                      DateTime.now()
                                                                              .minute %
                                                                          10),
                                                            ),
                                                      mode: CupertinoDatePickerMode
                                                          .dateAndTime, //这里改模式

                                                      onDateTimeChanged:
                                                          (dateTime) {
                                                        controller
                                                                .pickerSelectTime =
                                                            dateTime;
                                                      }),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                    ),
                                    SizedBox(
                                      height: 22,
                                    ),
                                  ],
                                ),
                              ),
                              isActive: controller.currentStep >= 0,
                              state: controller.stepStates[0]),
                          Step(
                              title: new Text('人數、類別'),
                              content: Form(
                                key: controller.formKeys[1],
                                onChanged: () {
                                  controller.stepFormFieldOnChange(
                                      1, controller.checkStep1);
                                },
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Offstage(
                                          offstage: widget.isEditMode,
                                          child: Column(
                                            children: [
                                              RadioListTile<bool>(
                                                title: const Text('快速開局'),
                                                subtitle: Text('用戶可直接加入'),
                                                value: false,
                                                groupValue: controller.isOnline,
                                                onChanged: (bool? value) {
                                                  setState(() {
                                                    controller.isOnline =
                                                        value ?? true;
                                                  });
                                                },
                                              ),
                                              RadioListTile<bool>(
                                                title: const Text('審核開局'),
                                                subtitle: Text('需要手動審核加入用戶'),
                                                value: true,
                                                groupValue: controller.isOnline,
                                                onChanged: (bool? value) {
                                                  setState(() {
                                                    controller.isOnline =
                                                        value ?? false;
                                                  });
                                                },
                                              ),
                                              SizedBox(
                                                height: 16,
                                              ),
                                            ],
                                          )),
                                      Container(
                                        width: 210,
                                        child: TextFormField(
                                          controller: controller
                                              .categoryEditingController,
                                          readOnly: true,
                                          validator: (value) =>
                                              validateForm(value),
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.category,
                                            ),
                                            filled: true,
                                            labelText: '類別',
                                            suffixIcon:
                                                Icon(Icons.arrow_drop_down),
                                          ),
                                          onTap: () {
                                            showModalBottomSheet(
                                              isScrollControlled: true,
                                              context: context,
                                              builder: (context) {
                                                return CategoryBottomSheet(
                                                  onSelected: (categoryType) {
                                                    String category =
                                                        GetNameUtil
                                                            .getCategoryName(
                                                                categoryType);
                                                    controller.categoryType =
                                                        categoryType;
                                                    controller
                                                        .categoryEditingController
                                                        .text = category;
                                                    Navigator.pop(context);
                                                  },
                                                );
                                              },
                                            );
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        height: 16,
                                      ),
                                      Container(
                                        width: 210,
                                        child: TextFormField(
                                          controller:
                                              controller.needNumbersController,
                                          readOnly: true,
                                          validator: (value) =>
                                              validateForm(value),
                                          decoration: InputDecoration(
                                              prefixIcon: Icon(
                                                Icons.person,
                                              ),
                                              filled: true,
                                              labelText: '邀請人數'
                                                  '',
                                              suffixIcon:
                                                  Icon(Icons.arrow_drop_down)),
                                          onTap: () {
                                            showModalBottomSheet(
                                              context: context,
                                              backgroundColor: Colors.white,
                                              builder: (BuildContext context) {
                                                return Flex(
                                                  direction: Axis.vertical,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        TextButton(
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          child: Text('取消'),
                                                        ),
                                                        TextButton(
                                                          onPressed: () {
                                                            int selectedNeedNumber =
                                                                controller
                                                                        .pickerSelectedNeedNumbers +
                                                                    1;
                                                            controller
                                                                    .needNumbersController
                                                                    .text =
                                                                selectedNeedNumber
                                                                    .toString();

                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          child: Text('確認'),
                                                        )
                                                      ],
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                    ),
                                                    Expanded(
                                                      child: CupertinoPicker(
                                                        useMagnifier: true,
                                                        backgroundColor:
                                                            Colors.white,
                                                        onSelectedItemChanged:
                                                            (value) {
                                                          controller
                                                                  .pickerSelectedNeedNumbers =
                                                              value;
                                                        },
                                                        scrollController:
                                                            FixedExtentScrollController(
                                                                initialItem:
                                                                    controller
                                                                        .pickerSelectedNeedNumbers),
                                                        itemExtent: 60.0,
                                                        children: controller
                                                            .needNumbersItems
                                                            .map(
                                                              (label) => Center(
                                                                child: Text(
                                                                  label
                                                                      .toString(),
                                                                ),
                                                              ),
                                                            )
                                                            .toList(),
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              },
                                            );
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        height: 16,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              isActive: controller.currentStep >= 1,
                              state: controller.stepStates[1]),
                          Step(
                              title: new Text('封面'),
                              content: Form(
                                key: controller.formKeys[2],
                                child: FormField(
                                  builder: (state) {
                                    return Align(
                                      alignment: Alignment.centerLeft,
                                      child: Column(
                                        children: <Widget>[
                                          InkWell(
                                            child: SizedBox(
                                                height: 135,
                                                width: 240,
                                                child: controller.imageByte !=
                                                        null
                                                    ? Image.memory(
                                                        controller.imageByte!,
                                                        height: 135,
                                                        width: 240,
                                                        fit: BoxFit.fitWidth,
                                                      )
                                                    : widget.isEditMode
                                                        ? CachedNetworkImage(
                                                            imageUrl: URLGetter
                                                                .getGameRoomCoverUrl(widget
                                                                    .createRoomSaveModel
                                                                    .roomId!),
                                                            fit:
                                                                BoxFit.fitWidth,
                                                            placeholder: (context,
                                                                    url) =>
                                                                CircularProgressIndicator(),
                                                            errorWidget:
                                                                (context, url,
                                                                        error) =>
                                                                    Icon(Icons
                                                                        .error),
                                                          )
                                                        : DottedBorder(
                                                            borderType:
                                                                BorderType
                                                                    .RRect,
                                                            radius:
                                                                Radius.circular(
                                                                    12),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    6),
                                                            dashPattern: [
                                                              16,
                                                              4
                                                            ],
                                                            color: Colors.grey,
                                                            strokeWidth: 2,
                                                            child: Center(
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Icon(
                                                                    Icons
                                                                        .add_photo_alternate_outlined,
                                                                    color: Colors
                                                                        .grey,
                                                                    size: 80,
                                                                  ),
                                                                  Text(
                                                                    '新增照片',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey),
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                          )),
                                            onTap: () async {
                                              var result =
                                                  await Navigator.pushNamed(
                                                context,
                                                ImageEditorPage.id,
                                              );
                                              setState(() {
                                                controller.imageByte =
                                                    result as Uint8List;
                                                controller
                                                    .stepFormFieldOnChange(2,
                                                        controller.checkStep2);
                                              });
                                            },
                                          ),
                                          SizedBox(
                                            height: 16,
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                  validator: (string) {
                                    if (controller.imageByte != null &&
                                        controller.imageByte!.isNotEmpty) {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                              isActive: controller.currentStep >= 2,
                              state: controller.stepStates[2]),
                          Step(
                              title: new Text('介紹'),
                              content: Form(
                                key: controller.formKeys[3],
                                onChanged: () {
                                  controller.stepFormFieldOnChange(
                                      3, controller.checkStep3);
                                },
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      key: titleKey,
                                      padding:
                                          EdgeInsets.symmetric(vertical: 16),
                                      child: TextFormField(
                                        focusNode: titleFocusNode,
                                        textInputAction: TextInputAction.next,
                                        maxLength: 12,
                                        validator: (value) =>
                                            validateForm(value),
                                        controller:
                                            controller.titleEditingController,
                                        decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          helperText: '*必填',
                                          labelText: '標題',
                                          errorStyle: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                        onTap: () {
                                          Scrollable.ensureVisible(
                                              titleKey.currentContext!);
                                        },
                                        onFieldSubmitted: (s) {
                                          FocusScope.of(context).nextFocus();
                                        },
                                      ),
                                    ),
                                    Padding(
                                      key: infoKey,
                                      padding:
                                          EdgeInsets.only(top: 16, bottom: 140),
                                      child: TextFormField(
                                        textInputAction:
                                            TextInputAction.newline,
                                        validator: (value) =>
                                            validateForm(value),
                                        controller:
                                            controller.infoEditingController,
                                        // enabled: true,
                                        keyboardType: TextInputType.multiline,
                                        maxLines: null,
                                        minLines: 6,
                                        maxLength: 1000,
                                        // expands: true,
                                        decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: '介紹',
                                          helperText: '*必填',
                                          errorStyle: TextStyle(
                                              color: Colors.deepOrange),
                                        ),
                                        onTap: () {
                                          Scrollable.ensureVisible(
                                              infoKey.currentContext!);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              isActive: controller.currentStep >= 3,
                              state: controller.stepStates[3]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Material(
                elevation: 8,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Align(
                      child: Offstage(
                        offstage: !widget.isEditMode,
                        child: OutlinedButton.icon(
                          onPressed: () {
                            dismissRoom(widget.createRoomSaveModel.roomId!);
                          },
                          icon: Icon(Icons.clear),
                          label: Text('解散房間'),
                          style: ElevatedButton.styleFrom(
                            fixedSize: Size(140, 48),
                          ),
                        ),
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 16),
                      child: widget.isEditMode
                          ? ElevatedButton(
                              onPressed: controller.readyForCreate
                                  ? () => controller.update(context,
                                      widget.createRoomSaveModel.roomId!)
                                  : null,
                              child: controller.isLoading
                                  ? SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator(
                                        color: Colors.white,
                                      ),
                                    )
                                  : Text(
                                      '確認',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18),
                                    ),
                              style: ElevatedButton.styleFrom(
                                fixedSize: Size(120, 48),
                              ),
                            )
                          : ElevatedButton(
                              onPressed: controller.readyForCreate
                                  ? () => controller.create(context)
                                  : null,
                              child: controller.isLoading
                                  ? SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator(
                                        color: Colors.white,
                                      ),
                                    )
                                  : Text(
                                      '開局',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18),
                                    ),
                              style: ElevatedButton.styleFrom(
                                fixedSize: Size(120, 48),
                              ),
                            ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
