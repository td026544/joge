import 'package:boardgame/components/lobby_room_card.dart';
import 'package:boardgame/repository/history_room_change_notifiyer_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class HistoryAttendRooms extends ConsumerStatefulWidget {
  const HistoryAttendRooms({Key? key}) : super(key: key);

  @override
  _MyAttendRoomsState createState() => _MyAttendRoomsState();
}

class _MyAttendRoomsState extends ConsumerState<HistoryAttendRooms> {
  late ScrollController _scrollController;
  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.30;
      if (maxScroll - currentScroll <= delta) {
        ref.read(historyRoomChangeNotifierProvider.notifier).fetch();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        final asyncData = ref.watch(historyRoomChangeNotifierProvider);
        return asyncData.when(
          data: (maps) {
            return ListView.builder(
              controller: _scrollController,
              itemCount: maps.length,
              itemBuilder: (context, index) => LobbyRoomCard(
                data: maps[index],
              ),
            );
          },
          loading: (data) => CircularProgressIndicator(),
          error: (data, e, s) => Text(
            e.toString(),
          ),
        );
      },
    );
  }
}
