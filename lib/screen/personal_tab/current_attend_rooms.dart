import 'package:boardgame/repository/current_room_change_notifiyer_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CurrentAttendRooms extends ConsumerStatefulWidget {
  const CurrentAttendRooms({Key? key}) : super(key: key);

  @override
  _MyAttendRoomsState createState() => _MyAttendRoomsState();
}

class _MyAttendRoomsState extends ConsumerState<CurrentAttendRooms> {
  late ScrollController _scrollController;
  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.30;
      if (maxScroll - currentScroll <= delta) {
        ref.read(myGameRoomStateNotifierProvider.notifier).fetch();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        final asyncData = ref.watch(myGameRoomStateNotifierProvider);
        return asyncData.when(
          data: (maps) {
            return ListView.builder(
              controller: _scrollController,
              itemCount: maps.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Colors.black87,
                  width: 344,
                  height: 344,
                ),
              ),
            );
          },
          loading: (data) => CircularProgressIndicator(),
          error: (data, e, s) => Text(
            e.toString(),
          ),
        );
      },
    );
  }
}
