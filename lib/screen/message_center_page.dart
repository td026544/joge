import 'package:badges/badges.dart';
import 'package:boardgame/arguments/chat_screen_arguments.dart';
import 'package:boardgame/components/friend_card.dart';
import 'package:boardgame/components/indicator.dart';
import 'package:boardgame/models/conversation.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/chat_messages_state_notifyier_provider.dart';
import 'package:boardgame/providers/conversations_cards_provider.dart';
import 'package:boardgame/providers/friends_provider.dart';
import 'package:boardgame/providers/message_notify_provider.dart';
import 'package:boardgame/screen/chat_personal_page.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:boardgame/utils/text_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'chat_screen.dart';

class MessageCenterScreen extends ConsumerStatefulWidget {
  @override
  _MessageCenterScreenState createState() => _MessageCenterScreenState();
}

class _MessageCenterScreenState extends ConsumerState<MessageCenterScreen> {
  Widget getSubTile(MsgType? msgType, Conversation conversation) {
    switch (msgType) {
      case MsgType.text:
        return Text(
          TextUtil.jsonUf8ToString(conversation.msg ?? ""),
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
        );
      case MsgType.image:
        return Text('[圖片]');
      case MsgType.service:
        return Text(
          TextUtil.jsonUf8ToString(conversation.msg ?? ""),
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
        );
      case MsgType.typing:
        return Consumer(
          builder: (context, ref, child) {
            final isTyping = ref
                .watch(typingStateStateNotifierProvider(conversation.chatId))
                .state;
            return Text('typing');
            // return CardTypingIndicator(
            //   showIndicator: isTyping,
            // );
          },
        );
      case MsgType.none:
        return Text('');
      default:
        return Text('');
    }
  }

  void deleteConversation(String chatId, int index) {
    ref.read(conversationsStateNotifierProvider.notifier).deleteItem(index);
    ref.read(chatUseCaseProvider).deleteLocalDbConversation(chatId);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            toolbarHeight: 48,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            title: Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black38,
                    width: 1.0,
                  ),
                ),
              ),
              child: TabBar(
                labelColor: Colors.orange,
                unselectedLabelColor: Colors.black38,
                indicatorColor: Colors.orange,
                indicator: MD2Indicator(),
                tabs: [
                  Tab(
                    text: '聊天室',
                  ),
                  Tab(
                    text: '好友',
                  ),
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: [
              Consumer(
                builder: (context, ref, child) {
                  final conversations =
                      ref.watch(conversationsStateNotifierProvider);
                  return conversations.when(
                      data: (conversations) {
                        conversations.sort((a, b) => b.time.compareTo(a.time));

                        return ListView.separated(
                          itemCount: conversations.length,
                          itemBuilder: (context, index) {
                            Conversation conversation = conversations[index];
                            bool isRoomChat = conversation.chatType ==
                                EnumToString.convertToString(ChatType.room);

                            return Slidable(
                              child: ListTile(
                                minVerticalPadding: 16,
                                leading: Badge(
                                  elevation: 0,
                                  badgeColor:
                                      Theme.of(context).primaryColorLight,
                                  toAnimate: false,
                                  position: BadgePosition.bottomEnd(),
                                  showBadge: isRoomChat,
                                  badgeContent: Icon(
                                    Icons.group,
                                    size: 18,
                                    color: Colors.orange,
                                  ),
                                  child: ClipOval(
                                    child: CachedNetworkImage(
                                      imageUrl: isRoomChat
                                          ? URLGetter.getGameRoomCoverUrl(
                                              conversation.chatId)
                                          : URLGetter.getAvatarUrl(
                                              conversation.toId),
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      width: 48.0,
                                      height: 48.0,
                                    ),
                                  ),
                                ),
                                title: Text(conversation.sender),
                                subtitle: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: getSubTile(
                                      EnumToString.fromString(
                                          MsgType.values, conversation.msgType),
                                      conversation),
                                ),
                                trailing: Consumer(
                                  builder: (context, ref, child) {
                                    final totalUnReadCount = ref.watch(
                                        chatUnReadCountStateNotifierProvider);
                                    final unReadCount = ref
                                            .watch(
                                                chatUnReadCountStateNotifierProvider
                                                    .notifier)
                                            .unreadCountMap[conversation.chatId] ??
                                        0;
                                    return Offstage(
                                      offstage: unReadCount == 0,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 12),
                                        child: SizedBox(
                                          width: 24,
                                          height: 24,
                                          child: Material(
                                            type: MaterialType.circle,
                                            color: Colors.redAccent,
                                            elevation: 2.0,
                                            child: Center(
                                              child: Text(
                                                unReadCount.toString(),
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                onTap: () async {
                                  ref
                                      .read(chatUnReadCountStateNotifierProvider
                                          .notifier)
                                      .readItMessage(conversation.chatId);
                                  switch (EnumToString.fromString(
                                      ChatType.values, conversation.chatType)) {
                                    case ChatType.room:
                                      await Navigator.pushNamed(
                                        context,
                                        ChatScreen.id,
                                        arguments: ChatScreenArguments(
                                            conversation: conversation),
                                      );
                                      break;
                                    case ChatType.user:
                                      await Navigator.pushNamed(
                                        context,
                                        ChatPersonalScreen.id,
                                        arguments: ChatScreenArguments(
                                            conversation: conversation),
                                      );
                                      break;
                                    default:
                                  }

                                  ref
                                      .read(chatUnReadCountStateNotifierProvider
                                          .notifier)
                                      .readItMessage(conversation.chatId);
                                },
                              ),
                              endActionPane: ActionPane(
                                motion: ScrollMotion(),
                                children: [
                                  SlidableAction(
                                    onPressed: (context) => deleteConversation(
                                        conversation.chatId, index),
                                    backgroundColor: Colors.red,
                                    foregroundColor: Colors.white,
                                    icon: Icons.delete,
                                    label: '刪除',
                                    flex: 1,
                                  ),
                                ],
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) =>
                              Divider(),
                        );
                      },
                      loading: (data) => CircularProgressIndicator(),
                      error: (data, e, s) => Text('erorr'));
                },
              ),
              Consumer(
                builder: (context, ref, child) {
                  MyUser? user = ref.watch(userStateProvider).state;
                  if (user == null) return Text('No user');
                  final friendsAsyncValue = ref.watch(myFriendsFutureProvider);

                  // final myChatRoomsChangeNotifier = watch(myChatRoomChangeNotifier);
                  // final chatRooms = watch(myChatRooms).state;
                  return friendsAsyncValue.when(
                    loading: (data) => CircularProgressIndicator(),
                    error: (data, error, stack) => Text(error.toString()),
                    data: (friendsData) {
                      return ListView.separated(
                          itemBuilder: (context, index) {
                            FriendDataDoc friendData = friendsData[index];
                            return FriendCard(
                              doc: friendData,
                              ableToNavChat: true,
                            );
                          },
                          separatorBuilder: (context, index) => Divider(),
                          itemCount: friendsData.length);
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FriendDataDoc {
  final String uid;
  final String status;

  FriendDataDoc(this.uid, this.status);
}
