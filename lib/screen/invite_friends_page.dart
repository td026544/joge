import 'package:boardgame/components/friend_card.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/friends_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class InviteFriendsPage extends ConsumerStatefulWidget {
  static final String id = "invite_friend";
  const InviteFriendsPage({Key? key}) : super(key: key);

  @override
  _InviteFriendsPageState createState() => _InviteFriendsPageState();
}

class _InviteFriendsPageState extends ConsumerState<InviteFriendsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            '好友邀請',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Consumer(
          builder: (context, ref, child) {
            MyUser? user = ref.watch(userStateProvider).state;
            if (user == null) return Text('No user');
            final friendsAsyncValue = ref.watch(inviteFriendsFutureProvider);

            // final myChatRoomsChangeNotifier = watch(myChatRoomChangeNotifier);
            // final chatRooms = watch(myChatRooms).state;
            return friendsAsyncValue.when(
              loading: (data) => CircularProgressIndicator(),
              error: (data, error, stack) => Text(error.toString()),
              data: (friendsMaps) {
                return ListView.separated(
                    itemBuilder: (context, index) {
                      return FriendCard(
                        doc: friendsMaps[index],
                        ableToNavChat: false,
                      );
                    },
                    separatorBuilder: (context, index) => Divider(),
                    itemCount: friendsMaps.length);
              },
            );
          },
        ));
  }
}
