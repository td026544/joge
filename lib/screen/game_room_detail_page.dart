import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:boardgame/arguments/chat_screen_arguments.dart';
import 'package:boardgame/arguments/game_room_detail_arguments.dart';
import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/arguments/rating_room_arguments.dart';
import 'package:boardgame/components/comment_list_tile.dart';
import 'package:boardgame/controller/game_room_detail_view_controller.dart';
import 'package:boardgame/models/conversation.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/rate_data.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/comments_provider.dart';
import 'package:boardgame/providers/conversations_cards_provider.dart';
import 'package:boardgame/repository/ratings_repository.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:boardgame/screen/rating_page.dart';
import 'package:boardgame/screen/room_not_found_page.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:boardgame/usecase/room_usecase.dart';
import 'package:boardgame/utils/text_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'chat_screen.dart';
import 'manage_room_page.dart';

final roomDetailStreamProvider = StreamProvider.family
    .autoDispose<RoomDetail,String>((ref, roomId) =>
        FirebaseFirestore.instance.collection('rooms').doc(roomId).snapshots().map((event) => RoomDetail.fromJson(event.data() as Map<String,dynamic>)));

class UserComment {
  final String id;
  final String text;
  final int time;

  UserComment(this.id, this.text, this.time);
}

class GameRoomDetailScreen extends ConsumerStatefulWidget {
  static final id = "game_room_detail_screen";
  final String roomId;

  const GameRoomDetailScreen({Key? key, required this.roomId})
      : super(key: key);

  @override
  _GameRoomDetailScreenState createState() => _GameRoomDetailScreenState();
}

class _GameRoomDetailScreenState extends ConsumerState<GameRoomDetailScreen> {
  int likeCount = 0;
  bool isSendAble = false;
  final double appbarHeight = 66;
  final commentFocusNode = FocusNode();
  final commentKey = new GlobalKey();
  final commentTopKey = new GlobalKey();

  @override
  void initState() {
    ref.read(gameRoomDetailViewController).init(widget.roomId);
    timeago.setLocaleMessages('zh', timeago.ZhMessages());
    likeCount = 0;
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  attend() async {
    try {
      ref.read(roomUseCaseProvider).attend(widget.roomId);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('加入失敗，請稍後重試'),
        ),
      );
    }
  }

  submitAudit() async {
    try {
      ref.read(roomUseCaseProvider).submitAutit(widget.roomId);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('申請失敗，請稍後重試'),
        ),
      );
    }
  }

  cancelAudit() async {
    try {
      ref.read(roomUseCaseProvider).cancelAudit(widget.roomId);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('申請失敗，請稍後重試'),
        ),
      );
    }
  }

  GoogleMap buildMap(
    GeoPoint geoPoint,
    String placeName,
    String address,
  ) {
    final CameraPosition kGooglePlex = CameraPosition(
      target: LatLng(geoPoint.latitude, geoPoint.longitude),
      zoom: 16.0,
    );
    List<Marker> _markers = <Marker>[];
    _markers.add(
      Marker(
        markerId: MarkerId('SomeId'),
        position: LatLng(geoPoint.latitude, geoPoint.longitude),
        infoWindow: InfoWindow(title: placeName, snippet: address),
      ),
    );
    return GoogleMap(
      initialCameraPosition: kGooglePlex,
      markers: Set<Marker>.of(_markers),
      onMapCreated: (GoogleMapController controller) {
        controller.showMarkerInfoWindow(
          MarkerId('SomeId'),
        );
      },
    );
  }

  Future<void> navToChat() async {
    try {
      await ref.read(chatUseCaseProvider).loadChatToCheckIsSubscribeTopic();
      await ref.read(conversationsStateNotifierProvider.notifier).getData();

      Conversation? conversation = ref
          .read(conversationsStateNotifierProvider.notifier)
          .getConversationById(widget.roomId);
      if (conversation != null) {
        Navigator.pushNamed(
          context,
          ChatScreen.id,
          arguments: ChatScreenArguments(conversation: conversation),
        );
      }
    } catch (e) {
      print(e);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('進入失敗，請稍後重試'),
        ),
      );
    }
  }

  void navToManagement() async {
    Navigator.pushNamed(
      context,
      ManageRoomPage.id,
      arguments: GameRoomDetailArguments(
        roomId: widget.roomId,
      ),
    );
  }

  exitRoom() async {
    try {
      await ref.read(roomUseCaseProvider).exitGameRoom(widget.roomId);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('退出成功'),
        ),
      );
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('退出失敗，請稍後重試'),
        ),
      );
    }
  }

  buildSubmitActionButton(
      bool isAuditRoom, bool hasSubmitAudit, String attendeesText) {
    // ${attendees.length}/$needNumbers'
    if (isAuditRoom) {
      return hasSubmitAudit
          ? OutlinedButton(
              onPressed: () => cancelAudit(),
              child: Text(
                '取消審核 $attendeesText',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                minimumSize: Size(180, 44),
              ),
            )
          : ElevatedButton(
              onPressed: () => submitAudit(),
              child: Text(
                '提交審核 $attendeesText',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              style: ElevatedButton.styleFrom(
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                minimumSize: Size(180, 44),
              ),
            );
    } else {
      return ElevatedButton(
        onPressed: () => attend(),
        child: Text(
          '參加 $attendeesText',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        style: ElevatedButton.styleFrom(
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          minimumSize: Size(180, 44),
        ),
      );
    }
  }

  void _showSheet(String creatorId, [ReplyToUser? user]) {
    bool isSendAble = false;
    ReplyToUser? replyUser = user;
    String oldText = "";
    TextEditingController textEditingController = TextEditingController();
    FocusNode commentFocusNode = FocusNode();
    FocusNode keyBoardFocusNode = FocusNode();
    ScrollController scrollController = ScrollController();

    showModalBottomSheet(
      context: context,
      isScrollControlled: true, // set this to true
      builder: (_) {
        return SafeArea(
          child: Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: DraggableScrollableSheet(
              initialChildSize: 0.9,
              expand: false,
              builder: (_, controller) {
                return StatefulBuilder(
                  builder: (context, setState) {
                    final viewController =
                        ref.watch(gameRoomDetailViewController);

                    return Container(
                      child: Flex(
                        direction: Axis.vertical,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.close),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                TextButton.icon(
                                  onPressed: () {
                                    setState(() {
                                      viewController.handleLikePost(creatorId);
                                    });
                                  },
                                  style: TextButton.styleFrom(
                                    primary: Colors.grey,
                                  ),
                                  icon: viewController.isLike
                                      ? Icon(
                                          Icons.favorite,
                                          color: Colors.redAccent,
                                        )
                                      : Icon(Icons.favorite_border),
                                  label: Text('推${viewController.likeCount}'),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: CustomScrollView(
                              controller: scrollController,
                              slivers: [
                                Consumer(builder: (context, ref, child) {
                                  final myPostComment = ref.watch(
                                      justPostCommentChangeNotifierProvider(
                                          widget.roomId));

                                  return SliverList(
                                    delegate: SliverChildBuilderDelegate(
                                      (BuildContext context, int index) {
                                        return CommentListTile(
                                          data: myPostComment[index],
                                          onReply: (replyToUser) {
                                            // _showSheet(creatorId, replyToUser);
                                          },
                                        );
                                      },
                                      childCount: myPostComment.length,
                                    ),
                                  );
                                }),
                                Consumer(
                                  builder: (context, ref, child) {
                                    final commentChangeNotifier = ref.watch(
                                        commentChangeNotifierProvider(
                                                widget.roomId)
                                            .notifier);
                                    return PagedSliverList<int,
                                        Map<String, dynamic>>(
                                      pagingController: commentChangeNotifier
                                          .pagingController,
                                      builderDelegate:
                                          PagedChildBuilderDelegate<
                                              Map<String, dynamic>>(
                                        animateTransitions: false,
                                        transitionDuration:
                                            const Duration(milliseconds: 500),
                                        itemBuilder: (context, item, index) =>
                                            CommentListTile(
                                          data: item,
                                          onReply: (replyModel) {
                                            setState(() {
                                              replyUser = replyModel;
                                              commentFocusNode.requestFocus();
                                            });
                                          },
                                        ),
                                        firstPageProgressIndicatorBuilder:
                                            (_) => CircleAvatar(
                                          backgroundColor: Colors.red,
                                        ),
                                        newPageProgressIndicatorBuilder: (_) =>
                                            CircleAvatar(
                                          backgroundColor: Colors.blue,
                                        ),
                                        noItemsFoundIndicatorBuilder: (_) =>
                                            Text('沒發現'),
                                        noMoreItemsIndicatorBuilder: (_) =>
                                            Center(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 24),
                                            child: Text(
                                              '已經到底了',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .caption,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                          Material(
                            elevation: 4,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: RawKeyboardListener(
                                      onKey: (event) {
                                        print(event.toStringShort());
                                        if (event.logicalKey ==
                                            LogicalKeyboardKey.backspace) {
                                          if (replyUser != null &&
                                              !isSendAble) {
                                            setState(() {
                                              replyUser = null;
                                            });
                                          }
                                        }
                                      },
                                      focusNode: keyBoardFocusNode,
                                      child: TextField(
                                        // key: commentKey,
                                        autofocus: true,
                                        focusNode: commentFocusNode,
                                        textInputAction:
                                            TextInputAction.newline,
                                        keyboardType: TextInputType.multiline,
                                        maxLines: 7,
                                        minLines: 1,
                                        controller: textEditingController,
                                        onChanged: (value) {
                                          if (textEditingController
                                              .text.isNotEmpty) {
                                            if (isSendAble != true) {
                                              setState(() {
                                                isSendAble = true;
                                              });
                                            }
                                          } else {
                                            if (isSendAble != false) {
                                              setState(() {
                                                isSendAble = false;
                                              });
                                            }
                                          }
                                        },
                                        decoration: InputDecoration(
                                            prefix: replyUser != null
                                                ? ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        minWidth: 40, //宽度尽可能大
                                                        maxWidth:
                                                            120.0 //最小高度为50像素
                                                        ),
                                                    child: Text(
                                                      replyUser!.nickName,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.blue),
                                                    ),
                                                  )
                                                : null,
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(32.0),
                                              ),
                                              borderSide: BorderSide(
                                                  width: 0.5,
                                                  color: Colors.grey),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(32.0),
                                              ),
                                              borderSide: BorderSide(
                                                  width: 0.5,
                                                  color: Colors.black38),
                                            ),
                                            hintText: ' 寫下留言............',
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    horizontal: 16,
                                                    vertical: 8),
                                            filled: false),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(
                                      Icons.send_rounded,
                                      color: isSendAble
                                          ? Colors.lightBlue
                                          : Colors.grey,
                                    ),
                                    onPressed: isSendAble
                                        ? () {
                                            viewController.addComment(creatorId,
                                                textEditingController.text);
                                            textEditingController.clear();
                                            setState(() {
                                              isSendAble = false;
                                              replyUser = null;
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              scrollController.animateTo(0,
                                                  duration: Duration(
                                                      milliseconds: 500),
                                                  curve: Curves.easeOut);
                                            });
                                          }
                                        : null,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
              },
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Consumer(
        builder: (context, watch, child) {
          print(widget.roomId);
          AsyncValue<RoomDetail> room =
              ref.watch(roomDetailStreamProvider(widget.roomId));
          MyUser? myUser = ref.watch(userStateProvider).state;

          final viewController = ref.watch(gameRoomDetailViewController);

          return room.when(
            loading: (data) => const CircularProgressIndicator(),
            error: (data, err, stack) => Text('Error: $err'),
            data: (data) {

              // if (!docSnapShot.exists) {
              //   return RoomNotFound();
              // }

              List<dynamic> likeUsers = [];
              data.likes.forEach((key, value) {
                if (value) {
                  likeUsers.add(key);
                }
              });
              bool hasAttend = data.attendees.contains(myUser!.id);
              bool hasSubmitAudit = data.reviews.contains(myUser.id);
              bool isAuditRoom = data.detail.isAudit;
              viewController.likeCount = likeUsers.length;
              if (myUser != null) {
                viewController.isLike = likeUsers.contains(myUser.id);
              }

              String avatarUrl = URLGetter.getAvatarUrl( data.creator.id);
              String roomUrl = URLGetter.getGameRoomCoverUrl(widget.roomId);

              bool isCreator = myUser.id == data.creator.id;

              String attendNumberText =
                  '${data.attendees.length}/${data.detail.needNumbers + 1}';

              return Scaffold(
                body: Flex(
                  direction: Axis.vertical,
                  children: [
                    Expanded(
                      child: CustomScrollView(
                        slivers: [
                          SliverAppBar(
                            iconTheme: IconThemeData(
                              color: Colors.white, //change your color here
                            ),
                            pinned: true,
                            floating: false,
                            expandedHeight: 240,
                            flexibleSpace: FlexibleSpaceBar(
                              background: CachedNetworkImage(
                                imageUrl: roomUrl,
                                fit: BoxFit.cover,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                            actions: [
                              IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.share),
                              ),
                              PopupMenuButton(
                                icon: Icon(Icons.more_vert),
                                itemBuilder: (BuildContext context) {
                                  return hasAttend
                                      ? <PopupMenuEntry>[
                                          PopupMenuItem(
                                            child: TextButton(
                                              child: Text(
                                                '退出此團',
                                                style: TextStyle(
                                                    color: Colors.black87),
                                              ),
                                              onPressed: () => exitRoom(),
                                            ),
                                          ),
                                        ]
                                      : <PopupMenuEntry>[
                                          PopupMenuItem(
                                            child: TextButton(
                                              child: Text(
                                                '檢視聚會',
                                                style: TextStyle(
                                                    color: Colors.black87),
                                              ),
                                              onPressed: () async {},
                                            ),
                                          ),
                                        ];
                                },
                              ),
                            ],
                          ),
                          SliverList(
                            delegate: SliverChildListDelegate(
                              [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 4,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          data.detail.title,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5,
                                        ),
                                      ),
                                      ListTile(
                                        minLeadingWidth: 16,
                                        leading: Icon(Icons.calendar_today),
                                        title: Text(
                                          TextUtil.getStartDateText(
                                            data.detail.startTime,
                                          ),
                                          style: TextStyle(
                                              color: Color(0xffc66900),
                                              fontSize: 16),
                                        ),
                                        onTap: () {
                                          final Event event = Event(
                                            title: 'Event title',
                                            description: 'Event description',
                                            location: 'Event location',
                                            startDate: DateTime.now()
                                                .add(Duration(minutes: 1)),
                                            androidParams: AndroidParams(
                                              emailInvites: [], // on Android, you can add invite emails to your event.
                                            ),
                                            endDate: DateTime.now()
                                                .add(Duration(hours: 1)),
                                          );
                                          Add2Calendar.addEvent2Cal(event);
                                        },
                                      ),
                                      data.detail.isOnline
                                          ? ListTile(
                                              minLeadingWidth: 16,
                                              leading: Icon(Icons.live_tv),
                                              title: Text(
                                                '線上局',
                                                style: TextStyle(
                                                    color: Color(0xffc66900),
                                                    fontSize: 16),
                                              ),
                                            )
                                          : ExpansionTile(
                                              title: ListTile(
                                                minLeadingWidth: 0,
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 0.0),
                                                leading:
                                                    Icon(Icons.location_on),
                                                title: Text(
                                                  TextUtil.getPlaceName(
                                                      data.detail.placeName,
                                                      data.detail.address),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Color(0xffc66900),
                                                      fontSize: 16),
                                                ),
                                                subtitle: Text(
                                                  data.detail.address,
                                                  style: TextStyle(
                                                      color: Colors.grey[700],
                                                      fontSize: 13),
                                                ),
                                              ),
                                              children: [
                                                SizedBox(
                                                  height: 240,
                                                  width: double.infinity,
                                                  child: Builder(
                                                    builder: (context) {
                                                      return buildMap(data.detail.position!.geopoint!,
                                                          data.detail.placeName, data.detail.address);
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                      Divider(),
                                      ListTile(
                                        leading: CachedNetworkImage(
                                          imageUrl: avatarUrl,
                                          fit: BoxFit.fill,
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  CircleAvatar(
                                            radius: 36,
                                            backgroundImage: imageProvider,
                                          ),
                                        ),
                                        title: Text('局長'),
                                        subtitle:
                                            Text(data.creator.nickname),
                                        onTap: () {
                                          Navigator.pushNamed(
                                            context,
                                            PersonalPageById.id,
                                            arguments: PersonalByIdArguments(
                                              uid: data.creator.id,
                                            ),
                                          );
                                        },
                                      ),
                                      Divider(),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        '介紹',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                      SizedBox(
                                        height: 16,
                                      ),
                                      Text(
                                        data.detail.info,
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(),
                                SizedBox(
                                  height: 16,
                                ),
                              ],
                            ),
                          ),
                          SliverToBoxAdapter(
                            key: commentTopKey,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                TextButton.icon(
                                  onPressed: () {
                                    setState(() {
                                      viewController.handleLikePost(data.creator.id);
                                    });
                                  },
                                  style: TextButton.styleFrom(
                                    primary: Colors.grey,
                                  ),
                                  icon: viewController.isLike
                                      ? Icon(Icons.favorite)
                                      : Icon(Icons.favorite_border),
                                  label: Text('推${viewController.likeCount}'),
                                ),
                                TextButton.icon(
                                  style: TextButton.styleFrom(
                                    primary: Colors.grey,
                                  ),
                                  onPressed: () {
                                    _showSheet(data.creator.id);
                                    // Scrollable.ensureVisible(
                                    //     commentKey.currentContext!);
                                    // commentFocusNode.requestFocus();
                                  },
                                  icon: Icon(Icons.comment),
                                  label: Text('留言'),
                                ),
                                TextButton.icon(
                                  style: TextButton.styleFrom(
                                    primary: Colors.grey,
                                  ),
                                  onPressed: () {},
                                  icon: FaIcon(FontAwesomeIcons.share),
                                  label: Text('分享'),
                                ),
                              ],
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Divider(),
                          ),
                          Consumer(builder: (context, ref, child) {
                            final myPostComment = ref.watch(
                                justPostCommentChangeNotifierProvider(
                                    widget.roomId));
                            return SliverList(
                              delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                                  return CommentListTile(
                                    data: myPostComment[index],
                                    onReply: (replyToUser) {
                                      // _showSheet(creatorId, replyToUser);
                                    },
                                  );
                                },
                                childCount: myPostComment.length,
                              ),
                            );
                          }),
                          Consumer(
                            builder: (context, ref, child) {
                              final commentChangeNotifier = ref.watch(
                                  commentChangeNotifierProvider(widget.roomId)
                                      .notifier);
                              return PagedSliverList<int, Map<String, dynamic>>(
                                pagingController:
                                    commentChangeNotifier.pagingController,
                                builderDelegate: PagedChildBuilderDelegate<
                                    Map<String, dynamic>>(
                                  animateTransitions: false,
                                  transitionDuration:
                                      const Duration(milliseconds: 500),
                                  itemBuilder: (context, item, index) =>
                                      CommentListTile(
                                    data: item,
                                    onReply: (replyModel) {
                                      setState(() {
                                        _showSheet(data.creator.id, replyModel);
                                      });
                                    },
                                  ),
                                  firstPageProgressIndicatorBuilder: (_) =>
                                      CircleAvatar(
                                    backgroundColor: Colors.red,
                                  ),
                                  newPageProgressIndicatorBuilder: (_) =>
                                      CircleAvatar(
                                    backgroundColor: Colors.blue,
                                  ),
                                  noItemsFoundIndicatorBuilder: (_) =>
                                      Text('沒發現'),
                                  noMoreItemsIndicatorBuilder: (_) => Center(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 24),
                                      child: Text(
                                        '已經到底了',
                                        style:
                                            Theme.of(context).textTheme.caption,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    Material(
                        elevation: 8,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  width: 8,
                                ),
                                Offstage(
                                  offstage: !isCreator,
                                  child: TextButton.icon(
                                    onPressed: () => navToManagement(),
                                    style: ElevatedButton.styleFrom(
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      // minimumSize: Size(180, 44),
                                    ),
                                    label: Text(
                                      '管理',
                                      style: TextStyle(
                                          // color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    icon: Icon(Icons.edit_rounded),
                                  ),
                                ),
                                TextButton.icon(
                                  onPressed: () async {
                                    Navigator.pushNamed(
                                      context,
                                      RatingPage.id,
                                      arguments: RatingRoomArgument(
                                        data,
                                        widget.roomId,
                                      ),
                                    );
                                  },
                                  style: ElevatedButton.styleFrom(
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(30.0),
                                    ),
                                    // minimumSize: Size(180, 44),
                                  ),
                                  label: Text(
                                    '分享',
                                    style: TextStyle(
                                        // color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  icon: Icon(Icons.share),
                                ),
                                Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0, horizontal: 16),
                                    child: data.attendees.contains(myUser.id)
                                        ? OutlinedButton(
                                            onPressed: () => navToChat(),
                                            child: Text(
                                              '進入聊天室',
                                              style: TextStyle(
                                                  // color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        30.0),
                                              ),
                                              minimumSize: Size(180, 44),
                                            ),
                                          )
                                        : buildSubmitActionButton(isAuditRoom,
                                            hasSubmitAudit, attendNumberText))
                              ],
                            ),
                          ],
                        ))
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
