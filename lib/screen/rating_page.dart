import 'package:boardgame/components/rating_card.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/repository/ratings_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tcard/tcard.dart';
import 'package:boardgame/models/rate_data.dart';
class AttendeesData {
  final String uid;
  final String nickname;
  String comment;
  String ratingType;

  AttendeesData(
      {required this.uid,
      required this.nickname,
      required this.comment,
      required this.ratingType});
}

class RateUserData {
  //beRateUID,
  final String uid;
  String comment = "";
  String rateType = "";
  String nickname = "";
  RateUserData(
      {required this.uid,
      required this.nickname,
      required this.rateType,
      required this.comment});
}

class RatingPage extends ConsumerStatefulWidget {
  static const String id = '/rating_page';
  final RoomDetail roomDetail;
  final String roomId;
  const RatingPage({Key? key, required this.roomDetail, required this.roomId})
      : super(key: key);

  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState extends ConsumerState<RatingPage> {
  late List<RateUserData> rateDatas;

  late List<RateUserData> rateDataList;
  int currentIndex = 0;
  String roomName = "";
  late Timestamp startTime;
  TCardController _tController = TCardController();
  int? selectedGoodFeature;
  FixedExtentScrollController listWheelScrollController =
      FixedExtentScrollController();
  TextEditingController commentTextEditingController = TextEditingController();

  @override
  void initState() {
    MyUser? myUser = ref.read(userStateProvider).state;
    Map<String, dynamic> attendeesData = widget.roomDetail.attendeesData;
    roomName = widget.roomDetail.detail.title;
    startTime = widget.roomDetail.detail.startTime;
    rateDatas = attendeesData.entries
        .map((entry) => RateUserData(
              uid: entry.key,
              comment: entry.value['comment'] ?? "",
              rateType: entry.value['rateType'] ?? "",
              nickname: entry.value['nickname'] ?? "",
            ))
        .where((element) => element.uid != myUser!.id)
        .toList();

    // rateDataList = List.generate(attendeesDataList.length,
    //         (index) => RateData(attendeesDataList[index].uid, widget.roomName))
    //     .toList();

    super.initState();
  }

  bool isRated(int index) {
    if (rateDatas[index].comment.isNotEmpty &&
        rateDatas[index].rateType.isNotEmpty) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final keyBoardBottom = MediaQuery.of(context).viewInsets.bottom;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('comment'),
      ),
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                String uid = rateDatas[index].uid;
                if (uid.isEmpty) {
                  return Text('error');
                }
                String docId = widget.roomId + "-" + uid;
                return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RatingCard(
                      rateDatas[index],
                      docId,
                      roomName,
                    ),);
              },
              itemCount: rateDatas.length,
            ),
          ),
        ],
      ),
    );
  }
}

final rateDataProvider = FutureProvider.autoDispose
    .family<Map<String, dynamic>, String>((ref, docId) async {
  Map<String, dynamic> data =
      await ref.read(ratingsRepository).getRating1(docId);
  return data;
});
