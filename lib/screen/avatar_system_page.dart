// import 'dart:math';
//
// import 'package:boardgame/components/avatar_component.dart';
// import 'package:boardgame/controller/avatarSystemChangeNotifier.dart';
// import 'package:boardgame/models/avatar_bo.dart';
// import 'package:boardgame/models/avatar_colors.dart';
// import 'package:boardgame/models/color_with_text.dart';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:flutter_svg/svg.dart';
//
// class AvatarSystemPage extends StatefulWidget {
//   static const String id = '/avatar_system_screen';
//
//   @override
//   _AvatarState createState() => _AvatarState();
// }
//
// class _AvatarState extends State<AvatarSystemPage>
//     with SingleTickerProviderStateMixin {
//   List<Tab> myTabs = <Tab>[
//     Tab(text: '頭'),
//     Tab(text: '身體'),
//     Tab(text: '臉'),
//     Tab(text: '配件'),
//     Tab(text: '鬍子'),
//   ];
//   late TabController tabController;
//   double itemSize = 80;
//
//   generate() async {
//     await context
//         .read(avatarSystemChangeNotifierProvider)
//         .randomGenerate(context);
//   }
//
//   @override
//   void initState() {
//     tabController = TabController(length: myTabs.length, vsync: this);
//     // accessoriesIndex = _random.nextInt(9);
//     // facialHairIndex = _random.nextInt(17);
//     context.read(avatarSystemChangeNotifierProvider).initAvatar(context);
//
//     setState(() {});
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('個人資料'),
//         actions: [
//           Consumer(
//             builder: (context, watch, child) {
//               return TextButton(
//                 onPressed: () {
//                   context
//                       .read(avatarSystemChangeNotifierProvider)
//                       .submitAvatar();
//                   // FirebaseAuth.instance.signOut();
//                 },
//                 child: Text(
//                   '完成',
//                   style: TextStyle(color: Colors.white),
//                 ),
//               );
//             },
//           ),
//         ],
//       ),
//       body: Consumer(
//         builder: (context, watch, child) {
//           final provider = watch(avatarSystemChangeNotifierProvider);
//           return Column(
//             children: [
//               SizedBox(
//                 height: 16,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 mainAxisSize: MainAxisSize.max,
//                 children: [
//                   Column(
//                     children: [
//                       Beep(
//                         avatar: provider.avatar,
//                         size: 150,
//                         color: provider.backGroundColorValue.color,
//                       ),
//                       ElevatedButton.icon(
//                           onPressed: () async {
//                             await generate();
//                           },
//                           icon: Icon(
//                             Icons.casino,
//                             color: Colors.white,
//                           ),
//                           label: Text(
//                             '隨機產生',
//                             style: TextStyle(color: Colors.white),
//                           )),
//                     ],
//                   ),
//                   Column(
//                     children: [
//                       Row(
//                         children: [
//                           DropdownButton<ColorWithText>(
//                             value: provider.skinColorValue,
//                             icon: const Icon(Icons.arrow_drop_down),
//                             iconSize: 24,
//                             elevation: 16,
//                             onChanged: (ColorWithText? newValue) {
//                               setState(() {
//                                 provider.skinColorValue = newValue!;
//                                 context
//                                     .read(avatarSystemChangeNotifierProvider)
//                                     .updateSkinColor(newValue);
//                               });
//                             },
//                             items: skinColors
//                                 .map<DropdownMenuItem<ColorWithText>>(
//                                     (ColorWithText value) {
//                               return DropdownMenuItem<ColorWithText>(
//                                 value: value,
//                                 child: Row(
//                                   children: [
//                                     Text(value.name),
//                                     Icon(
//                                       Icons.circle,
//                                       color: value.color,
//                                     )
//                                   ],
//                                 ),
//                               );
//                             }).toList(),
//                           ),
//                         ],
//                       ),
//                       Row(
//                         children: [
//                           DropdownButton<ColorWithText>(
//                             value: provider.clothColorValue1,
//                             icon: const Icon(Icons.arrow_drop_down),
//                             iconSize: 24,
//                             elevation: 16,
//                             onChanged: provider.isCloth1Enable()
//                                 ? (ColorWithText? newValue) {
//                                     setState(() {
//                                       provider.clothColorValue1 = newValue!;
//                                       context
//                                           .read(
//                                               avatarSystemChangeNotifierProvider)
//                                           .updateClothColor1(newValue);
//                                     });
//                                   }
//                                 : null,
//                             items: clothColors1
//                                 .map<DropdownMenuItem<ColorWithText>>(
//                                     (ColorWithText value) {
//                               return DropdownMenuItem<ColorWithText>(
//                                 value: value,
//                                 child: Row(
//                                   children: [
//                                     Text(value.name),
//                                     Icon(
//                                       Icons.circle,
//                                       color: value.color,
//                                     )
//                                   ],
//                                 ),
//                               );
//                             }).toList(),
//                           ),
//                           SizedBox(
//                             width: 8,
//                           ),
//                           DropdownButton<ColorWithText>(
//                             value: provider.clothColorValue2,
//                             icon: const Icon(Icons.arrow_drop_down),
//                             iconSize: 24,
//                             elevation: 16,
//                             onChanged: provider.isCloth2Enable()
//                                 ? (ColorWithText? newValue) {
//                                     setState(() {
//                                       provider.clothColorValue2 = newValue!;
//                                       context
//                                           .read(
//                                               avatarSystemChangeNotifierProvider)
//                                           .updateClothColor2(newValue);
//                                     });
//                                   }
//                                 : null,
//                             items: clothColors2
//                                 .map<DropdownMenuItem<ColorWithText>>(
//                                     (ColorWithText value) {
//                               return DropdownMenuItem<ColorWithText>(
//                                 value: value,
//                                 child: Row(
//                                   children: [
//                                     Text(value.name),
//                                     Icon(
//                                       Icons.circle,
//                                       color: value.color,
//                                     )
//                                   ],
//                                 ),
//                               );
//                             }).toList(),
//                           ),
//                         ],
//                       ),
//                       DropdownButton<ColorWithText>(
//                         value: provider.hairColorValue,
//                         icon: const Icon(Icons.arrow_drop_down),
//                         iconSize: 24,
//                         elevation: 16,
//                         onChanged: provider.isHairColorEnable()
//                             ? (ColorWithText? newValue) {
//                                 setState(() {
//                                   provider.hairColorValue = newValue!;
//                                   context
//                                       .read(avatarSystemChangeNotifierProvider)
//                                       .updateHairColor(newValue);
//                                 });
//                               }
//                             : null,
//                         items: hairColors.map<DropdownMenuItem<ColorWithText>>(
//                             (ColorWithText value) {
//                           return DropdownMenuItem<ColorWithText>(
//                             value: value,
//                             child: Row(
//                               children: [
//                                 Text(value.name),
//                                 Icon(
//                                   Icons.circle,
//                                   color: value.color,
//                                 )
//                               ],
//                             ),
//                           );
//                         }).toList(),
//                       ),
//                       DropdownButton<ColorWithText>(
//                         value: provider.backGroundColorValue,
//                         icon: const Icon(Icons.arrow_drop_down),
//                         iconSize: 24,
//                         elevation: 16,
//                         onChanged: (ColorWithText? newValue) {
//                           setState(() {
//                             provider.backGroundColorValue = newValue!;
//                             context
//                                 .read(avatarSystemChangeNotifierProvider)
//                                 .updateBackgroundColor(newValue);
//                           });
//                         },
//                         items: backgroundColors
//                             .map<DropdownMenuItem<ColorWithText>>(
//                                 (ColorWithText value) {
//                           return DropdownMenuItem<ColorWithText>(
//                             value: value,
//                             child: Row(
//                               children: [
//                                 Text(value.name),
//                                 Icon(
//                                   Icons.circle,
//                                   color: value.color,
//                                 )
//                               ],
//                             ),
//                           );
//                         }).toList(),
//                       ),
//                     ],
//                   )
//                 ],
//               ),
//               TabBar(
//                 labelColor: Colors.black87,
//                 controller: tabController,
//                 tabs: myTabs,
//                 indicatorColor: Colors.orange[800],
//               ),
//               SizedBox(
//                 height: 8,
//               ),
//               Expanded(
//                 child: TabBarView(
//                   controller: tabController,
//                   children: [
//                     Scrollbar(
//                       child: GridView.builder(
//                         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                             crossAxisCount: 3, //每行三列
//                             childAspectRatio: 1.0 //显示区域宽高相等
//                             ),
//                         itemCount: 50,
//                         itemBuilder: (context, index) {
//                           return Column(
//                             children: [
//                               InkWell(
//                                 child: SvgPicture.asset(
//                                   'assets/images/head/head$index.svg',
//                                   width: itemSize,
//                                   height: itemSize,
//                                 ),
//                                 onTap: () {
//                                   context
//                                       .read(avatarSystemChangeNotifierProvider)
//                                       .updateAvatarHead(context, index);
//                                 },
//                               ),
//                               Text(index.toString())
//                             ],
//                           );
//                         },
//                       ),
//                     ),
//                     Scrollbar(
//                       child: GridView.builder(
//                         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                             crossAxisCount: 3, //每行三列
//                             childAspectRatio: 1.0 //显示区域宽高相等
//                             ),
//                         itemCount: 30,
//                         itemBuilder: (context, index) {
//                           return Column(
//                             children: [
//                               InkWell(
//                                 child: SvgPicture.asset(
//                                   'assets/images/body/body$index.svg',
//                                   width: itemSize,
//                                   height: itemSize,
//                                 ),
//                                 onTap: () {
//                                   context
//                                       .read(avatarSystemChangeNotifierProvider)
//                                       .updateAvatarBody(context, index);
//                                 },
//                               ),
//                               Text(index.toString())
//                             ],
//                           );
//                         },
//                       ),
//                     ),
//                     Scrollbar(
//                       child: GridView.builder(
//                         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                           crossAxisCount: 3, //每行三列
//                         ),
//                         itemCount: 33,
//                         itemBuilder: (context, index) {
//                           return Container(
//                             child: Column(
//                               children: [
//                                 InkWell(
//                                   child: SvgPicture.asset(
//                                     'assets/images/face/face$index.svg',
//                                     width: itemSize,
//                                     height: itemSize,
//                                   ),
//                                   onTap: () {
//                                     context
//                                         .read(
//                                             avatarSystemChangeNotifierProvider)
//                                         .updateAvatarFace(context, index);
//                                   },
//                                 ),
//                                 Text(index.toString())
//                               ],
//                             ),
//                           );
//                         },
//                       ),
//                     ),
//                     Scrollbar(
//                       child: GridView.builder(
//                         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                           crossAxisCount: 3, //每行三列
//                         ),
//                         itemCount: 9,
//                         itemBuilder: (context, index) {
//                           return Container(
//                             child: Column(
//                               children: [
//                                 InkWell(
//                                   child: SvgPicture.asset(
//                                     'assets/images/accessories/accessories$index.svg',
//                                     width: itemSize,
//                                     height: itemSize,
//                                   ),
//                                   onTap: () {
//                                     context
//                                         .read(
//                                             avatarSystemChangeNotifierProvider)
//                                         .updateAvatarAccessories(
//                                             context, index);
//                                   },
//                                 ),
//                                 Text(index.toString())
//                               ],
//                             ),
//                           );
//                         },
//                       ),
//                     ),
//                     Scrollbar(
//                       child: GridView.builder(
//                         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                           crossAxisCount: 3, //每行三列
//                         ),
//                         itemCount: 17,
//                         itemBuilder: (context, index) {
//                           return Container(
//                             child: Column(
//                               children: [
//                                 InkWell(
//                                   child: SvgPicture.asset(
//                                     'assets/images/facial_hair/facial_hair$index.svg',
//                                     width: itemSize,
//                                     height: itemSize,
//                                   ),
//                                   onTap: () {
//                                     context
//                                         .read(
//                                             avatarSystemChangeNotifierProvider)
//                                         .updateAvatarFacialHair(context, index);
//                                   },
//                                 ),
//                                 Text(index.toString())
//                               ],
//                             ),
//                           );
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ],
//           );
//         },
//       ),
//     );
//   }
// }
//
// class Beep extends ConsumerWidget {
//   final AvatarBO avatar;
//   final double size;
//   final Color color;
//
//   Beep({required this.avatar, required this.size, required this.color});
//
//   @override
//   Widget build(BuildContext context, ScopedReader watch) {
//     return ClipOval(
//       child: Container(
//         alignment: Alignment.center,
//         color: color,
//         height: size,
//         width: size,
//         child: Center(
//           child: Stack(
//             alignment: Alignment.topLeft,
//             children: [
//               Body(
//                 index: avatar.bodyIndex,
//                 imageByte: avatar.bodyByte,
//                 size: Size(size, size),
//                 scale: 0.1,
//               ),
//               Head(
//                 index: avatar.headIndex,
//                 imageByte: avatar.headByte,
//                 size: Size(size, size),
//                 scale: 0.1,
//               ),
//               Face(
//                 index: avatar.faceIndex,
//                 imageByte: avatar.faceByte,
//                 size: Size(size, size),
//                 scale: 0.1,
//               ),
//               Accessories(
//                 index: avatar.accessoriesIndex,
//                 imageByte: avatar.accessoriesByte,
//                 size: Size(size, size),
//                 scale: 0.1,
//               ),
//               FacialHair(
//                 index: avatar.facialHairIndex,
//                 imageByte: avatar.facialHairByte,
//                 size: Size(size, size),
//                 scale: 0.1,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
//
// class LoadSvgAsset {
//   late String imageByte;
//   late String colorCode;
//   late double height;
//   late double width;
//
//   Future<String> loadAsset(BuildContext context, String path) async {
//     return await DefaultAssetBundle.of(context).loadString(path);
//   }
//
//   initByte(int index) {}
// }
