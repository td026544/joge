import 'package:badges/badges.dart';
import 'package:boardgame/components/audit_card.dart';
import 'package:boardgame/models/create_room_save_model.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/screen/game_room_detail_page.dart';
import 'package:boardgame/usecase/room_usecase.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


class ManageRoomPage extends ConsumerStatefulWidget {
  static const String id = '/manage_room_page';
  final String roomId;

  ManageRoomPage(this.roomId);

  @override
  _ManageRoomPageState createState() => _ManageRoomPageState();
}

class _ManageRoomPageState extends ConsumerState<ManageRoomPage> {
  @override
  void initState() {
    super.initState();
  }

  showKickOffConfirmDialog(String userId) {
    return showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("提示"),
          content: Text("確定要強制退出玩家?"),
          actions: <Widget>[
            TextButton(
              child: Text("取消"),
              onPressed: () => Navigator.of(context).pop(), // 关闭对话框
            ),
            TextButton(
              child: Text("確認"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

  void kickOffUser(String userId, String userNickname) async {
    bool isKickOff = await showKickOffConfirmDialog(userId);
    if (isKickOff == true) {
      try {
        await ref
            .read(roomUseCaseProvider)
            .kickOffUser(widget.roomId, userId, userNickname);
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('已強制退出使用者'),
          ),
        );
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('發生未知錯誤'),
          ),
        );
      }
    }
  }

  CreateRoomSaveModel getCreateModel(Map<String, dynamic> data) {
    Map<String, dynamic> detail = data['detail'];
    bool isOnline = detail['isOnline'];

    String? address = isOnline ? null : detail['address'];
    String? placeName = isOnline ? null : detail['placeName'];
    String city = isOnline ? null : detail['city'];
    Timestamp timestamp = detail['startTime'];
    int needNumbers = detail['needNumbers'];
    String type = detail['gameType'];
    String info = detail['info'];
    String title = detail['title'];

    bool isAudit = detail['isAudit'];
    GeoPoint? geoPoint = detail['position']['geopoint'];

    return CreateRoomSaveModel(
        address: address,
        startTime: timestamp.millisecondsSinceEpoch,
        needNumbers: needNumbers,
        type: type,
        city: city,
        latitude: isOnline ? null : geoPoint?.latitude,
        longitude: isOnline ? null : geoPoint?.longitude,
        placeName: placeName,
        roomId: widget.roomId,
        isOnline: isOnline,
        isAudit: isAudit,
        imageByte: null,
        info: info,
        title: title,
        stepStates: [1, 1, 1, 1]);
  }

  @override
  Widget build(BuildContext context) {
    AsyncValue<RoomDetail> room =
        ref.watch(roomDetailStreamProvider(widget.roomId));
    MyUser? myUser = ref.watch(userStateProvider).state;
    return room.when(
        data: (data) {


          // Map<String, dynamic> reviewsData = data['reviewsData'] ?? {};
          // List<dynamic> attendeesId = data['attendees'] ?? [];
          // String creatorId = data['creator']['id'];
          // Map<String, dynamic> attendeesData = data['attendeesData'] ?? {};
          return DefaultTabController(
            length: 2,
            child: Scaffold(
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                appBar: AppBar(
                  title: Text(
                    '管理',
                    style: TextStyle(color: Colors.white),
                  ),
                  bottom: TabBar(
                    labelColor: Colors.white,
                    unselectedLabelColor: Theme.of(context).primaryColorLight,
                    indicatorColor: Colors.orange,
                    tabs: [
                      Tab(
                        text: '審核名單',
                      ),
                      Tab(
                        text: '已加入',
                      ),
                    ],
                  ),
                  actions: [],
                ),
                body: Flex(
                  direction: Axis.vertical,
                  children: [
                    Expanded(
                      child: TabBarView(
                        children: [
                          ListView.separated(
                            itemBuilder: (context, index) {
                              String userId = data.reviews[index];
                              Map<String, dynamic> reviewData =
                              data.reviewsData[userId];
                              var a = reviewData;
                              return AuditCard(
                                  reviewData, userId, widget.roomId);
                            },
                            itemCount: data.reviews.length,
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return Divider();
                            },
                          ),
                          ListView.separated(
                            itemBuilder: (context, index) {
                              String userId = data.attendees[index];
                              String nickname = data.attendeesData[userId]['nickname'];
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8),
                                child: ListTile(
                                  leading: Badge(
                                    padding: EdgeInsets.all(2),
                                    showBadge: userId == data.creator.id,
                                    elevation: 0,
                                    badgeColor:
                                        Theme.of(context).primaryColorLight,
                                    toAnimate: false,
                                    position: BadgePosition.bottomEnd(),
                                    // showBadge: isRoomChat,
                                    badgeContent: Icon(
                                      Icons.stars_rounded,
                                      size: 24,
                                      color: Colors.orange,
                                    ),
                                    child: ClipOval(
                                      child: CachedNetworkImage(
                                        imageUrl:
                                            URLGetter.getAvatarUrl(userId),
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) =>
                                            CircularProgressIndicator(),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                        width: 48.0,
                                        height: 48.0,
                                      ),
                                    ),
                                  ),
                                  title: Text(nickname),
                                  trailing: Offstage(
                                    offstage:  data.creator.id == userId,
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.close,
                                        color: Colors.grey,
                                      ),
                                      onPressed: () async {
                                        kickOffUser(userId, nickname);
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: data.attendees.length,
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return Divider();
                            },
                          ),
                        ],
                      ),
                    ),
                    Material(
                      elevation: 8,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: 8,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextButton.icon(
                              onPressed: () {
                                // CreateRoomSaveModel createRoomModel =
                                //     getCreateModel();
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //     builder: (_) {
                                //       return NewCreateRoomPage(
                                //         createRoomSaveModel: createRoomModel,
                                //         isEditMode: true,
                                //       );
                                //     },
                                //   ),
                                // );
                              },
                              style: ElevatedButton.styleFrom(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                minimumSize: Size(180, 44),
                              ),
                              label: Text(
                                '編輯房間',
                                style: TextStyle(
                                    // color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              icon: Icon(Icons.edit_rounded),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: OutlinedButton(
                              onPressed: () {},
                              child: Text(
                                '報名截止',
                                style: TextStyle(
                                    // color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              style: ElevatedButton.styleFrom(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                minimumSize: Size(180, 44),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                )),
          );
        },
        error: (e, s, data) => Text('erorr'),
        loading: (data) => CircularProgressIndicator());
  }
}
