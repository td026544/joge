import 'dart:math';

import 'package:boardgame/components/new_room_card.dart';
import 'package:boardgame/controller/personal_page_view_controller.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/history_room_change_notifiyer_provider.dart';
import 'package:boardgame/repository/friends_repository.dart';
import 'package:boardgame/screen/setting_page.dart';
import 'package:boardgame/screen/subpage/personal_tabs_list_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PersonalPage extends ConsumerStatefulWidget {
  static const String id = '/personal_page';
  final MyUser? user;

  PersonalPage({this.user});

  @override
  _PersonalPageState createState() => _PersonalPageState();
}

class _PersonalPageState extends ConsumerState<PersonalPage> {
  late TextEditingController editingController;
  bool isReadOnly = false;
  FocusNode myFocusNode = FocusNode();

  @override
  initState() {
    editingController = TextEditingController();
    super.initState();
    init();
  }

  init() async {
    ref.read(personalPageViewController).init();
  }

  double get randHeight => Random().nextInt(100).toDouble();

  late List<Widget> _randomChildren;

  // Children with random heights - You can build your widgets of unknown heights here
  // I'm just passing the context in case if any widgets built here needs  access to context based data like Theme or MediaQuery
  List<Widget> _randomHeightWidgets(BuildContext context) {
    return _randomChildren = List.generate(3, (index) {
      final height = randHeight.clamp(
        50.0,
        MediaQuery.of(context)
            .size
            .width, // simply using MediaQuery to demonstrate usage of context
      );
      return Container(
        color: Colors.primaries[index],
        height: height,
        child: Text('Random Height Child ${index + 1}'),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        MyUser? myUser = ref.watch(userStateProvider).state;
        if (myUser == null) return Text('NoUser');
        int age = (DateTime.now().year - myUser.age.toDate().year);
        return Scaffold(
          body: DefaultTabController(
            length: 3,
            child: NestedScrollView(
              // allows you to build a list of elements that would be scrolled away till the body reached the top
              headerSliverBuilder: (context, _) {
                return [
                  SliverAppBar(
                    iconTheme: IconThemeData(
                      color: Colors.grey, //change your color here
                    ),
                    actions: [
                      IconButton(
                          icon: Icon(Icons.settings),
                          onPressed: () =>
                              Navigator.pushNamed(context, SettingPage.id))
                    ],
                    backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                    expandedHeight: 264,
                    title: Row(
                      children: [
                        Flexible(
                          child: Text(
                            myUser.nickname,
                            style: TextStyle(color: Colors.black87),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Flexible(
                          child: Text(
                            age.toString() + 'y',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.grey, fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    // floating: true,
                    pinned: true,
                    // collapsedHeight: 96,
                    forceElevated: true,
                    flexibleSpace: FlexibleSpaceBar(
                      collapseMode: CollapseMode.none,
                      background: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 88),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  ClipOval(
                                    child: CachedNetworkImage(
                                      imageUrl:
                                          URLGetter.getAvatarUrl(myUser.id),
                                      fit: BoxFit.fill,
                                      imageBuilder: (context, imageProvider) =>
                                          CircleAvatar(
                                        radius: 44,
                                        backgroundImage: imageProvider,
                                      ),
                                      errorWidget: (context, string, dynamic) {
                                        return CircleAvatar(
                                          radius: 44,
                                          backgroundColor: Colors.grey,
                                        );
                                      },
                                      placeholder: (context, string) {
                                        return CircleAvatar(
                                          radius: 44,
                                          backgroundColor: Colors.grey,
                                        );
                                      },
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        '12',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text('參加')
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Consumer(
                                        builder: (BuildContext context, watch,
                                            child) {
                                          MyUser? myUser =
                                              ref.read(userStateProvider).state;
                                          if (myUser == null) return Text('-');
                                          final friendsDocAsyncData = ref.watch(
                                              friendsFutureProvider(myUser.id));
                                          return friendsDocAsyncData.when(
                                            data: (data) {
                                              Map<String, dynamic> friends =
                                                  data['users'];
                                              int count = friends.length;
                                              return Text(
                                                count.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              );
                                            },
                                            loading: (data) =>
                                                CircularProgressIndicator(),
                                            error: (data, e, s) =>
                                                Text('error'),
                                          );
                                          return Text(
                                            '12',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          );
                                        },
                                      ),
                                      Text('好友')
                                    ],
                                  ),
                                  Column(
                                    children: [Text('title'), Text('content')],
                                  )
                                  // ClipOval(
                                  //   child: UserAvatar(
                                  //     avatar: myUser.avatar,
                                  //     size: 88,
                                  //     scale: 0.060,
                                  //   ),
                                  // ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                children: [
                                  Text(
                                    myUser.bio,
                                  )
                                ],
                              ),
                            )

                            // Text('2'),
                          ],
                        ),
                      ),
                    ),
                    bottom: PreferredSize(
                      preferredSize: Size.fromHeight(48),
                      child: Material(
                        child: TabBar(
                          labelColor: Colors.orange,
                          unselectedLabelColor: Colors.black38,
                          indicatorColor: Colors.orange,
                          tabs: [
                            Tab(
                              child: Row(
                                children: [
                                  Icon(Icons.category),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text('揪局')
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                            Tab(
                              child: Row(
                                children: [
                                  Icon(Icons.stars),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text('評價')
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                            Tab(
                              child: Row(
                                children: [
                                  Icon(Icons.history),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text('歷史')
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ];
              },
              // You tab view goes here
              body: TabBarView(
                children: [
                  MyGameRoomTabListView(),
                  RatesTabListView(myUser.id),
                  Consumer(
                    builder: (context, watch, child) {
                      final asyncData =
                          ref.watch(historyRoomStateNotifierProvider);
                      return asyncData.maybeWhen(
                          data: (datas) {
                            return ListView.builder(
                              itemCount: datas.length,
                              itemBuilder: (context, index) => NewRoomCard(
                                data: datas[index],
                              ),
                            );
                          },
                          orElse: () => CircularProgressIndicator());
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
