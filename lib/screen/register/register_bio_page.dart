import 'package:boardgame/components/cta_btn.dart';
import 'package:boardgame/controller/register_controller.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/repository/user_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../home_page.dart';

class RegisterBIOPage extends ConsumerStatefulWidget {
  static final id = "register_bio_page";

  @override
  _CreateBIOState createState() => _CreateBIOState();
}

class _CreateBIOState extends ConsumerState<RegisterBIOPage> {
  TextEditingController _textEditingController = TextEditingController();
  bool loading = false;

  ifSuccessNavToHome() async {
    final userRef = FirebaseFirestore.instance.collection('users');
    final DocumentSnapshot doc =
        await userRef.doc(FirebaseAuth.instance.currentUser!.uid).get();

    if (doc.exists) {
      MyUser? myUser = await ref
          .read(userRepository)
          .getUser(FirebaseAuth.instance.currentUser!.uid);
      ref.read(userStateProvider).state = myUser;
      Navigator.pushNamedAndRemoveUntil(
          context, HomeScreen.id, (Route<dynamic> route) => false);
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            LinearProgressIndicator(
              value: 1.0,
            ),
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Text(
                '介紹自己',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Expanded(
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 32),
                  child: TextFormField(
                    controller: _textEditingController,

                    // validator: (value) => validateForm(value),

                    // enabled: true,
                    keyboardType: TextInputType.multiline,
                    maxLines: 6,
                    minLines: 6,
                    maxLength: 120,
                    decoration: InputDecoration(
                      hintText: "跟各位局友打個招呼吧",
                      border: OutlineInputBorder(),
                      labelText: '介紹',
                      errorStyle: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 24),
              child: CTAButton(
                  onPressed: () async {
                    // if (USE_EMULATOR) {
                    //   await context.read(userRepository).testCreateUser();
                    // }
                    ref.read(registerPageViewController).registerUserData.bio =
                        _textEditingController.text;

                    setState(() {
                      loading = true;
                    });
                    try {
                      ref.read(registerPageViewController).createUser();
                    } catch (e) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text('Error'),
                        ),
                      );
                    }
                    ifSuccessNavToHome();

                    setState(() {
                      loading = false;
                    });

                    // Navigator.pushNamed(context, RegisterHobbiesPage.id);
                  },
                  text: '繼續'),
            ),
          ],
        ),
      ),
    );
  }
}
