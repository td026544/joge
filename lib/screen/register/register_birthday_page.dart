import 'package:boardgame/components/cta_btn.dart';
import 'package:boardgame/controller/register_controller.dart';
import 'package:boardgame/interfaces/register_page.dart';
import 'package:boardgame/screen/register/register_gender_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

class RegisterMyBirthdayPage extends ConsumerStatefulWidget {
  static final id = "register_birthday_page";

  @override
  _RegisterMyBirthdayPageState createState() => _RegisterMyBirthdayPageState();
}

class _RegisterMyBirthdayPageState extends ConsumerState<RegisterMyBirthdayPage>
    implements RegisterPage {
  late DateTime? _dateTime = DateTime.utc(1990, 1, 1);
  TextEditingController _textEditingController = TextEditingController();

  _showDatePicker() async {
    var date = await showDatePicker(
      context: context,
      initialDate: DateTime(1990, 1, 1),
      firstDate: DateTime(1900),
      lastDate: DateTime(2050),
      locale: Locale('zh', 'TW'),
    );
    if (date == null) return;
    print(date);
    setState(() {
      _textEditingController.text = DateFormat('yyyy/MM/dd').format(date);
      _dateTime = date;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            LinearProgressIndicator(
              value: 0.51,
            ),
            Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.grey,
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 24),
                    child: Text(
                      '我的生日是',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32),
                      child: TextFormField(
                        readOnly: true,
                        controller: _textEditingController,
                        onTap: () {
                          _showDatePicker();
                        },
                        decoration: const InputDecoration(
                          icon: Icon(Icons.calendar_today),
                          hintText: 'YYYY/mm/dd',
                          labelText: '生日 *',
                        ),
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 24),
                      child: CTAButton(
                          text: '繼續',
                          onPressed: isFillData() ? () => finish() : null))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool isFillData() {
    return _textEditingController.text.isNotEmpty;
    if (_textEditingController.text.isNotEmpty) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void setRegisterUserData() {
    ref.read(registerPageViewController).registerUserData.birthday =
        Timestamp.fromDate(_dateTime!);
  }

  @override
  void finish() {
    setRegisterUserData();
    Navigator.pushNamed(context, RegisterGenderPage.id);
  }
}
