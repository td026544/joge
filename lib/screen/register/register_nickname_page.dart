import 'package:boardgame/components/cta_btn.dart';
import 'package:boardgame/controller/register_controller.dart';
import 'package:boardgame/interfaces/register_page.dart';
import 'package:boardgame/screen/register/register_birthday_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RegisterNicknamePage extends ConsumerStatefulWidget {
  static final id = "register_nickname_page";

  @override
  _MyNickNamePageState createState() => _MyNickNamePageState();
}

class _MyNickNamePageState extends ConsumerState<RegisterNicknamePage>
    implements RegisterPage {
  late TextEditingController _textEditingController;
  @override
  void initState() {
    _textEditingController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            LinearProgressIndicator(
              value: 0.34,
            ),
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 24),
                    child: Text(
                      '我的暱稱是',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32),
                      child: TextFormField(
                        controller: _textEditingController,
                        maxLength: 10,
                        decoration: const InputDecoration(
                          hintText: '希望別人怎麼稱呼你',
                          labelText: '暱稱 *',
                        ),
                        onChanged: (v) {
                          if (v.isEmpty) {
                            setState(() {});
                          } else {
                            if (v.length > 0) {
                              setState(() {});
                            }
                          }
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 24),
                    child: CTAButton(
                        text: '繼續',
                        onPressed: isFillData() ? () => finish() : null),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool isFillData() {
    return _textEditingController.text.isNotEmpty;
  }

  @override
  void setRegisterUserData() {
    ref.read(registerPageViewController).registerUserData.nickName =
        _textEditingController.text;
  }

  @override
  void finish() {
    setRegisterUserData();
    Navigator.pushNamed(context, RegisterMyBirthdayPage.id);
  }
}
