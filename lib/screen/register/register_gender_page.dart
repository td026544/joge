import 'package:boardgame/components/cta_btn.dart';
import 'package:boardgame/controller/register_controller.dart';
import 'package:boardgame/interfaces/register_page.dart';
import 'package:boardgame/screen/register/register_hobbies_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RegisterGenderPage extends ConsumerStatefulWidget {
  static final id = "register_gender_page";

  @override
  _RegisterGenderPageState createState() => _RegisterGenderPageState();
}

class _RegisterGenderPageState extends ConsumerState<RegisterGenderPage>
    implements RegisterPage {
  bool isMale = true;

  getMaleColor() {
    return isMale ? Colors.orange : Colors.grey;
  }

  getFemaleColor() {
    return isMale ? Colors.grey : Colors.orange;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            LinearProgressIndicator(
              value: 0.68,
            ),
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 24),
                    child: Text(
                      '我是',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  Column(
                    children: [
                      OutlinedButton.icon(
                        onPressed: () {
                          setState(() {
                            isMale = true;
                          });
                        },
                        style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ),
                          minimumSize: Size(120, 40),
                          side: BorderSide(
                              width: isMale ? 2 : 1, color: getMaleColor()),
                        ),
                        label: Text(
                          "男生",
                          style: TextStyle(color: getMaleColor(), fontSize: 18),
                        ),
                        icon: FaIcon(
                          FontAwesomeIcons.male,
                          color: getMaleColor(),
                        ),
                        // icon: Icon(Icons.fe, color: getMaleColor()),
                      ),
                      OutlinedButton.icon(
                        onPressed: () {
                          setState(() {
                            isMale = false;
                          });
                        },
                        style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ),
                          minimumSize: Size(120, 40),
                          side: BorderSide(
                              width: isMale ? 1 : 2, color: getFemaleColor()),
                        ),
                        label: Text(
                          "女生",
                          style:
                              TextStyle(color: getFemaleColor(), fontSize: 18),
                        ),
                        icon: FaIcon(
                          FontAwesomeIcons.female,
                          color: getFemaleColor(),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 24),
                    child: CTAButton(
                        onPressed: isFillData() ? () => finish() : null,
                        text: '繼續'),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void finish() {
    setRegisterUserData();
    Navigator.pushNamed(context, RegisterHobbiesPage.id);
  }

  @override
  bool isFillData() {
    return true;
  }

  @override
  void setRegisterUserData() {
    ref.read(registerPageViewController).registerUserData.isMale = isMale;
  }
}
