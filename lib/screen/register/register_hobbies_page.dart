import 'package:boardgame/components/cta_btn.dart';
import 'package:boardgame/controller/register_controller.dart';
import 'package:boardgame/interfaces/register_page.dart';
import 'package:boardgame/screen/register/register_avatar_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class Hobby {
  String name;
  bool isSelected;

  Hobby(this.name, this.isSelected);
}

class RegisterHobbiesPage extends ConsumerStatefulWidget {
  static final id = "register_hobbies_page";

  @override
  _RegisterHobbiesPageState createState() => _RegisterHobbiesPageState();
}

class _RegisterHobbiesPageState extends ConsumerState<RegisterHobbiesPage>
    with AutomaticKeepAliveClientMixin
    implements RegisterPage {
  List<Hobby> hobbies = [];
  List<String> selectedHobbies = [];
  List<String> hobbiesName = [
    "跑步",
    "爬山",
    "散步",
    "美食",
    "咖啡",
    "看展",
    "動漫",
    "密室逃脫",
    "狼人殺",
    "桌游",
    "夜生活",
    "聊天",
    "閱讀",
    "電影",
    "唱歌",
    "影集",
    "貓奴",
    "狗派",
    "游泳",
    "攝影",
    "旅行",
    "攀岩",
    "卡牌",
    "逛街",
    "羽球",
    "網球",
    "桌球",
    "排球",
    "籃球",
    "健身",
    "野餐",
    "城市解謎",
    "志工",
    "衝浪",
    "街舞",
    "釣魚",
    "看海",
    "血拚",
    "騎單車",
    "騎重機",
    "喝一杯",
    "潛水",
    "藝術",
    "棒球",
    "瑜珈",
    "跑酷",
    "生存遊戲",
    "漆彈",
    "露營",
    "社交舞"
  ];
  @override
  void initState() {
    hobbiesName.shuffle();
    List.generate(hobbiesName.length,
        (index) => hobbies.add(Hobby(hobbiesName[index], false)));
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            LinearProgressIndicator(
              value: 0.85,
            ),
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24, bottom: 16),
              child: Text(
                '我的興趣',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Expanded(
              child: Scrollbar(
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Wrap(
                      runSpacing: 6.0,
                      spacing: 6.0,
                      children: List.generate(
                          hobbies.length,
                          (index) => ChoiceChip(
                                backgroundColor: Colors.grey[100],
                                label: Text(
                                  hobbies[index].name,
                                  style: TextStyle(
                                      color: hobbies[index].isSelected
                                          ? Colors.orange
                                          : Colors.grey),
                                ),
                                selected: hobbies[index].isSelected,
                                onSelected: (bool value) {
                                  if (value) {
                                    if (!selectedHobbies
                                        .contains(hobbies[index].name)) {
                                      if (selectedHobbies.length > 5) {
                                        return;
                                      }
                                      selectedHobbies.add(hobbies[index].name);
                                    }
                                    hobbies[index].isSelected = value;
                                  } else {
                                    if (selectedHobbies
                                        .contains(hobbies[index].name)) {
                                      selectedHobbies
                                          .remove(hobbies[index].name);
                                    }
                                  }
                                  setState(() {
                                    hobbies[index].isSelected = value;
                                  });
                                },
                              )),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 24),
              child: CTAButton(
                text: '繼續 ${selectedHobbies.length.toString()}/6',
                onPressed: isFillData() ? () => finish() : null,
              ),
            ),
          ],
        ),
      ),
    );
    // return Scaffold(
    //   body: Wrap(
    //     children: List.generate(
    //         hobbies.length,
    //         (index) => Chip(
    //               label: Text(hobbies[index]),
    //             )),
    //   ),
    // );
  }

  @override
  bool get wantKeepAlive => false;

  @override
  void finish() {
    setRegisterUserData();
    Navigator.pushNamed(context, RegisterAvatarPage.id);
  }

  @override
  bool isFillData() {
    return selectedHobbies.length > 2;
  }

  @override
  void setRegisterUserData() {
    ref.read(registerPageViewController).registerUserData.hobbies =
        selectedHobbies;
  }
}
