import 'dart:convert';
import 'dart:typed_data';

import 'package:boardgame/components/cta_btn.dart';
import 'package:boardgame/controller/register_controller.dart';
import 'package:boardgame/interfaces/register_page.dart';
import 'package:boardgame/screen/register/register_bio_page.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_editor/image_editor.dart';
import 'package:image_picker/image_picker.dart ' as picker;

class RegisterAvatarPage extends ConsumerStatefulWidget {
  static final id = "register_avatar_page";

  const RegisterAvatarPage({Key? key}) : super(key: key);

  @override
  _RegisterAvatarPageState createState() => _RegisterAvatarPageState();
}

class _RegisterAvatarPageState extends ConsumerState<RegisterAvatarPage>
    implements RegisterPage {
  final GlobalKey<ExtendedImageEditorState> editorKey =
      GlobalKey<ExtendedImageEditorState>();

  ImageProvider? provider;
  File? image;
  double cropRatio = 1;
  Uint8List? cropImage;

  @override
  void initState() {
    super.initState();
  }

  void getImage() async {
    picker.ImagePicker imagePicker = picker.ImagePicker();
    final pickedFile = await imagePicker.pickImage(
        maxHeight: 675,
        maxWidth: 960,
        source: picker.ImageSource.gallery,
        imageQuality: 25);

    if (pickedFile != null) {
      setState(() {
        image = File(pickedFile.path);
        print((image!.lengthSync() / 1024).toString());
        provider = ExtendedFileImageProvider(image!, cacheRawData: true);
      });
    } else {
      print('No image selected.');
    }
  }

  void rotate(bool right) {
    editorKey.currentState?.rotate(right: right);
  }

  void reset() {
    editorKey.currentState?.reset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            LinearProgressIndicator(
              value: 0.68,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                      onPressed: image == null ? null : () => getImage(),
                      child: Text('重新挑選')),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '挑選一張大頭貼',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Column(
                    children: [
                      buildImage(),
                      SizedBox(
                        height: 16,
                      ),
                      Offstage(
                        offstage: image == null,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            OutlinedButton.icon(
                              onPressed: () => rotate(false),
                              icon: Icon(
                                Icons.rotate_left,
                                color: Theme.of(context).primaryColor,
                              ),
                              label: Text('左轉'),
                            ),
                            OutlinedButton.icon(
                              onPressed: () => rotate(true),
                              icon: Icon(
                                Icons.rotate_right,
                                color: Theme.of(context).primaryColor,
                              ),
                              label: Text('右轉'),
                            ),

                            OutlinedButton.icon(
                              onPressed: () => reset(),
                              icon: Icon(
                                Icons.refresh,
                                color: Theme.of(context).primaryColor,
                              ),
                              label: Text('重設'),
                            )
                            // IconButton(
                            //   onPressed: () {
                            //   },
                            //   icon: Icon(
                            //     Icons.rotate_left,
                            //     size: 48,
                            //     color: Theme.of(context).primaryColor,
                            //   ),
                            // ),
                            // IconButton(
                            //   onPressed: () {
                            //     reset();
                            //     // rotate(false);
                            //     // reset();
                            //   },
                            //   icon: Icon(
                            //     Icons.rotate_right,
                            //     size: 48,
                            //     color: Theme.of(context).primaryColor,
                            //   ),
                            // ),
                            // IconButton(
                            //   onPressed: () {
                            //     reset();
                            //     // rotate(false);
                            //     // reset();
                            //   },
                            //   icon: Icon(
                            //     Icons.refresh,
                            //     size: 48,
                            //     color: Theme.of(context).primaryColor,
                            //   ),
                            // )
                          ],
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 24),
                    child: CTAButton(
                      // onPressed: isFillData() ? () => finish() : null,
                      text: '繼續',
                      onPressed: isFillData() ? () => finish() : null,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: const Text('選擇封面照'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo),
            onPressed: _pick,
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              height: 480,
              width: MediaQuery.of(context).size.width,
              // color: Colors.red
              child: buildImage(),
            ),
            Divider(),
            Column(
              children: <Widget>[
                OutlinedButton.icon(
                  icon: Icon(Icons.photo_library),
                  label: Text("相簿挑選"),
                  onPressed: () {
                    getImage();
                  },
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(160, 40),
                    side: BorderSide(width: 1.0, color: Colors.orange),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                ElevatedButton.icon(
                  icon: Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                  onPressed: () async {
                    await crop();
                  },
                  label: Text(
                    '完成',
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    minimumSize: Size(160, 40),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
      // bottomNavigationBar: _buildFunctions(),
    );
  }

  Widget buildImage() {
    if (provider == null) {
      return InkWell(
        onTap: getImage,
        child: Container(
          height: 200,
          width: 200,
          child: DottedBorder(
            borderType: BorderType.RRect,
            radius: Radius.circular(12),
            padding: EdgeInsets.all(6),
            dashPattern: [16, 4],
            color: Colors.grey,
            strokeWidth: 2,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.add_photo_alternate_outlined,
                    color: Colors.grey,
                    size: 80,
                  ),
                  Text(
                    '新增照片',
                    style: TextStyle(color: Colors.grey),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
    return Container(
      color: Colors.grey,
      height: MediaQuery.of(context).size.width - 16,
      width: MediaQuery.of(context).size.width - 16,
      child: ExtendedImage(
        image: provider!,
        // height: 1000,
        width: MediaQuery.of(context).size.width,
        extendedImageEditorKey: editorKey,
        mode: ExtendedImageMode.editor,
        fit: BoxFit.contain,
        initEditorConfigHandler: (_) => EditorConfig(
          maxScale: 4.0,
          cropRectPadding: const EdgeInsets.all(20.0),
          hitTestSize: 20.0,
          cropAspectRatio: cropRatio,
        ),
      ),
    );
  }

  void setCropRatio(double? angle) {
    print(editorKey.currentState?.editAction!.rotateAngle.toString());
    if (angle == null) {
      cropRatio = 16 / 9;
    }
    if (angle == 180.0 || angle == 0.0) {
      cropRatio = 16 / 9;
    } else if (angle == 90.0 || angle == 270.0) {
      cropRatio = 9 / 16;
    }
  }

  Widget _buildFunctions() {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.flip),
          label: 'Flip',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.rotate_left),
          label: 'Rotate left',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.rotate_right),
          label: 'Rotate right',
        ),
      ],
      onTap: (int index) {
        switch (index) {
          case 0:
            flip();
            break;
          case 1:
            rotate(false);
            setState(() {
              setCropRatio(editorKey.currentState?.editAction!.rotateAngle);
            });
            break;
          case 2:
            rotate(true);
            // setState(() {
            //   setCropRatio(editorKey.currentState?.editAction!.rotateAngle);
            // });
            break;
        }
      },
      currentIndex: 0,
      selectedItemColor: Theme.of(context).primaryColor,
      unselectedItemColor: Theme.of(context).primaryColor,
    );
  }

  Future<bool> crop([bool test = false]) async {
    final ExtendedImageEditorState? state = editorKey.currentState;
    if (state == null) {
      return false;
    }
    final Rect? rect = state.getCropRect();
    if (rect == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("The crop rect is null ."),
      ));
      return false;
    }
    final EditActionDetails action = state.editAction!;
    final double radian = action.rotateAngle;

    final bool flipHorizontal = action.flipY;
    final bool flipVertical = action.flipX;
    // final img = await getImageFromEditorKey(editorKey);
    final Uint8List? img = state.rawImageData;

    if (img == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("The img is null."),
      ));
      return false;
    }

    final ImageEditorOption option = ImageEditorOption();

    option.addOption(ClipOption.fromRect(rect));
    option.addOption(
        FlipOption(horizontal: flipHorizontal, vertical: flipVertical));
    if (action.hasRotateAngle) {
      option.addOption(RotateOption(radian.toInt()));
    }

    option.addOption(ColorOption.saturation(sat));
    option.addOption(ColorOption.brightness(bright));
    option.addOption(ColorOption.contrast(con));
    option.addOption(ScaleOption(128, 128));

    option.outputFormat = const OutputFormat.png(88);

    print(const JsonEncoder.withIndent('  ').convert(option.toJson()));

    final DateTime start = DateTime.now();
    final Uint8List? result = await ImageEditor.editImage(
      image: img,
      imageEditorOption: option,
    );

    print('result.length = ${result?.length}');

    final Duration diff = DateTime.now().difference(start);

    print('image_editor time : $diff');
    // ScaffoldMessenger.of(context).showSnackBar(
    //   SnackBar(
    //     content: Text('handle duration: $diff'),
    //   ),
    // );

    // showToast('handle duration: $diff',
    //     duration: const Duration(seconds: 5), dismissOtherToast: true);
    if (result == null) {
      return false;
    } else {
      cropImage = result;
      return true;
    }

    // showPreviewDialog(result);
  }

  void flip() {
    editorKey.currentState?.flip();
  }

  void showPreviewDialog(Uint8List image) {
    showDialog<void>(
      context: context,
      builder: (BuildContext ctx) => GestureDetector(
        onTap: () => Navigator.pop(context),
        child: Container(
          color: Colors.grey.withOpacity(0.5),
          child: Center(
            child: SizedBox.fromSize(
              size: const Size.square(200),
              child: Container(
                child: Image.memory(image),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _pick() async {
    final picker.PickedFile? result =
        await picker.ImagePicker().getImage(source: picker.ImageSource.camera);

    if (result == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('The pick file is null'),
        ),
      );
      return;
    }
    print(result.path);
    provider = ExtendedFileImageProvider(File(result.path));
    setState(() {});
  }

  double sat = 1;
  double bright = 1;
  double con = 1;

  @override
  void finish() async {
    bool isSuccessCrop = await crop();
    if (isSuccessCrop) {
      setRegisterUserData();
      Navigator.pushNamed(context, RegisterBIOPage.id);
    }
  }

  @override
  bool isFillData() {
    return image != null;
  }

  @override
  void setRegisterUserData() {
    ref.read(registerPageViewController).registerUserData.imageByte =
        cropImage!;
  }
}
