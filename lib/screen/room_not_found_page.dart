import 'package:flutter/material.dart';

class RoomNotFound extends StatelessWidget {
  const RoomNotFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          '此局已被移除',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/place_holders/not_found.png'),
            Text(
              '此局已被移除',
              style: Theme.of(context).textTheme.subtitle2,
            ),
            SizedBox(
              height: 16,
            ),
            ElevatedButton.icon(
              onPressed: () {
                Navigator.pop(context);
              },
              label: Text(
                '返回',
                style: TextStyle(color: Colors.white),
              ),
              icon: Icon(
                Icons.arrow_back_ios_rounded,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
