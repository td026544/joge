import 'dart:math';

import 'package:boardgame/components/lobby_room_card.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/friends_provider.dart';
import 'package:boardgame/repository/friends_repository.dart';
import 'package:boardgame/repository/history_room_change_notifiyer_provider.dart';
import 'package:boardgame/screen/setting_page.dart';
import 'package:boardgame/usecase/friend_usecase.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final getUserDataFutureProvider =
    FutureProvider.family<MyUser, String>((ref, uid) async {
  DocumentSnapshot documentSnapshot =
      await FirebaseFirestore.instance.collection('users').doc(uid).get();
  Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic>;
  MyUser? user = MyUser.fromJson(data);
  return user;
});

class PersonalPageById extends ConsumerStatefulWidget {
  static const String id = '/personal_by_id_page';
  final String uid;
  final bool canInvite;

  PersonalPageById({required this.uid, this.canInvite = false});

  @override
  _PersonalPageByIdState createState() => _PersonalPageByIdState();
}

class _PersonalPageByIdState extends ConsumerState<PersonalPageById> {
  late TextEditingController editingController;
  bool isReadOnly = false;
  FocusNode myFocusNode = FocusNode();

  @override
  initState() {
    editingController = TextEditingController();
    super.initState();
    init();
  }

  init() async {
    // context.read(personalPageViewController).init();
  }

  int getAge(Timestamp userAge) {
    return (DateTime.now().year - userAge.toDate().year);
  }

  double get randHeight => Random().nextInt(100).toDouble();

  late List<Widget> _randomChildren;

  // Children with random heights - You can build your widgets of unknown heights here
  // I'm just passing the context in case if any widgets built here needs  access to context based data like Theme or MediaQuery
  List<Widget> _randomHeightWidgets(BuildContext context) {
    return _randomChildren = List.generate(3, (index) {
      final height = randHeight.clamp(
        50.0,
        MediaQuery.of(context)
            .size
            .width, // simply using MediaQuery to demonstrate usage of context
      );
      return Container(
        color: Colors.primaries[index],
        height: height,
        child: Text('Random Height Child ${index + 1}'),
      );
    });
  }

  Widget getFriendButton(Map<String, dynamic> myFriends, String toUserId) {
    MyUser? myUser = ref.read(userStateProvider).state;
    if (myUser == null) throw Exception('Nouser');
    if (myUser.id == toUserId) {
      return Container();
    }
    if (myFriends.containsKey(toUserId)) {
      FriendsStatus? friendsStatus =
          EnumToString.fromString(FriendsStatus.values, myFriends[toUserId]) ??
              FriendsStatus.none;

      switch (friendsStatus) {
        case FriendsStatus.beInvited:
          return ElevatedButton(
            onPressed: () {
              ref.read(friendUseCaseProvider).acceptFriend(toUserId);
              setState(() {});
            },
            child: Text(
              '接受好友邀請',
              style: TextStyle(color: Colors.white),
            ),
          );
        case FriendsStatus.invite:
          return OutlinedButton(
            onPressed: () {},
            child: const Text("邀請中"),
          );
        case FriendsStatus.match:
          return OutlinedButton(
            onPressed: () {},
            child: const Text("好友"),
          );
        case FriendsStatus.ban:
          return OutlinedButton(
            onPressed: () {},
            child: const Text("已封鎖"),
          );
        case FriendsStatus.none:
          return Text('');
      }
    } else {
      //todo 有參加過的才能家好友
      //todo 從聊天室近來才能加好友
      //todo 評價頁

      bool canInvite = true;
      return ElevatedButton(
        onPressed: canInvite
            ? () {
                ref.read(friendsRepository).inviteFriend(toUserId);
              }
            : null,
        child: Text(
          '加好友',
          style: TextStyle(color: Colors.white),
        ),
      );
    }
    // for (var friendData in myFriends) {
    //   if (friendData.uid == toUserId &&
    //       friendData.status == describeEnum(FriendsStatus.match))
    //
    //   if (friendData.uid == toUserId &&
    //       friendData.status == describeEnum(FriendsStatus.beInvited))
    //     return OutlinedButton(
    //       onPressed: () {},
    //       child: const Text("邀請中"),
    //     );
    //   return ElevatedButton(onPressed: null, child: Text('邀請中'));
    //   if (friendData.uid == toUserId &&
    //       friendData.status == describeEnum(FriendsStatus.invite))
    //     return ElevatedButton(
    //       onPressed: () {
    //         try {
    //           ref.read(friendsRepository).acceptInviteFriend(toUserId);
    //           setState(() {});
    //         } catch (e) {}
    //       },
    //       child: Text(
    //         '接受好友邀請',
    //         style: TextStyle(color: Colors.white),
    //       ),
    //     );
    //   if (friendData.uid == toUserId &&
    //       friendData.status == describeEnum(FriendsStatus.ban))
    //     return Text('封鎖');
    // }
  }

  List<PopupMenuEntry> buildMenu() {
    return <PopupMenuEntry>[
      PopupMenuItem(
        child: TextButton(
          child: Text(
            '刪除好友',
            style: TextStyle(color: Colors.black87),
          ),
          onPressed: () =>
              ref.read(friendUseCaseProvider).deleteFriend(widget.uid),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        final myUserAsyncValue =
            ref.watch(getUserDataFutureProvider(widget.uid));

        return myUserAsyncValue.when(
          data: (userData) => Scaffold(
            body: DefaultTabController(
              length: 2,
              child: NestedScrollView(
                // allows you to build a list of elements that would be scrolled away till the body reached the top
                headerSliverBuilder: (context, _) {
                  return [
                    SliverAppBar(
                        iconTheme: IconThemeData(
                          color: Colors.grey, //change your color here
                        ),
                        actions: [
                          PopupMenuButton(
                            itemBuilder: (BuildContext context) {
                              return buildMenu();
                            },
                          ),
                          IconButton(
                              icon: Icon(Icons.settings),
                              onPressed: () =>
                                  Navigator.pushNamed(context, SettingPage.id))
                        ],
                        backgroundColor:
                            Theme.of(context).scaffoldBackgroundColor,
                        expandedHeight: 320,
                        title: Row(
                          children: [
                            Flexible(
                              child: Text(
                                userData.nickname,
                                style: TextStyle(color: Colors.black87),
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Flexible(
                              child: Text(
                                getAge(userData.age).toString() + 'y',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 16),
                              ),
                            )
                          ],
                        ),
                        // floating: true,
                        pinned: true,
                        // collapsedHeight: 96,
                        forceElevated: true,
                        flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.none,
                          background: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 88),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      ClipOval(
                                        child: CachedNetworkImage(
                                          imageUrl: URLGetter.getAvatarUrl(
                                              userData.id),
                                          fit: BoxFit.fill,
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  CircleAvatar(
                                            radius: 44,
                                            backgroundImage: imageProvider,
                                          ),
                                        ),
                                      ),

                                      Column(
                                        children: [
                                          Text(
                                            '12',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text('參加')
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Consumer(
                                            builder: (BuildContext context,
                                                watch, child) {
                                              final friendsDocAsyncData = ref
                                                  .watch(friendsFutureProvider(
                                                      widget.uid));
                                              return friendsDocAsyncData.when(
                                                  data: (data) {
                                                    Map<String, dynamic>
                                                        friends = data['users'];
                                                    int count = friends.length;
                                                    return Text(
                                                      count.toString(),
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    );
                                                  },
                                                  loading: (data) =>
                                                      CircularProgressIndicator(),
                                                  error: (data, e, s) =>
                                                      Text('error'));
                                              return Text(
                                                '12',
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              );
                                            },
                                          ),
                                          Text('好友'),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Text('title'),
                                          Text('content')
                                        ],
                                      )

                                      // ClipOval(
                                      //   child: UserAvatar(
                                      //     avatar: myUser.avatar,
                                      //     size: 88,
                                      //     scale: 0.060,
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        userData.bio,
                                      ),
                                      Consumer(
                                        builder: (context, ref, child) {
                                          final AsyncValue<Map<String, dynamic>>
                                              friendsAsyncValue = ref.watch(
                                                  rawFriendsStreamProvider);
                                          return friendsAsyncValue.when(
                                              data: (data) => getFriendButton(
                                                  data, widget.uid),
                                              loading: (data) =>
                                                  CircularProgressIndicator(),
                                              error: (data, e, s) =>
                                                  Text('error'));
                                        },
                                      ),
                                      // ElevatedButton(
                                      //   onPressed: () {
                                      //     context
                                      //         .read(friendsRepository)
                                      //         .inviteFriend(userData.id);
                                      //   },
                                      //   child: Text(
                                      //     '加好友',
                                      //     style: TextStyle(
                                      //         color: Colors.white),
                                      //   ),
                                      //   style: ButtonStyle(
                                      //     shape:
                                      //         MaterialStateProperty.all<
                                      //             RoundedRectangleBorder>(
                                      //       RoundedRectangleBorder(
                                      //         borderRadius:
                                      //             BorderRadius.circular(
                                      //                 18.0),
                                      //       ),
                                      //     ),
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                )

                                // Text('2'),
                              ],
                            ),
                          ),
                        ),
                        bottom: PreferredSize(
                          preferredSize: Size.fromHeight(48),
                          child: Material(
                            child: TabBar(
                              labelColor: Colors.orange,
                              unselectedLabelColor: Colors.black38,
                              indicatorColor: Colors.orange,
                              tabs: [
                                Tab(
                                  child: Row(
                                    children: [
                                      Icon(Icons.stars),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text('評價')
                                    ],
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  ),
                                ),
                                Tab(
                                  child: Row(
                                    children: [
                                      Icon(Icons.history),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text('歷史')
                                    ],
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )),
                  ];
                },
                // You tab view goes here
                body: Column(
                  children: <Widget>[
                    Expanded(
                      child: TabBarView(
                        children: [
                          Consumer(
                            builder: (context, ref, child) {
                              final asyncData =
                                  ref.watch(historyRoomChangeNotifierProvider);
                              return asyncData.when(
                                data: (maps) {
                                  return ListView.builder(
                                    itemCount: maps.length,
                                    itemBuilder: (context, index) =>
                                        LobbyRoomCard(
                                      data: maps[index],
                                    ),
                                  );
                                },
                                loading: (data) => CircularProgressIndicator(),
                                error: (data, e, s) => Text(
                                  e.toString(),
                                ),
                              );
                            },
                          ),
                          Consumer(
                            builder: (context, ref, child) {
                              final asyncData =
                                  ref.watch(historyRoomChangeNotifierProvider);
                              return asyncData.when(
                                data: (maps) {
                                  return ListView.builder(
                                    itemCount: maps.length,
                                    itemBuilder: (context, index) =>
                                        LobbyRoomCard(
                                      data: maps[index],
                                    ),
                                  );
                                },
                                loading: (data) => CircularProgressIndicator(),
                                error: (data, e, s) => Text(
                                  e.toString(),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          loading: (data) => CircularProgressIndicator(),
          error: (data, e, s) => Text('error'),
        );

        // MyUser? myUser = watch(userStateProvider).state;
        // int age = (DateTime.now().year - myUser!.age.toDate().year);
      },
    );
  }
}
