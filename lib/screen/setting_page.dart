import 'package:boardgame/providers/auth_providers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../main.dart';

class SettingPage extends ConsumerStatefulWidget {
  static const String id = '/setting_page';

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends ConsumerState<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('設定'),
      ),
      body: Column(
        children: [
          ListTile(
            leading: Icon(Icons.security),
            title: Text('客服'),
          ),
          TextButton(
            onPressed: () async {
              final GoogleSignIn googleSignIn = GoogleSignIn();
              FirebaseAuth.instance.signOut();
              googleSignIn.signOut();

              ref.read(userStateProvider).state = null;
              Navigator.of(context).popUntil(ModalRoute.withName(Root.id));

              // Navigator.pushNamedAndRemoveUntil(
              //     context, Root.id, (Route<dynamic> route) => false);
            },
            child: Text('登出'),
          )
        ],
      ),
    );
  }
}
