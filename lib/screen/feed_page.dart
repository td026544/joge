import 'package:badges/badges.dart';
import 'package:boardgame/arguments/game_room_detail_arguments.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/feed_item.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/feeds_change_notifiyer_provider.dart';
import 'package:boardgame/providers/friends_provider.dart';
import 'package:boardgame/repository/feeds_repository.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'game_room_detail_page.dart';
import 'invite_friends_page.dart';

class FeedPage extends ConsumerStatefulWidget {
  static const String id = '/feed_page';
  const FeedPage({Key? key}) : super(key: key);

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends ConsumerState<FeedPage> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    localLatestTime();
  }

  Future<int> localLatestTime() async {
    MyUser? user = ref.read(userStateProvider).state;
    if (user == null) throw Exception('noUser');
    final localFeedItems = await ref
        .read(feedsRepository)
        .getMoreLimitFeedsFromFeedsTable(user.id, 1, 0);
    if (localFeedItems.isNotEmpty) {
      return localFeedItems.last.time.millisecondsSinceEpoch;
    } else {
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            title: Text(
              '通知',
            ),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, InviteFriendsPage.id);
                },
                icon: Consumer(
                  builder: (context, ref, child) {
                    final friendsAsyncValue =
                        ref.watch(inviteFriendsFutureProvider);
                    return friendsAsyncValue.maybeWhen(
                      data: (data) {
                        int unreadCount = data.length;
                        return Badge(
                          shape: BadgeShape.circle,
                          badgeColor: Colors.redAccent,
                          badgeContent: Text(
                            unreadCount.toString(),
                            style: TextStyle(color: Colors.white),
                          ),
                          position: BadgePosition.topEnd(),
                          showBadge: true,
                          child: Icon(
                            Icons.person_add,
                            color: Colors.grey,
                          ),
                        );
                      },
                      orElse: () => Icon(
                        Icons.person_add,
                        color: Colors.grey,
                      ),
                    );
                  },
                ),
              )
            ],
          ),
          body: Container(
            child: CustomScrollView(
              controller: scrollController,
              slivers: <Widget>[
                SliverToBoxAdapter(),
                Consumer(
                  builder: (context, watch, child) {
                    final remoteFeedFutureProvider =
                        ref.watch(remoteFeedItemFutureProvider);
                    return remoteFeedFutureProvider.when(
                      data: (data) => SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            return Container(
                                height: 84,
                                color: Theme.of(context).primaryColorLight,
                                child: FeedItemTile(feedItem: data[index]));
                          },
                          childCount: data.length,
                        ),
                      ),
                      loading: (data) => SliverToBoxAdapter(
                          child: CircularProgressIndicator()),
                      error: (data, e, s) => SliverToBoxAdapter(
                        child: Text(e.toString()),
                      ),
                    );
                  },
                ),
                Consumer(
                  builder: (context, ref, child) {
                    final localFeeds =
                        ref.watch(localFeedsStateNotifierProvider.notifier);
                    return PagedSliverList<int, FeedItem>.separated(
                      pagingController: localFeeds.pagingController,
                      builderDelegate: PagedChildBuilderDelegate<FeedItem>(
                        animateTransitions: false,
                        transitionDuration: const Duration(milliseconds: 500),
                        itemBuilder: (context, item, index) => Container(
                            height: 68, child: FeedItemTile(feedItem: item)),
                        firstPageProgressIndicatorBuilder: (_) => CircleAvatar(
                          backgroundColor: Colors.red,
                        ),
                        newPageProgressIndicatorBuilder: (_) => CircleAvatar(
                          backgroundColor: Colors.blue,
                        ),
                        noItemsFoundIndicatorBuilder: (_) => Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ColorFiltered(
                                colorFilter: const ColorFilter.mode(
                                  Color(0xfff7fbf7),
                                  BlendMode.modulate,
                                ),
                                child: Image.asset(
                                    'assets/images/place_holders/not_found1.png'),
                              ),
                              Text(
                                '目前沒有通知',
                                style: Theme.of(context).textTheme.caption,
                              )
                            ],
                          ),
                        ),
                        noMoreItemsIndicatorBuilder: (_) => Center(
                          child: Text(
                            '已經到底了',
                            style: Theme.of(context).textTheme.caption,
                          ),
                        ),
                      ),
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider();
                      },
                    );
                  },
                ),
              ],
            ),
          )),
    );
  }
}

class FeedItemTile extends StatelessWidget {
  final FeedItem feedItem;

  const FeedItemTile({Key? key, required this.feedItem}) : super(key: key);

  String getImgUrl(FeedItem feedItem) {
    FeedType? feedType=EnumToString.fromString(FeedType.values, feedItem.type);
    switch (feedType) {
      case FeedType.COMMENT_POST:
        return URLGetter.getAvatarUrl(feedItem.uid);
      case FeedType.LIKE_POST:
        return URLGetter.getAvatarUrl(feedItem.uid);
        default:
          return URLGetter.getAvatarUrl("");

    }
  }

  @override
  Widget build(BuildContext context) {
    Timestamp time =feedItem.time;
    String title = feedItem.nickname + ":" + feedItem.type.toString();
    return ListTile(
      leading: SizedBox(
        width: 40,
        height: 40,
        child: CachedNetworkImage(
          imageUrl:  getImgUrl(feedItem),
          fit: BoxFit.fill,
          imageBuilder: (context, imageProvider) => CircleAvatar(
            radius: 20,
            backgroundImage: imageProvider,
          ),
        ),
      ),
      title: Text(title),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(DateFormat.Hms().format(time.toDate())),
          Text(
            timeago.format(time.toDate(), locale: 'zh'),
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
      onTap: () {
        print(feedItem.navId);
        Navigator.pushNamed(
          context,
          GameRoomDetailScreen.id,
          arguments: GameRoomDetailArguments(roomId: feedItem.navId),
        );
      },
    );
  }
}
