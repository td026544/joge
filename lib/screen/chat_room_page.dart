// import 'package:boardgame/arguments/game_room_detail_arguments.dart';
// import 'package:boardgame/arguments/rating_room_arguments.dart';
// import 'package:boardgame/components/loading_more_place_holder.dart';
// import 'package:boardgame/controller/chat_room_view_controller_old.dart';
// import 'package:boardgame/models/constants.dart';
// import 'package:boardgame/models/message.dart';
// import 'package:boardgame/models/url_getter.dart';
// import 'package:boardgame/providers/messages_change_notifiyer_provider.dart';
// import 'package:boardgame/repository/messages_repository.dart';
// import 'package:boardgame/screen/rating_page.dart';
// import 'package:bubble/bubble.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:timeago/timeago.dart' as timeago;
//
// import 'game_room_detail_page.dart';
//
// const String id = 'chat_room';
// final _fireStore = FirebaseFirestore.instance;
//
// class ChatRoomPage extends ConsumerStatefulWidget {
//   final DocumentSnapshot chatRoomDoc;
//   static const String id = '/chat_rome_page';
//
//   const ChatRoomPage({Key? key, required this.chatRoomDoc}) : super(key: key);
//
//   @override
//   _ChatRoomPageState createState() => _ChatRoomPageState();
// }
//
// class _ChatRoomPageState extends ConsumerState<ChatRoomPage> {
//   @override
//   void initState() {
//     ref.read(chatRoomViewControllerOld).initState(widget.chatRoomDoc);
//
//     super.initState();
//   }
//
//   @override
//   void deactivate() {
//     ref.read(chatRoomViewControllerOld).dispose();
//     super.deactivate();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(onWillPop: () async {
//       // context
//       //     .read(messageCountChangeNotifier)
//       //     .upDateRoomReadCount(context.read(chatRoomViewController).roomId);
//       print('onWillPop');
//       return true;
//     }, child: Consumer(
//       builder: (context, ref, child) {
//         final controller = ref.watch(chatRoomViewControllerOld);
//         return Scaffold(
//           appBar: AppBar(
//             actions: <Widget>[
//               PopupMenuButton(
//                 icon: Icon(Icons.more_vert),
//                 itemBuilder: (BuildContext context) => <PopupMenuEntry>[
//                   PopupMenuItem(
//                     child: TextButton(
//                       child: Text(
//                         '退出此團',
//                         style: TextStyle(color: Colors.black87),
//                       ),
//                       onPressed: () async {
//                         ref
//                             .read(chatRoomViewControllerOld)
//                             .exitRoom(controller.chatRoomDoc.id);
//                         // read.controller.exitRoom(room.id);
//                       },
//                     ),
//                   ),
//                   PopupMenuItem(
//                     child: TextButton(
//                       child: Text(
//                         '檢視聚會',
//                         style: TextStyle(color: Colors.black87),
//                       ),
//                       onPressed: () async {
//                         Navigator.pushNamed(
//                           context,
//                           GameRoomDetailScreen.id,
//                           arguments: GameRoomDetailArguments(
//                               roomId: controller.chatRoomDoc.id),
//                         );
//                         // read.controller.exitRoom(room.id);
//                       },
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//             title: Text(controller.chatRoomDoc['title']),
//             bottom: PreferredSize(
//               preferredSize: const Size.fromHeight(40.0),
//               child: GestureDetector(
//                 child: Container(
//                   height: 40,
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       CircleAvatar(
//                         maxRadius: 16,
//                         backgroundColor: Colors.brown.shade800,
//                         child: Text('ss'),
//                       )
//                     ],
//                   ),
//                 ),
//                 onTap: () {
//                   showDialog<void>(
//                     context: context,
//                     builder: (context) => AlertDialog(
//                       title: Text("成員"),
//                       content: Container(
//                         height: 160,
//                         width: 200,
//                         child: new ListView.builder(
//                           shrinkWrap: true,
//                           itemCount: (controller.chatRoomDoc['attendees']
//                                   as List<String>)
//                               .length,
//                           itemBuilder: (context, index) {
//                             final uid =
//                                 controller.chatRoomDoc['attendees'][index];
//                             //todo
//                             // final nickname =
//                             //     chatRoomDoc.attendeesInfo?[index].nickname;
//                             // final nickname =
//                             //     room.data()['attendeesInfo'][uid]['nickname'];
//                             return new ListTile(
//                               title: new Text('nickname'),
//                               trailing: IconButton(
//                                 icon: Icon(Icons.close),
//                                 onPressed: () {
//                                   if (FirebaseAuth.instance.currentUser?.uid ==
//                                       controller.chatRoomDoc['creator.id']) {
//                                     ref
//                                         .read(chatRoomViewControllerOld)
//                                         .kickOffAttendee(
//                                             controller.chatRoomDoc.id, uid!);
//                                     // controller.kickOffAttendee(room.id, uid);
//                                   }
//                                 },
//                               ),
//                             );
//                           },
//                         ),
//                       ),
//                       actions: <Widget>[
//                         TextButton(
//                           child: Text("CLOSE"),
//                           onPressed: () {
//                             ref
//                                 .read(chatRoomViewControllerOld)
//                                 .textEditingController
//                                 .dispose();
//                           },
//                         )
//                       ],
//                     ),
//                   );
//                 },
//               ),
//             ),
//           ),
//           body: SafeArea(
//             child: Container(
//               child: Flex(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                 direction: Axis.vertical,
//                 children: <Widget>[
//                   MaterialBanner(
//                     content: const Text('給你剛認識的朋友一個評價吧'),
//                     leading: CircleAvatar(
//                       child: Icon(
//                         Icons.star,
//                         color: Colors.yellow,
//                       ),
//                       backgroundColor: Colors.deepPurple[200],
//                     ),
//                     forceActionsBelow: true,
//                     actions: [
//                       TextButton(
//                         child: const Text('去評價'),
//                         onPressed: () {
//                           Navigator.pushNamed(context, RatingPage.id,
//                               arguments: RatingRoomArgument(
//                                   controller.chatRoomDoc,
//                                   controller.chatRoomDoc['title'],
//                                   controller.chatRoomDoc.id));
//                         },
//                       ),
//                     ],
//                   ),
//                   Consumer(
//                     builder: (context, watch, child) {
//                       final localMessageProvider = ref.watch(
//                           messagesChangeNotifierProvider(controller.roomId));
//                       return LoadingMorePlaceHolder(
//                           isLoading: localMessageProvider.isLoading);
//                     },
//                   ),
//                   ChatListView(
//                     roomId: controller.roomId,
//                   ),
//                   ChatInput(
//                     chatRoomId: controller.chatRoomDoc.id,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         );
//       },
//     ));
//   }
// }
//
// class ChatListView extends StatefulWidget {
//   final String roomId;
//
//   const ChatListView({Key? key, required this.roomId}) : super(key: key);
//
//   @override
//   _ChatListViewState createState() => _ChatListViewState();
// }
//
// class _ChatListViewState extends State<ChatListView> {
//   final List<Message> allMessages = <Message>[];
//   // @override
//   // void deactivate() {
//   //
//   //   super.deactivate();
//   // }
//
//   @override
//   Widget build(BuildContext context) {
//     return Consumer(
//       builder: (context, ref, child) {
//         AsyncValue<List<Message>> streamMessagesProvider =
//             ref.watch(remoteMessageFutureProvider(widget.roomId));
//         final localMessageProvider =
//             ref.watch(messagesChangeNotifierProvider(widget.roomId));
//         return streamMessagesProvider.when(
//             data: (messages) {
//               allMessages.clear();
//               allMessages.addAll(messages);
//               allMessages.addAll(localMessageProvider.localMessages);
//
//               return Expanded(
//                 child: ListView.builder(
//                   controller: localMessageProvider.scrollController,
//                   reverse: true,
//                   itemCount: allMessages.length,
//                   itemBuilder: (context, index) {
//                     return MessageBubble(
//                       message: allMessages[index],
//                       isMe: FirebaseAuth.instance.currentUser!.uid ==
//                           allMessages[index].sender,
//                       isNotify: allMessages[index].sender == "NOTIFY",
//                     );
//                   },
//                 ),
//               );
//             },
//             loading: (data) => Expanded(
//                   child: ListView.builder(
//                     controller: localMessageProvider.scrollController,
//                     reverse: true,
//                     itemCount: allMessages.length,
//                     itemBuilder: (context, index) {
//                       return MessageBubble(
//                         message: allMessages[index],
//                         isMe: FirebaseAuth.instance.currentUser!.uid ==
//                             allMessages[index].sender,
//                         isNotify: allMessages[index].sender == "NOTIFY",
//                       );
//                     },
//                   ),
//                 ),
//             error: (data, e, s) => Text('error'));
//       },
//     );
//   }
// }
//
// class MessageBubble extends ConsumerWidget {
//   MessageBubble(
//       {required this.message, required this.isMe, required this.isNotify});
//   late final Message message;
//   late final bool isMe;
//   late final bool isNotify;
//
//   CrossAxisAlignment getCrossAxisAlignment() {
//     if (message.sender == "NOTIFY") {
//       return CrossAxisAlignment.center;
//     } else {
//       return isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start;
//     }
//   }
//
//   MainAxisAlignment getMainAxisAlignment() {
//     if (message.sender == "NOTIFY") {
//       return MainAxisAlignment.center;
//     } else {
//       return isMe ? MainAxisAlignment.end : MainAxisAlignment.start;
//     }
//   }
//
//   Alignment getAlignment() {
//     if (message.sender == "NOTIFY") {
//       return Alignment.topCenter;
//     } else {
//       return isMe ? Alignment.topRight : Alignment.topLeft;
//     }
//   }
//
//   Widget getNotify() {
//     return Center(
//       child: Bubble(
//         color: Colors.grey[200],
//         margin: BubbleEdges.only(top: 10),
//         radius: Radius.circular(20.0),
//         child: Text(message.text),
//       ),
//     );
//   }
//
//   Widget getMessage(BuildContext context) {
//     if (isNotify) return getNotify();
//     if (isMe) {
//       return meMessage(context);
//     } else {
//       return otherMessage(context);
//     }
//   }
//
//   Widget otherMessage(BuildContext context) {
//     String nickname =
//         context.read(chatRoomViewControllerOld).getNickName(message.sender);
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.start,
//       mainAxisSize: MainAxisSize.max,
//       children: [
//         CachedNetworkImage(
//           imageUrl: URLGetter.getAvatarUrl(message.sender),
//           fit: BoxFit.fill,
//           imageBuilder: (context, imageProvider) => CircleAvatar(
//             radius: 20,
//             backgroundImage: imageProvider,
//           ),
//         ),
//         Column(
//           mainAxisSize: MainAxisSize.min,
//           crossAxisAlignment: getCrossAxisAlignment(),
//           children: <Widget>[
//             Text(
//               nickname,
//               style: TextStyle(
//                 fontSize: 12.0,
//                 color: Colors.black54,
//               ),
//             ),
//             Bubble(
//               color:
//                   isMe ? Theme.of(context).primaryColorLight : Colors.grey[200],
//               margin: BubbleEdges.only(top: 10),
//               radius: Radius.circular(20.0),
//               nip: isMe ? BubbleNip.rightTop : BubbleNip.leftTop,
//               child: Text(message.text),
//             ),
//             Text(
//               timeago.format(
//                   Timestamp.fromMillisecondsSinceEpoch(message.time).toDate(),
//                   locale: 'zh'),
//               style: TextStyle(
//                 fontSize: 12.0,
//                 color: Colors.black54,
//               ),
//             ),
//           ],
//         )
//       ],
//     );
//   }
//
//   Widget meMessage(BuildContext context, WidgetRef ref) {
//     String nickname = 'nickname';
//     try {
//       nickname =
//           ref.read(chatRoomViewControllerOld).getNickName(message.sender);
//     } catch (e) {}
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.end,
//       mainAxisSize: MainAxisSize.max,
//       children: [
//         Column(
//           mainAxisSize: MainAxisSize.min,
//           crossAxisAlignment: CrossAxisAlignment.end,
//           children: <Widget>[
//             Text(
//               nickname,
//               style: TextStyle(
//                 fontSize: 12.0,
//                 color: Colors.black54,
//               ),
//             ),
//             Bubble(
//               color:
//                   isMe ? Theme.of(context).primaryColorLight : Colors.grey[200],
//               margin: BubbleEdges.only(top: 10),
//               radius: Radius.circular(20.0),
//               nip: isMe ? BubbleNip.rightTop : BubbleNip.leftTop,
//               child: Text(message.text),
//             ),
//             Text(
//               timeago.format(
//                   Timestamp.fromMillisecondsSinceEpoch(message.time).toDate(),
//                   locale: 'zh'),
//               style: TextStyle(
//                 fontSize: 12.0,
//                 color: Colors.black54,
//               ),
//             ),
//           ],
//         ),
//         CachedNetworkImage(
//           imageUrl: URLGetter.getAvatarUrl(message.sender),
//           fit: BoxFit.fill,
//           imageBuilder: (context, imageProvider) => CircleAvatar(
//             radius: 20,
//             backgroundImage: imageProvider,
//           ),
//         ),
//       ],
//     );
//   }
//
//   @override
//   Widget build(BuildContext context, watch) {
//     return Padding(padding: EdgeInsets.all(10.0), child: getMessage(context));
//   }
// }
//
// class ChatInput extends ConsumerStatefulWidget {
//   final String chatRoomId;
//
//   const ChatInput({Key? key, required this.chatRoomId}) : super(key: key);
//
//   @override
//   _ChatInputState createState() => _ChatInputState();
// }
//
// class _ChatInputState extends ConsumerState<ChatInput> {
//   late TextEditingController textEditingController;
//   bool isSendAble = false;
//
//   @override
//   void initState() {
//     textEditingController = TextEditingController();
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     textEditingController.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Material(
//       elevation: 8,
//       child: Row(
//         children: [
//           Expanded(
//             child: TextField(
//               controller: textEditingController,
//               onChanged: (value) {
//                 if (textEditingController.text.isNotEmpty) {
//                   isSendAble = true;
//                 } else {
//                   isSendAble = false;
//                 }
//                 setState(() {});
//               },
//               decoration: kMessageTextFieldDecoration,
//             ),
//           ),
//           IconButton(
//             icon: Icon(
//               Icons.send,
//               color: isSendAble ? Colors.lightBlue : Colors.grey,
//             ),
//             onPressed: isSendAble
//                 ? () {
//                     ref.read(chatRoomViewControllerOld).addMessage(
//                         widget.chatRoomId, textEditingController.text);
//                     textEditingController.clear();
//                     isSendAble = false;
//                     setState(() {});
//                   }
//                 : null,
//           ),
//         ],
//       ),
//     );
//   }
// }
