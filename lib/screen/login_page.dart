import 'package:boardgame/authentication_service.dart';
import 'package:boardgame/components/rounded_button.dart';
import 'package:boardgame/models/constants.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/screen/register/register_nickname_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:google_sign_in/google_sign_in.dart';

final GoogleSignIn googleSignIn = GoogleSignIn();
final userRef = FirebaseFirestore.instance.collection('users');

class LoginScreen extends StatefulWidget {
  static const String id = '/login_screen';
  final bool isNavToRegisterPage;

  const LoginScreen({Key? key, this.isNavToRegisterPage = false})
      : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email = "";
  String password = "";
  @override
  void initState() {
    super.initState();
    if (FirebaseAuth.instance.currentUser != null) {
      if (widget.isNavToRegisterPage) {
        Future.microtask(
            () => Navigator.pushNamed(context, RegisterNicknamePage.id));
      } else {
        // FirebaseAuth.instance.signOut();
      }
    }

    // FirebaseAuth.instance.signOut();

    // if (widget.isNavToRegisterPage) {
    //   if (mounted) {
    //     setState(() {

    //     });
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        AuthenticationService authenticationService =
            ref.watch(authServiceProvider);
        // if (widget.isNavToRegisterPage && mounted) {
        //   Navigator.pushNamed(context, RegisterGenderPage.id);
        // }
        return Scaffold(
          backgroundColor: Colors.white,
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                  child: Container(
                    height: 200.0,
                  ),
                ),
                SizedBox(
                  height: 48.0,
                ),
                TextField(
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    email = value;
                  },
                  decoration: kTextFieldDecoration.copyWith(
                      hintText: 'Enter your email'),
                ),
                SizedBox(
                  height: 8.0,
                ),
                TextField(
                  obscureText: true,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    password = value;
                  },
                  decoration: kTextFieldDecoration.copyWith(
                      hintText: 'Enter your password'),
                ),
                SizedBox(
                  height: 24.0,
                ),
                SignInButton(
                  Buttons.Google,
                  text: "Sign up with Google",
                  onPressed: () async {
                    authenticationService.googleSignIn();
                    // await _authController.handleGoogleSignIn();
                    // _authController.isAuth.value = true;
                  },
                ),
                SignInButton(
                  Buttons.FacebookNew,
                  text: "Sign up with Facebook",
                  onPressed: () {
                    authenticationService.facebookSignIn();

                    // _authController.handleFacebookSignIn();
                    // _authController.isAuth.value = true;
                  },
                ),
                RoundedButton(
                  title: 'Log In',
                  colour: Colors.lightBlueAccent,
                  onPressed: () async {
                    authenticationService.signInWithEmailAndPassword(
                        email, password);
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

// class LoginScreen extends StatefulWidget {
//
//   @override
//   _LoginScreenState createState() => _LoginScreenState();
// }
//
// class _LoginScreenState extends State<LoginScreen> {
//   bool showSpinner = false;
//   String email;
//   String password;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return
//   }
//
//
//   Future logIn(BuildContext context) async {
//     setState(() {
//       showSpinner = true;
//     });
//     try {
//       final user = await FirebaseAuth.instance
//           .signInWithEmailAndPassword(email: email, password: password);
//       if (user != null) {
//         // user.user.
//       }
//       setState(() {
//         showSpinner = false;
//       });
//     } catch (e) {
//       setState(() {
//         showSpinner = false;
//       });
//       print(e);
//     }
//   }
// }
