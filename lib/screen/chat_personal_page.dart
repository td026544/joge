import 'dart:async';

import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/components/message_bubble.dart';
import 'package:boardgame/components/my_auto_complete.dart';
import 'package:boardgame/components/raw_auto_complete.dart' as My;
import 'package:boardgame/components/type_indicator.dart';
import 'package:boardgame/controller/chat_room_view_controller.dart';
import 'package:boardgame/models/chat_message.dart';
import 'package:boardgame/models/conversation.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/chat_messages_state_notifyier_provider.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:boardgame/utils/date_utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' hide Category;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ChatPersonalScreen extends ConsumerStatefulWidget {
  const ChatPersonalScreen({required this.conversation, Key? key})
      : super(key: key);
  static const String id = '/chat_personal_screen';

  final Conversation conversation;

  @override
  _ChatPersonalScreenState createState() => _ChatPersonalScreenState();
}

class _ChatPersonalScreenState extends ConsumerState<ChatPersonalScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            iconTheme: IconThemeData(
              color: Colors.orange, //change your color here
            ),
            title: Row(
              children: [
                GestureDetector(
                  child: ClipOval(
                    child: CachedNetworkImage(
                      imageUrl:
                          URLGetter.getAvatarUrl(widget.conversation.toId),
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      width: 40.0,
                      height: 40.0,
                    ),
                  ),
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      PersonalPageById.id,
                      arguments: PersonalByIdArguments(
                        uid: widget.conversation.toId,
                      ),
                    );
                  },
                ),
                SizedBox(width: 16),
                Text(
                  widget.conversation.name,
                  style: TextStyle(color: Colors.black87),
                ),
              ],
            )),
        body: SafeArea(
          child: ChatListView(
            chatId: widget.conversation.chatId,
            conversation: widget.conversation,
            enableSend: true,
          ),
        ),
      ),
    );
  }
}

class ChatListView extends ConsumerStatefulWidget {
  final String chatId;
  final Conversation conversation;
  final bool enableSend;

  const ChatListView(
      {this.enableSend = true,
      Key? key,
      required this.chatId,
      required this.conversation})
      : super(key: key);

  @override
  _ChatListViewState createState() => _ChatListViewState();
}

class _ChatListViewState extends ConsumerState<ChatListView>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  static String _displayStringForOption(MyUser option) => option.nickname;
  String myUserId = "";
  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    MyUser? myUser = ref.read(userStateProvider).state;
    myUserId = myUser?.id ?? "";
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.inactive) {
      if (ref.read(chatRoomViewController(widget.conversation)).isMyTyping ==
          true) {
        ref
            .read(chatUseCaseProvider)
            .sendMessageToServerTyping(widget.chatId, false);
      }
    }
    if (state == AppLifecycleState.resumed) {}
  }

  // String buildTime(int lastTime,int time) {
  //   DateTime lastDateTime = DateTime.fromMillisecondsSinceEpoch(lastTime);
  //   DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(time);
  //   if(lastTime.)
  //   return DateFormat.jm('zh').format(dateTime);
  // }
  isLastMessageSameDay(List<ChatMessage> allMessages, int index) {
    if (index + 1 < allMessages.length) {
      int lastTime = allMessages[index + 1].time;
      int time = allMessages[index].time;
      return MyTimeUtils.isLastTimeSameDay(lastTime, time);
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Flex(
      direction: Axis.vertical,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Consumer(
        //   builder: (context, watch, child) {
        //     final provider =
        //         watch(chatMessagesStateNotifierProvider(widget.chatId));
        //     return LoadingMorePlaceHolder(isLoading: controller.isLoading);
        //   },
        // ),
        Expanded(
          child: CustomScrollView(
            controller: ref
                .read(chatRoomViewController(widget.conversation))
                .scrollController,
            reverse: true,
            slivers: [
              Consumer(
                builder: (context, watch, child) {
                  final isTyping = ref
                      .watch(typingStateStateNotifierProvider(widget.chatId))
                      .state;
                  return SliverToBoxAdapter(
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: TypingIndicator(
                        showIndicator: isTyping,
                      ),
                    ),
                  );
                },
              ),
              Consumer(
                builder: (context, watch, child) {
                  final chatMessagesAsyncData = ref.watch(
                    chatMessagesStateNotifierProvider(widget.chatId),
                  );
                  return chatMessagesAsyncData.maybeWhen(data: (messages) {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) {
                        return MessageBubble(
                          message: messages[index],
                          isMe: myUserId == messages[index].senderId,
                          nickname: ref
                                      .read(chatRoomViewController(
                                          widget.conversation))
                                      .toUser !=
                                  null
                              ? ref
                                  .read(chatRoomViewController(
                                      widget.conversation))
                                  .toUser!
                                  .nickname
                              : widget.conversation.name,
                          bubbleShape: ref
                              .read(chatRoomViewController(widget.conversation))
                              .getMessageShowType(messages, index),
                          isLastMessageSameDay:
                              isLastMessageSameDay(messages, index),
                          senderId: messages[index].senderId,
                        );
                      }, childCount: messages.length),
                    );
                  }, orElse: () {
                    return SliverToBoxAdapter(
                      child: CircularProgressIndicator(),
                    );
                  });
                },
              ),
            ],
          ),
        ),

        Consumer(
          builder: (context, ref, child) {
            final controller =
                ref.watch(chatRoomViewController(widget.conversation));

            return Row(
              children: [
                IconButton(
                  onPressed: () async {
                    await controller.pickImage(context);
                    final snackBar = SnackBar(content: Text('正在傳送你的照片'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                  icon: Icon(Icons.add),
                  color: Colors.grey,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 8, bottom: 8, left: 0, right: 8),
                    child: My.RawAutocomplete<MyUser>(
                      focusNode: controller.textFieldFocusNode,
                      textEditingController: controller.textEditingController,
                      optionsViewBuilder: (BuildContext context,
                          AutocompleteOnSelected<MyUser> onSelected,
                          Iterable<MyUser> options) {
                        return AutocompleteOptions<MyUser>(
                          displayStringForOption: _displayStringForOption,
                          onSelected: onSelected,
                          options: options,
                        );
                      },
                      displayStringForOption: _displayStringForOption,
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        if (textEditingValue.text == '') {
                          return const Iterable<MyUser>.empty();
                        }
                        if (textEditingValue
                                .text[textEditingValue.text.length - 1] ==
                            '@') {
                          return controller.toUsers;
                        }
                        RegExp lastReg =
                            RegExp(r'^.*[@][\u4E00-\u9FA5A-Za-z0-9]+$');
                        if (textEditingValue.text.contains(lastReg)) {
                          List<String> strings =
                              textEditingValue.text.split('@');
                          return controller.toUsers.where(
                            (MyUser option) {
                              return option.nickname.contains(strings.last);
                            },
                          );
                        }

                        if (controller.toUsers.contains(lastReg)) {
                          return controller.toUsers.where(
                            (MyUser option) {
                              return option.nickname.contains(
                                  textEditingValue.text.toLowerCase());
                            },
                          );
                        }
                        return const Iterable<MyUser>.empty();
                      },
                      onSelected: (MyUser selection) {
                        print(
                            'You just selected ${_displayStringForOption(selection)}');
                      },
                      fieldViewBuilder: (BuildContext context,
                          TextEditingController fieldTextEditingController,
                          FocusNode fieldFocusNode,
                          VoidCallback onFieldSubmitted) {
                        return TextField(
                          textInputAction: TextInputAction.newline,
                          keyboardType: TextInputType.multiline,
                          maxLines: 7,
                          minLines: 1,
                          enabled: widget.enableSend,
                          focusNode: fieldFocusNode,
                          controller: fieldTextEditingController,
                          decoration: InputDecoration(
                              suffixIcon: IconButton(
                                onPressed: () {
                                  if (controller.emojiShowing) {
                                    controller.emojiShowing = false;
                                    controller.textFieldFocusNode
                                        .requestFocus();
                                  } else {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                    controller.emojiShowing = true;
                                  }
                                },
                                icon: controller.emojiShowing
                                    ? Icon(Icons.keyboard, color: Colors.grey)
                                    : Icon(
                                        Icons.emoji_emotions_outlined,
                                        color: Colors.grey,
                                      ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(40.0),
                                ),
                                borderSide:
                                    BorderSide(width: 1, color: Colors.grey),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(40.0),
                                ),
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black38),
                              ),
                              contentPadding: EdgeInsets.all(16),
                              filled: true,
                              hintText: widget.enableSend ? null : '無法輸入訊息',
                              fillColor: Colors.white),
                          onTap: () {
                            Timer(
                                Duration(milliseconds: 200),
                                () => controller.scrollController.animateTo(0.0,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.easeIn));
                          },
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 48,
                  width: 48,
                  child: Consumer(
                    builder: (context, ref, child) {
                      final isSendAble =
                          ref.watch(isSendAbleStateProvider).state;
                      return IconButton(
                        padding: new EdgeInsets.all(0.0),
                        iconSize: 28,
                        icon: Icon(
                          Icons.send_rounded,
                          color: isSendAble ? Colors.lightBlue : Colors.grey,
                        ),
                        onPressed: isSendAble ? controller.onSendMessage : null,
                      );
                    },
                  ),
                ),
              ],
            );
          },
        ),
        Consumer(
          builder: (context, ref, child) {
            final controller =
                ref.watch(chatRoomViewController(widget.conversation));
            return Offstage(
              offstage: !controller.emojiShowing,
              child: SizedBox(
                height: 250,
                child: EmojiPicker(
                    onEmojiSelected: (Category category, Emoji emoji) {
                      controller.onEmojiSelected(emoji);
                    },
                    onBackspacePressed: controller.onBackspacePressed,
                    config: const Config(
                        columns: 7,
                        emojiSizeMax: 32.0,
                        verticalSpacing: 0,
                        horizontalSpacing: 0,
                        initCategory: Category.RECENT,
                        bgColor: Color(0xFFF2F2F2),
                        indicatorColor: Colors.blue,
                        iconColor: Colors.grey,
                        iconColorSelected: Colors.blue,
                        progressIndicatorColor: Colors.blue,
                        backspaceColor: Colors.blue,
                        showRecentsTab: true,
                        recentsLimit: 28,
                        noRecentsText: 'No Recents',
                        noRecentsStyle:
                            TextStyle(fontSize: 20, color: Colors.black26),
                        categoryIcons: CategoryIcons(),
                        buttonMode: ButtonMode.MATERIAL)),
              ),
            );
          },
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
