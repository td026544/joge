import 'package:boardgame/screen/home_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

final userRef = FirebaseFirestore.instance.collection('users');

class FirstSettingScreen extends StatefulWidget {
  static const String id = '/first_setting_screen';

  FirstSettingScreen();

  @override
  _FirstSettingScreenState createState() => _FirstSettingScreenState();
}

class _FirstSettingScreenState extends State<FirstSettingScreen> {
  final nameEditingController = TextEditingController();
  var isReadOnly = true;
  // FocusNode myFocusNode;
  User? user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    nameEditingController.text = user!.displayName!;
    // TODO: implement initState
    super.initState();
    // myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // myFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('個人資料'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            SizedBox(
              height: 16,
            ),
            CircleAvatar(
              radius: 36,
              child: Text('a'),
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 200,
                  child: TextFormField(
                    controller: nameEditingController,
                    decoration: InputDecoration(
                      labelText: 'Label text',
                      border: OutlineInputBorder(),
                      // suffixIcon: Icon(
                      //   Icons.error,
                      // ),
                    ),
                  ),
                  // child: TextFormField(
                  //   controller: nameEditingController,
                  //   decoration: InputDecoration(
                  //     labelText: '暱稱',
                  //   ),
                  //   onFieldSubmitted: (value) {},
                  // ),
                ),
              ],
            ),
            RaisedButton(
              child: Text('完成'),
              onPressed: () {
                userRef
                    .doc(user?.uid)
                    .set({
                      "email": user?.email,
                      "gender": "",
                      "age": "",
                      "phoneNumber": user?.phoneNumber,
                      "username": user?.displayName,
                      "nickname": nameEditingController.text,
                      "pushToken": "",
                      "createTime": DateTime.now().millisecondsSinceEpoch,
                      "rooms": [],
                    })
                    .then(
                        (value) => Navigator.pushNamed(context, HomeScreen.id))
                    .catchError((e) {
                      final snackBar = SnackBar(content: Text('完成失敗!'));

                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      // Navigator.of(context).pop(false);
                    });
              },
            )
          ],
        ),
      ),
    );
  }
}
