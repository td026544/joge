import 'package:boardgame/components/new_room_card.dart';
import 'package:boardgame/components/rating_history_card.dart';
import 'package:boardgame/providers/ratings_list_change_notifiyier.dart';
import 'package:boardgame/repository/current_room_change_notifiyer_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class MyGameRoomTabListView extends ConsumerStatefulWidget {
  const MyGameRoomTabListView({Key? key}) : super(key: key);

  @override
  _MyGameRoomTabListViewState createState() => _MyGameRoomTabListViewState();
}

class _MyGameRoomTabListViewState extends ConsumerState<MyGameRoomTabListView> {
  @override
  Widget build(BuildContext context) {
    final asyncData = ref.watch(myGameRoomStateNotifierProvider);
    return asyncData.maybeWhen(
        data: (datas) {
          return ListView.builder(
            itemCount: datas.length,
            itemBuilder: (context, index) => NewRoomCard(
              data: datas[index],
            ),
          );
        },
        orElse: () => CircularProgressIndicator());
  }
}

class RatesTabListView extends ConsumerStatefulWidget {
  final String userId;

  RatesTabListView(this.userId);

  @override
  _RatesTabListViewState createState() => _RatesTabListViewState();
}

class _RatesTabListViewState extends ConsumerState<RatesTabListView> {
  @override
  Widget build(BuildContext context) {
    final ratingStateNotifier =
        ref.watch(ratingsStateNotifierProvider(widget.userId).notifier);

    return PagedListView<int, Map<String, dynamic>>(
      pagingController: ratingStateNotifier.pagingController,
      builderDelegate: PagedChildBuilderDelegate<Map<String, dynamic>>(
        animateTransitions: false,
        transitionDuration: const Duration(milliseconds: 500),
        itemBuilder: (context, item, index) => RatingDisplayCard(
          data: item,
          toId: widget.userId,
        ),
        firstPageProgressIndicatorBuilder: (_) => CircleAvatar(
          backgroundColor: Colors.red,
        ),
        newPageProgressIndicatorBuilder: (_) => CircleAvatar(
          backgroundColor: Colors.blue,
        ),
        noItemsFoundIndicatorBuilder: (_) => Text('沒發現'),
        noMoreItemsIndicatorBuilder: (_) => Text('已經到底了'),
      ),
    );
  }
}
