import 'package:badges/badges.dart';
import 'package:boardgame/managers/mqtt_manager.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/bookmarks_provider.dart';
import 'package:boardgame/providers/conversations_cards_provider.dart';
import 'package:boardgame/providers/friends_provider.dart';
import 'package:boardgame/providers/geo_provider.dart';
import 'package:boardgame/providers/message_notify_provider.dart';
import 'package:boardgame/providers/my_chat_room_change_notifyer.dart';
import 'package:boardgame/providers/unread_feeds_state_notifyer.dart';
import 'package:boardgame/repository/feeds_repository.dart';
import 'package:boardgame/repository/user_repository.dart';
import 'package:boardgame/screen/game_lobby_page.dart';
import 'package:boardgame/screen/register/register_nickname_page.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'feed_page.dart';
import 'message_center_page.dart';
import 'personal_page.dart';

class HomeScreen extends ConsumerStatefulWidget {
  static const String id = '/home_screen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

final bottomNavigationIndexProvider = StateProvider<int>((ref) => 0);

class _HomeScreenState extends ConsumerState<HomeScreen> {
  @override
  void initState() {
    MyUser? user = ref.read(userStateProvider).state;
    print('NOOOOO');
    // if (user != null) {
    //   ifNotHaveTokenUpdateToken();
    // }
    super.initState();
    init();
  }

  init() async {
    await ref.read(chatUseCaseProvider).reloadChats();
  }

  ifNotHaveTokenUpdateToken() async {
    String? token = await getToken();
    if (token != null) {
      ref.read(userRepository).updateMyToken(token);
    }
  }

  checkIsNeedSettingProfile() async {
    bool isUserExist =
        await isUserInFireStore(FirebaseAuth.instance.currentUser!);

    if (!isUserExist) {
      Navigator.pushNamedAndRemoveUntil(
          context, RegisterNicknamePage.id, (Route<dynamic> route) => false);
    } else {
      MyUser? myUser = await ref
          .read(userRepository)
          .getUser(FirebaseAuth.instance.currentUser!.uid);
      ref.read(userStateProvider).state = myUser;
    }
  }

  Future<String?> getToken() async {
    // FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
    // return _firebaseMessaging.getToken().then((token) {
    //   return token; // Print the Token in Console
    // });
  }

  Future<bool> isUserInFireStore(User user) async {
    final userRef = FirebaseFirestore.instance.collection('users');
    final DocumentSnapshot doc = await userRef.doc(user.uid).get();
    return doc.exists;
  }

  final List<Widget> _bodys = [
    GameLobbyPage(),
    MessageCenterScreen(),
    FeedPage(),
    PersonalPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Consumer(builder: (context, watch, child) {
        final mqttManager = ref.watch(mqttProvider);
        final conversations = ref.watch(conversationsStateNotifierProvider);
        final remoteFeedFutureProvider =
            ref.watch(remoteFeedItemFutureProvider);
        int index = ref.watch(bottomNavigationIndexProvider).state;

        AsyncValue<List<String>> bookMarks =
            ref.watch(bookmarkStateNotifierProvider);
        AsyncValue<Map<String, dynamic>> friendsAsyncValue =
            ref.watch(rawFriendsStreamProvider);
        AsyncValue<GeoPoint?> geoPoint = ref.watch(geoProvider);

        // final roomsChangeNotifier = watch(myChatRoomChangeNotifier);

        return Scaffold(
          body: _bodys[index],
          bottomNavigationBar: BottomNavigationBar(
            showSelectedLabels: true,
            showUnselectedLabels: true,
            selectedItemColor: Colors.orangeAccent,
            unselectedItemColor: Colors.grey,
            currentIndex: index,
            type: BottomNavigationBarType.fixed,
            onTap: (index) {
              ref.read(bottomNavigationIndexProvider).state = index;
              if (index == 1) {
                ref.read(isHaveNewMessageChangeNotifier).updateLeftOnRead();
              } else if (index == 2) {
                ref
                    .read(haveNewFeedsStateNotifierProvider.notifier)
                    .updateLeftOnRead();
              }
            },
            items: [
              BottomNavigationBarItem(
                  icon: Icon(
                    FontAwesomeIcons.frog,
                  ),
                  label: '首頁'),
              BottomNavigationBarItem(
                  icon: Consumer(
                    builder: (context, watch, child) {
                      final chatUnReadCountAsyncValue =
                          ref.watch(chatUnReadCountStateNotifierProvider);
                      return chatUnReadCountAsyncValue.maybeWhen(
                          orElse: () => Icon(FontAwesomeIcons.commentDots),
                          data: (count) {
                            return Badge(
                              shape: BadgeShape.circle,
                              badgeColor: Colors.redAccent,
                              position: BadgePosition.topEnd(top: 1, end: -5),
                              child: Icon(FontAwesomeIcons.commentDots),
                              showBadge: count != 0,
                            );
                          });
                    },
                  ),
                  label: '聊天室'),
              BottomNavigationBarItem(
                  icon: Consumer(
                    builder: (context, watch, child) {
                      bool isHaveNewFeed =
                          ref.watch(haveNewFeedsStateNotifierProvider);
                      return Badge(
                        shape: BadgeShape.circle,
                        badgeColor: Colors.redAccent,

                        position: BadgePosition.topEnd(top: 1, end: -1),
                        child: Icon(Icons.notifications_rounded),
                        showBadge: isHaveNewFeed,
                        // badgeContent: Text(
                        //   unReadCount.toString(),
                        //   style: TextStyle(
                        //       color: Colors.white,
                        //       fontSize: 10,
                        //       fontWeight: FontWeight.bold),
                        // ),
                      );
                    },
                  ),
                  label: '通知'),
              // BottomNavigationBarItem(
              //     icon: Icon(Icons.notifications), label: '通知'),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: '個人'),
            ],
          ),
        );
      }),
    );
  }
}
