import 'package:boardgame/components/drop_chip.dart';
import 'package:boardgame/components/new_room_card.dart';
import 'package:boardgame/components/section_tile.dart';
import 'package:boardgame/models/lists.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/providers/game_room_change_notifiyer_provider.dart';
import 'package:boardgame/providers/geo_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

final _fireStore = FirebaseFirestore.instance;
final filterAreaStateProvider = StateProvider<int>((ref) => 0);

class AllPageView extends ConsumerStatefulWidget {
  @override
  _AllPageViewState createState() => _AllPageViewState();
}

class _AllPageViewState extends ConsumerState<AllPageView> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        ref.read(allRoomChangeNotifierProvider.notifier).refresh();
      },
      child: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Consumer(
                  builder: (context, ref, child) {
                    int areasSelectIndex =
                        ref.watch(filterAreaStateProvider).state;
                    return DropChip(
                      text: areas[areasSelectIndex],
                      isSelected: areasSelectIndex != 0,
                      onSelected: (v) async {
                        var result = await showModalBottomSheet<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return Flex(
                              direction: Axis.vertical,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.close,
                                        color: Colors.black54,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Text(
                                      '地區',
                                      style:
                                          Theme.of(context).textTheme.subtitle1,
                                    )
                                  ],
                                ),
                                Expanded(
                                  child: SingleChildScrollView(
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 24, vertical: 8),
                                      child: SectionTileListView(
                                        texts: areas,
                                        initSelectedIndex: areasSelectIndex,
                                        onSelectedIndexChanged: (index) {
                                          ref
                                              .read(filterAreaStateProvider)
                                              .state = index;
                                          Navigator.pop(context);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                        // setState(() {});
                      },
                    );
                  },
                ),
                Row(
                  children: [
                    Consumer(
                      builder: (context, ref, child) {
                        bool isShowGeo = ref.watch(isShowGeoProvider).state;
                        return Switch(
                          value: isShowGeo,
                          onChanged: (bool) {
                            ref.read(isShowGeoProvider).state = bool;
                          },
                        );
                      },
                    ),
                    Text(
                      '顯示距離',
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ],
                )
              ],
            ),
          ),
          Consumer(
            builder: (context, ref, child) {
              final allRoomChangeNotifier =
                  ref.watch(allRoomChangeNotifierProvider.notifier);
              return PagedSliverList<int, RoomDetail>(
                pagingController: allRoomChangeNotifier.pagingController,
                builderDelegate: PagedChildBuilderDelegate<RoomDetail>(
                  animateTransitions: false,
                  transitionDuration: const Duration(milliseconds: 500),
                  itemBuilder: (context, item, index) => NewRoomCard(
                    data: item,
                  ),
                  firstPageProgressIndicatorBuilder: (_) => CircleAvatar(
                    backgroundColor: Colors.red,
                  ),
                  newPageProgressIndicatorBuilder: (_) => CircleAvatar(
                    backgroundColor: Colors.blue,
                  ),
                  noItemsFoundIndicatorBuilder: (_) => Text('沒發現'),
                  noMoreItemsIndicatorBuilder: (_) => Text('已經到底了'),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
