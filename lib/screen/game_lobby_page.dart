import 'package:animations/animations.dart';
import 'package:boardgame/components/category_bottom_sheet.dart';
import 'package:boardgame/components/indicator.dart';
import 'package:boardgame/components/new_room_card.dart';
import 'package:boardgame/models/create_room_save_model.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/providers/bookmarks_provider.dart';
import 'package:boardgame/providers/create_room_state_provider.dart';
import 'package:boardgame/usecase/book_mark_usecase.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geolocator/geolocator.dart';

import 'hometabs/all_page_view.dart';
import 'new_create_room_page.dart';

final _fireStore = FirebaseFirestore.instance;

final paringRef = FirebaseFirestore.instance.collection('paring_system');

class GameLobbyPage extends StatefulWidget {
  @override
  _GameLobbyPageState createState() => _GameLobbyPageState();
}

class _GameLobbyPageState extends State<GameLobbyPage>
    with SingleTickerProviderStateMixin {
  bool isFabExtend = true;
  final tabs = [
    '為您推薦',
    '收藏',
    '熱門',
    '即將開始',
    '類別',
  ];
  var radius = Radius.circular(2);
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: tabs.length, vsync: this);
    getPos();
  }

  getPos() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.low);
    var a = 'b';
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        switch (notification.runtimeType) {
          case ScrollStartNotification:
            setState(() {
              isFabExtend = false;
            });
            break;
          case ScrollUpdateNotification:
            if (notification.metrics.pixels == 0) {
              setState(() {
                isFabExtend = true;
              });
            }
            break;
          case ScrollEndNotification:
            break;
          case OverscrollNotification:
            break;
        }
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          body: NestedScrollView(
            floatHeaderSlivers: true,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return [
                SliverPersistentHeader(
                  pinned: true,
                  delegate: StickyTabBarDelegate(
                    child: TabBar(
                      labelColor: Colors.orange,
                      unselectedLabelColor: Colors.black38,
                      indicatorColor: Colors.orange,
                      controller: tabController,
                      indicator: MD2Indicator(),
                      isScrollable: true,
                      tabs: [
                        for (final tab in tabs)
                          Tab(
                            text: tab,
                          ),
                      ],
                    ),
                  ),
                ),
              ];
            },
            body: TabBarView(
              controller: tabController,
              children: [
                AllPageView(),
                BookMarksPageView(),
                Text('3'),
                Text('4'),
                Text('5'),
              ],
            ),
          ),
          floatingActionButton: Consumer(
            builder: (context, ref, child) {
              CreateRoomSaveModel? createRoomSaveModel =
                  ref.watch(createRoomStateProvider).state;
              return OpenContainer(
                closedElevation: 4.0,
                closedColor: Theme.of(context).primaryColor,
                openColor: Colors.white,
                transitionDuration: const Duration(milliseconds: 500),
                closedShape: isFabExtend
                    ? RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(64.0),
                      )
                    : CircleBorder(),
                closedBuilder: (context, action) {
                  return FloatingActionButton.extended(
                    onPressed: null,
                    isExtended: isFabExtend,
                    label: Text(
                      '開局',
                      style: TextStyle(color: Colors.white),
                    ),
                    icon: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  );
                },
                openBuilder: (context, action) => NewCreateRoomPage(
                    createRoomSaveModel:
                        createRoomSaveModel ?? CreateRoomSaveModel.empty()),
              );
              return FloatingActionButton.extended(
                onPressed: () async {
                  // var result = await Navigator.pushNamed(
                  //   context,
                  //   CreateRoomPage.id,
                  // );
                  Navigator.pushNamed(context, NewCreateRoomPage.id);

                  setState(() {
                    isFabExtend = !isFabExtend;
                  });
                },
                isExtended: isFabExtend,
                label: Text(
                  '開局',
                  style: TextStyle(color: Colors.white),
                ),
                icon: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class BookMarksPageView extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ref) {
    List<String> bookmarks = ref.watch(bookmarkStateNotifierProvider).value;
    List<String> notExistBookmarks = [];

    return Column(
      children: [
        Row(
          children: [
            TextButton(
                onPressed: () {
                  showModalBottomSheet(
                    isScrollControlled: true,
                    context: context,
                    builder: (context) {
                      return CategoryBottomSheet(
                        onSelected: (categoryType) {
                          String category =
                              EnumToString.convertToString(categoryType);
                          print(category);
                        },
                      );
                    },
                  );
                },
                child: Text('分類')),
            Text('運動'),
          ],
        ),
        Expanded(
          child: ListView.builder(
              itemCount: bookmarks.length,
              itemBuilder: (
                context,
                index,
              ) {
                return Consumer(
                  builder: (context, ref, child) {
                    AsyncValue<DocumentSnapshot> bookMarkData = ref.watch(
                      myBookMarkProvider(bookmarks[index]),
                    );

                    return bookMarkData.when(
                      // data: (data) => Text(data['title']),
                      data: (doc) {
                        if (doc.exists) {
                          final item = doc.data() as Map<String, dynamic>;
                          if (notExistBookmarks.isNotEmpty &&
                              index == bookmarks.length - 1) {
                            ref
                                .read(bookmarkUseCaseProvider)
                                .removeBookmarks(notExistBookmarks);
                            notExistBookmarks.clear();
                          }
                          return NewRoomCard(
                            data: RoomDetail.fromJson(item),
                          );
                        } else {
                          notExistBookmarks.add(doc.id);
                          if (notExistBookmarks.isNotEmpty &&
                              index == bookmarks.length - 1) {
                            ref
                                .read(bookmarkUseCaseProvider)
                                .removeBookmarks(notExistBookmarks);
                            notExistBookmarks.clear();
                          }
                          return Text('empty');
                        }
                      },
                      loading: (data) => CircularProgressIndicator(),
                      error: (data, e, s) => Text('error'),
                    );
                  },
                );
              }),
        )
      ],
    );
  }
}

final myBookMarkProvider =
    FutureProvider.family<DocumentSnapshot, String>((ref, roomId) {
  return FirebaseFirestore.instance.collection('rooms').doc(roomId).get();
});

class StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar child;

  StickyTabBarDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      child: child,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.black12,
            width: 1.0,
          ),
        ),
        color: Theme.of(context).scaffoldBackgroundColor,
      ),
    );
  }

  @override
  double get maxExtent => child.preferredSize.height;

  @override
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
