import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CarouselOnBoardingPage extends StatefulWidget {
  static const String id = '/carousel_onboarding_page';

  const CarouselOnBoardingPage({Key? key}) : super(key: key);

  @override
  _CarouselOnBoardingPageState createState() => _CarouselOnBoardingPageState();
}

class _CarouselOnBoardingPageState extends State<CarouselOnBoardingPage> {
  final PageController controller = PageController(initialPage: 0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double imageSize = MediaQuery.of(context).size.width - 48;
    return Scaffold(
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: PageView(
              scrollDirection: Axis.horizontal,
              controller: controller,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      child: SvgPicture.asset('assets/images/onboarding/1.svg',
                          semanticsLabel: 'Acme Logo'),
                      width: imageSize,
                      height: imageSize,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      '找到志同道合的朋友',
                      style: TextStyle(fontSize: 24, color: Colors.black87),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      '找到制同',
                      style: TextStyle(fontSize: 15, color: Colors.black54),
                    ),
                    SizedBox(
                      height: 56,
                    ),
                  ],
                ),
                Center(
                  child: Text('Second Page'),
                ),
                Center(
                  child: Text('Third Page'),
                )
              ],
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Theme.of(context).scaffoldBackgroundColor,
            ),
            onPressed: () {},
            child: Text(
              '立即開始',
              style:
                  TextStyle(color: Colors.orange, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 24,
          ),
          SizedBox(
            height: 24,
          ),
          SmoothPageIndicator(
              controller: controller, // PageController
              count: 3,
              effect: WormEffect(
                  dotWidth: 12,
                  dotHeight: 12,
                  activeDotColor: Colors.orange), // your preferred effect
              onDotClicked: (index) {}),
          SizedBox(
            height: 24,
          )
        ],
      ),
    );
  }
}
