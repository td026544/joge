import 'package:boardgame/models/message.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/comments_provider.dart';
import 'package:boardgame/repository/chat_room_repository.dart';
import 'package:boardgame/repository/feeds_repository.dart';
import 'package:boardgame/repository/game_room_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final _fireStore = FirebaseFirestore.instance;

final textProvider = StateProvider.autoDispose<String>((ref) => "");
final sendAbleProvider = StateProvider.autoDispose<bool>((ref) => false);

final messagesCache = StateProvider<List<Message>>((ref) => []);

final gameRoomDetailViewController = ChangeNotifierProvider.autoDispose((ref) {
  return GameRoomDetailViewController(ref.read);
});

class GameRoomDetailViewController extends ChangeNotifier {
  late String text;
  bool _isSendAble = false;
  final Reader read;
  bool isLike = false;
  int likeCount = 0;

  GameRoomDetailViewController(this.read);
  late String roomId;

  init(String roomId) {
    this.roomId = roomId;
  }

  Future<void> exitRoom() async {
    await read(chatRoomRepository).exitRoom(roomId);
  }

  Future<void> kickOffAttendee(String userUid) async {
    await read(chatRoomRepository).kickOffAttendee(roomId, userUid);
  }

  Future<void> addMessage(String sendText) async {
    await read(chatRoomRepository).addMessage(roomId, sendText);
    isSendAble = false;
  }

  handleLikePost(String creatorId) async {
    if (isLike) {
      likeCount -= 1;
      isLike = false;
      // likes[FirebaseAuth.instance.currentUser!.uid] = false;
      read(gameRoomRepository).updateLike(roomId, isLike);
    } else {
      likeCount += 1;
      isLike = true;

      var isSuccess = await read(gameRoomRepository).updateLike(roomId, isLike);
      if (isSuccess) {
        read(feedsRepository).addLikeToFeed(creatorId, roomId);
      }
    }
    notifyListeners();
  }

  bool get isSendAble => _isSendAble;

  set isSendAble(bool value) {
    _isSendAble = value;
    notifyListeners();
  }

  addComment(String creatorId, String comment) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    var isSuccess = await read(gameRoomRepository).addComment(roomId, comment);
    if (isSuccess) {
      read(feedsRepository).addCommentToFeed(creatorId, roomId);
      read(justPostCommentChangeNotifierProvider(roomId).notifier)
          .addComment(comment);
    }
  }
}
