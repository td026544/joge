import 'dart:typed_data';

import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/register_user_data.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/repository/user_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter_riverpod/flutter_riverpod.dart';

final registerPageViewController =
    Provider((ref) => RegisterViewController(ref.read));

class RegisterViewController {
  final Reader read;
  late MyUser user;
  late RegisterUserData registerUserData;
  RegisterViewController(this.read) {
    registerUserData = RegisterUserData();
  }

  Future<String> uploadImageByte(Uint8List imageByte) async {
    try {
      String postId = DateTime.now().millisecondsSinceEpoch.toString();
      // Reference ref = firebase_storage.FirebaseStorage.instance.ref(
      //     'images/${FirebaseAuth.instance.currentUser!.uid + postId + ".jpg"}');
      Reference ref = firebase_storage.FirebaseStorage.instance.ref(
          '$USERS_PATH/${FirebaseAuth.instance.currentUser!.uid}/$AVATAR_NAME');
      await ref.putData(imageByte);

      String url = await ref.getDownloadURL();
      return url;
    } catch (e) {
      throw Exception('upload Image error');
      // e.g, e.code == 'canceled'
    }
  }

  Future<void> createUser() async {
    try {
      if (registerUserData.imageByte == null) return;
      print(registerUserData.imageByte);
      String url = await uploadImageByte(registerUserData.imageByte!);
      registerUserData.avatarUrl = url;
      await read(userRepository).createUser(registerUserData);
    } catch (e) {
      throw Exception('creteUserFail');
    }
  }
}
