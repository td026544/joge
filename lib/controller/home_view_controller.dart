import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final myChatRoomsStateProvider =
    StateProvider.autoDispose<List<DocumentSnapshot>>((ref) => []);

final homeViewController =
    Provider.autoDispose((ref) => HomeViewController(ref.read));

class HomeViewController {
  final Reader read;
  HomeViewController(this.read);

  initState() async {
    // read(myChatRooms).state = await read(myChatRoomsProvider.last);
  }
}
