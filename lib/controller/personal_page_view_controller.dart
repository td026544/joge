import 'package:boardgame/models/my_user.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final personalPageViewController =
    Provider.autoDispose((ref) => PersonalPageViewController(ref.read));

class PersonalPageViewController {
  final Reader read;
  late MyUser user;

  PersonalPageViewController(this.read);

  void init() async {
    // read(userStateProvider).state = await read(userRepository)
    //     .getUser(FirebaseAuth.instance.currentUser.uid);
  }

  //
  Future<void> getUser() async {}

  // Future<void> attend(String roomId) async {
  //   await read(gameRoomRepository).attend(roomId);
  // }
}
