import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:boardgame/arguments/game_room_detail_arguments.dart';
import 'package:boardgame/models/create_room_post_model.dart';
import 'package:boardgame/models/create_room_save_model.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/url_getter.dart';
import 'package:boardgame/providers/create_room_state_provider.dart';
import 'package:boardgame/providers/place_service_v1.dart';
import 'package:boardgame/repository/game_room_repository.dart';
import 'package:boardgame/screen/game_room_detail_page.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:boardgame/usecase/file_usecase.dart';
import 'package:boardgame/utils/enum_to_string.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

final createRoomPageViewController = ChangeNotifierProvider.autoDispose(
    (ref) => CreateRoomPageViewController(ref.read));

class CreateRoomPageViewController extends ChangeNotifier {
  final Reader read;
  late ScrollController scrollController;
  late TextEditingController timeEditingController;
  late TextEditingController needNumbersController;
  late TextEditingController categoryEditingController;
  late TextEditingController placeEditingController;
  late TextEditingController titleEditingController;
  late TextEditingController infoEditingController;

  // late bool feeEditingControllerVisibility;
  DateTime? startTime;
  late ImagePicker picker;
  File? image;
  Uint8List? imageByte;
  late bool isLoading;
  late Place place;
  late int currentStep;
  late bool readyForCreate;
  int pickerSelectedNeedNumbers = 3;
  CategoryType? categoryType;
  bool isOnline = false;
  bool isAudit = false;
  bool isEditMode = false;
  DateTime? pickerSelectTime;

  List<bool> stepComplete = [false, false, false, false];
  List<StepState> stepStates = [
    StepState.indexed,
    StepState.indexed,
    StepState.indexed,
    StepState.indexed
  ];

  List<int> needNumbersItems = List<int>.generate(19, (i) => i + 1);
  List<GlobalKey<FormState>> formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>()
  ];

  dispose() {
    super.dispose();
    timeEditingController.dispose();
    placeEditingController.dispose();
    titleEditingController.dispose();
    infoEditingController.dispose();
    scrollController.dispose();
  }

  reset() {
    picker = ImagePicker();
    timeEditingController = TextEditingController();
    placeEditingController = TextEditingController();
    titleEditingController = TextEditingController();
    infoEditingController = TextEditingController();
    needNumbersController = TextEditingController();
    categoryEditingController = TextEditingController();
    scrollController = ScrollController(initialScrollOffset: 400);
    isLoading = false;
    place = Place();
    isOnline = false;
    pickerSelectedNeedNumbers = 3;
    currentStep = 0;
    stepComplete = [false, false, false, false];
    stepStates = [
      StepState.indexed,
      StepState.indexed,
      StepState.indexed,
      StepState.indexed
    ];
    imageByte = null;
    image = null;
    startTime = null;
    readyForCreate = false;
    isAudit = false;
    FocusManager.instance.primaryFocus?.unfocus();

    notifyListeners();
  }

  CreateRoomPageViewController(this.read) {
    reset();
  }

  void getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      print('No image selected.');
    }
  }

  Future<String> uploadImageByte(Uint8List imageByte, String roomId) async {
    try {
      // Reference ref = firebase_storage.FirebaseStorage.instance.ref(
      //     'images/${FirebaseAuth.instance.currentUser!.uid + postId + ".jpg"}');
      Reference ref = firebase_storage.FirebaseStorage.instance
          .ref('rooms/${roomId + COVER_NAME}');
      await ref.putData(imageByte);

      String url = await ref.getDownloadURL();
      return url;
    } on FirebaseException catch (e) {
      return "";
      // e.g, e.code == 'canceled'
    }
  }

  Future<String> createRoomProcess() async {
    for (var formKey in formKeys) {
      if (!formKey.currentState!.validate())
        throw ('formKey.currentState error');
    }
    if (imageByte != null) {
      DocumentReference roomRef =
          FirebaseFirestore.instance.collection('rooms').doc();
      String roomId = roomRef.id;
      String downloadUrl =
          await read(fileUseCaseProvider).uploadImageByte(imageByte!, roomId);
      CreateRoomPostModel argsModel = buildArgsModel(roomId);
      String docId = await read(gameRoomRepository).createRoom(argsModel);
      return docId;
    } else {
      throw ('imageBytes==null');
    }
  }

  Future<String> updateRoomProcess(String roomId) async {
    for (int i = 0; i < formKeys.length; i++) {
      GlobalKey<FormState> formKey = formKeys[i];
      if (i == 2) continue; //不一定要照片
      if (!formKey.currentState!.validate())
        throw ('formKey.currentState error');
    }

    if (imageByte != null) {
      String downloadUrl =
          await read(fileUseCaseProvider).uploadImageByte(imageByte!, roomId);
    }

    CreateRoomPostModel argsModel = buildArgsModel(roomId);
    String docId = await read(gameRoomRepository).updateRoom(argsModel);
    return docId;
  }

  CreateRoomPostModel buildArgsModel(String roomId) {
    print('titleEditingController:${titleEditingController.text}');
    CreateRoomPostModel createRoomPostModel = CreateRoomPostModel(
        address: isOnline ? '' : placeEditingController.text,
        city: isOnline ? '' : place.city!,
        startTime: Timestamp.fromDate(startTime!),
        needNumbers: needNumbersController.text.isEmpty
            ? 1
            : int.parse(needNumbersController.text),
        placeName: isOnline ? '' : place.name!,
        geoPoint: isOnline ? GeoPoint(0, 0) : place.geoPoint!,
        type: EnumToString.convertToString(categoryType),
        title: titleEditingController.text,
        info: infoEditingController.text,
        isOnline: isOnline,
        roomId: roomId,
        isAudit: isAudit);
    return createRoomPostModel;
  }

  tapped(int step) {
    currentStep = step;
    notifyListeners();
  }

  continued(BuildContext context, int step) async {
    if (currentStep < 3) {
      currentStep += 1;
      stepStates[step] = getStepState(step);
    } else {}
    notifyListeners();
  }

  cancel() {
    currentStep > 0 ? currentStep -= 1 : null;
  }

  create(BuildContext context) async {
    isLoading = true;
    notifyListeners();
    try {
      String roomId = await createRoomProcess();

      Navigator.pushReplacementNamed(context, GameRoomDetailScreen.id,
          arguments: GameRoomDetailArguments(roomId: roomId));
      await read(chatUseCaseProvider).reloadChats();
    } catch (e) {
      print(e);
      isLoading = false;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('創建失敗請稍後在試'),
        ),
      );
    }

    isLoading = false;
    notifyListeners();
  }

  update(BuildContext context, String roomId) async {
    isLoading = true;
    notifyListeners();
    try {
      await updateRoomProcess(roomId);

      Navigator.pushReplacementNamed(context, GameRoomDetailScreen.id,
          arguments: GameRoomDetailArguments(roomId: roomId));
    } catch (e) {
      print(e);
      isLoading = false;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('創建失敗請稍後在試'),
        ),
      );
    }

    isLoading = false;
    notifyListeners();
  }

  String? validationTime(dynamic value) {
    if (value is int) {
      return null;
    }
    if (value == null || value.isEmpty) {
      return '*必填';
    } else {
      return null;
    }
  }

  StepState getStepState(int step) {
    switch (step) {
      case 0:
        if (currentStep >= 0) {
          if (stepComplete[0]) {
            return StepState.complete;
          } else {
            return StepState.editing;
          }
        } else {
          return StepState.disabled;
        }
      case 1:
        if (currentStep >= 1) {
          if (stepComplete[1]) {
            return StepState.complete;
          } else {
            return StepState.editing;
          }
        } else {
          return StepState.disabled;
        }
      case 2:
        if (currentStep >= 2) {
          if (stepComplete[2]) {
            return StepState.complete;
          } else {
            return StepState.editing;
          }
        } else {
          return StepState.disabled;
        }
      case 3:
        if (currentStep >= 3) {
          if (stepComplete[3]) {
            return StepState.complete;
          } else {
            return StepState.editing;
          }
        } else {
          return StepState.disabled;
        }
      default:
        return StepState.disabled;
    }
  }

  bool checkStep0() {
    if (isOnline) {
      return timeEditingController.text.isNotEmpty;
    } else {
      return placeEditingController.text.isNotEmpty &&
          timeEditingController.text.isNotEmpty;
    }
  }

  bool checkStep1() {
    if (needNumbersController.text.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  bool checkStep2() {
    if (isEditMode) return true;

    if (imageByte != null && imageByte!.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  bool checkStep3() {
    if (titleEditingController.text.isNotEmpty &&
        infoEditingController.text.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  stepFormFieldOnChange(int step, bool Function() checkStepFun) {
    if (checkStepFun()) {
      stepComplete[step] = true;
    } else {
      stepComplete[step] = false;
    }
    readyForCreate = stepComplete.every((element) => element == true);
    notifyListeners();
  }

  checkAll() {
    stepComplete[0] = checkStep0();
    stepComplete[1] = checkStep1();
    stepComplete[2] = checkStep2();
    stepComplete[3] = checkStep3();
    readyForCreate = stepComplete.every((element) => element == true);
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      notifyListeners();

      // Add Your Code here.
    });
  }

  clearAll() {
    stepComplete[0] = checkStep0();
    stepComplete[1] = checkStep1();
    stepComplete[2] = checkStep2();
    stepComplete[3] = checkStep3();
    readyForCreate = stepComplete.every((element) => element == true);
    notifyListeners();
  }

  save(String? roomId) {
    String? address = this.isOnline ? null : placeEditingController.text;
    String? city = this.isOnline ? '' : place.city;
    int? startTime =
        this.startTime != null ? this.startTime!.millisecondsSinceEpoch : null;
    int? needNumbers = needNumbersController.text.isEmpty
        ? null
        : int.parse(needNumbersController.text);
    String? placeName = this.isOnline ? null : place.name;
    double? longitude = this.isOnline ? null : place.geoPoint?.longitude;
    double? latitude = this.isOnline ? null : place.geoPoint?.latitude;
    String? type = categoryType == null
        ? null
        : EnumToString.convertToString(categoryType);
    String? title = titleEditingController.text.isNotEmpty
        ? titleEditingController.text
        : null;
    String? info = infoEditingController.text.isNotEmpty
        ? infoEditingController.text
        : null;
    bool isOnline = this.isOnline;
    String roomId = "";
    String? base64ImageString;
    if (this.imageByte != null) {
      base64ImageString = base64Encode(imageByte!);
    }
    List<int> stepInts = [];
    for (StepState stepState in stepStates) {
      stepInts.add(StepState.values.indexOf(stepState));
    }
    // String imageByte = this.imageByte?;

    read(createRoomStateProvider).state = CreateRoomSaveModel(
      address: address,
      city: city,
      startTime: startTime,
      needNumbers: needNumbers,
      placeName: placeName,
      imageByte: base64ImageString,
      longitude: longitude,
      latitude: latitude,
      type: type,
      info: info,
      isAudit: isAudit,
      isOnline: isOnline,
      title: title,
      roomId: roomId,
      stepStates: stepInts,
    );
  }

  loadModel(CreateRoomSaveModel createRoomSaveModel, bool isEditMode) {
    this.isEditMode = isEditMode;
    if (createRoomSaveModel.isOnline != null) {
      isOnline = createRoomSaveModel.isOnline!;
    }
    if (createRoomSaveModel.imageByte != null) {
      imageByte = base64Decode(createRoomSaveModel.imageByte!);
    }
    if (createRoomSaveModel.startTime != null) {
      DateTime dateTime =
          DateTime.fromMillisecondsSinceEpoch(createRoomSaveModel.startTime!);

      String formattedDate = DateFormat.MMMMd().format(dateTime);
      startTime = dateTime;
      timeEditingController.text = formattedDate;
    }
    placeEditingController.text = createRoomSaveModel.address ?? "";
    titleEditingController.text = createRoomSaveModel.title ?? "";
    infoEditingController.text = createRoomSaveModel.info ?? "";
    if (createRoomSaveModel.needNumbers != null) {
      needNumbersController.text = createRoomSaveModel.needNumbers.toString();
      pickerSelectedNeedNumbers = createRoomSaveModel.needNumbers! - 1;
    }
    if (createRoomSaveModel.imageByte != null) {
      imageByte = base64Decode(createRoomSaveModel.imageByte!);
    }
    if (createRoomSaveModel.type != null) {
      CategoryType categoryType = EnumToString.fromString(
          CategoryType.values, createRoomSaveModel.type!) as CategoryType;
      String category = GetNameUtil.getCategoryName(categoryType);
      categoryEditingController.text = category;
      this.categoryType = categoryType;
    }

// -----------------------
    place.name = createRoomSaveModel.placeName ?? "";
    place.city = createRoomSaveModel.city ?? "";
    isAudit = createRoomSaveModel.isAudit ?? false;
    isOnline = createRoomSaveModel.isOnline ?? isOnline;
    if (createRoomSaveModel.latitude != null &&
        createRoomSaveModel.longitude != null) {
      place.geoPoint = GeoPoint(
          createRoomSaveModel.latitude!, createRoomSaveModel.latitude!);
    }
    for (int i = 0; i < stepStates.length; i++) {
      stepStates[i] = StepState.values[createRoomSaveModel.stepStates[i]];
    }
    checkAll();
  }

  void confirmSelectedTime() {
    if (pickerSelectTime != null) {
      var outputFormat = DateFormat('MM/dd/ hh:mm a', 'zh');
      var outputDate = outputFormat.format(pickerSelectTime!);
      // String formattedDate = formatDate(pickerSelectTime!,
      //     [mm, '月', dd, '日', D, '   ', HH, '時', nn, '分'], LocaleType.zh);
      timeEditingController.text = outputDate;
      startTime = pickerSelectTime;
    }
  }

  // bool checkStep2() {
  //   if(isOnline){
  //     return timeEditingController.text.isNotEmpty;
  //   }else{
  //     return placeEditingController.text.isNotEmpty&&timeEditingController.text.isNotEmpty;
  //   }
  // }
}
