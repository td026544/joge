// import 'package:boardgame/model/message.dart';
// import 'package:boardgame/repository/chat_room_repository.dart';
//
// final _fireStore = FirebaseFirestore.instance;
//
// final textProvider = StateProvider.autoDispose<String>((ref) => "");
// final sendAbleProvider = StateProvider.autoDispose<bool>((ref) => false);
//
// final messagesCache = StateProvider<List<Message>>((ref) => []);
//
// final chatRoomViewController = Provider.autoDispose((ref) {
//   return ChatRoomViewController(ref.read);
// });
//
// class ChatRoomViewController {
//   TextEditingController textEditingController = TextEditingController();
//   final Reader read;
//   ChatRoomViewController(this.read);
//   late DocumentSnapshot chatRoomDoc;
//   late String roomId;
//   List<Message> cacheMessages = <Message>[];
//   List<Message> localMessages = <Message>[];
//
//   late Map<String, dynamic> data;
//
//   initState(DocumentSnapshot documentSnapshot) {
//     chatRoomDoc = documentSnapshot;
//     roomId = chatRoomDoc.id;
//     data = chatRoomDoc.data() as Map<String, dynamic>;
//   }
//
//   Future<void> exitRoom(String roomId) async {
//     try {
//       await read(chatRoomRepository).exitRoom(roomId);
//     } catch (e) {}
//   }
//
//   Future<void> kickOffAttendee(String roomId, String userUid) async {
//     await read(chatRoomRepository).kickOffAttendee(roomId, userUid);
//   }
//
//   Future<void> addMessage(String roomId, String sendText) async {
//     await read(chatRoomRepository).addMessage(roomId, sendText);
//     // isSendAble = false;
//   }
//
//   void dispose() {
//     textEditingController.dispose();
//   }
//
//   String getNickName(String uid) {
//     // String nickname = 'nickname';
//     // try {
//     //   nickname = chatRoomDoc['attendeesData.$uid.nickname'] ?? '用戶';
//     // } catch (e) {
//     //   return nickname;
//     // }
//     return "aaaa";
//   }
// }
import 'dart:io';

import 'package:boardgame/components/message_bubble.dart';
import 'package:boardgame/models/chat_message.dart';
import 'package:boardgame/models/conversation.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/chat_messages_state_notifyier_provider.dart';
import 'package:boardgame/providers/users_providers.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final isSendAbleStateProvider = StateProvider.autoDispose((ref) => false);

final chatRoomViewController = ChangeNotifierProvider.family
    .autoDispose<ChatRoomViewController, Conversation>((ref, conversation) {
  return ChatRoomViewController(ref.read, conversation);
});

class ChatRoomViewController extends ChangeNotifier {
  final Reader read;
  final Conversation conversation;
  final picker = ImagePicker();
  bool _isBlocked = false;

  late TextEditingController textEditingController;
  late FocusNode textFieldFocusNode;
  late ScrollController scrollController;
  bool _emojiShowing = false;
  double oldScrollPosition = 0;
  final double updateViewScrollValue = 100;
  bool isLoading = false; // track if products fetching
  bool isMyTyping = false;
  bool isSendAble = false;
  MyUser? toUser;
  List<MyUser> toUsers = [];
  final List<ChatMessage> allMessages = <ChatMessage>[];

  ChatRoomViewController(this.read, this.conversation) {
    scrollController = ScrollController();
    textEditingController = TextEditingController();
    textEditingController.addListener(onTextLatestValue);
    textFieldFocusNode = FocusNode();
    textFieldFocusNode.addListener(() {
      if (textFieldFocusNode.hasFocus && _emojiShowing) {
        _emojiShowing = false;
        notifyListeners();
      }
    });
    scrollController.addListener(
      () {
        double maxScroll = scrollController.position.maxScrollExtent;
        double currentScroll = scrollController.position.pixels;

        double delta = 200.0;
        if (maxScroll - currentScroll <= delta) {
          if (isLoading) return;
          isLoading = true;
          read(chatMessagesStateNotifierProvider(conversation.chatId).notifier)
              .getMore()
              .whenComplete(() {
            isLoading = false;
          });
        }
      },
    );
    init();
  }
  init() async {
    if (conversation.chatType == EnumToString.convertToString(ChatType.user)) {
      MyUser? toUser =
          await read(toUserFutureProvider(conversation.toId).future);
      if (toUser != null) {
        toUsers.add(toUser);
      }
    }

    updateIsBlocked();
  }

  @override
  void dispose() {
    super.dispose();
    textEditingController.dispose();
  }

  onActionSelected(String actionName) {
    if (actionName == "BLOCK") {
      read(chatUseCaseProvider).blockUser(conversation.chatId);
      updateIsBlocked();
      notifyListeners();
    } else if (actionName == "UNBLOCK") {
      read(chatUseCaseProvider).unBlockUser(conversation.chatId);
      updateIsBlocked();
      notifyListeners();
    }
  }

  void onTextLatestValue() {
    if (textEditingController.text.isNotEmpty) {
      if (!isMyTyping) {
        isMyTyping = true;
        read(chatUseCaseProvider)
            .sendMessageToServerTyping(conversation.chatId, true);
      }
      read(isSendAbleStateProvider).state = true;
    } else {
      isMyTyping = false;

      read(chatUseCaseProvider)
          .sendMessageToServerTyping(conversation.chatId, false);
      read(isSendAbleStateProvider).state = false;
    }
  }

  void onTextLatestValueNew(String text) {
    if (text.isNotEmpty) {
      if (!isMyTyping) {
        isMyTyping = true;
        read(chatUseCaseProvider)
            .sendMessageToServerTyping(conversation.chatId, true);
      }
      read(isSendAbleStateProvider).state = true;
    } else {
      isMyTyping = false;

      read(chatUseCaseProvider)
          .sendMessageToServerTyping(conversation.chatId, false);
      read(isSendAbleStateProvider).state = false;
    }
  }

  onEmojiSelected(Emoji emoji) {
    // textFieldFocusNode.requestFocus();
    textEditingController
      ..text += emoji.emoji
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: textEditingController.text.length));
  }

  onBackspacePressed() {
    if (textEditingController.text.isEmpty) {
      emojiShowing = false;
    }
    textEditingController
      ..text = textEditingController.text.characters.skipLast(1).toString()
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: textEditingController.text.length));
  }

  Future pickImage(BuildContext context) async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      File tempFile = File(pickedFile.path);
      read(chatUseCaseProvider)
          .sendImageToServer(context, tempFile, conversation.chatId);

      // userDataFunction.sendNotification(
      //   toUid: widget.toContact.phoneNumber,
      //   title: "You have New Messages",
      //   content: "Click To View",
      // );
    }
  }

  void updateIsBlocked() async {
    bool temp = await DBManager.db.isBlocked(conversation.chatId);
    isBlocked = temp;
  }

  BubbleShape getMessageShowType(List<ChatMessage> messages, int index) {
    // 如果前面不是自己的訊息，或是第一個。
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw ('noUser');
    final bool isFirst = index == 0;
    final bool isEnd = index == messages.length - 1;
    final bool hasNext = index + 1 < messages.length;
    final bool hasLast = index - 1 >= 0;
    bool nextIsSameType = false;
    bool lastIsSameType = false;

    bool isSelf = messages[index].senderId == myUser.id;

    if (hasNext) {
      bool isNextSelf = messages[index + 1].senderId == myUser.id;
      if (isSelf == isNextSelf) {
        nextIsSameType = true;
      }
    }
    if (hasLast) {
      bool isBeforeSelf = messages[index - 1].senderId == myUser.id;

      if (isSelf == isBeforeSelf) {
        lastIsSameType = true;
      }
    }

    //-----

    if (messages.length == 1) {
      return BubbleShape.single;
    }

    if (isFirst && nextIsSameType) {
      return BubbleShape.foot;
    }
    if (isEnd && lastIsSameType) {
      return BubbleShape.head;
    }
    if (hasLast && lastIsSameType) {
      if (hasNext && !nextIsSameType) {
        return BubbleShape.head;
      }
    }
    if (hasNext && nextIsSameType) {
      if (hasLast && !lastIsSameType) {
        return BubbleShape.foot;
      }
    }
    if (isFirst && !nextIsSameType || isEnd && !nextIsSameType) {
      return BubbleShape.single;
    }
    if (!nextIsSameType && !lastIsSameType) {
      return BubbleShape.single;
    }
    return BubbleShape.body;
  }

  bool get emojiShowing => _emojiShowing;

  set emojiShowing(bool value) {
    _emojiShowing = value;
    notifyListeners();
  }

  bool get isBlocked => _isBlocked;

  set isBlocked(bool value) {
    _isBlocked = value;
    notifyListeners();
  }

  onSendMessage() {
    isSendAble = false;
    isMyTyping = false;
    emojiShowing = false;
    read(chatUseCaseProvider).sendMessageToServer(textEditingController.text,
        conversation.chatId, ["P3QrTNmeLRbZVXikJ4xe1J2QFS42"]);
    textEditingController.clear();
  }
}
