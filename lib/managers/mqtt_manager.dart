import 'dart:convert';

import 'package:boardgame/configs/Constants.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/chat_messages_state_notifyier_provider.dart';
import 'package:boardgame/providers/conversations_cards_provider.dart';
import 'package:boardgame/providers/message_notify_provider.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

final mqttProvider = Provider.autoDispose<MQTTManager>((ref) {
  final mqTTManager = MQTTManager(
      serverAddress: Constants.mqttIp, clientName: 'agfdg', read: ref.read);
  // ref.maintainState = true;
  ref.onDispose(() {
    mqTTManager.disconnect();
  });
  return mqTTManager;
});

class MQTTManager {
  final String serverAddress;
  final String clientName;
  late MqttServerClient _client;
  final Reader read;

  // ChatBloc chatBloc;
  // HomeBloc homeBloc;

  MQTTManager(
      {required this.read,
      required this.serverAddress,
      required this.clientName}) {
    initializeMQTTClient();
    connectWithOutUserName();
  }

  void initializeMQTTClient() {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    _client = MqttServerClient.withPort(serverAddress, myUser.id, 1883);
    // _client = MqttServerClient(serverAddress, clientName);
    _client.keepAlivePeriod = 43200;
    _client.logging(on: false);
    _client.onDisconnected = onDisconnected;
    _client.onConnected = onConnect;
    _client.onSubscribed = onSubscribed;
    // auto reconnecting when client is disconnected to server
    _client.autoReconnect = true;

    final connMess = MqttConnectMessage()
        // .withClientIdentifier(clientName)

        // .withWillTopic(
        //     'willtopic') // If you set this you must set a will message
        // .withWillMessage('My Will message')
        // .startClean()
        // .withWillRetain()
        .withWillQos(MqttQos.atLeastOnce);

    _client.connectionMessage = connMess;
  }

  void onSubscribed(String topic) {
    print('EXAMPLE::Subscription confirmed for topic $topic');
  }

  void disconnect() {
    _client.disconnect();
  }

  bool getConnectionStatus() {
    if (_client.connectionStatus!.state == MqttConnectionState.connected) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> connect(String username, String password) async {
    try {
      await _client.connect(username, password);
    } on Exception catch (e) {
      print('EXAMPLE::client exception - $e');
      _client.disconnect();
    }
  }

  Future<void> connectWithOutUserName() async {
    try {
      await _client.connect();
      // var resut = _client.subscriptionsManager!.subscriptions;

      if (_client.connectionStatus!.state == MqttConnectionState.connected) {
        print('client connected');
      } else {
        print(
            'client connection failed - disconnecting, state is ${_client.connectionStatus!.state}');
        _client.disconnect();
      }
      // _client.subscribe("abc", MqttQos.atLeastOnce);
      _client.updates!.listen((List<MqttReceivedMessage<MqttMessage>> c) {
        final MqttPublishMessage message = c[0].payload as MqttPublishMessage;
        final payload =
            MqttPublishPayload.bytesToStringAsString(message.payload.message);
        print('${c[0].topic}:${payload}');
      });

      print('sucess');
    } on Exception catch (e) {
      print('EXAMPLE::client exception - $e');
      _client.disconnect();
    }
  }

  void subscribeTopic(String topic) {
    _client.subscribe(topic, MqttQos.atLeastOnce);

    /// The client has a change notifier object(see the Observable class) which we then listen to to get
    /// notifications of published updates to each subscribed topic.
  }

  void unSubscribeTopic(String topic) {
    _client.unsubscribe(topic);
  }

  Stream<List<MqttReceivedMessage<MqttMessage>>>? get messageStream =>
      _client.updates;

  void publish(String myMessage, String topic) {
    final builder = MqttClientPayloadBuilder();
    builder.addString(myMessage);
    _client.publishMessage(topic, MqttQos.atLeastOnce, builder.payload!,
        retain: false);
  }

  void onDisconnected() {
    print('Client Disconnect');
  }

  void onConnect() {
    // chatBloc = BlocProvider.of<ChatBloc>(context);
    // homeBloc = BlocProvider.of<HomeBloc>(context);

    _client.updates!.listen((List<MqttReceivedMessage<MqttMessage>> c) async {
      final MqttPublishMessage recMess = c[0].payload as MqttPublishMessage;
      final String chatId = c[0].topic;
      final msg =
          MqttPublishPayload.bytesToStringAsString(recMess.payload.message);
      print("THIS IS THE MESSAGE BEFORE DECODING");
      print(msg);
      decodeMessage(msg, chatId);
    });
  }

  void decodeMessage(String msg, String chatId) async {
    Map parsedMsg = json.decode(msg);

    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    String myUid = myUser.id;
    MsgType? msgType =
        EnumToString.fromString(MsgType.values, parsedMsg["type"]);
    if (msgType == null) return;
    handleMessageType(msgType, chatId, parsedMsg, myUid);
  }

  handleMessageType(
      MsgType msgType, String chatId, Map msg, String myUid) async {
    final int currTime = DateTime.now().millisecondsSinceEpoch;
    switch (msgType) {
      case MsgType.text:
        if (msg["uid"] == myUid) {
          // if uid of message is same as mine then that means
          // I sent the message, so save it to db as sent message
          print("seetting messages to local db 1");
          await DBManager.db.addNewMessageToMessagesTable(chatId, msg["msg"],
              msg["type"], currTime, msg["uid"]); // 1 bcox message is sent

        } else {
          print("seetting messages to local db 0");
          // save the message to local db as received message
          await DBManager.db.addNewMessageToMessagesTable(
              chatId, msg["msg"], msg["type"], currTime, msg["uid"]);
          read(typingStateStateNotifierProvider(chatId)).state = false;
          read(chatUnReadCountStateNotifierProvider.notifier)
              .updateNewMessage1(chatId);
// 0 bcox message is received
        }
        read(chatMessagesStateNotifierProvider(chatId).notifier).addMessage();
        DBManager.db.updateMessageToChatTable(
            chatId, msg["msg"], msg["type"], currTime, myUid);
        read(conversationsStateNotifierProvider.notifier).getData();
        break;
      case MsgType.image:
        if (msg["uid"] == myUid) {
          // if uid of message is same as mine then that means
          // I sent the message, so save it to db as sent message
          print("seetting messages to local db 1");
          await DBManager.db.addNewMessageToMessagesTable(chatId, msg["msg"],
              msg["type"], currTime, msg["uid"]); // 1 bcox message is sent

        } else {
          print("seetting messages to local db 0");
          // save the message to local db as received message
          await DBManager.db.addNewMessageToMessagesTable(
              chatId, msg["msg"], msg["type"], currTime, msg["uid"]);
          read(typingStateStateNotifierProvider(chatId)).state = false;
          read(chatUnReadCountStateNotifierProvider.notifier)
              .updateNewMessage1(chatId);
// 0 bcox message is received
        }
        read(chatMessagesStateNotifierProvider(chatId).notifier).addMessage();
        DBManager.db.updateMessageToChatTable(
            chatId, msg["msg"], msg["type"], currTime, myUid);
        read(conversationsStateNotifierProvider.notifier).getData();
        break;
      case MsgType.service:
        if (msg["uid"] == myUid) {
          // if uid of message is same as mine then that means
          // I sent the message, so save it to db as sent message
          print("seetting messages to local db 1");
          await DBManager.db.addNewMessageToMessagesTable(chatId, msg["msg"],
              msg["type"], currTime, msg["uid"]); // 1 bcox message is sent

        } else {
          print("seetting messages to local db 0");
          // save the message to local db as received message
          await DBManager.db.addNewMessageToMessagesTable(
              chatId, msg["msg"], msg["type"], currTime, msg["uid"]);
          read(typingStateStateNotifierProvider(chatId)).state = false;
          read(chatUnReadCountStateNotifierProvider.notifier)
              .updateNewMessage1(chatId);
// 0 bcox message is received
        }
        read(chatMessagesStateNotifierProvider(chatId).notifier).addMessage();
        DBManager.db.updateMessageToChatTable(
            chatId, msg["msg"], msg["type"], currTime, myUid);
        read(conversationsStateNotifierProvider.notifier).getData();
        break;
      case MsgType.typing:
        if (msg["msg"] == "true" && msg["uid"] != myUid) {
          read(typingStateStateNotifierProvider(chatId)).state = true;
        } else {
          read(typingStateStateNotifierProvider(chatId)).state = false;
        }
        break;
      case MsgType.none:
        break;
    }
  }
}
