import 'package:flutter/material.dart';

class Constants {
  static final Color stuffColor = Colors.blueAccent[400]!;
  static final Color textStuffColor = Colors.blueGrey[700]!;
  static final int resendOtpTime = 35;
  static const bool USE_EMULATOR = false;

  static const firstRun = "firstRun";
  static const sessionUid = "phoneNumber";
  static const sessionUsername = 'sessionUsername';
  static const fullName = 'fullName';
  static const sessionName = 'sessionName';
  static const sessionProfilePictureUrl = 'sessionProfilePictureUrl';
  static const sessionCountryCode = 'sessionCountryCode';

  static const configDarkMode = 'configDarkMode';

  static const profilePicChangeMsg = "profile_pic_change";
  static const mqttIp = '34.80.245.132'; //主機位置
  static const port = 1883; //MQTT port
  static const oneSignalAppId = "a1b3d198-b0c7-4387-acdd-632f6b14e22b";
  // static const clientID = 'broker-cn.emqx.io'; //Mqtt Client
  // static const username = 'username'; //Mqtt username
  // static const password = 'password'; //Mqtt password
}
