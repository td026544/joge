import 'package:boardgame/models/room_detail.dart';

class RatingRoomArgument {
  final RoomDetail roomDetail;
  final String roomId;

  RatingRoomArgument(this.roomDetail, this.roomId);
}
