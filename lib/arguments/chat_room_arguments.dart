import 'package:cloud_firestore/cloud_firestore.dart';

class ChatRoomArguments {
  final DocumentSnapshot chatRoomDoc;

  ChatRoomArguments({required this.chatRoomDoc});
}
