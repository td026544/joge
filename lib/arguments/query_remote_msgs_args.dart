// @freezed
// abstract class QueryRemoteMsgArgs with QueryRemoteMsgArgs {
//   factory QueryRemoteMsgArgs({
//     required String roomId,
//     required int locale,
//   }) = _QueryRemoteMsgArgs;
// }

import 'package:equatable/equatable.dart';

class QueryRemoteMsgArgs extends Equatable {
  final String id;
  final int timeStamp;
  QueryRemoteMsgArgs({
    required this.id,
    this.timeStamp = 0,
  });

  @override
  List<Object?> get props => [id, timeStamp];

// @override
// List<Object> get props => [id, timeStamp];
}
// class QueryRemoteMsgArgs {
//   final String id;
//   final int timeStamp;
//   QueryRemoteMsgArgs({
//     required this.id,
//     this.timeStamp = 0,
//   });
//
//   @override
//   bool operator ==(Object o) {
//     if (identical(this, o)) return true;
//
//     return o is QueryRemoteMsgArgs && o.id == id && o.timeStamp == timeStamp;
//   }
//
//   @override
//   int get hashCode => id.hashCode ^ timeStamp.hashCode;
//
//   // @override
//   // List<Object> get props => [id, timeStamp];
// }
