import 'package:boardgame/models/conversation.dart';

class ChatScreenArguments {
  final Conversation conversation;

  ChatScreenArguments({required this.conversation});
}
