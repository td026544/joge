import 'package:boardgame/models/create_room_save_model.dart';

class CreateRoomArguments {
  final CreateRoomSaveModel createRoomSaveModel;

  CreateRoomArguments({required this.createRoomSaveModel});
}
