import 'dart:io';

import 'package:boardgame/arguments/game_room_detail_arguments.dart';
import 'package:boardgame/arguments/personal_arguments.dart';
import 'package:boardgame/arguments/rating_room_arguments.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/screen/carousel_onboarding_page.dart';
import 'package:boardgame/screen/chat_personal_page.dart';
import 'package:boardgame/screen/chat_screen.dart';
import 'package:boardgame/screen/feed_page.dart';
import 'package:boardgame/screen/first_setting_page.dart';
import 'package:boardgame/screen/game_room_detail_page.dart';
import 'package:boardgame/screen/home_page.dart';
import 'package:boardgame/screen/image_editor_page.dart';
import 'package:boardgame/screen/invite_friends_page.dart';
import 'package:boardgame/screen/login_page.dart';
import 'package:boardgame/screen/manage_room_page.dart';
import 'package:boardgame/screen/personal_page.dart';
import 'package:boardgame/screen/personal_page_by_id.dart';
import 'package:boardgame/screen/rating_page.dart';
import 'package:boardgame/screen/register/register_avatar_page.dart';
import 'package:boardgame/screen/register/register_bio_page.dart';
import 'package:boardgame/screen/register/register_birthday_page.dart';
import 'package:boardgame/screen/register/register_gender_page.dart';
import 'package:boardgame/screen/register/register_hobbies_page.dart';
import 'package:boardgame/screen/register/register_nickname_page.dart';
import 'package:boardgame/screen/setting_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'arguments/chat_screen_arguments.dart';
import 'arguments/login_page_arguments.dart';

const bool USE_EMULATOR = false;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  // await FirebaseAppCheck.instance
  //     .activate(webRecaptchaSiteKey: 'recaptcha-v3-site-key');

  // await FirebaseAuth.instance.useEmulator('http://localhost:9099');
  String myhost = Platform.isAndroid ? '10.0.2.2:8080' : 'localhost:8080';

  if (USE_EMULATOR) {
    // [Firestore | localhost:8080]
    FirebaseFirestore.instance.settings = const Settings(
      host: "10.0.2.2:8080",
      sslEnabled: false,
      persistenceEnabled: false,
    );
    await FirebaseStorage.instance.useStorageEmulator('10.0.2.2', 9199);
    FirebaseFunctions.instance.useFunctionsEmulator('http://localhost', 5001);

    // [Authentication | localhost:9099]
    await FirebaseAuth.instance.useEmulator('http://localhost:9099');

    // [Storage | localhost:9199]

  }
  // https://documentation.onesignal.com/docs/flutter-sdk-setup
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.setAppId("a1b3d198-b0c7-4387-acdd-632f6b14e22b");

// The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission
  OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
    print("Accepted permission: $accepted");
  });
  String externalUserId =
      '123456789'; // You will supply the external user id to the OneSignal SDK
  OneSignal.shared.setExternalUserId(externalUserId);

  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
  //   print('Got a message whilst in the foreground!');
  //   print('Message data: ${message.notification?.body}');
  //   print('Message data: ${message.data}');
  //
  //   if (message.notification != null) {
  //     print('Message also contained a notification: ${message.notification}');
  //   }
  // });
  // eyil0J2vSz6jq1GT2fSlnJ:APA91bGbMTnVJ1-1v2Rj_DUvYHb8WHWMHvReSZxjv1OEAaHBjOxq85uInpdB1w7_xxaG79tvMJ_3ZQGEF7gxI3HUvwz_JP8FccaFVY6nSej1mLjINFBMgj8CfW5ZWVyPQAjZeT1h41jU

  // firebaseMessaging = configureMessaging();

  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;

  timeago.setLocaleMessages('zh', timeago.ZhMessages());

  runApp(
    ProviderScope(
      child: MyApp(),
    ),
  );
}
// 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&language=$lang&key=$apiKey&sessiontoken=$sessionToken&components=country:tw&location=25.0283092,121.5636229';

final ThemeData myTheme = buildTheme();
ThemeData buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(

      // buttonTheme: base.buttonTheme.copyWith(
      //   buttonColor: kShrinePink100,
      //   colorScheme: base.colorScheme.copyWith(
      //     secondary: kShrineBrown900,
      //   ),
      // ),
      // buttonBarTheme: base.buttonBarTheme.copyWith(
      //   buttonTextTheme: ButtonTextTheme.accent,
      // ),
      // primaryIconTheme: base.iconTheme.copyWith(color: kShrineBrown900),
      // inputDecorationTheme: InputDecorationTheme(
      //   border: CutCornersBorder(),
      // ),
      // textTheme: _buildShrineTextTheme(base.textTheme),
      // primaryTextTheme: _buildShrineTextTheme(base.primaryTextTheme),
      // accentTextTheme: _buildShrineTextTheme(base.accentTextTheme),
      // iconTheme: _customIconTheme(base.iconTheme),
      );
}

class MyApp extends StatelessWidget {
  final MaterialColor kPrimaryColor = const MaterialColor(
    0xFF0E7AC7,
    const <int, Color>{
      50: const Color(0xFFFF9800),
      100: const Color(0xFFFF9800),
      200: const Color(0xFFFF9800),
      300: const Color(0xFFFF9800),
      400: const Color(0xFFFF9800),
      500: const Color(0xFFFF9800),
      600: const Color(0xFFFF9800),
      700: const Color(0xFFFF9800),
      800: const Color(0xFFFF9800),
      900: const Color(0xFFFF9800),
    },
  );
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.orange,
        accentColor: Colors.orangeAccent,
        hintColor: Colors.grey,
        indicatorColor: Colors.white,
        selectedRowColor: Colors.pink,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Colors.red,
          selectionColor: Colors.green,
          selectionHandleColor: Colors.red,
        ),
        primaryTextTheme: TextTheme(
          headline6: TextStyle(color: Colors.white),
        ),
        appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(color: Colors.white),
        ),
        typography: Typography.material2018(),
      ),
      initialRoute: Root.id,
      // home: Root(),
      routes: {
        Root.id: (context) => Root(),
        RegisterNicknamePage.id: (context) => RegisterNicknamePage(),
        RegisterMyBirthdayPage.id: (context) => RegisterMyBirthdayPage(),
        RegisterGenderPage.id: (context) => RegisterGenderPage(),
        FirstSettingScreen.id: (context) => FirstSettingScreen(),
        HomeScreen.id: (context) => HomeScreen(),
        PersonalPage.id: (context) => PersonalPage(),
        FeedPage.id: (context) => FeedPage(),
        ImageEditorPage.id: (context) => ImageEditorPage(),
        RegisterHobbiesPage.id: (context) => RegisterHobbiesPage(),
        RegisterBIOPage.id: (context) => RegisterBIOPage(),
        RegisterAvatarPage.id: (context) => RegisterAvatarPage(),
        SettingPage.id: (context) => SettingPage(),
        CarouselOnBoardingPage.id: (context) => CarouselOnBoardingPage(),
        InviteFriendsPage.id: (context) => InviteFriendsPage(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == GameRoomDetailScreen.id) {
          final GameRoomDetailArguments args =
              settings.arguments as GameRoomDetailArguments;

          return MaterialPageRoute(
            builder: (context) {
              return GameRoomDetailScreen(roomId: args.roomId);
            },
          );
        }
        if (settings.name == ManageRoomPage.id) {
          final GameRoomDetailArguments args =
              settings.arguments as GameRoomDetailArguments;
          return MaterialPageRoute(
            builder: (context) {
              return ManageRoomPage(args.roomId);
            },
          );
        }

        if (settings.name == ChatScreen.id) {
          final ChatScreenArguments args =
              settings.arguments as ChatScreenArguments;

          return MaterialPageRoute(
            builder: (context) {
              return ChatScreen(
                conversation: args.conversation,
              );
            },
          );
        }
        if (settings.name == ChatPersonalScreen.id) {
          final ChatScreenArguments args =
              settings.arguments as ChatScreenArguments;

          return MaterialPageRoute(
            builder: (context) {
              return ChatPersonalScreen(
                conversation: args.conversation,
              );
            },
          );
        }
        if (settings.name == LoginScreen.id) {
          final LoginPageArguments args =
              settings.arguments as LoginPageArguments;
          return MaterialPageRoute(
            builder: (context) {
              return LoginScreen(
                isNavToRegisterPage: args.isNavToRegisterPage,
              );
            },
          );
        }
        if (settings.name == PersonalPageById.id) {
          final PersonalByIdArguments args =
              settings.arguments as PersonalByIdArguments;
          return MaterialPageRoute(
            builder: (context) {
              return PersonalPageById(
                uid: args.uid,
              );
            },
          );
        }
        if (settings.name == RatingPage.id) {
          final RatingRoomArgument args =
              settings.arguments as RatingRoomArgument;
          return MaterialPageRoute(
            builder: (context) {
              return RatingPage(
                roomDetail: args.roomDetail,
                roomId: args.roomId,
              );
            },
          );
        }
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate, //多加这行代码
      ],
      supportedLocales: [
        //此处
        const Locale('zh', 'TW'),
        const Locale('en', 'US'),
      ],
    );
  }
}

class Root extends StatefulWidget {
  static const String id = '/';

  const Root({Key? key}) : super(key: key);

  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final fireBaseUser = ref.watch(firebaseUserFutureProvider);
      // final myUser = watch(userStateProvider).state;

      return fireBaseUser.when(
          data: (fireBaseUser) {
            if (fireBaseUser != null) {
              final myUser = ref.read(userStateProvider).state;

              if (myUser != null) {
                return HomeScreen();
                Navigator.pushNamedAndRemoveUntil(context,
                    RegisterNicknamePage.id, (Route<dynamic> route) => false);
              } else {
                // Navigator.pushNamedAndRemoveUntil(context,
                //     RegisterNicknamePage.id, (Route<dynamic> route) => false);
                return LoginScreen(
                  isNavToRegisterPage: true,
                );
                Navigator.pushNamed(context, RegisterBIOPage.id);

                Navigator.pushNamedAndRemoveUntil(context,
                    RegisterNicknamePage.id, (Route<dynamic> route) => false);
              }
            } else {
              return LoginScreen();
            }
          },
          loading: (data) => Text('loading'),
          error: (data, e, s) => Text('dsfsdfsd'));
    });
  }
}

// class Root extends ConsumerWidget {
//
//
//
//   static const String id = '/';
//   @override
//   Widget build(BuildContext context, ScopedReader watch) {
//     final fireBaseUser = watch(userFutureProvider);
//     final fireBaseUser = watch(userFutureProvider);
//
//     return fireBaseUser.when(data: (user) {
//       if (user != null) {
//         return HomeScreen();
//       } else {
//         return LoginScreen();
//       }
//     }, loading: () {
//       return Text('loading');
//     }, error: (Object error, StackTrace? stackTrace) {
//       print(error.toString());
//     });
//   }
// }
