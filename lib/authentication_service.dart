import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_sign_in/google_sign_in.dart';

// flutter pub run build_runner build
class AuthenticationService {
  final FirebaseAuth _firebaseAuth;
  final Reader read;

  AuthenticationService(this._firebaseAuth, this.read);
  Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();
  handleSignIn(User user) {
    if (user != null) {
      createUserInFireStore();
      print(' isAuth = true;');
      // setState(() {
      //   isAuth = true;
      // });
      print('User is signed in!');
    } else {
      // setState(() {
      //   print(' isAuth = false;');
      //   isAuth = false;
      // });
      print('User is currently signed out!');
    }
    print('handleSignIn()');
  }

  Future<String> googleSignIn() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    try {
      final GoogleSignInAccount? googleSignInAccount =
          await googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleSignInAccount!.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await FirebaseAuth.instance.signInWithCredential(credential);
      return googleSignInAccount.id;
    } on FirebaseAuthException catch (e) {
      return e.message!;
    }
  }

  Future<String?> facebookSignIn() async {
    final LoginResult result = await FacebookAuth.instance
        .login(); // by default we request the email and the public profile
    if (result.status == LoginStatus.success) {
      // you are logged
      final AccessToken accessToken = result.accessToken!;
      AuthCredential authCredential =
          FacebookAuthProvider.credential(accessToken.token);
      await FirebaseAuth.instance.signInWithCredential(authCredential);
    }
    return "123";
  }

  Future<void> signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  // Future<String?> facebookSignIn() async {
  //   final FacebookLogin facebookLogin = FacebookLogin();
  //   final result = await facebookLogin.logIn(['email']);
  //
  //   // final result = await facebookLogin.logInWithReadPermissions(['email']);
  //
  //   switch (result.status) {
  //     case FacebookLoginStatus.loggedIn:
  //       final FacebookAccessToken accessToken = result.accessToken;
  //       AuthCredential authCredential =
  //           FacebookAuthProvider.credential(accessToken.token);
  //
  //       await FirebaseAuth.instance.signInWithCredential(authCredential);
  //       return FirebaseAuth.instance.currentUser!.uid;
  //       break;
  //     case FacebookLoginStatus.cancelledByUser:
  //       print("FacebookLoginStatus.cancelledByUser");
  //       return "FacebookLoginStatus.cancelledByUser";
  //       // _showCancelledMessage();
  //       break;
  //     case FacebookLoginStatus.error:
  //       return result.errorMessage;
  //
  //       // _showErrorOnUI(result.errorMessage);
  //       break;
  //   }
  // }

  createUserInFireStore() async {
    final userRef = FirebaseFirestore.instance.collection('users');
    final user = FirebaseAuth.instance.currentUser;
    final DocumentSnapshot doc = await userRef.doc(user?.uid).get();

    if (!doc.exists) {
      userRef.doc(user?.uid).set({
        "id": user?.uid,
        "email": user?.email,
        "gender": "",
        "age": "",
        "phoneNumber": user?.phoneNumber,
        "nickname": user?.phoneNumber,
        "pushToken": "",
        "username": user?.displayName,
        "createTime": DateTime.now().millisecondsSinceEpoch,
        "rooms": [],
      });
      // Provider.of<UserModel>(context, listen: false)
      //     .updateUser(userDoc.get('username'), userDoc.id);

      // Navigator.pushNamed(context, WelcomeScreen.id);
    } else {}
  }
}
