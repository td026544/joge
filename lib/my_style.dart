import 'package:flutter/material.dart';

const darkInputTheme = InputDecorationTheme(
  fillColor: Colors.orange,
  helperStyle: TextStyle(color: Colors.white),
  labelStyle: TextStyle(
    color: Colors.white70,
  ),
  filled: true,
);
const dartInputTheme1 = InputDecorationTheme(
  fillColor: Colors.orange,
  helperStyle: TextStyle(color: Colors.white),
  labelStyle: TextStyle(
    color: Colors.white70,
  ),
  filled: true,
  focusColor: Colors.white,
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.orangeAccent),
  ),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
  border: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
);
