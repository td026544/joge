import 'dart:convert';

import 'package:boardgame/configs/constants.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

final userUseCaseProvider = Provider((ref) => UserUseCaseImpl(ref.read));

abstract class UserUseCase {
  logOut();
}

class UserUseCaseImpl extends UserUseCase {
  final Reader read;

  UserUseCaseImpl(this.read);
  void logOut() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    FirebaseAuth.instance.signOut();
    googleSignIn.signOut();

    read(userStateProvider).state = null;
  }

  void sendNotification(
      {required List<String> toUids,
      required String title,
      required String content}) async {
    var body = jsonEncode({
      "include_external_user_ids": toUids,
      // "include_player_ids": toUids,
      "app_id": Constants.oneSignalAppId,
      "contents": {"en": content},
      "headings": {"en": title},
      // "collapse_id": "123",
      // "android_channel_id": "0940813c-e319-4ff3-8d52-a202bf767b3a"
    });
    http.Response response = await http.post(
        Uri.parse('https://onesignal.com/api/v1/notifications'),
        body: body,
        headers: {
          "content-type": "application/json",
          "Authorization":
              "Basic M2I4YWQ2ZDEtYmVkNC00YjFmLWFhMmUtMzQ2MDVlNGNiZjE0"
        });
    if (response.statusCode == 200) {
      print("NOTIFICATION SENT SUCCESSFULLY");
    } else {
      print('Notification Error | Error Code: ${response.statusCode}');
    }
  }
}
