import 'package:boardgame/repository/bookmarks_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final bookmarkUseCaseProvider =
    Provider((ref) => BookmarkUseCaseImpl(ref.read));

class BookmarkUseCaseImpl {
  final Reader read;

  BookmarkUseCaseImpl(this.read);

  void removeBookmarks(List<String> bookmarks) async {
    await read(bookMarkRepositoryProvider).removeBookMarks(bookmarks);
  }
}
