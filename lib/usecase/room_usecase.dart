import 'package:boardgame/managers/mqtt_manager.dart';
import 'package:boardgame/models/attend_user_data.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/repository/game_room_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'chat_usecase.dart';

final roomUseCaseProvider = Provider((ref) => RoomUseCaseImpl(ref.read));

abstract class RoomUseCase {
  create();
}

class RoomUseCaseImpl extends RoomUseCase {
  final Reader read;

  RoomUseCaseImpl(this.read);
  void create() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    FirebaseAuth.instance.signOut();
    googleSignIn.signOut();
    read(userStateProvider).state = null;
  }

  attend(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    await read(gameRoomRepository).attend(roomId);
    read(mqttProvider).subscribeTopic(roomId);
    // FirebaseMessaging.instance.subscribeToTopic(widget.roomId);
    String text = "${myUser.nickname}加入局";
    read(chatUseCaseProvider).sendMessageToServerService(
        text, roomId, ["P3QrTNmeLRbZVXikJ4xe1J2QFS42"]);
  }

  submitAutit(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    await read(gameRoomRepository).submitAudit(roomId);
    // context.read(mqttProvider).subscribeTopic(widget.roomId);
    // FirebaseMessaging.instance.subscribeToTopic(widget.roomId);
    // String text = "${myUser.nickname}加入局";
    // read(chatUseCaseProvider).sendMessageToServerService(
    //     text, roomId, ["P3QrTNmeLRbZVXikJ4xe1J2QFS42"]);
    //
    // read(chatRoomRepository).addNotifyMessage(roomId, "${myUser.nickname}加入局");
  }

  cancelAudit(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    await read(gameRoomRepository).cancelAudit(roomId);
    // context.read(mqttProvider).subscribeTopic(widget.roomId);
    // FirebaseMessaging.instance.subscribeToTopic(widget.roomId);
    // String text = "${myUser.nickname}加入局";
    // read(chatUseCaseProvider).sendMessageToServerService(
    //     text, roomId, ["P3QrTNmeLRbZVXikJ4xe1J2QFS42"]);
    //
    // read(chatRoomRepository).addNotifyMessage(roomId, "${myUser.nickname}加入局");
  }

  confirmAudit(String roomId, AttendUserData attendUserData) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    await read(gameRoomRepository).confirmAudit(roomId, attendUserData);
  }

  exitGameRoom(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    await read(gameRoomRepository).exit(roomId);
    read(mqttProvider).unSubscribeTopic(roomId);
    String text = "${myUser.nickname} 離開";
    read(chatUseCaseProvider).sendMessageToServerService(text, roomId, [""]);
  }

  kickOffUser(
      String roomId, String kickUserUid, String kickUserNickname) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    await read(gameRoomRepository).kickOffAttendee(roomId, kickUserUid);
    String text = "${myUser.nickname}已強制將$kickUserNickname退出";
    read(chatUseCaseProvider).sendMessageToServerService(
        text, roomId, ["P3QrTNmeLRbZVXikJ4xe1J2QFS42"]);
  }

  Future<List<String>> dismissRoom(String roomId) async {
    List<String> userIds = await read(gameRoomRepository).deleteRoom(roomId);
    read(mqttProvider).unSubscribeTopic(roomId);
    return userIds;
  }
}
