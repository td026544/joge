import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/repository/friends_repository.dart';
import 'package:boardgame/usecase/chat_usecase.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final friendUseCaseProvider = Provider((ref) => FriendUseCaseImpl(ref.read));

abstract class FriendUseCase {}

class FriendUseCaseImpl extends FriendUseCase {
  final Reader read;

  FriendUseCaseImpl(this.read);

  void acceptFriend(String toId) async {
    await read(friendsRepository).acceptInviteFriend(toId);
    await read(chatUseCaseProvider).reloadChats();
  }

  void deleteFriend(String toId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    if (myUser.id == toId) return;
    await read(friendsRepository).deleteFriend(toId);
    await read(chatUseCaseProvider).reloadChats();
  }
}
