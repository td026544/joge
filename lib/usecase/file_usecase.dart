import 'dart:io';
import 'dart:typed_data';

import 'package:boardgame/models/url_getter.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter_riverpod/flutter_riverpod.dart';

final fileUseCaseProvider = Provider((ref) => FileUseCaseImpl(ref.read));

abstract class FileUseCase {}

class FileUseCaseImpl extends FileUseCase {
  final Reader read;

  FileUseCaseImpl(this.read);

  Future<String> uploadFile(File imageFile) async {
    try {
      String postId = DateTime.now().millisecondsSinceEpoch.toString();
      Reference ref = firebase_storage.FirebaseStorage.instance
          .ref('rooms/${postId + COVER_NAME}');
      await ref.putFile(imageFile);

      String url = await ref.getDownloadURL();
      return url;
    } on FirebaseException catch (e) {
      return "";
    }
  }

  Future<String> uploadImageByte(Uint8List imageByte, String roomId) async {
    try {
      Reference ref = firebase_storage.FirebaseStorage.instance
          .ref('rooms/${roomId + COVER_NAME}');
      await ref.putData(imageByte);

      String url = await ref.getDownloadURL();
      return url;
    } catch (e) {
      throw (e);
    }
  }
}
