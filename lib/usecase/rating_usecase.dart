import 'package:boardgame/repository/ratings_repository.dart';
import 'package:boardgame/screen/rating_page.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final ratingUseCaseProvider = Provider((ref) => RatingUseCaseImpl(ref.read));

abstract class RatingUseCase {
  addRating(RateUserData rateData, String roomId, String roomName);
  updateRating(RateUserData rateData, String roomId);
}

class RatingUseCaseImpl implements RatingUseCase {
  final Reader read;

  RatingUseCaseImpl(this.read);

  @override
  addRating(RateUserData rateData, String roomId, String roomName) {
    read(ratingsRepository).addRating(rateData, roomId, roomName);
  }

  @override
  updateRating(RateUserData rateData, String roomId) {
    read(ratingsRepository).updateRating(rateData, roomId);
  }
}
