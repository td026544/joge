import 'dart:convert';
import 'dart:io';

import 'package:boardgame/managers/mqtt_manager.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/conversations_cards_provider.dart';
import 'package:boardgame/repository/chat_repository.dart';
import 'package:boardgame/usecase/user_usecase.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:boardgame/utils/socail_media_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final chatUseCaseProvider = Provider((ref) => ChatUseCaseImpl(ref.read));

abstract class ChatUseCase {
  loadChatToCheckIsSubscribeTopic();
}

class ChatUseCaseImpl extends ChatUseCase {
  final Reader read;

  ChatUseCaseImpl(this.read);

  loadChatToCheckIsSubscribeTopic() async {
    var chats = await read(chatRepository).fetchMyChats();
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    // read(chatsStateProvider).state = chats;???
    // read(chatsStateProvider).state = chats;
    Map<String, dynamic> roomChats = chats['rooms'] ?? {};
    Map<String, dynamic> userChats = chats['users'] ?? {};
    final conversations = read(conversationsStateNotifierProvider);

    for (var conversation in conversations.value) {
      if (!roomChats.containsKey(conversation.toId)) {
        try {
          read(mqttProvider).unSubscribeTopic(conversation.toId);
        } catch (e) {
          print(e);
        }
      }
    }

    try {
      for (var chat in roomChats.entries) {
        if (chat.value != null) {
          bool chatIsExist =
              await DBManager.db.checkIfChatExistsInDb(chat.value, myUser.id);
          if (!chatIsExist) {
            await addInstalledUserToDBAndSubscribe(
                chat.key, chat.value, ChatType.room);
          } else if (chatIsExist) {
            // await addInstalledUserToDBAndSubscribe(chat.userId, chat.chatId);
            // however if contact exists in local db but does not exist in primary firestore
            // users database because he deleted his account then remove him from local db
            // removeUninstalledUserFromDB(context, tempNum);
          }
        }
      }
    } catch (e, s) {
      print(s);
    }
    try {
      for (var chat in userChats.entries) {
        if (chat.value != null) {
          bool contactExists =
              await DBManager.db.checkIfChatExistsInDb(chat.value, myUser.id);
          if (!contactExists) {
            await addInstalledUserToDBAndSubscribe(
                chat.key, chat.value, ChatType.user);
          } else if (contactExists) {
            // await addInstalledUserToDBAndSubscribe(chat.userId, chat.chatId);

            // however if contact exists in local db but does not exist in primary firestore
            // users database because he deleted his account then remove him from local db
            // removeUninstalledUserFromDB(context, tempNum);
          }
        }
      }
    } catch (e, s) {
      print(s);
    }
  }

  Future<void> addInstalledUserToDBAndSubscribe(
      String toId, String chatId, ChatType chatType) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    final contactRef;
    switch (chatType) {
      case ChatType.room:
        contactRef = FirebaseFirestore.instance.collection('rooms').doc(toId);
        DocumentSnapshot documentSnapshot = await contactRef.get();
        if (documentSnapshot.exists) {
          String title = documentSnapshot["detail.title"];
          String chatTypeString = EnumToString.convertToString(chatType);
          await DBManager.db.createRow(
              toId: toId,
              chatId: chatId,
              contactName: title,
              username: title,
              photoUrl: 'mockPhotoUrl',
              isContact: 1,
              chatType: chatTypeString,
              myId: myUser.id); //// true because here this is a phone contact
          // Subscribe to the chatId
          MQTTManager manager = read(mqttProvider);
          manager.subscribeTopic(chatId);
        }
        break;
      case ChatType.user:
        contactRef = FirebaseFirestore.instance.collection('users').doc(toId);
        DocumentSnapshot documentSnapshot = await contactRef.get();
        if (documentSnapshot.exists) {
          String nickname = documentSnapshot["nickname"];
          String chatTypeString = EnumToString.convertToString(chatType);
          await DBManager.db.createRow(
            toId: toId,
            chatId: chatId,
            contactName: nickname,
            username: nickname,
            photoUrl: 'mockPhotoUrl',
            isContact: 1,
            chatType: chatTypeString,
            myId: myUser.id,
          ); // tru
          // await DBManager.db.createRow(
          //     toId,
          //     chatId,
          //     nickname,
          //     nickname,
          //     'mockPhotoUrl',
          //     1,
          //     chatTypeString); // true because here this is a phone contact

          // Subscribe to the chatId
          MQTTManager manager = read(mqttProvider);
          manager.subscribeTopic(chatId);
        }
        break;
    }
  }

  void sendMessageToServer(String msg, String chatId, List<String> userIds) {
    print("calling send message to server");
    MyUser? user = read(userStateProvider).state;
    if (user == null) throw Exception('noUser');
    var encodedText = utf8.encode(msg);
    Map msgMap = {
      "msg": "$encodedText",
      "type": EnumToString.convertToString(MsgType.text),
      "uid": "${user.id}"
    };

    String msgToSend = json.encode(msgMap);
    read(mqttProvider).publish(msgToSend, chatId);

    if (msg.isNotEmpty) {
      read(userUseCaseProvider).sendNotification(
          toUids: userIds, title: "${user.nickname}傳訊息給你", content: msg);
    }
  }

  void sendMessageToServerService(
      String msg, String chatId, List<String> userIds) {
    print("calling send message to server");
    MyUser? user = read(userStateProvider).state;
    if (user == null) throw Exception('noUser');
    var encodedText = utf8.encode(msg);
    Map msgMap = {
      "msg": "$encodedText",
      "type": EnumToString.convertToString(MsgType.service),
      "uid": "${user.id}"
    };
    String msgToSend = json.encode(msgMap);
    read(mqttProvider).publish(msgToSend, chatId);

    // if (msg.isNotEmpty) {
    //   read(userUseCaseProvider).sendNotification(
    //       toUids: userIds, title: "${user.nickname}傳訊息給你", content: msg);
    // }
  }

  void sendMessageToServerTyping(String chatId, bool isTyping) {
    print("calling send message to server");
    MyUser? user = read(userStateProvider).state;
    if (user == null) throw Exception('noUser');
    Map msgMap;
    if (isTyping) {
      msgMap = {"msg": "true", "type": "typing", "uid": "${user.id}"};
    } else {
      msgMap = {"msg": "false", "type": "typing", "uid": "${user.id}"};
    }
    String msgToSend = json.encode(msgMap);
    read(mqttProvider).publish(msgToSend, chatId);
  }

  void blockUser(String chatId) {
    DBManager.db.updateBlockStatus(0, chatId);
    read(mqttProvider).unSubscribeTopic(chatId);
  }

  void unBlockUser(String chatId) {
    DBManager.db.updateBlockStatus(1, chatId);
    read(mqttProvider).subscribeTopic(chatId);
  }

  void sendImageToServer(
      BuildContext context, File imageFile, String chatId) async {
    MyUser? user = read(userStateProvider).state;
    if (user == null) throw Exception('noUser');

    // compress the image & make it smaller in size
    // File? newFile = await testCompressAndGetFile(imageFile);

    // change the image to bytes format
    final bytes = imageFile.readAsBytesSync().lengthInBytes;
    final kb = bytes / 1024;
    final mb = kb / 1024;
    if (mb > 1) throw Exception('file Too Big');
    String url = await read(socialMediaProvider).uploadImageByte(imageFile);
    // String base64Image = base64Encode(imageFile.readAsBytesSync());
    var encodedText = utf8.encode(url);

    String msgToSend =
        '{"msg" : "$encodedText", "type" : "image", "uid" : "${user.id}"}';
    //
    // while (!read(mqttProvider).getConnectionStatus()) {
    //   print("NOT CONNECTED BEFORE PUBLISHING");
    //   await Future.delayed(Duration(seconds: 1));
    // }

    print("CONNECTED BEFORE PUBLISHING");
    read(mqttProvider).publish(msgToSend, chatId);
  }

  void exitChatRoom(String chatId) {
    read(mqttProvider).unSubscribeTopic(chatId);
  }

  void deleteLocalDbConversation(String chatId) async {
    await DBManager.db.deleteConversation(chatId);
  }

  Future<void> reloadChats() async {
    await read(conversationsStateNotifierProvider.notifier).getData();
    await loadChatToCheckIsSubscribeTopic();
  }
}
