import 'package:boardgame/utils/socail_media_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

abstract class MessageBody {
  late final String allText;

  initAllText(List<InlineSpan>? textSpans) {
    if (textSpans != null) {
      String s = "";
      for (var textSpan in textSpans) {
        s += textSpan.toPlainText();
      }
      allText = s;
    } else {
      allText = "";
    }
  }

  void longPressMenu(BuildContext context, WidgetRef ref) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        // Using Wrap makes the bottom sheet height the height of the content.
        // Otherwise, the height will be half the height of the screen.
        return Wrap(
          children: [
            ListTile(
              leading: Icon(Icons.share),
              title: Text('分享'),
              onTap: () {
                ref.read(socialMediaProvider).shareText(allText);
              },
            ),
            ListTile(
              leading: Icon(Icons.copy),
              title: Text('複製'),
              onTap: () {
                ref.read(socialMediaProvider).copyText(context, allText);
              },
            ),
          ],
        );
      },
    );
  }
}
