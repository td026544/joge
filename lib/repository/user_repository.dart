import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/register_user_data.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final userRepository =
    Provider.autoDispose<UserRepository>((ref) => UserRepositoryImpl(ref.read));

abstract class UserRepository {
  Future<MyUser?> getUser(String uid);
  Future<void> createUser(RegisterUserData registerUserData);
  Future<void> updateMyToken(String token);
  Future<bool> testCreateUser();
}

class UserRepositoryImpl extends UserRepository {
  final Reader read;

  UserRepositoryImpl(this.read);
  Future<MyUser?> getUser(
    String uid,
  ) async {
    try {
      DocumentSnapshot documentSnapshot =
          await FirebaseFirestore.instance.collection('users').doc(uid).get();
      Map<String, dynamic> data =
          documentSnapshot.data() as Map<String, dynamic>;
      MyUser? user = MyUser.fromJson(data);
      return user;
    } catch (e) {
      print(e.toString());
      return null;
    }

    // var b = jsonEncode(a);
    // print(b);
    // return querySnapshot.docs
    //     .map((e) => MyChatRoom(ChatRoom.fromJson(e.data()), read))
    //     .toList();
  }



  Future<void> createUser(RegisterUserData registerUserData) async {
    final user = FirebaseAuth.instance.currentUser;
    WriteBatch batch = FirebaseFirestore.instance.batch();

    final userRef =
        FirebaseFirestore.instance.collection('users').doc(user?.uid);
    final bookMarksRef =
        FirebaseFirestore.instance.collection('bookmarks').doc(user?.uid);
    final ratingsRef = FirebaseFirestore.instance
        .collection('ratings')
        .doc(user?.uid)
        .collection('ratingsRef');
    final myFriendsRef =
        FirebaseFirestore.instance.collection('friends').doc(user?.uid);
    final chatsRef =
        FirebaseFirestore.instance.collection('chats').doc(user?.uid);
    batch.set(userRef, {
      "id": user?.uid,
      "email": user?.email,
      "gender": registerUserData.isMale,
      "age": registerUserData.birthday,
      "phoneNumber": user?.phoneNumber,
      "nickname": registerUserData.nickName,
      "pushToken": "",
      "bio": registerUserData.bio,
      "hobbies": registerUserData.hobbies,
      "createTime": FieldValue.serverTimestamp(),
      "rooms": [],
    });
    batch.set(myFriendsRef, {'users': {}});
    batch.set(chatsRef, {'rooms': {}, 'users:': {}});

    batch.set(bookMarksRef, {'bookmarks': []});

    batch
        .commit()
        .then((value) => {})
        .catchError((onError) => throw Exception("Error"));
  }

  @override
  Future<void> updateMyToken(String token) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    FirebaseFirestore.instance
        .collection('users')
        .doc(myUser.id)
        .update({'pushToken': token});
  }

  @override
  Future<bool> testCreateUser() {
    final userRef = FirebaseFirestore.instance.collection('users');
    final user = FirebaseAuth.instance.currentUser;
    return userRef
        .doc(user?.uid)
        .set({
          "id": user?.uid,
          "email": user?.email,
          "gender": 'male',
          "age": null,
          "phoneNumber": user?.phoneNumber,
          "nickname": "nickname",
          "pushToken": "",
          "bio": "registerUserData.bio",
          "hobbies": [],
          "createTime": FieldValue.serverTimestamp(),
          "rooms": [],
          // 'avatar': {
          //   'bodyIndex': avatar.bodyIndex,
          //   'faceIndex': avatar.faceIndex,
          //   'headIndex': avatar.headIndex,
          //   'accessoriesIndex': avatar.accessoriesIndex,
          //   'facialHairIndex': avatar.facialHairIndex,
          //   'skinColorCode': avatar.skinColorCode,
          //   'hairColorCode': avatar.hairColorCode,
          //   'clothColorCode1': avatar.clothColorCode1,
          //   'clothColorCode2': avatar.clothColorCode2,
          //   'backGroundColor': avatar.backGroundColor
          // }
        })
        .then((value) => true)
        .catchError((onError) => throw Exception("Error"));
  }
}
