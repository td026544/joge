import 'dart:collection';

import 'package:boardgame/components/rating_card.dart';
import 'package:boardgame/models/creator.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/models/rate_data.dart';
import 'package:boardgame/models/rate_reply.dart';
import 'package:boardgame/models/room_detail.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/screen/rating_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:boardgame/models/rate_data.dart';

final ratingsRepository =
    Provider<RatingsRepository>((ref) => RatingsRepositoryImpl(ref.read));

abstract class RatingsRepository {
  Future<void> updateRating(RateUserData rateData, String roomId);

  Future<void> addRating(RateUserData rateData, String roomId, String roomName);
  Future<void> addRatings(List<RateUserData> rateDataList, String roomId);

  Future<Map<String, dynamic>> getRating1(String docId);
  Future<void> addRating1(RateUserData rateData, String roomId, String roomName);

// Future<bool> addLikeToFeed(String creatorId, String roomId);
  // Future<bool> addCommentToFeed(String creatorId, String roomId);
  // Stream<List<FeedItem>> getFeedItem(String userId);
  // Future<List<FeedItem>> getMoreLimitFeedsFromFeedsTable(
  //     String userId, int limit, int lastTime);
}

class RatingsRepositoryImpl implements RatingsRepository {
  final Reader read;
  final _fireStore = FirebaseFirestore.instance;

  RatingsRepositoryImpl(this.read);
  @override
  Future<void> addRating(
      RateUserData rateData, String roomId, String roomName) async {
    CollectionReference ratingsRef = FirebaseFirestore.instance
        .collection('ratings')
        .doc(rateData.uid)
        .collection('ratings');
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    await ratingsRef.doc().set({
      "uid": myUser.id,
      "nickname": myUser.nickname,
      "comment": rateData.comment,
      "rateType": rateData.rateType,
      "roomId": rateData.uid,
      "roomName": roomName,
      "time": FieldValue.serverTimestamp(),
    }, SetOptions(merge: true));
  }

  @override
  Future<void> updateRating(RateUserData rateData, String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    DocumentReference ratingRef = FirebaseFirestore.instance
        .collection('ratings')
        .doc(myUser.id)
        .collection('rates')
        .doc(roomId);

    await ratingRef.set({
      'updateTime': FieldValue.serverTimestamp(),
      "attendeesData": {
        "${rateData.uid}": {
          "comment": rateData.comment,
          "rateType": rateData.rateType,
          "nickname": rateData.nickname,
        },
      }
    }, SetOptions(merge: true));
  }

  @override
  Future<void> addRatings(List<RateUserData> rateDataList, String roomId) async {
    assert(rateDataList.isNotEmpty);
    DocumentReference chatRoomRef =
        FirebaseFirestore.instance.collection('chatRooms').doc(roomId);
    DocumentReference roomRef =
        FirebaseFirestore.instance.collection('rooms').doc(roomId);
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    if (rateDataList.isEmpty) throw Exception('EmptyList');

    WriteBatch batch = FirebaseFirestore.instance.batch();

    for (RateUserData rateData in rateDataList) {
      String toUserId = rateData.uid;
      CollectionReference ratingsRef = FirebaseFirestore.instance
          .collection('ratings')
          .doc(toUserId)
          .collection('rate');
      QuerySnapshot querySnapshot = await ratingsRef
          .where('roomId', isEqualTo: roomId)
          .where('uid', isEqualTo: myUser.id)
          .get();
      if (querySnapshot.docs.isEmpty) {
        batch.set(
          ratingsRef.doc(),
          {
            "uid": myUser.id,
            "nickname": myUser.nickname,
            "comment": rateData.comment,
            "rateType": rateData.rateType,
            'roomName': "",
            'roomId': roomId,
            "time": FieldValue.serverTimestamp(),
          },
          SetOptions(merge: true),
        );
        batch.update(
          chatRoomRef,
          {
            'attendeesData.${myUser.id}.isRated': true,
          },
        );
        batch.update(
          roomRef,
          {'attendeesData.${myUser.id}.isRated': true},
        );
        batch.commit();
      }
    }
  }


  @override
  Future<Map<String, dynamic>> getRating1(String docId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    DocumentReference docRef = _fireStore
        .collection('ratings')
        .doc(myUser.id)
        .collection('rates')
        .doc(docId);
    DocumentSnapshot documentSnapshot = await docRef.get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> docData =
          documentSnapshot.data() as Map<String, dynamic>;
      Map<String, dynamic> data = {
        'comment': docData['toUser']['comment'] ?? "",
        'rateType': docData['toUser']['rateType'] ?? "",
      };
      return data;
    } else {
      Map<String, dynamic> data = {
        'comment': '',
        'rateType': '',
      };
      return data;
    }
  }

  @override
  Future<void> addRating1(
      RateUserData rateData, String docId, String roomName) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    DocumentReference ratingRef = FirebaseFirestore.instance
        .collection('ratings')
        .doc(myUser.id)
        .collection('rates')
        .doc(docId);
    await ratingRef.set({
      'time': FieldValue.serverTimestamp(),
      'roomName': roomName,
      "toUser": {
        "uid": rateData.uid,
        "comment": rateData.comment,
        "rateType": rateData.rateType,
        "nickname": rateData.nickname,
      },
      'creator': {
        "id": myUser.id,
        "nickname": myUser.nickname,
        "type":"",
      },

    }, SetOptions(merge: true));
  }
}
