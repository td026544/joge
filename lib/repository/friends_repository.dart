import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final friendsRepository =
    Provider<FriendsRepository>((ref) => FriendsRepositoryImpl(ref.read));
final friendsFutureProvider =
    FutureProvider.autoDispose.family<DocumentSnapshot, String>((ref, userId) {
  var doc = ref.read(friendsRepository).getFriendsDoc(userId);
  ref.maintainState = true;
  return doc;
});

abstract class FriendsRepository {
  Future<DocumentSnapshot> getFriendsDoc(String uid);
  Future<void> inviteFriend(String uid);
  Future<void> acceptInviteFriend(String uid);
  Future<void> deleteFriend(String uid);
}

class FriendsRepositoryImpl implements FriendsRepository {
  final Reader read;

  FriendsRepositoryImpl(this.read);

  @override
  Future<DocumentSnapshot> getFriendsDoc(String uid) async {
    DocumentSnapshot doc =
        await FirebaseFirestore.instance.collection('friends').doc(uid).get();

    return doc;
  }

  @override
  Future<void> inviteFriend(String toId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    DocumentReference myFriendsRef =
        FirebaseFirestore.instance.collection('friends').doc(myUser.id);

    DocumentReference invitedUserRef =
        FirebaseFirestore.instance.collection('friends').doc(toId);

    WriteBatch batch = FirebaseFirestore.instance.batch();

    batch.update(
        myFriendsRef, {'users.$toId': describeEnum(FriendsStatus.invite)});
    batch.update(invitedUserRef,
        {'users.${myUser.id}': describeEnum(FriendsStatus.beInvited)});
    batch.commit();
  }

  @override
  Future<void> deleteFriend(String toId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    WriteBatch batch = FirebaseFirestore.instance.batch();
    DocumentReference toIdChatRef =
        FirebaseFirestore.instance.collection('chats').doc(toId);
    DocumentReference myChatRef =
        FirebaseFirestore.instance.collection('chats').doc(myUser.id);
    DocumentReference myFriendsRef =
        FirebaseFirestore.instance.collection('friends').doc(myUser.id);
    DocumentReference toUserRef =
        FirebaseFirestore.instance.collection('friends').doc(toId);

    batch.update(toIdChatRef, {'users.${myUser.id}': FieldValue.delete()});
    batch.update(myChatRef, {'users.$toId': FieldValue.delete()});

    batch.update(toUserRef, {'users.${myUser.id}': FieldValue.delete()});
    batch.update(myFriendsRef, {'users.$toId': FieldValue.delete()});
    batch.commit();
  }

  @override
  Future<void> acceptInviteFriend(String invitedUid) async {
    assert(invitedUid.isNotEmpty);
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return;
    WriteBatch batch = FirebaseFirestore.instance.batch();
    DocumentReference toIdChatRef =
        FirebaseFirestore.instance.collection('chats').doc(invitedUid);
    DocumentReference myChatRef =
        FirebaseFirestore.instance.collection('chats').doc(myUser.id);
    DocumentReference myFriendsRef =
        FirebaseFirestore.instance.collection('friends').doc(myUser.id);

    DocumentReference invitedUserRef =
        FirebaseFirestore.instance.collection('friends').doc(invitedUid);
    DocumentReference documentReference =
        FirebaseFirestore.instance.collection('chats').doc();
    batch.update(toIdChatRef, {'users.${myUser.id}': documentReference.id});
    batch.update(myChatRef, {'users.$invitedUid': documentReference.id});

    batch.update(
        myFriendsRef, {'users.$invitedUid': describeEnum(FriendsStatus.match)});
    batch.update(invitedUserRef,
        {'users.${myUser.id}': describeEnum(FriendsStatus.match)});
    batch.update(invitedUserRef,
        {'users.${myUser.id}': describeEnum(FriendsStatus.match)});
    batch.commit();
  }
}
