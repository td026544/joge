import 'dart:convert';

import 'package:boardgame/arguments/query_remote_msgs_args.dart';
import 'package:boardgame/models/enums.dart';
import 'package:boardgame/models/exceptions.dart';
import 'package:boardgame/models/feed.dart';
import 'package:boardgame/models/feed_item.dart';
import 'package:boardgame/models/my_feed.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:boardgame/providers/unread_feeds_state_notifyer.dart';
import 'package:boardgame/utils/db_manager.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:sqflite/sqflite.dart';

final lastTimeFeedStateProvider = StateProvider<int?>((ref) => null);
final localFeedTimeFutureProvider = FutureProvider<int>((ref) async {
  MyUser? user = ref.read(userStateProvider).state;
  if (user == null) throw Exception('noUser');
  final localFeedItems = await ref
      .read(feedsRepository)
      .getMoreLimitFeedsFromFeedsTable(user.id, 1, 0);
  if (localFeedItems.isNotEmpty) {
    return localFeedItems.last.time.millisecondsSinceEpoch;
  } else {
    return 0;
  }
});
final remoteFeedItemFutureProvider =
    FutureProvider.autoDispose<List<FeedItem>>((ref) async {
  MyUser? user = ref.read(userStateProvider).state;
  if (user == null) throw Exception('noUser');

  int? lastTimeState = ref.read(lastTimeFeedStateProvider).state;
  int lastTime = 0;
  if (lastTimeState == null) {
    final localItems = await ref
        .read(feedsRepository)
        .getMoreLimitFeedsFromFeedsTable(user.id, 1, 0);
    if (localItems.isNotEmpty) {
      lastTime = localItems.first.time.millisecondsSinceEpoch;
      ref.read(lastTimeFeedStateProvider).state = localItems.first.time.millisecondsSinceEpoch;
    }
  } else {
    lastTime = lastTimeState;
  }
  // final localFeedItems = await ref.watch(localFeedsProvider(userId).future);

  List<FeedItem> remoteItems =
      await ref.watch(feedItemsRemoteStreamProvider(QueryRemoteMsgArgs(
    id: user.id,
    timeStamp: lastTime,
  )).last);



  List<FeedItem> allFeedItems = <FeedItem>[];
  allFeedItems.clear();
  if(remoteItems.isNotEmpty){
    allFeedItems.addAll(remoteItems);

  }

  ref.maintainState = true;

  if (remoteItems.isNotEmpty) {
    ref.read(haveNewFeedsStateNotifierProvider.notifier).updateHaveUnread();

    await DBManager.db.addNewFeedsToMessagesTable(remoteItems, user.id);
  }

  return allFeedItems;
});

final feedItemsRemoteStreamProvider = StreamProvider.autoDispose
    .family<List<FeedItem>, QueryRemoteMsgArgs>((ref, queryRemoteMsgArgs) {
    return ref.read(feedsRepository).getFeedFromRemote(queryRemoteMsgArgs);
});

final feedsRepository =
    Provider<FeedsRepository>((ref) => FeedsRepositoryImpl(ref.read));

abstract class FeedsRepository {
  Future<bool> addLikeToFeed(String creatorId, String roomId);
  Future<bool> addCommentToFeed(String creatorId, String roomId);
  Future<List<FeedItem>> getMoreLimitFeedsFromFeedsTable(
      String userId, int limit, int lastTime);
  Stream<List<FeedItem>> getFeedFromRemote(QueryRemoteMsgArgs queryRemoteMsgArgs);
}

class FeedsRepositoryImpl implements FeedsRepository {
  final feedRef = FirebaseFirestore.instance.collection('feeds');

  final Reader read;

  FeedsRepositoryImpl(this.read);

  Future<List<FeedItem>> getMoreLimitFeedsFromFeedsTable(
      String userId, int limit, int lastTime) async {
    final Database db = await DBManager.db.database;
    if (lastTime == 0) {
      var res = await db.query("Feeds",
          where: "myId = ?",
          orderBy: "time DESC",
          limit: limit,
          whereArgs: [userId]);

      return res.map((e) => FeedItem.fromDB(e)).toList();
    } else {
      var res = await db.query("Feeds",
          where: "myId = ? AND time < ?",
          orderBy: "time DESC",
          limit: limit,
          whereArgs: [userId, lastTime]);
      return res.map((e) => FeedItem.fromDB(e)).toList();
    }
  }

  ///按讚
  Future<bool> addLikeToFeed(String creatorId, String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return Future.value(false);

    return feedRef
        .doc(creatorId)
        .collection("feedItems")
        .add({
          "type": EnumToString.convertToString(FeedType.LIKE_POST),
          "nickname": myUser.nickname,
          "uid": myUser.id,
          "navId": roomId,
          "imgUrl": "",
          "msg": "",
          "time": FieldValue.serverTimestamp(),
        })
        .then((value) => true)
        .catchError((e) => false);
  }

  @override
  Future<bool> addCommentToFeed(String creatorId, String roomId) {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return Future.value(false);

    return feedRef
        .doc(creatorId)
        .collection("feedItems")
        .add({
          "type": EnumToString.convertToString(FeedType.COMMENT_POST),
          "nickname": myUser.nickname,
          "imgUrl": "",
          "msg": "",
          "uid": myUser.id,
          "navId": roomId,
          "time": FieldValue.serverTimestamp(),
        })
        .then((value) => true)
        .catchError((e) => false);
  }

  @override
  Stream<List<FeedItem>> getFeedFromRemote(QueryRemoteMsgArgs queryRemoteMsgArgs) {
    MyUser? user = read(userStateProvider).state;
    if(user==null) throw NoUserException('noUser');

    CollectionReference reference = FirebaseFirestore.instance
        .collection('feeds')
        .doc(queryRemoteMsgArgs.id)
        .collection('feedItems');
    if (queryRemoteMsgArgs.timeStamp == 0) {
      return reference
          .orderBy('time', descending: false)
          .snapshots()
          .map((QuerySnapshot querySnapshot) {
        List<FeedItem> feeds = <FeedItem>[];
        querySnapshot.docs.forEach((element) {
          Map<String, dynamic> data = element.data() as Map<String, dynamic>;
          data.putIfAbsent("id", () => element.id);
          data.putIfAbsent("myId", () => user.id);
          FeedItem feed=FeedItem.fromJson(data);
          feeds.add(feed);
        });
        return feeds;
      });
    } else {
      return reference
          .orderBy('time', descending: false)
          .startAfter([
        Timestamp.fromMillisecondsSinceEpoch(queryRemoteMsgArgs.timeStamp)
      ])
          .snapshots()
          .map((QuerySnapshot querySnapshot) {
        List<FeedItem> feeds = <FeedItem>[];
        querySnapshot.docs.forEach((element) {
          Map<String, dynamic> data = element.data() as Map<String, dynamic>;
          data.putIfAbsent("id", () => element.id);
          data.putIfAbsent("myId", () => user.id);
          try{
            FeedItem feed=FeedItem.fromJson(data);
            feeds.add(feed);

          }catch(e){
            var ee=e;
            print('11');
          }
        });

        return feeds;
      });
    }
  }
}
