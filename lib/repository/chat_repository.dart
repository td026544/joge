import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final chatsStateProvider = StateProvider<Map<String, dynamic>>((ref) => {});

final chatRepository =
    Provider<ChatRepository>((ref) => ChatRepositoryImpl(ref.read));

abstract class ChatRepository {
  Future<String> addChat(String toUserId);
  Future<Map<String, dynamic>> fetchMyChats();
}

class ChatRepositoryImpl extends ChatRepository {
  final Reader read;
  FirebaseFirestore _fireStore = FirebaseFirestore.instance;
  CollectionReference chatsRef = FirebaseFirestore.instance.collection('chats');

  ChatRepositoryImpl(this.read);

  @override
  Future<String> addChat(String toUserId) async {
    // ref createChatIdForContact
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return '';
    String myUserId = myUser.id;
    DocumentReference myChatRef = chatsRef.doc(myUserId);
    DocumentReference toUserChatRef = chatsRef.doc(toUserId);
    DocumentSnapshot chatSnapshot = await myChatRef.get();
    String chatId = _fireStore.collection("chatId").doc().id;
    if (!chatSnapshot.exists) return '';
    Map<String, dynamic> data = chatSnapshot.data() as Map<String, dynamic>;

    // if chatId doesn't exists for that contact then create new ChatId and update it
    // for both the user and the contact
    // else use the chatId which already exists

    if (data[toUserId] == null) {
      await myChatRef.update({'chats.$toUserId': chatId});
      await toUserChatRef.update(
        {'chats.$myUserId': chatId},
      );
    }
    // else {
    //   chatId = data['chats'][toUserId];
    // }
    // MQTTManager manager = read<MQTTState>().manager;
    // print("SUBSCRIBING TO TOPIC : $chatId");
    // manager.subscribeTopic(chatId);

    return chatId;
  }

  @override
  Future<Map<String, dynamic>> fetchMyChats() async {
    final MyUser? user = read(userStateProvider).state;
    if (user == null) throw ('noUser');
    DocumentSnapshot doc =
        await FirebaseFirestore.instance.collection('chats').doc(user.id).get();
    Map<String, dynamic> chatsMap = doc.data() as Map<String, dynamic>;

    return chatsMap;
  }
}

class Chat {
  final String chatId;
  final String userId;

  Chat(this.userId, this.chatId);
}
