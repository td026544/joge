import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final testsRepository =
    Provider<TestsRepository>((ref) => TestsRepositoryImpl(ref.read));

abstract class TestsRepository {
  Future<void> getFruit();
}

class TestsRepositoryImpl implements TestsRepository {
  final Reader read;

  TestsRepositoryImpl(this.read);

  @override
  Future<void> getFruit() async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('listFruit');
    final results = await callable();
    List fruit =
        results.data; // ["Apple", "Banana", "Cherry", "Date", "Fig", "Grapes"]
    print(fruit.toString());
  }
}
