import 'package:boardgame/models/attend_user_data.dart';
import 'package:boardgame/models/create_room_post_model.dart';
import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geoflutterfire/geoflutterfire.dart';

final _fireStore = FirebaseFirestore.instance;
final gameRoomRepository =
    Provider<GameRoomRepository>((ref) => GameRoomRepositoryImpl(ref.read));

abstract class GameRoomRepository {
  Future<String> createRoom(CreateRoomPostModel createRoomPostModel);
  Future<String> updateRoom(CreateRoomPostModel createRoomPostModel);

  Future<void> submitAudit(String roomId);
  Future<void> cancelAudit(String roomId);
  Future<void> confirmAudit(String roomId, AttendUserData attendUserData);

  Future<void> attend(String roomId);
  Future<bool> updateLike(String roomId, bool isLike);
  Future<void> exit(String roomId);
  kickOffAttendee(String roomId, String userUid);
  Future<bool> addComment(String roomId, String comment);
  Future<Map<String, dynamic>> getRoom(String roomId);

  Future<List<String>> deleteRoom(String roomId);
}

class GameRoomRepositoryImpl implements GameRoomRepository {
  final Reader read;

  GameRoomRepositoryImpl(this.read);

  Future<Map<String, dynamic>> getRoom(String roomId) async {
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    DocumentSnapshot documentSnapshot = await roomRef.get();
    Map<String, dynamic> data = documentSnapshot as Map<String, dynamic>;
    return data;
  }

  Future<String> createRoom(CreateRoomPostModel createRoomPostModel) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw ('no user');
    final geo = Geoflutterfire();
    DocumentReference roomRef =
        _fireStore.collection('rooms').doc(createRoomPostModel.roomId);
    DocumentReference chatRef = _fireStore.collection('chats').doc(myUser.id);
    GeoFirePoint myLocation = geo.point(
        latitude: createRoomPostModel.geoPoint.latitude,
        longitude: createRoomPostModel.geoPoint.longitude);
    WriteBatch batch = _fireStore.batch();
    batch.set(roomRef, {
      'detail': {
        'title': createRoomPostModel.title,
        'info': createRoomPostModel.info,
        'address': createRoomPostModel.address,
        'city': createRoomPostModel.city,
        'startTime': createRoomPostModel.startTime,
        'needNumbers': createRoomPostModel.needNumbers,
        'position': myLocation.data,
        'placeName': createRoomPostModel.placeName,
        'gameType': createRoomPostModel.type,
        'banUsers': [],
        'isPublic': true,
        'isAudit': createRoomPostModel.isAudit,
        'isOnline': createRoomPostModel.isOnline,
        'createTime': FieldValue.serverTimestamp(),
      },
      'creator': {
        'id': myUser.id,
        'nickname': myUser.nickname,
        'type': '',
      },
      'attendees': [myUser.id],
      'attendeesData': {
        "${myUser.id}": {
          'nickname': myUser.nickname,
        },
      },
      'isOutdated': false,
      'id': roomRef.id,
      'status': "",
      'banUsers': [],
      'reviews': [],
      'likes': {},
      'commentsCount': 0,
    });
    batch.update(chatRef, {'rooms.${roomRef.id}': roomRef.id});
    var results = await batch.commit();
    return roomRef.id;
  }

  Future<String> updateRoom(CreateRoomPostModel createRoomPostModel) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw ('no user');
    final geo = Geoflutterfire();

    DocumentReference roomRef =
        _fireStore.collection('rooms').doc(createRoomPostModel.roomId);
    GeoFirePoint myLocation = geo.point(
        latitude: createRoomPostModel.geoPoint.latitude,
        longitude: createRoomPostModel.geoPoint.longitude);
    roomRef.update({
      'detail': {
        'title': createRoomPostModel.title,
        'info': createRoomPostModel.info,
        'address': createRoomPostModel.address,
        'city': createRoomPostModel.city,
        'startTime': createRoomPostModel.startTime,
        'needNumbers': createRoomPostModel.needNumbers,
        'position': myLocation.data,
        'placeName': createRoomPostModel.placeName,
        'gameType': createRoomPostModel.type,
        'banUsers': [],
        'isPublic': true,
        'isAudit': createRoomPostModel.isAudit,
        'isOnline': createRoomPostModel.isOnline,
        'createTime': FieldValue.serverTimestamp(),
      },
      'creator': {
        'id': myUser.id,
        'nickname': myUser.nickname,
        'type': '',
      },
      'attendees': [myUser.id],
      'attendeesData': {
        "${myUser.id}": {
          'nickname': myUser.nickname,
        },
      },
      'reviews': [],
      'reviewsData': {},
      'id': roomRef.id,
      'status': "",
      'banUsers': [],
      'likes': {},
      'commentsCount': 0,
    });

    return roomRef.id;
  }

  Future<bool> updateLike(String roomId, bool isLike) async {
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);

    String userId = FirebaseAuth.instance.currentUser!.uid;
    return roomRef
        .update({'likes.$userId': isLike})
        .then((value) => true)
        .catchError((onError) => false);
  }

  Future<void> attend(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');

    _fireStore.runTransaction(
      (transaction) async {
        DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);

        DocumentReference chatRef =
            _fireStore.collection('chats').doc(myUser.id);

        DocumentSnapshot roomSnapshot = await transaction.get(roomRef);
        Map<String, dynamic> data = roomSnapshot.data() as Map<String, dynamic>;
        int needNumbers = data['detail']['needNumbers'];
        List<dynamic> attendees = data['attendees'];

        if (attendees.length < needNumbers + 1) {
          attendees.add(
            myUser.id,
          );

          transaction.update(
            roomRef,
            {
              'attendees': FieldValue.arrayUnion(attendees),
              'attendeesData.${myUser.id}': {
                'nickname': myUser.nickname,
                'age': myUser.age,
                'isRated': false,
              }
            },
          );
          transaction.update(
            chatRef,
            {
              'rooms': {roomRef.id: roomRef.id}
            },
          );
        }
      },
    );
  }

  Future<void> submitAudit(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');

    _fireStore.runTransaction(
      (transaction) async {
        DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);

        DocumentReference chatRef =
            _fireStore.collection('chats').doc(myUser.id);

        DocumentSnapshot roomSnapshot = await transaction.get(roomRef);
        Map<String, dynamic> data = roomSnapshot.data() as Map<String, dynamic>;
        int needNumbers = data['detail']['needNumbers'];
        List<dynamic> reviews = data['reviews'];

        if (reviews.length < needNumbers) {
          reviews.add(
            myUser.id,
          );

          transaction.update(
            roomRef,
            {
              'reviews': FieldValue.arrayUnion(reviews),
              'reviewsData.${myUser.id}': {
                'nickname': myUser.nickname,
                'age': myUser.age,
              }
            },
          );
          // transaction.update(
          //   chatRef,
          //   {
          //     'rooms': {roomRef.id: roomRef.id}
          //   },
          // );
        }
      },
    );
  }

  Future<Map<String, dynamic>> testGet(String roomId) async {
    QuerySnapshot doc = await _fireStore
        .collection('chats')
        .where('rooms.$roomId', isEqualTo: roomId)
        .get();
    Map<String, dynamic> data = doc as Map<String, dynamic>;
    return data;
  }

  Future<void> cancelAudit(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');

    _fireStore.runTransaction(
      (transaction) async {
        DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);

        DocumentReference chatRef =
            _fireStore.collection('chats').doc(myUser.id);

        DocumentSnapshot roomSnapshot = await transaction.get(roomRef);
        Map<String, dynamic> data = roomSnapshot.data() as Map<String, dynamic>;
        int needNumbers = data['detail']['needNumbers'];
        List<dynamic> reviews = data['reviews'];

        reviews.add(
          myUser.id,
        );
        transaction.update(
          roomRef,
          {
            'reviews': FieldValue.arrayRemove(reviews),
            'reviewsData.${myUser.id}': FieldValue.delete()
          },
        );
      },
    );
  }

  Future<void> exit(String roomId) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');
    WriteBatch batch = _fireStore.batch();
    List<dynamic> prepareToRemoveValue = [myUser.id];
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    DocumentReference chatRef = _fireStore.collection('chats').doc(myUser.id);

    batch.update(
        roomRef, {'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
    batch.update(roomRef, {'attendeesData.${myUser.id}': FieldValue.delete()});
    batch.update(chatRef, {'rooms.$roomId': FieldValue.delete()});
    var results = await batch.commit();
    // DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    // roomRef.update({'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
  }

  @override
  Future<bool> addComment(String roomId, String comment) {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return Future.value(false);
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);

    DocumentReference commentRef = _fireStore
        .collection('comments')
        .doc(roomId)
        .collection('comments')
        .doc();
    return commentRef
        .set({
          'id': myUser.id,
          'name': myUser.nickname,
          'comment': comment,
          'time': FieldValue.serverTimestamp()
        })
        .then((value) => true)
        .catchError((e) => false);
    // WriteBatch batch = _fireStore.batch();
    //
    // batch.update(roomRef, {'commentsCount': FieldValue.increment(1)});
    // Avatar avatar = myUser.avatar;
    //
    // batch.set(commentRef, {
    //   'nickname': myUser.nickname,
    //   'id': myUser.id,
    //   'comment': comment,
    //   'time': FieldValue.serverTimestamp(),
    //   'avatar': {
    //     'bodyIndex': avatar.bodyIndex,
    //     'faceIndex': avatar.faceIndex,
    //     'headIndex': avatar.headIndex,
    //     'accessoriesIndex': avatar.accessoriesIndex,
    //     'facialHairIndex': avatar.facialHairIndex,
    //     'skinColorCode': avatar.skinColorCode,
    //     'hairColorCode': avatar.hairColorCode,
    //     'clothColorCode1': avatar.clothColorCode1,
    //     'clothColorCode2': avatar.clothColorCode2,
    //     'backGroundColor': avatar.backGroundColor
    //   },
    // });
    // return batch.commit().then((value) => true).catchError((e) => false);
  }

  @override
  kickOffAttendee(String roomId, String userUid) async {
    WriteBatch batch = _fireStore.batch();
    List<dynamic> prepareToRemoveValue = [userUid];
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    DocumentReference chatRoomRef = _fireStore.collection('chats').doc(userUid);
    batch.update(
        roomRef, {'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
    batch.update(roomRef, {'attendeesData.$userUid': FieldValue.delete()});

    batch.update(chatRoomRef, {'rooms.$roomId': FieldValue.delete()});
    batch.commit();

    // DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    // roomRef.update({'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
  }

  @override
  Future<List<String>> deleteRoom(String roomId) async {
    List<String> attendIds = [];
    return await _fireStore.runTransaction(
      (transaction) async {
        DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
        DocumentSnapshot roomSnapshot = await transaction.get(roomRef);
        Map<String, dynamic> data = roomSnapshot.data() as Map<String, dynamic>;
        Map<String, dynamic> attendeesData = data['attendeesData'];
        for (MapEntry attendeeData in attendeesData.entries) {
          String userId = attendeeData.key;
          attendIds.add(userId);
          DocumentReference chatRoomRef =
              _fireStore.collection('chats').doc(userId);
          transaction
              .update(chatRoomRef, {'rooms.$roomId': FieldValue.delete()});
        }
        transaction.delete(roomRef);
        return attendIds;
      },
    );
  }

  @override
  Future<void> confirmAudit(
      String roomId, AttendUserData attendUserData) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('noUser');

    _fireStore.runTransaction(
      (transaction) async {
        DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);

        DocumentReference chatRef =
            _fireStore.collection('chats').doc(attendUserData.uid);

        DocumentSnapshot roomSnapshot = await transaction.get(roomRef);
        Map<String, dynamic> data = roomSnapshot.data() as Map<String, dynamic>;
        int needNumbers = data['detail']['needNumbers'];
        List<dynamic> attendees = data['attendees'];
        List<dynamic> reviews = data['reviews'];
        attendees.add(attendUserData.uid);
        if (attendees.length < needNumbers) {
          attendees.add(
            attendUserData.uid,
          );

          transaction.update(
            roomRef,
            {
              'reviews': FieldValue.arrayRemove(reviews),
              'reviewsData.${attendUserData.uid}': FieldValue.delete(),
              'attendees': FieldValue.arrayUnion(attendees),
              'attendeesData.${attendUserData.uid}': {
                'nickname': attendUserData.nickname,
                'isRated': false,
              }
            },
          );
          transaction.update(
            chatRef,
            {
              'rooms': {roomRef.id: roomRef.id}
            },
          );
        }
      },
    );
  }
}
