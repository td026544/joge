import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final _fireStore = FirebaseFirestore.instance;
final chatRoomRepository =
    Provider<ChatRoomRepository>((ref) => ChatRoomRepositoryImpl(ref.read));

abstract class ChatRoomRepository {
  exitRoom(String roomId);
  kickOffAttendee(String roomId, String userUid);
  addMessage(String roomId, String sendText);
  Future<String> createChatRoom(MyUser toUser);
}

class ChatRoomRepositoryImpl implements ChatRoomRepository {
  final Reader read;

  ChatRoomRepositoryImpl(this.read);
  addMessage(String roomId, String sendText) async {
    DocumentReference messagesRef = _fireStore
        .collection('chatRooms')
        .doc(roomId)
        .collection('messages')
        .doc();

    messagesRef.set({
      'text': sendText,
      'sender': FirebaseAuth.instance.currentUser!.uid,
      'time': FieldValue.serverTimestamp(), // es
      // sage DateTime
    });
  }

  @override
  exitRoom(String roomId) async {
    WriteBatch batch = _fireStore.batch();
    List<dynamic> prepareToRemoveValue = [
      FirebaseAuth.instance.currentUser!.uid
    ];
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    DocumentReference chatRef = _fireStore.collection('chats').doc(roomId);

    batch.update(
        roomRef, {'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
    batch.update(chatRef, {'rooms.$roomId': FieldValue.delete()});
    var results = await batch.commit();

    // DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    // roomRef.update({'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
  }

  kickOffAttendee(String roomId, String userUid) async {
    WriteBatch batch = _fireStore.batch();
    List<dynamic> prepareToRemoveValue = [userUid];
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    DocumentReference chatRoomRef =
        _fireStore.collection('chatRooms').doc(roomId);

    batch.update(
        roomRef, {'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
    batch.update(chatRoomRef,
        {'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
    var results = await batch.commit();

    // DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    // roomRef.update({'attendees': FieldValue.arrayRemove(prepareToRemoveValue)});
  }

  @override
  Future<String> createChatRoom(MyUser toUser) async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw Exception('no User');
    DocumentReference chatRoomRef = _fireStore.collection('chatRooms').doc();

    return chatRoomRef.set({
      'uid': chatRoomRef.id,
      'title': '',
      'startTime': FieldValue.serverTimestamp(),
      'createTime': FieldValue.serverTimestamp(),
      'attendees': [myUser.id],
      'attendeesData': {
        "${myUser.id}": {
          'nickname': myUser.nickname,
          'age': myUser.age,
          'bio': "",
          'hobbies': [],
          'isRated': false,
          'pushToken': myUser.pushToken,
        },
        "${toUser.id}": {
          'nickname': toUser.nickname,
          'age': toUser.age,
          'bio': "",
          'hobbies': [],
          'isRated': false,
          'pushToken': toUser.pushToken,
        },
      },
      'creator': {
        'id': myUser.id,
        'age': myUser.age,
        'nickname': myUser.nickname,
      },
      'toUserId': toUser.id,
      'state': "",
      'type': "CHAT",
    }).then((value) => chatRoomRef.id);
  }

  //
  //
  // Stream<List<Message>> getMessages(String roomId) {
  //   CollectionReference reference =
  //   _fireStore.collection('chatRooms').doc(roomId).collection('messages');
  //   // var lastTime = 0;
  //   // if (localMessages.isNotEmpty) {
  //   //   lastTime = localMessages.last.time;
  //   // }
  //
  //   return reference
  //       .orderBy('time', descending: false)
  //   // .startAfterDocument(_lastDocument)
  //       .where('time', isGreaterThan: 0)
  //       .snapshots()
  //       .map((QuerySnapshot querySnapshot) {
  //     List<Message> msgs = <Message>[];
  //     msgs.addAll(read(localMessagesProvider).state);
  //     // msgs.addAll(localMessages);
  //
  //     // if (!isLoaded) {
  //     //   msgs.addAll(localMessages);
  //     //   isLoaded = true;
  //     // }
  //     print(
  //         'querySnapshot.docs.length:' + querySnapshot.docs.length.toString());
  //     // _lastDocument = querySnapshot.docs.last;
  //     // msgs.addAll(localMessages);
  //
  //     querySnapshot.docs.forEach((element) {
  //       var message = Message(element.data()['sender'], element.data()['text'],
  //           element.data()['time']);
  //       // if (localMessages.last != message) {
  //       //   localMessages.add(message);
  //       // }
  //       msgs.add(message);
  //     });
  //     print('localMessages.length > 0');
  //     return msgs;
  //   });
  // }

}
