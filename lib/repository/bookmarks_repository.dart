import 'package:boardgame/models/my_user.dart';
import 'package:boardgame/providers/auth_providers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final _fireStore = FirebaseFirestore.instance;
final bookMarkRepositoryProvider =
    Provider<BookMarkRepository>((ref) => BookMarkRepositoryImpl(ref.read));

abstract class BookMarkRepository {
  Future<bool> updateLike(String roomId, bool isLike);

  Future<void> addToBookMark(String roomId);
  Future<void> removeBookMark(String roomId);
  Future<void> removeBookMarks(List<String> roomIds);

  Future<Map<String, dynamic>> getBookMarks();
}

class BookMarkRepositoryImpl implements BookMarkRepository {
  final Reader read;

  BookMarkRepositoryImpl(this.read);

  Future<bool> updateLike(String roomId, bool isLike) async {
    DocumentReference roomRef = _fireStore.collection('rooms').doc(roomId);
    if (isLike) {}

    String userId = FirebaseAuth.instance.currentUser!.uid;
    return roomRef
        .update({'likes.$userId': isLike})
        .then((value) => true)
        .catchError((onError) => false);
  }

  @override
  Future<void> addToBookMark(String roomId) {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return Future.value(false);

    DocumentReference bookmarksRef =
        _fireStore.collection('bookmarks').doc(myUser.id);
    return bookmarksRef.set({
      'bookmarks': FieldValue.arrayUnion([roomId])
    }, SetOptions(merge: true));
  }

  @override
  Future<void> removeBookMark(String roomId) {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) return Future.value(false);

    DocumentReference bookmarksRef =
        _fireStore.collection('bookmarks').doc(myUser.id);

    return bookmarksRef.set({
      'bookmarks': FieldValue.arrayRemove([roomId])
    }, SetOptions(merge: true));
  }

  @override
  Future<Map<String, dynamic>> getBookMarks() async {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw ('nouser');
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('bookmarks')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> data =
          documentSnapshot.data() as Map<String, dynamic>;
      return data;
    } else {
      return {};
    }
  }

  @override
  Future<void> removeBookMarks(List<String> roomIds) {
    MyUser? myUser = read(userStateProvider).state;
    if (myUser == null) throw 'Nouser';

    DocumentReference bookmarksRef =
        _fireStore.collection('bookmarks').doc(myUser.id);

    return bookmarksRef.update({'bookmarks': FieldValue.arrayRemove(roomIds)});
  }
}
